# UETWork Client

[![pipeline status](https://gitlab.com/patida12/uetwork_client/badges/develop/pipeline.svg)](https://gitlab.com/patida12/uetwork_client/-/commits/develop) [![coverage report](https://gitlab.com/patida12/uetwork_client/badges/develop/coverage.svg)](https://gitlab.com/patida12/uetwork_client/-/commits/develop)

This is the React client for UETWork - an internship management system.

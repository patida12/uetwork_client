import { PartnerStatus } from '../models';

export interface JobNews {
  id: number;
  partnerId: number;
  partnerName: string;
  content?: string;
  isRegistered: boolean;
  createdAt: string;
  title: string;
  endRegAt: string;
}

export interface LatestTerm {
  id: number;
  year: number;
  term: number;
  startDate: string;
  endDate: string;
  startRegAt: string;
  endRegAt: string;
  isRegistered: boolean;
  isActive: boolean;
  isRegistrationExpired: boolean;
}

export interface Partner {
  id: number;
  logoUrl: string;
  name: string;
  // null indicates that this partner hasn't been registered by anyone yet.
  status?: PartnerStatus;
}

export interface JobNewDetail extends JobNews {
  logoUrl: string;
  termId: number;
  partnerContactId: number;
  partnerContactName: string;
  partnerContactEmail: string;
  partnerContactPhoneNumber: string;
  startRegAt: string;
  jobCount: number;
}

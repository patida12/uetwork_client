import { useContext, useState } from 'react';
import { useHistory } from 'react-router-dom';
import {
  getUserInfo,
  updateAccessToken,
  updateRefreshToken,
} from '../../api/authApi';
import { UserContext } from '../../contexts/UserContext';
import useIsMounted from './useIsMounted';

export const useUserInfo = () => {
  const history = useHistory();
  const { currentUser, setCurrentUser } = useContext(UserContext);
  // HOTFIX: can't update react state unmounted component
  const isMounted = useIsMounted();

  const [loading, setLoading] = useState(true);

  const isLoggedIn = async () => {
    return currentUser != null;
  };

  const getInfo = async () => {
    if (currentUser == null) {
      await getUserInfo()
        .then(res => {
          setCurrentUser(res);
        })
        .catch(e => {
          console.error(e);
          setCurrentUser(undefined);
        });
    }

    if (isMounted()) {
      setLoading(false);
    }
    return currentUser;
  };

  const logout = () => {
    updateAccessToken('');
    updateRefreshToken('');
    setCurrentUser(undefined);
    history.push('/login');
  };

  return {
    currentUser,
    isLoggedIn,
    getInfo,
    getInfoLoading: loading,
    logout,
  };
};

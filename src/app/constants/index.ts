export const GRID_EXPAND_CELL_WIDTH = 200;
export const FORMAT_DAYJS = 'YYYY/MM/DD';
export const DEFAULT_PAGE = 1;
export const DEFAULT_PER_PAGE = 25;

export const OPTIONS = [
  {
    key: 25,
  },
  {
    key: 50,
  },
  {
    key: 100,
  },
];
export const DEFAULT_ROWS_PER_PAGE = OPTIONS[0].key;

export const CLASS_FIELD = 'schoolClass';
export const ALL_CLASSES_EN = 'All';
export const ALL_CLASSES_VI = 'Tất cả';
export const FORMATS = [
  'header',
  'bold',
  'italic',
  'underline',
  'strike',
  'blockquote',
  'list',
  'bullet',
  'indent',
  'link',
  'image',
];

export * from './admin.constants';
export * from './role.constants';
export * from './status.constants';

export const NOT_AVAILABLE = 'N/A';

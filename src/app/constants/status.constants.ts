import { ChipPropsColorOverrides } from '@mui/material/Chip';
import { OverridableStringUnion } from '@mui/types';
let colors: OverridableStringUnion<
  | 'default'
  | 'primary'
  | 'secondary'
  | 'error'
  | 'info'
  | 'success'
  | 'warning',
  ChipPropsColorOverrides
>[] = [
  'warning',
  'error',
  'success',
  'primary',
  'default',
  'secondary',
  'info',
];

export const TERM_STATUS = [
  { key: 'WAITING', value: 'WAITING', color: colors[0] },
  { key: 'FAILED', value: 'FAILED', color: colors[1] },
  { key: 'PASSED', value: 'PASSED', color: colors[2] },
  { key: 'SELECTED', value: 'SELECTED', color: colors[3] },
];

export const INTERNSHIP_TYPE = [
  { key: 'ALL', value: '' },
  { key: 'ASSOCIATE', value: 'ASSOCIATE' },
  { key: 'OTHER', value: 'OTHER' },
];

export const SENT_MAIL_STATUS = [
  { key: 'ALL', value: '' },
  { key: 'SENT', value: true },
  { key: 'NOT_SEND', value: false },
];

export const PARTNER_STATUS = [
  { key: 'ALL', value: '', color: colors[3] },
  { key: 'PENDING', value: 'PENDING', color: colors[0] },
  { key: 'ACCEPTED', value: 'ACCEPTED', color: colors[2] },
  { key: 'REJECTED', value: 'REJECTED', color: colors[1] },
];

export const TRUE_FALSE_OPTIONS = [
  { key: 'ALL', value: '' },
  { key: 'true', value: true },
  { key: 'false', value: false },
];

export const TOP_DATA_COLOR = [
  { key: 'Top 1', value: 'Top 1', color: colors[3] },
  { key: 'Top 2', value: 'Top 2', color: colors[6] },
  { key: 'Top 3', value: 'Top 3', color: colors[2] },
  { key: 'Top 4', value: 'Top 4', color: colors[0] },
  { key: 'Top 5', value: 'Top 5', color: colors[1] },
];

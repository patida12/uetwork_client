import { messages } from 'app/student/messages';
import { t } from 'locales/i18n';

export const CustomCardButtonText = {
  CANCEL: t(messages.cancel()),
  REGISTER: t(messages.register()),
  EXPIRED: t(messages.expired()),
};

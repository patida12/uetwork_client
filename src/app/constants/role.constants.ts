export const ROLE_OPTIONS = [
  { id: 1, name: 'STUDENT' },
  { id: 2, name: 'PARTNER' },
  { id: 3, name: 'LECTURER' },
];

export enum AppRole {
  STUDENT = 'STUDENT',
  PARTNER = 'PARTNER',
  LECTURER = 'LECTURER',
  ORG_ADMIN = 'ORG_ADMIN',
  SYSTEM_ADMIN = 'SYSTEM_ADMIN',
}

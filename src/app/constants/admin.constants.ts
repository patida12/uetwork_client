export const PATH_TERMS_PAGE = '/admin/org/term';
export const PATH_ORGS_PAGE = '/admin/sys/orgs';
export const PATH_REQUESTS_PAGE = '/partner/applications';
export const PATH_NEW_POST_PAGE = '/partner/posts/new';
export const PATH_MY_STUDENTS_PAGE = '/lecturer/my-students';
export const URL_DOWNLOAD_LIST_STUDENTS_TEMPLATE =
  'https://firebasestorage.googleapis.com/v0/b/uetwork-dbbf5.appspot.com/o/Danh%20s%C3%A1ch%20sinh%20vi%C3%AAn.xlsx?alt=media&token=97752944-4c56-435c-b5d4-1fd2f4d03756';

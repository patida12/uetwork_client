export const PATH_AUTH = {
  login: '/login',
  register: '/register',
  resetPassword: '/reset-password',
  completeResetPassword: '/reset-password/complete',
};

export const PATH_STUDENT = {
  home: '/student/home',
};

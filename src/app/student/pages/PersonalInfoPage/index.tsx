import { Box, Container, styled, Typography } from '@mui/material';
import { ChangePasswordCard } from 'app/components/ChangePasswordCard/ChangePasswordCard';
import { t } from 'locales/i18n';
import { messages } from '../../messages';
import { PersonalInfoCard } from './components/PersonalInfoCard';

const PersonalInfoPage: React.FC = (): JSX.Element => {
  return (
    <StyledContainer maxWidth="lg">
      <Box my={5}>
        <Typography variant="h1">{t(messages.personalInfo())}</Typography>
      </Box>
      <PersonalInfoCard className="mb-2" />
      <ChangePasswordCard className="mb-2" />
    </StyledContainer>
  );
};

const StyledContainer = styled(Container)(({ theme }) => ({
  '& > .mb-2': {
    marginBottom: theme.spacing(2),
  },
}));

export { PersonalInfoPage };

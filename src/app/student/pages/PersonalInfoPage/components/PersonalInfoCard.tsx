import { messages } from '../../../messages';
import { t } from 'locales/i18n';
import { LoadingButton } from '@mui/lab';
import {
  Card,
  TextField,
  CardActions,
  CardContent,
  styled,
  Typography,
  Grid,
  MenuItem,
} from '@mui/material';
import { useAsync } from 'app/hooks/useAsync';
import * as Yup from 'yup';
import { useFormik } from 'formik';
import { useSnackbar } from 'notistack';
import { useEffect } from 'react';
import { getInfo, updateInfo } from 'api/studentApi';
import { Student } from 'models';
import { SchoolClass } from 'models/schoolClass';
import profileStudentApi from '../../../../../api/profileStudentApi';

interface FormFields {
  fullName: string;
  personalEmail: string;
  orgEmail: string;
  phoneNumber: string;
  schoolClassId: number;
}

interface PersonalInfoCardProps {
  className?: string;
  initialValues?: FormFields;
}

const PersonalInfoCard: React.FC<PersonalInfoCardProps> = ({
  className,
  initialValues,
}): JSX.Element => {
  const { enqueueSnackbar } = useSnackbar();
  const { run, loading } = useAsync();
  const { run: runGetInfo } = useAsync<Student>();
  const { run: runGetClasses, data: classes } = useAsync<SchoolClass[]>({
    data: [],
  });

  const formik = useFormik<FormFields>({
    initialValues: initialValues ?? {
      fullName: '',
      personalEmail: '',
      orgEmail: '',
      phoneNumber: '',
      schoolClassId: 0,
    },
    validationSchema: PersonalInfoSchema,
    onSubmit: values => {
      run(
        updateInfo({
          ...values,
          personalEmail:
            values.personalEmail === '' ? undefined : values.personalEmail,
          schoolClassId:
            values.schoolClassId === 0 ? undefined : values.schoolClassId,
        })
          .then(() => {
            enqueueSnackbar(t(messages.saveChangesSuccess()), {
              variant: 'success',
            });
          })
          .catch(e => {
            enqueueSnackbar(t(messages.saveChangesFailed()), {
              variant: 'error',
            });
          }),
      );
    },
  });

  useEffect(() => {
    runGetInfo(
      getInfo().then(res => {
        formik.setValues({
          fullName: res.fullName,
          orgEmail: res.orgEmail,
          personalEmail: res.personalEmail,
          phoneNumber: res.phoneNumber,
          schoolClassId: res.schoolClass == null ? 0 : res.schoolClass.id,
        });

        return res;
      }),
    );
    runGetClasses(profileStudentApi.getClassesListing().then(res => res.item));
  }, [runGetInfo]);

  const partnerInfo = formik.values;

  return (
    <Card variant="outlined" className={className}>
      <CardContent>
        <StyledCardTitle variant="h2" component="h2">
          {t(messages.personalInfo())}
        </StyledCardTitle>

        <Grid container spacing={2}>
          <Grid item xs={12}>
            <TextField
              name="fullName"
              value={partnerInfo.fullName}
              label={t(messages.fullName())}
              fullWidth
              onChange={formik.handleChange}
              error={formik.touched.fullName && Boolean(formik.errors.fullName)}
              helperText={formik.touched.fullName && formik.errors.fullName}
            />
          </Grid>
          <Grid item xs={12} md={6}>
            <TextField
              name="orgEmail"
              value={partnerInfo.orgEmail}
              disabled
              label={t(messages.orgEmail())}
              fullWidth
              onChange={formik.handleChange}
            />
          </Grid>
          <Grid item xs={12} md={6}>
            <TextField
              name="personalEmail"
              value={partnerInfo.personalEmail}
              label={t(messages.personalEmail())}
              fullWidth
              onChange={formik.handleChange}
              error={
                formik.touched.personalEmail &&
                Boolean(formik.errors.personalEmail)
              }
              helperText={
                formik.touched.personalEmail && formik.errors.personalEmail
              }
            />
          </Grid>

          <Grid item xs={12} md={6}>
            {/* TODO: Use specialized input for phone number */}
            <TextField
              name="phoneNumber"
              value={partnerInfo.phoneNumber}
              label={t(messages.phoneNumber())}
              fullWidth
              onChange={formik.handleChange}
              error={
                formik.touched.phoneNumber && Boolean(formik.errors.phoneNumber)
              }
              helperText={
                formik.touched.phoneNumber && formik.errors.phoneNumber
              }
            />
          </Grid>
          <Grid item xs={12} md={6}>
            <TextField
              name="schoolClassId"
              select
              value={partnerInfo.schoolClassId}
              label={t(messages.schoolClassName())}
              fullWidth
              onChange={formik.handleChange}
              error={
                formik.touched.schoolClassId &&
                Boolean(formik.errors.schoolClassId)
              }
              helperText={
                formik.touched.schoolClassId && formik.errors.schoolClassId
              }
            >
              <MenuItem value={0} disabled>
                <em>{t(messages.noneChosen())}</em>
              </MenuItem>
              {classes!.map(c => (
                <MenuItem key={c.id} value={c.id}>
                  {c.name}
                </MenuItem>
              ))}
            </TextField>
          </Grid>
        </Grid>
      </CardContent>

      <StyledCardActions>
        <LoadingButton
          type="submit"
          variant="contained"
          disabled={Object.keys(formik.errors).length !== 0}
          loading={loading}
          onClick={() => formik.handleSubmit()}
        >
          {t(messages.saveChanges())}
        </LoadingButton>
      </StyledCardActions>
    </Card>
  );
};

const PersonalInfoSchema = Yup.object().shape({
  fullName: Yup.string().required(t(messages.required())),
  orgEmail: Yup.string().email(t(messages.emailNotValidMsg())),
  personalEmail: Yup.string().email(t(messages.emailNotValidMsg())),
  phoneNumber: Yup.string(),
  schoolClassId: Yup.number(),
});

const StyledCardActions = styled(CardActions)(({ theme }) => ({
  padding: theme.spacing(2),
  display: 'flex',
  justifyContent: 'end',
}));

const StyledCardTitle = styled(Typography)(({ theme }) => ({
  marginBottom: theme.spacing(4),
}));

export { PersonalInfoCard };
export type { FormFields };

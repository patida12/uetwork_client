import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogProps,
  DialogTitle,
  TextField,
} from '@mui/material';
import { useFormik } from 'formik';
import { messages } from '../messages';
import { t } from 'locales/i18n';
import { useAsync } from '../../../../hooks/useAsync';
import { LoadingButton } from '@mui/lab';
import * as Yup from 'yup';
import { useSnackbar } from 'notistack';
import { createPartnerRequest } from 'api/mocks/studentApi';

interface CreatePartnerRequestDialogProps extends DialogProps {
  partnerName?: string;
  onClose: () => void;
}

const CreatePartnerRequestDialog: React.FC<CreatePartnerRequestDialogProps> = ({
  partnerName,
  ...props
}): JSX.Element => {
  const { enqueueSnackbar } = useSnackbar();
  const { loading, run } = useAsync();

  const formik = useFormik({
    initialValues: {
      name: partnerName ?? '',
      homepageUrl: '',
      address: '',
      phoneNumber: '',
      description: '',
      email: '',
    },
    onSubmit: async values => {
      run(
        createPartnerRequest(values)
          .then(() => {
            enqueueSnackbar(t(messages.partnerRequestCreatedSuccess()), {
              variant: 'success',
            });
            props.onClose();
          })
          .catch(e => {
            enqueueSnackbar(t(messages.partnerRequestCreatedFailed()), {
              variant: 'error',
            });
          }),
      );
    },
    validationSchema: PartnerRequestSchema,
  });

  const {
    name,
    homepageUrl,
    address,
    phoneNumber,
    description,
    email,
  } = formik.values;

  return (
    <Dialog {...props}>
      <DialogTitle>{t(messages.createPartnerRequest())}</DialogTitle>
      <DialogContent>
        <TextField
          name="name"
          fullWidth
          autoFocus
          margin="normal"
          label={t(messages.partnerName())}
          value={name}
          onChange={formik.handleChange}
          error={formik.touched.name && Boolean(formik.errors.name)}
          helperText={formik.touched.name && formik.errors.name}
          placeholder="Công Ty Cổ Phần Công Nghệ TEKO Việt Nam"
          required
        />
        <TextField
          name="homepageUrl"
          fullWidth
          margin="normal"
          label={t(messages.homepageUrl())}
          value={homepageUrl}
          onChange={formik.handleChange}
          error={
            formik.touched.homepageUrl && Boolean(formik.errors.homepageUrl)
          }
          helperText={formik.touched.homepageUrl && formik.errors.homepageUrl}
          placeholder="https://teko.vn/"
          required
        />
        <TextField
          name="address"
          fullWidth
          margin="normal"
          label={t(messages.address())}
          value={address}
          onChange={formik.handleChange}
          error={formik.touched.address && Boolean(formik.errors.address)}
          helperText={formik.touched.address && formik.errors.address}
          placeholder="Tầng 12, Tòa nhà Peakview, 36 Hoàng Cầu, Đống Đa, Hà Nội"
          required
        />
        <TextField
          name="email"
          fullWidth
          margin="normal"
          label={t(messages.email())}
          value={email}
          onChange={formik.handleChange}
          error={formik.touched.email && Boolean(formik.errors.email)}
          helperText={formik.touched.email && formik.errors.email}
          placeholder="tuyendung@teko.vn"
          required
        />
        <TextField
          name="phoneNumber"
          fullWidth
          margin="normal"
          label={t(messages.phoneNumber())}
          value={phoneNumber}
          onChange={formik.handleChange}
          error={
            formik.touched.phoneNumber && Boolean(formik.errors.phoneNumber)
          }
          helperText={formik.touched.phoneNumber && formik.errors.phoneNumber}
          placeholder="+842473086336"
          required
        />
        <TextField
          name="description"
          fullWidth
          margin="normal"
          label={t(messages.description())}
          value={description}
          onChange={formik.handleChange}
          multiline
          rows={5}
        />
      </DialogContent>
      <DialogActions sx={{ p: 3 }}>
        <Button onClick={() => props.onClose()}>{t(messages.cancel())}</Button>
        <LoadingButton
          onClick={() => formik.handleSubmit()}
          type="submit"
          variant="contained"
          loading={loading}
        >
          {t(messages.confirm())}
        </LoadingButton>
      </DialogActions>
    </Dialog>
  );
};

const PartnerRequestSchema = Yup.object().shape({
  name: Yup.string().required(t(messages.required())),
  homepageUrl: Yup.string().required(t(messages.required())).url(),
  address: Yup.string().required(t(messages.required())),
  phoneNumber: Yup.string().required(t(messages.required())),
  description: Yup.string(),
  email: Yup.string()
    .required(t(messages.required()))
    .email(t(messages.emailNotValidMsg())),
});

export { CreatePartnerRequestDialog };

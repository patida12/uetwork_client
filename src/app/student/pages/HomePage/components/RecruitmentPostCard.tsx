import { LoadingButton } from '@mui/lab';
import {
  Button,
  Card,
  CardActionArea,
  CardActions,
  CardContent,
  CardMedia,
  CardProps,
  Typography,
} from '@mui/material';
import { t } from 'locales/i18n';
import { messages } from '../messages';
import { useHistory } from 'react-router-dom';
import { useAsync } from 'app/hooks/useAsync';
import {
  registerPartnerPost,
  unregisterPartner,
} from '../../../../../api/studentApi';
import { useAppSelector } from '../../../../../store/hook';
import { useSnackbar } from 'notistack';

interface RecruitmentPostCardProps extends CardProps {
  postInfo: {
    id: number;
    termId: number;
    title: string;
    startRegAt: string;
    endRegAt: string;
    partnerId: number;
    partnerName: string;
    isRegistrationExpired: boolean;
    isRegistered: boolean;
    isSelfRegistered: boolean;
  };
  onAction?: () => void;
}

const RecruitmentPostCard: React.FC<RecruitmentPostCardProps> = ({
  postInfo,
  onAction = () => {},
  ...props
}): JSX.Element => {
  const history = useHistory();
  const { latestTerm } = useAppSelector(state => state.student);
  const { enqueueSnackbar } = useSnackbar();

  const {
    run: runRegisterPartner,
    loading: loadingRegisterPartner,
  } = useAsync();

  const {
    run: runUnregisterPartner,
    loading: loadingUnregisterPartner,
  } = useAsync();

  const redirectToReaderPage = () => {
    history.push(`posts/${postInfo.id}`);
  };

  const handleRegister = () => {
    runRegisterPartner(
      registerPartnerPost(latestTerm.id, postInfo.id)
        .then(() => {
          enqueueSnackbar(t(messages.partnerRegistrationSuccess()), {
            variant: 'success',
          });
          onAction();
        })
        .catch(e => {
          enqueueSnackbar(e.response?.data.message, {
            variant: 'error',
          });
          console.error(e);
        }),
    );
  };

  const handleUnregister = () => {
    runUnregisterPartner(
      unregisterPartner(latestTerm.id, postInfo.partnerId)
        .then(() => {
          enqueueSnackbar(t(messages.cancelRegistrationSuccess()), {
            variant: 'success',
          });
          onAction();
        })
        .catch(e => {
          enqueueSnackbar(e.response?.data.message, {
            variant: 'error',
          });
          console.error(e);
        }),
    );
  };

  let actionButton: JSX.Element;
  if (postInfo.isRegistered && !postInfo.isSelfRegistered) {
    actionButton = (
      <LoadingButton
        color="error"
        loading={loadingUnregisterPartner}
        onClick={handleUnregister}
      >
        {t(messages.cancelRegistration())}
      </LoadingButton>
    );
  } else if (postInfo.isRegistrationExpired) {
    actionButton = <Button disabled>{t(messages.expired())}</Button>;
  } else {
    actionButton = (
      <LoadingButton loading={loadingRegisterPartner} onClick={handleRegister}>
        {t(messages.register())}
      </LoadingButton>
    );
  }

  return (
    <Card variant="outlined" {...props}>
      <CardActionArea onClick={redirectToReaderPage}>
        <CardMedia
          component="img"
          alt="company logo"
          height="140"
          image="https://www.northernmed.com/wp-content/uploads/2020/12/building-placeholder-scaled.jpg"
        />
        <CardContent>
          <Typography
            title={postInfo.title}
            variant="h4"
            textOverflow="ellipsis"
            whiteSpace="nowrap"
            overflow="hidden"
          >
            {postInfo.title}
          </Typography>
          <Typography
            title={postInfo.partnerName}
            variant="subtitle2"
            color="grey.500"
            textOverflow="ellipsis"
            whiteSpace="nowrap"
            overflow="hidden"
          >
            {postInfo.partnerName}
          </Typography>
        </CardContent>
      </CardActionArea>
      <CardActions sx={{ justifyContent: 'end' }}>{actionButton}</CardActions>
    </Card>
  );
};

export { RecruitmentPostCard };
export type { RecruitmentPostCardProps };

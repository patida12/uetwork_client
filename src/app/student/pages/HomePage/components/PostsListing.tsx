import { Box, Grid, Pagination, Typography } from '@mui/material';
import { useEffect, useState } from 'react';
import { t } from '../../../../../locales/i18n';
import { PaginatedResultsMeta } from '../../../../../models';
import { StudentRecruitmentPost } from '../../../../../models/internshipPost';
import { useAppSelector } from '../../../../../store/hook';
import { useAsync } from '../../../../hooks/useAsync';
import LoadingComponent from '../../../Common/LoadingComponent';
import { messages as generalMessages } from 'app/student/messages';
import { getStudentPosts } from '../../../../../api/studentApi';
import { RecruitmentPostCard } from './RecruitmentPostCard';

const POST_LISTING_PER_PAGE = 12;
const POST_LISTING_DEFAULT_PAGE = 1;

const PostsListing: React.FC = (): JSX.Element => {
  const { latestTerm: activeTerm } = useAppSelector(state => state.student);
  const {
    run: runGetPosts,
    loading: loadingGetPosts,
    data: postsData,
  } = useAsync<{
    meta: PaginatedResultsMeta;
    posts: StudentRecruitmentPost[];
  }>({
    data: {
      meta: {
        perPage: 0,
        totalItems: 0,
        totalPages: 0,
        currentPage: 0,
      },
      posts: [],
    },
  });

  const [page, setPage] = useState(POST_LISTING_DEFAULT_PAGE);

  useEffect(() => {
    if (activeTerm.id) {
      runGetPosts(
        getStudentPosts(activeTerm.id, {
          page,
          perPage: POST_LISTING_PER_PAGE,
        }),
      );
    }
  }, [activeTerm.id, runGetPosts, page]);

  const loadingFirstTime = loadingGetPosts && postsData!.meta.currentPage === 0;

  return (
    <Box display="flex" flexDirection="column" mb={8}>
      <Typography my={4} variant="h1">
        {t(generalMessages.recruitmentDashboard())}
      </Typography>
      <Box flexGrow={1}>
        {loadingFirstTime ? (
          <LoadingComponent />
        ) : (
          <>
            <Grid container spacing={2}>
              {postsData!.posts.map(post => {
                const { post: p } = post;
                return (
                  <Grid key={p.id} item xs={12} sm={4} md={3}>
                    <RecruitmentPostCard
                      postInfo={{
                        ...post,
                        ...p,
                      }}
                      onAction={() => {
                        if (activeTerm.id) {
                          runGetPosts(
                            getStudentPosts(activeTerm.id, {
                              page,
                              perPage: POST_LISTING_PER_PAGE,
                            }),
                          );
                        }
                      }}
                    />
                  </Grid>
                );
              })}
              <Grid item xs={12} display="flex" justifyContent="end">
                <Pagination
                  color="primary"
                  page={page}
                  onChange={(
                    event: React.ChangeEvent<unknown>,
                    value: number,
                  ) => {
                    setPage(value);
                  }}
                  count={postsData!.meta.totalPages}
                />
              </Grid>
            </Grid>
          </>
        )}
      </Box>
    </Box>
  );
};

export { PostsListing };

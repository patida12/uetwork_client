import {
  Box,
  Typography,
  Autocomplete,
  TextField,
  Button,
  Badge,
  Avatar,
  styled,
  createFilterOptions,
  Link,
  FormHelperText,
} from '@mui/material';
import { Link as RouterLink } from 'react-router-dom';
import { t } from 'locales/i18n';
import { Partner } from 'types/student';
import { messages as generalMessages } from 'app/student/messages';
import { messages } from 'app/student/pages/HomePage/messages';
import MoreHorizIcon from '@mui/icons-material/MoreHoriz';
import DoneIcon from '@mui/icons-material/Done';
import ClearIcon from '@mui/icons-material/Clear';
import girlUsingLaptop from 'assets/undraw_co-working.svg';

import theme from 'styles/theme';
import { useAppSelector } from 'store/hook';
import { useState } from 'react';
import { useSnackbar } from 'notistack';
import { registerPartner } from 'api/studentApi';
import { CreatePartnerRequestDialog } from './CreatePartnerRequestDialog';
import { PartnerStatus } from '../../../../../models';
import { ConfirmDialog } from '../../../../pages/Partner/components/ConfirmDialog';
import { Trans } from 'react-i18next';

const PARTNER_STATUS = {
  PENDING: {
    color: theme.palette.warning.main,
    value: 'PENDING',
  },
  REJECTED: {
    color: theme.palette.error.main,
    value: 'REJECTED',
  },
  ACCEPTED: {
    color: theme.palette.success.main,
    value: 'ACCEPTED',
  },
};

const filterOptions = createFilterOptions({
  stringify: (option: Partner) => option.name,
  limit: 5,
  trim: true,
});

const RegisterPartnerSection: React.FC = (): JSX.Element => {
  const [chosenPartner, setChosenPartner] = useState<Partner | null>(null);
  const { latestTerm: activeTerm, partners } = useAppSelector(
    state => state.student,
  );
  const [inputPartner, setInputPartner] = useState('');
  const [showConfirmDialog, setShowConfirmDialog] = useState(false);

  const { enqueueSnackbar } = useSnackbar();

  const partnersOption = partners;

  const renderStatusBadge = (option: Partner) => {
    if (option.status === PARTNER_STATUS.PENDING.value) {
      return (
        <Status color={PARTNER_STATUS.PENDING.color}>
          <MoreHorizIcon fontSize="small" />
        </Status>
      );
    } else if (option.status === PARTNER_STATUS.REJECTED.value) {
      return (
        <Status color={PARTNER_STATUS.REJECTED.color}>
          <ClearIcon fontSize="small" />
        </Status>
      );
    } else if (option.status === PARTNER_STATUS.ACCEPTED.value) {
      return (
        <Status color={PARTNER_STATUS.ACCEPTED.color}>
          <DoneIcon fontSize="small" />
        </Status>
      );
    }
    return null;
  };

  const handleRegisterPartner = () => {
    if (chosenPartner != null) {
      registerPartner(activeTerm.id, chosenPartner.id)
        .then(response => {
          enqueueSnackbar(t(messages.partnerRegistrationSuccess()), {
            variant: 'success',
          });
        })
        .catch(error => {
          enqueueSnackbar(error.response?.data?.message ?? error.message, {
            variant: 'error',
          });
        });
    }
  };

  const [openCreatePartner, setOpenCreatePartner] = useState(false);

  return (
    <Box display="flex" flexDirection="row" alignItems="center" sx={{ my: 4 }}>
      <Box
        flexGrow={{ xs: 2, lg: 1 }}
        alignSelf={{ xs: 'stretch', lg: 'auto' }}
        sx={{ py: 16 }}
      >
        <Typography variant="h1">{t(messages.workingAt())}</Typography>
        <Box mt={2} display="flex" sx={{ mr: 4 }}>
          <Box flexGrow={1}>
            <Autocomplete
              fullWidth
              disablePortal
              id="register-partners"
              options={partnersOption}
              getOptionLabel={option => option.name}
              filterOptions={filterOptions}
              value={chosenPartner}
              inputValue={inputPartner}
              onChange={(event, value) => setChosenPartner(value)}
              onInputChange={(event, value) => setInputPartner(value)}
              size="medium"
              renderInput={params => (
                <TextField
                  {...params}
                  placeholder={t(messages.findYourCompany())}
                />
              )}
              noOptionsText={
                <>
                  {t(messages.noPartnerFound()) + '. '}
                  <Link
                    href="#"
                    underline="hover"
                    onClick={() => setOpenCreatePartner(true)}
                  >
                    {t(messages.createPartnerRequest())}
                  </Link>
                </>
              }
              renderOption={(props, option) => (
                <Box component="li" {...props} key={option.id}>
                  <Badge
                    sx={{ mr: 2, border: '1px solid white' }}
                    overlap="circular"
                    anchorOrigin={{
                      vertical: 'bottom',
                      horizontal: 'right',
                    }}
                    badgeContent={renderStatusBadge(option)}
                  >
                    <Avatar alt="Partner Logo" src={option.logoUrl} />
                  </Badge>
                  <Typography>{option.name}</Typography>
                </Box>
              )}
            />
          </Box>
          <Button
            variant="contained"
            disabled={chosenPartner == null}
            onClick={() => {
              if (
                chosenPartner != null &&
                isPendingStatus(chosenPartner.status)
              ) {
                setShowConfirmDialog(true);
                return;
              }

              // TODO: fix double request
              handleRegisterPartner();
            }}
            sx={{ ml: 1, px: 3 }}
          >
            {t(generalMessages.register())}
          </Button>
          <CreatePartnerRequestDialog
            open={openCreatePartner}
            onClose={() => setOpenCreatePartner(false)}
          />
          <ConfirmDialog
            open={showConfirmDialog}
            onClose={() => setShowConfirmDialog(false)}
            content={t(messages.pendingPartnerConfirmMsg())}
            title={t(messages.pendingPartnerConfirmTitle())}
            onConfirm={() => {
              handleRegisterPartner();
              setShowConfirmDialog(false);
            }}
            confirmText={t(messages.iAmSure())}
          />
        </Box>
        {/* TODO: smooth scroll to hash in internship info page */}
        <FormHelperText>
          <Trans
            i18nKey={messages.registerPartnerHelperText()[0]}
            components={[
              <Link
                underline="hover"
                component={RouterLink}
                to={{
                  pathname: 'internship',
                  hash: '#reg-results',
                }}
              />,
            ]}
          />
        </FormHelperText>
      </Box>
      <Box
        justifySelf={{ xs: 'center', lg: 'auto' }}
        flexGrow={{ xs: 1, lg: 0 }}
        order={{ xs: 1, lg: 2 }}
        display={{
          xs: 'none',
          md: 'block',
        }}
      >
        <img
          src={girlUsingLaptop}
          height="400px"
          alt="A girl is using laptop"
        />
      </Box>
    </Box>
  );
};

const Status = styled(Avatar)(({ color }) => ({
  width: 15,
  height: 15,
  backgroundColor: color,
}));

const isPendingStatus = (status?: PartnerStatus) =>
  status == null || status === PartnerStatus.PENDING;

export { RegisterPartnerSection };

import { Box, Typography, Button, alpha } from '@mui/material';
import { t } from 'locales/i18n';
import { LatestTerm } from 'types/student';
import { messages } from '../messages';
import { registerTerm, unregisterTerm } from 'api/studentApi';
import { useAppDispatch } from 'store/hook';
import { getLatestTermThunk } from 'store/studentSlice';
import { LoadingButton } from '@mui/lab';
import { useAsync } from 'app/hooks/useAsync';
import { ClearOutlined, AddOutlined } from '@mui/icons-material';
import { formatDate } from 'utils';

interface ActiveTermBannerProps {
  latestTerm: LatestTerm;
  loadingLatestTerm?: boolean;
}

const ActiveTermBanner: React.FC<ActiveTermBannerProps> = ({
  latestTerm,
  loadingLatestTerm = false,
}): JSX.Element => {
  const dispatch = useAppDispatch();

  const { loading } = useAsync();

  const handleRegister = () => {
    registerTerm(latestTerm.id).then(() => {
      dispatch(getLatestTermThunk());
    });
  };

  const handleUnregister = () => {
    unregisterTerm(latestTerm.id).then(() => {
      dispatch(getLatestTermThunk());
    });
  };

  if (!latestTerm.isActive) {
    return <></>;
  }

  let actionButton = (
    <LoadingButton
      startIcon={<AddOutlined />}
      loading={loading}
      onClick={handleRegister}
    >
      {t(messages.register())}
    </LoadingButton>
  );
  if (latestTerm.isRegistered) {
    actionButton = (
      <LoadingButton
        startIcon={<ClearOutlined />}
        color="error"
        loading={loading}
        onClick={handleUnregister}
      >
        {t(messages.cancelRegistration())}
      </LoadingButton>
    );
  } else if (latestTerm.isRegistrationExpired) {
    actionButton = <Button disabled>{t(messages.expired())}</Button>;
  }

  return (
    <Box
      sx={{
        backgroundColor: theme => alpha(theme.palette.primary.main, 0.15),
        borderRadius: '5px',
      }}
      display="flex"
      justifyContent="space-between"
      alignItems="center"
      p={2}
      my={2}
    >
      <div>
        <Typography variant="body1" fontWeight={600} component="span">
          {t(messages.ongoingTerms())}:{' '}
        </Typography>
        <Typography variant="body1" component="span">
          {t(messages.activeTermTitle(), {
            term: latestTerm.term,
            year: latestTerm.year,
          }) +
            ' (' +
            t(messages.expiredOn(), { date: formatDate(latestTerm.endRegAt) }) +
            ')'}
        </Typography>
      </div>
      {actionButton}
    </Box>
  );
};

export { ActiveTermBanner };
export type { ActiveTermBannerProps };

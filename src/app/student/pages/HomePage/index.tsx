import Container from '@mui/material/Container';
import { useAsync } from 'app/hooks/useAsync';
import { useEffect } from 'react';
import { useAppDispatch, useAppSelector } from 'store/hook';
import { getLatestTermThunk, getPartnersListThunk } from 'store/studentSlice';
import { RegisterPartnerSection } from './components/RegisterPartnerSection';
import { PostsListing } from './components/PostsListing';
import { ActiveTermBanner } from './components/ActiveTermBanner';

function HomePage() {
  const dispatch = useAppDispatch();
  const { latestTerm } = useAppSelector(state => state.student);
  const { run: runLatestTerm, loading: loadingLatestTerm } = useAsync();

  useEffect(() => {
    runLatestTerm(dispatch(getLatestTermThunk()));
  }, [dispatch, runLatestTerm]);

  useEffect(() => {
    if (latestTerm.id) {
      dispatch(getPartnersListThunk(latestTerm.id));
    }
  }, [latestTerm.id, dispatch]);

  // TODO: use `minisearch` to filter results.
  return (
    <>
      <Container maxWidth="lg">
        <ActiveTermBanner
          latestTerm={latestTerm}
          loadingLatestTerm={loadingLatestTerm}
        />
        <RegisterPartnerSection />
        <PostsListing />
      </Container>
    </>
  );
}

export default HomePage;

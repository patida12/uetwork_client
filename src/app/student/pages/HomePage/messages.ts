import { translations } from 'locales/translations';
import { _t } from 'utils/messages';

export const messages = {
  ongoingTerms: () => _t(translations.studentFeature.ongoingTerms),
  expiredOn: () => _t(translations.studentFeature.expiredOn),
  workingAt: () => _t(translations.studentFeature.workingAt),
  findYourCompany: () => _t(translations.studentFeature.findYourCompany),
  viewAll: () => _t(translations.common.button.viewAll),
  activeTermTitle: () =>
    _t(translations.internship.termFeature.activeTermTitle),
  expiredInNDays: () => _t(translations.internship.termFeature.expiredInNDays),
  createPartnerRequest: () =>
    _t(translations.internship.partnerFeature.createPartnerRequest),
  noPartnerFound: () =>
    _t(translations.internship.studentFeature.noPartnerFound),
  registerPartnerSuccess: () =>
    _t(translations.studentFeature.registerPartnerSuccess),
  registerPartnerHelperText: () =>
    _t(translations.studentFeature.registerPartnerHelperText),

  register: () => _t(translations.common.button.register),
  partnerRegistrationSuccess: () =>
    _t(translations.studentFeature.partnerRegistrationSuccess),
  expired: () => _t(translations.common.button.expired),
  cancelRegistration: () => _t(translations.common.button.cancelRegistration),
  cancelRegistrationSuccess: () =>
    _t(translations.studentFeature.cancelRegistrationSuccess),

  partnerName: () => _t(translations.partnerFeature.name),
  homepageUrl: () => _t(translations.partnerFeature.homepageUrl),
  address: () => _t(translations.partnerFeature.address),
  phoneNumber: () => _t(translations.lecturerFeature.phoneNumber),
  email: () => _t(translations.authFeature.email),
  description: () => _t(translations.partnerFeature.description),
  confirm: () => _t(translations.common.button.confirm),
  cancel: () => _t(translations.common.button.cancel),
  required: () => _t(translations.common.validate.required),
  urlNotValidMsg: () => _t(translations.studentFeature.urlNotValidMsg),
  emailNotValidMsg: () => _t(translations.common.validate.invalidEmail),
  phoneNumberNotValidMsg: () =>
    _t(translations.common.validate.phoneNumberNotValidMsg),
  partnerRequestCreatedSuccess: () =>
    _t(translations.studentFeature.partnerRequestCreatedSuccess),
  partnerRequestCreatedFailed: () =>
    _t(translations.studentFeature.partnerRequestCreatedFailed),

  pendingPartnerConfirmMsg: () =>
    _t(translations.studentFeature.pendingPartnerConfirmMsg),
  pendingPartnerConfirmTitle: () =>
    _t(translations.studentFeature.pendingPartnerConfirmTitle),
  iAmSure: () => _t(translations.studentFeature.iAmSure),
};

import { translations } from 'locales/translations';
import { _t } from 'utils/messages';

export const messages = {
  internshipTerm: () => _t(translations.termFeature.internshipTerm),
  lecturer: () => _t(translations.common.app.lecturer),
  internshipType: () =>
    _t(translations.internship.studentFeature.internshipType),
  internshipCompany: () =>
    _t(translations.internship.studentFeature.internshipCompany),
  report: () => _t(translations.internship.studentFeature.report),
  internshipRegistrationResult: () =>
    _t(translations.internship.studentFeature.internshipRegistrationResult),
  submitReport: () => _t(translations.internship.studentFeature.submitReport),
  reportMissing: () => _t(translations.internship.studentFeature.reportMissing),
  uploadReport: () => _t(translations.internship.studentFeature.uploadReport),
  registeredAt: () => _t(translations.internship.studentFeature.registeredAt),
  partnerName: () => _t(translations.internship.partnerFeature.name),
  result: () => _t(translations.internship.studentFeature.result),
  select: () => _t(translations.common.button.select),
  save: () => _t(translations.common.button.save),
  download: () => _t(translations.common.download),
  selectFile: () => _t(translations.internship.studentFeature.selectFile),
  selectPartner: () => _t(translations.internship.studentFeature.selectPartner),
  selectPartnerDialogTitle: () =>
    _t(translations.internship.studentFeature.selectPartnerDialogTitle),
  selectPartnerDialogContent: () =>
    _t(translations.internship.studentFeature.selectPartnerDialogContent),
  selectPartnerSuccess: () =>
    _t(translations.internship.studentFeature.selectPartnerSuccess),
  partnerStatus: () => _t(translations.partnerFeature.status),
  uploadReportSuccess: () =>
    _t(translations.internship.studentFeature.uploadReportSuccess),
  uploadReportLimitMsg: () =>
    _t(translations.internship.studentFeature.uploadReportLimitMsg),
  isSelfRegistered: () => _t(translations.studentFeature.isSelfRegistered),

  termYear: () => _t(translations.internship.termYear),
  noneChosen: () => _t(translations.common.noneChosen),
  noTermFound: () => _t(translations.internship.studentFeature.noTermFound),
};

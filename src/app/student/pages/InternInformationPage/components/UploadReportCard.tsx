import { LoadingButton } from '@mui/lab';
import {
  Button,
  Card,
  CardActions,
  CardContent,
  FormHelperText,
  Link,
  Typography,
} from '@mui/material';
import { t } from 'locales/i18n';
import { useState } from 'react';
import { uploadReport } from '../../../../../api/studentApi';
import { messages } from '../messages';
import { useAsync } from 'app/hooks/useAsync';
import { useSnackbar } from 'notistack';
import useIsMounted from 'app/hooks/useIsMounted';
interface UploadReportCardProps {
  termId: number;
  onSubmit?: () => void;
}

const UploadReportCard: React.FC<UploadReportCardProps> = ({
  termId,
  onSubmit,
}): JSX.Element => {
  const { enqueueSnackbar } = useSnackbar();
  const { loading, run } = useAsync();
  const [selectedFile, setSelectedFile] = useState<File | null>(null);
  const isMounted = useIsMounted();
  const handleUploadFile = () => {
    if (selectedFile == null) return;
    run(
      uploadReport(termId, selectedFile)
        .then(() => {
          enqueueSnackbar(t(messages.uploadReportSuccess()), {
            variant: 'success',
          });
          if (isMounted()) {
            setSelectedFile(null);
            onSubmit?.();
          }
        })
        .catch(e => {
          enqueueSnackbar(e.response?.data.message, { variant: 'error' });
        }),
    );
  };

  return (
    <Card variant="outlined" sx={{ p: 2 }}>
      <CardContent>
        <Typography variant="h2" fontWeight={500} component="div" mb={3}>
          {t(messages.submitReport())}
        </Typography>{' '}
        <Button
          sx={{ mr: 1 }}
          variant="contained"
          component="label"
          htmlFor="file-input"
        >
          {t(messages.selectFile())}
          <input
            type="file"
            hidden
            accept="application/pdf"
            id="file-input"
            onChange={e => {
              if (e.target.files == null || e.target.files.length === 0) return;

              if (e.target.files[0].size > 10 * 1024 * 1024) {
                enqueueSnackbar(t(messages.uploadReportLimitMsg()), {
                  variant: 'error',
                });
                return;
              }

              setSelectedFile(e.target.files[0]);
            }}
          />
        </Button>
        <label>{selectedFile == null ? '' : selectedFile.name}</label>
        <FormHelperText>{t(messages.uploadReportLimitMsg())}</FormHelperText>
        <Typography variant="body2" mt={2}>
          {t(messages.reportMissing())}{' '}
          <Link
            underline="hover"
            onClick={() => {
              // TODO: add report sample link
            }}
          >
            {t(messages.download())}
          </Link>
        </Typography>
      </CardContent>
      <CardActions sx={{ display: 'flex', justifyContent: 'end' }}>
        <LoadingButton
          disabled={selectedFile == null}
          onClick={handleUploadFile}
          variant="contained"
          loading={loading}
        >
          {t(messages.uploadReport())}
        </LoadingButton>
      </CardActions>
    </Card>
  );
};

// const Input = styled('input')({
//   display: 'none',
// });

export { UploadReportCard };

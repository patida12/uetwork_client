import Container from '@mui/material/Container';
import {
  downloadReport,
  getInternshipDetails,
  InternshipDetailsResponse,
  selectPartner,
} from 'api/studentApi';
import { useEffect, useState } from 'react';
import { useAsync } from 'app/hooks/useAsync';
import { AppliedPartner, PartnerStatus, TermLittleInfo } from 'models';
import { getTerms } from '../../../../api/commonApi';
import {
  alpha,
  Box,
  Button,
  Chip,
  Grid,
  Link,
  MenuItem,
  styled,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  TextField,
  Typography,
} from '@mui/material';
import { t } from '../../../../locales/i18n';
import { messages } from './messages';
import { messages as generalMessages } from '../../messages';
import CustomCard from '../../Common/CustomCard';
import { formatDate, isZeroValue } from 'utils';
import { RegistrationStatus } from '../../../../models/appliedStudent';
import noDataIllustration from 'assets/undraw_no_data.svg';
import LoadingComponent from '../../Common/LoadingComponent';
import { ConfirmDialog } from '../../../pages/Partner/components/ConfirmDialog';
import { Trans } from 'react-i18next';
import useIsMounted from '../../../hooks/useIsMounted';
import { useSnackbar } from 'notistack';
import { UploadReportCard } from './components/UploadReportCard';

function InternInformationPage() {
  const [termId, setTermId] = useState<number | undefined>();
  const isMounted = useIsMounted();

  const { enqueueSnackbar } = useSnackbar();
  const { loading: loadingGetTerms, run: runGetTerms, data: terms } = useAsync<
    TermLittleInfo[]
  >({
    data: [],
  });

  const {
    loading: loadingGetDetails,
    run: runGetDetails,
    data: internshipDetails,
  } = useAsync<InternshipDetailsResponse>({
    data: {
      appliedPartners: [],
      reportFileName: '',
    },
  });

  useEffect(() => {
    runGetTerms(
      getTerms().then(res => {
        if (res.length > 0 && isMounted()) {
          setTermId(res[0].id);
        }

        return res;
      }),
    );
  }, [runGetTerms, setTermId]);

  useEffect(() => {
    if (termId != null) {
      runGetDetails(getInternshipDetails(termId));
    }
  }, [runGetDetails, termId]);

  const [openConfirmDialog, setOpenConfirmDialog] = useState(false);

  if (loadingGetTerms || loadingGetDetails) {
    return <LoadingComponent />;
  }

  const renderNoData = () => {
    return (
      <Box sx={{ textAlign: 'center' }}>
        <Box sx={{ mb: 5, mx: 'auto', height: 160 }}>
          <img
            src={noDataIllustration}
            alt="no data illustration"
            height="100%"
          />
        </Box>

        <Typography variant="h3" gutterBottom>
          {t(messages.noTermFound())}
        </Typography>
      </Box>
    );
  };

  const selectedPartner =
    internshipDetails?.selectedPartnerIndex == null
      ? undefined
      : internshipDetails?.appliedPartners[
          internshipDetails.selectedPartnerIndex
        ];

  const hasSelected = selectedPartner != null;

  const renderInternshipInformation = () => {
    if (termId == null) {
      return <></>;
    }
    const supervisor = internshipDetails?.supervisor;
    const lecturerInfo =
      supervisor == null ? (
        t(generalMessages.notPresent())
      ) : (
        <>
          <div>{supervisor.name}</div>
          <div>
            <Link underline="hover" href={`mailto:${supervisor.orgEmail}`}>
              {supervisor.orgEmail}
            </Link>
          </div>
          <div>
            <Link underline="hover" href={`mailto:${supervisor.personalEmail}`}>
              {supervisor.personalEmail}
            </Link>
          </div>
          <div>{supervisor.phoneNumber}</div>
        </>
      );

    return (
      <Grid container spacing={2}>
        <Grid item xs={12} sm={2}>
          <Typography fontWeight={500}>{t(messages.lecturer())}</Typography>
        </Grid>
        <Grid item xs={12} sm={10}>
          {lecturerInfo}
        </Grid>
        <Grid item xs={12} sm={2}>
          <Typography fontWeight={500}>
            {t(messages.internshipType())}
          </Typography>
        </Grid>
        <Grid item xs={12} sm={10}>
          {selectedPartner == null
            ? t(generalMessages.notPresent())
            : selectedPartner.internshipType}
        </Grid>
        <Grid item xs={12} sm={2}>
          <Typography fontWeight={500}>
            {t(messages.internshipCompany())}
          </Typography>
        </Grid>
        <Grid item xs={12} sm={10}>
          {selectedPartner == null
            ? t(generalMessages.notPresent())
            : selectedPartner.name}
        </Grid>
        <Grid item xs={12} sm={2}>
          <Typography fontWeight={500}>{t(messages.report())}</Typography>
        </Grid>
        <Grid item xs={12} sm={10}>
          {isZeroValue(internshipDetails?.reportFileName) ? (
            t(generalMessages.notPresent())
          ) : (
            <Link
              underline="hover"
              sx={{ '&:hover': { cursor: 'pointer' } }}
              onClick={async () => {
                const url = await downloadReport(termId).catch(e => {
                  enqueueSnackbar(e.response?.data.message, {
                    variant: 'error',
                  });
                  throw e;
                });

                window.open(url, '_blank');
              }}
            >
              {t(messages.download())}
            </Link>
          )}
        </Grid>
      </Grid>
    );
  };

  const renderInternshipRegistrationResultTable = () => {
    const canSelect = (ap: AppliedPartner): boolean => {
      return (
        !hasSelected &&
        ap.applyStatus === RegistrationStatus.PASSED &&
        ap.partnerStatus === PartnerStatus.ACCEPTED
      );
    };

    return (
      <Table aria-label="applications result table">
        <TableHead>
          <TableRow>
            <TableCell>{t(messages.partnerName())}</TableCell>
            <TableCell>{t(messages.registeredAt())}</TableCell>
            <TableCell>{t(messages.partnerStatus())}</TableCell>
            <TableCell>{t(messages.result())}</TableCell>
            <TableCell></TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {internshipDetails!.appliedPartners.map(ap => (
            <TableRow key={ap.id}>
              <TableCell>{ap.name}</TableCell>
              <TableCell width={200}>{formatDate(ap.createdAt)}</TableCell>
              <TableCell>{renderPartnerStatusChip(ap.partnerStatus)}</TableCell>
              <TableCell width={150}>
                {renderStatusChip(ap.applyStatus)}
              </TableCell>
              <TableCell>
                {canSelect(ap) && (
                  <>
                    <Button
                      size="small"
                      onClick={() => setOpenConfirmDialog(true)}
                    >
                      {t(messages.select())}
                    </Button>
                    <ConfirmDialog
                      title={t(messages.selectPartnerDialogTitle())}
                      content={
                        <Trans
                          i18nKey={messages.selectPartnerDialogContent()[0]}
                          values={{ name: ap.name }}
                        />
                      }
                      open={openConfirmDialog}
                      onClose={() => setOpenConfirmDialog(false)}
                      onConfirm={async () => {
                        if (termId == null) return;
                        try {
                          await selectPartner(termId, ap.id);

                          if (isMounted()) {
                            setOpenConfirmDialog(false);
                            enqueueSnackbar(
                              t(messages.selectPartnerSuccess()),
                              { variant: 'success' },
                            );
                            runGetDetails(getInternshipDetails(termId));
                          }
                        } catch (e: any) {
                          enqueueSnackbar(e.response?.data.message, {
                            variant: 'error',
                          });
                        }
                      }}
                    />
                  </>
                )}
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    );
  };

  const renderContent = () => {
    if (terms == null || terms.length === 0 || termId == null) {
      return renderNoData();
    }

    return (
      <>
        <Box my={4}>
          <CustomCard
            mainTitle={t(generalMessages.internshipInformation())}
            content={renderInternshipInformation()}
          />
        </Box>
        <Box my={4}>
          <CustomCard
            id="reg-results"
            mainTitle={t(messages.internshipRegistrationResult())}
            content={renderInternshipRegistrationResultTable()}
          />
        </Box>
        <Box my={4}>
          <UploadReportCard
            termId={termId}
            onSubmit={() => runGetDetails(getInternshipDetails(termId))}
          />
        </Box>
      </>
    );
  };

  return (
    <Container maxWidth="lg">
      <Box
        my={5}
        display="flex"
        alignItems="center"
        justifyContent="space-between"
      >
        <Typography variant="h1">
          {t(generalMessages.internshipInformation())}
        </Typography>
        <TextField
          id="outlined-select-term"
          select
          label={t(messages.internshipTerm())}
          value={termId == null ? '' : termId}
          onChange={e => setTermId(parseInt(e.target.value))}
          sx={{
            minWidth: '200px',
          }}
        >
          {terms!.map(term => (
            <MenuItem key={term.id} value={term.id}>
              {t(messages.termYear(), { year: term.year, term: term.term })}
            </MenuItem>
          ))}
        </TextField>
      </Box>
      {renderContent()}
    </Container>
  );
}

const renderStatusChip = (status: RegistrationStatus) => {
  switch (status) {
    case RegistrationStatus.PASSED:
      return <StyledChip label={status} className="success" />;
    case RegistrationStatus.FAILED:
      return <StyledChip label={status} className="error" />;
    case RegistrationStatus.WAITING:
      return <StyledChip label={status} className="warning" />;
    case RegistrationStatus.SELECTED:
      return <StyledChip label={status} className="info" />;
    default:
      return <></>;
  }
};

const renderPartnerStatusChip = (status: PartnerStatus) => {
  switch (status) {
    case PartnerStatus.ACCEPTED:
      return <StyledChip label={status} className="success" />;
    case PartnerStatus.PENDING:
      return <StyledChip label={status} className="warning" />;
    case PartnerStatus.REJECTED:
      return <StyledChip label={status} className="error" />;
  }
};

const StyledChip = styled(Chip)(({ theme }) => ({
  fontWeight: 500,
  '&.warning': {
    backgroundColor: theme.palette.warning.light,
    color: theme.palette.warning.main,
  },
  '&.success': {
    backgroundColor: theme.palette.success.light,
    color: theme.palette.success.main,
  },
  '&.error': {
    backgroundColor: alpha(theme.palette.error.light, 0.2),
    color: theme.palette.error.main,
  },
  '&.info': {
    backgroundColor: alpha(theme.palette.primary.light, 0.2),
    color: theme.palette.primary.main,
  },
}));

export default InternInformationPage;

import { translations } from 'locales/translations';
import { _t } from 'utils/messages';

export const messages = {
  createdAt: () => _t(translations.internship.postFeature.createdAt),
  expiredAt: () => _t(translations.internship.partnerFeature.expiredAt),
  availableJobs: () => _t(translations.internship.partnerFeature.availableJobs),
  contact: () => _t(translations.partnerFeature.contact),
  register: () => _t(translations.common.button.register),
  partnerRegistrationSuccess: () =>
    _t(translations.studentFeature.partnerRegistrationSuccess),
  expired: () => _t(translations.common.button.expired),
  cancelRegistration: () => _t(translations.common.button.cancelRegistration),
  cancelRegistrationSuccess: () =>
    _t(translations.studentFeature.cancelRegistrationSuccess),
};

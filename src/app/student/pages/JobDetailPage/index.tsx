import AccountCircleOutlinedIcon from '@mui/icons-material/AccountCircleOutlined';
import EmailOutlinedIcon from '@mui/icons-material/EmailOutlined';
import LocalPhoneOutlinedIcon from '@mui/icons-material/LocalPhoneOutlined';
import { Avatar, Button, Typography } from '@mui/material';
// import Avatar from '@mui/material/Avatar';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';
import Link from '@mui/material/Link';
import {
  getStudentPostsById,
  registerPartnerPost,
  unregisterPartner,
  // registerPartnerPost,
  // unregisterPartnerPost,
} from 'api/studentApi';
import { useAsync } from 'app/hooks/useAsync';
import CustomCard from 'app/student/Common/CustomCard';
import LoadingComponent from 'app/student/Common/LoadingComponent';
import { t } from 'locales/i18n';
import { useEffect } from 'react';
import { useRouteMatch } from 'react-router-dom';
import { useAppSelector } from 'store/hook';
import { getFormattedDate } from 'utils/time';
import { StudentRecruitmentPost } from 'models/internshipPost';
import { messages } from './messages';
import { CorporateFareOutlined } from '@mui/icons-material';
import { DeltaRenderer } from '../../../components/DeltaRenderer/DeltaRenderer';
import { useSnackbar } from 'notistack';
import { LoadingButton } from '@mui/lab';

type URLParam = {
  id: string;
};

function JobDetailPage() {
  const { params }: { params: URLParam } = useRouteMatch();
  const { id: termId } = useAppSelector(state => state.student.latestTerm);
  const { run, loading, data: studentPost } = useAsync<StudentRecruitmentPost>({
    data: {
      post: {
        id: 0,
        termId: 0,
        partnerId: 0,
        partnerName: '',
        createdAt: '',
        title: '',
        endRegAt: '',
        jobCount: 0,
        startRegAt: '',
        partnerContactId: 0,
        partnerContactName: '',
        partnerContactEmail: '',
        partnerContactPhoneNumber: '',
        content: '',
      },
      isRegistered: false,
      isSelfRegistered: false,
      isRegistrationExpired: false,
    },
  });
  const { enqueueSnackbar } = useSnackbar();

  const postInfo = studentPost!.post;

  useEffect(() => {
    run(getStudentPostsById(termId, Number(params.id)));
  }, [params.id, run, termId]);

  const {
    run: runRegisterPartner,
    loading: loadingRegisterPartner,
  } = useAsync();

  const {
    run: runUnregisterPartner,
    loading: loadingUnregisterPartner,
  } = useAsync();

  const handleRegister = () => {
    runRegisterPartner(
      registerPartnerPost(postInfo.termId, postInfo.id)
        .then(() => {
          enqueueSnackbar(t(messages.partnerRegistrationSuccess()), {
            variant: 'success',
          });
          run(getStudentPostsById(termId, Number(params.id)));
        })
        .catch(e => {
          enqueueSnackbar(e.response?.data.message, {
            variant: 'error',
          });
          console.error(e);
        }),
    );
  };

  const handleUnregister = () => {
    runUnregisterPartner(
      unregisterPartner(postInfo.termId, postInfo.partnerId)
        .then(() => {
          enqueueSnackbar(t(messages.cancelRegistrationSuccess()), {
            variant: 'success',
          });
          run(getStudentPostsById(termId, Number(params.id)));
        })
        .catch(e => {
          enqueueSnackbar(e.response?.data.message, {
            variant: 'error',
          });
          console.error(e);
        }),
    );
  };

  let actionButton: JSX.Element;
  if (studentPost!.isRegistered && !studentPost!.isSelfRegistered) {
    actionButton = (
      <LoadingButton
        color="error"
        loading={loadingUnregisterPartner}
        onClick={handleUnregister}
        variant="contained"
      >
        {t(messages.cancelRegistration())}
      </LoadingButton>
    );
  } else if (studentPost!.isRegistrationExpired) {
    actionButton = (
      <Button disabled variant="contained">
        {t(messages.expired())}
      </Button>
    );
  } else {
    actionButton = (
      <LoadingButton
        loading={loadingRegisterPartner}
        onClick={handleRegister}
        variant="contained"
      >
        {t(messages.register())}
      </LoadingButton>
    );
  }

  const renderPartnerDetail = () => {
    return (
      <>
        <Box display="flex" alignItems="center">
          <Avatar>
            <CorporateFareOutlined />
          </Avatar>
          <Typography ml={2} variant="h6">
            {postInfo?.partnerName}
          </Typography>
        </Box>
        <Typography variant="h6" my={1}>
          {t(messages.createdAt())}
        </Typography>
        <Typography my={1}>{getFormattedDate(postInfo?.createdAt)}</Typography>
        <Typography variant="h6" my={1}>
          {t(messages.expiredAt())}
        </Typography>
        <Typography my={1}>{getFormattedDate(postInfo?.endRegAt)}</Typography>
        <Typography variant="h6" my={1}>
          {t(messages.availableJobs())}
        </Typography>
        <Typography my={1}>{postInfo?.jobCount}</Typography>
        <Typography variant="h6" my={1}>
          {t(messages.contact())}
        </Typography>
        <Box display="flex" my={1}>
          <AccountCircleOutlinedIcon />
          <Typography ml={2}>{postInfo?.partnerContactName}</Typography>
        </Box>
        <Box display="flex" my={1}>
          <EmailOutlinedIcon />
          <Link
            ml={2}
            underline="hover"
            href={`mailto:${postInfo?.partnerContactEmail}`}
          >
            {postInfo?.partnerContactEmail}
          </Link>
        </Box>
        <Box display="flex" my={1}>
          <LocalPhoneOutlinedIcon />
          <Typography ml={2}>{postInfo?.partnerContactPhoneNumber}</Typography>
        </Box>
      </>
    );
  };

  const loadingFirstTime = loading && postInfo.id === 0;

  return (
    <Container maxWidth="lg">
      {loadingFirstTime ? (
        <LoadingComponent />
      ) : (
        <Box display="flex" py={6}>
          <Grid container spacing={2}>
            <Grid item xs={12} lg={4}>
              <CustomCard
                content={renderPartnerDetail()}
                buttonText={actionButton}
              />
            </Grid>
            <Grid item xs={12} lg={8}>
              <CustomCard
                subTitle={postInfo?.partnerName}
                mainTitle={postInfo?.title as string}
                content={<DeltaRenderer deltaStr={postInfo?.content ?? ''} />}
              />
            </Grid>
          </Grid>
        </Box>
      )}
    </Container>
  );
}

export default JobDetailPage;

import { Box, Stack } from '@mui/material';
import { Route, Switch, useRouteMatch } from 'react-router-dom';
import { Footer } from '../Common/Footer';
import HeaderNavigation from '../Common/HeaderNavigation';
import HomePage from './HomePage';
import InternInformationPage from './InternInformationPage';
import JobDetailPage from './JobDetailPage';
import { PersonalInfoPage } from './PersonalInfoPage';

export default function StudentApp(): JSX.Element {
  const { path } = useRouteMatch();

  return (
    <Stack sx={{ minHeight: '100vh' }}>
      <HeaderNavigation />
      <Switch>
        <Route exact path={`${path}/home`} component={HomePage} />
        <Route path={`${path}/posts/:id`} component={JobDetailPage} />
        <Route path={`${path}/internship`} component={InternInformationPage} />
        <Route path={`${path}/account`} component={PersonalInfoPage} />
      </Switch>
      <Box sx={{ flexGrow: 1 }} />
      <Footer />
    </Stack>
  );
}

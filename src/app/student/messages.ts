import { translations } from 'locales/translations';
import { _t } from 'utils/messages';

export const messages = {
  home: () => _t(translations.common.app.home),
  recruitmentDashboard: () => _t(translations.common.app.recruitmentDashboard),
  findYourFavouriteJob: () =>
    _t(translations.studentFeature.findYourFavouriteJob),
  internshipInformation: () =>
    _t(translations.common.app.internshipInformation),
  personalInfo: () => _t(translations.accountFeature.personalInfo),
  changePassword: () => _t(translations.accountFeature.changePassword),
  logOut: () => _t(translations.accountFeature.logOut),
  register: () => _t(translations.common.button.register),
  cancel: () => _t(translations.common.button.cancel),
  expired: () => _t(translations.common.button.expired),
  cancelRegistration: () => _t(translations.common.button.cancelRegistration),

  notPresent: () => _t(translations.common.notPresent),
  noneChosen: () => _t(translations.common.noneChosen),

  emailNotValidMsg: () => _t(translations.common.validate.invalidEmail),
  phoneNumberNotValidMsg: () =>
    _t(translations.common.validate.phoneNumberNotValidMsg),
  required: () => _t(translations.common.validate.required),
  saveChanges: () => _t(translations.common.saveChanges),
  saveChangesSuccess: () => _t(translations.common.saveChangesSuccess),
  saveChangesFailed: () => _t(translations.common.saveChangesFailed),
  fullName: () => _t(translations.studentFeature.fullName),
  idNumber: () => _t(translations.studentFeature.idNumber),
  orgEmail: () => _t(translations.lecturerFeature.orgEmail),
  personalEmail: () => _t(translations.studentFeature.personalEmail),
  phoneNumber: () => _t(translations.studentFeature.phoneNumber),
  schoolClassName: () => _t(translations.studentFeature.schoolClass),
};

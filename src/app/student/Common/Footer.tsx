import { Box, Container, styled } from '@mui/material';
import { AppLogo } from '../../components/AppLogo';

const Footer: React.FC = (): JSX.Element => {
  return (
    <Box bgcolor="grey.900">
      <Container
        maxWidth="lg"
        sx={{
          display: 'flex',
          justifyContent: 'space-between',
          flexDirection: 'row',
        }}
      >
        <div>
          <WhiteAppLogo />
        </div>
        <div></div>
      </Container>
    </Box>
  );
};

const WhiteAppLogo = styled(AppLogo)({
  color: 'white',
});

export { Footer };

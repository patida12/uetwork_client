import MenuIcon from '@mui/icons-material/Menu';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Container from '@mui/material/Container';
import IconButton from '@mui/material/IconButton';
import Link from '@mui/material/Link';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import { styled } from '@mui/material/styles';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import { AppLogo } from 'app/components/AppLogo';
import { messages } from 'app/student/messages';
import { t } from 'locales/i18n';
import * as React from 'react';
import {
  Link as RouterLink,
  useLocation,
  useRouteMatch,
} from 'react-router-dom';
import { AccountMenu } from '../../components/AppLayout/AppHeader';
import { PATH_STUDENT } from '../../constants/path.constants';

const HeaderNavigation = () => {
  const { path } = useRouteMatch();

  const pages = [
    {
      title: t(messages.home()),
      path: `${path}/home`,
    },
    {
      title: t(messages.internshipInformation()),
      path: `${path}/internship`,
    },
  ];

  const [anchorElNav, setAnchorElNav] = React.useState<null | HTMLElement>(
    null,
  );
  const location = useLocation();

  const handleOpenNavMenu = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorElNav(event.currentTarget);
  };

  const handleCloseNavMenu = () => {
    setAnchorElNav(null);
  };

  return (
    <AppBar elevation={1} position="static" color="transparent">
      <Container maxWidth="lg">
        <Toolbar disableGutters>
          <Link
            component={RouterLink}
            underline="none"
            color="black"
            to={PATH_STUDENT.home}
            sx={{ display: { xs: 'none', md: 'flex' } }}
          >
            <CustomAppLogo />
          </Link>
          <Box sx={{ display: { xs: 'flex', md: 'none' } }}>
            <IconButton
              size="large"
              aria-label="account of current user"
              aria-controls="menu-appbar"
              aria-haspopup="true"
              onClick={handleOpenNavMenu}
              color="inherit"
            >
              <MenuIcon />
            </IconButton>
            <Menu
              id="menu-appbar"
              anchorEl={anchorElNav}
              anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'left',
              }}
              keepMounted
              transformOrigin={{
                vertical: 'top',
                horizontal: 'left',
              }}
              open={Boolean(anchorElNav)}
              onClose={handleCloseNavMenu}
              sx={{
                display: { xs: 'block', md: 'none' },
              }}
            >
              {pages.map((page: { title: string; path: string }) => (
                <MenuItem
                  key={page.title}
                  component={RouterLink}
                  to={page.path}
                >
                  <Typography textAlign="center">{page.title}</Typography>
                </MenuItem>
              ))}
            </Menu>
          </Box>
          <Link
            component={RouterLink}
            underline="none"
            color="black"
            to="/student"
            sx={{ flexGrow: 1, display: { xs: 'flex', md: 'none' } }}
          >
            <CustomAppLogo />
          </Link>
          <Box
            sx={{
              flexGrow: 1,
              display: { xs: 'none', md: 'flex' },
              justifyContent: 'space-evenly',
            }}
          >
            {pages.map((page: { title: string; path: string }) => (
              <Button
                key={page.title}
                component={RouterLink}
                to={page.path}
                variant="text"
                disableRipple
                sx={{
                  px: 0,
                  color: 'common.black',
                  borderBottom: location.pathname === page.path ? 3 : 0,
                  borderColor: 'primary.main',
                  borderRadius: 0,
                  '&:hover': {
                    bgcolor: 'common.white',
                  },
                }}
              >
                {page.title}
              </Button>
            ))}
          </Box>
          <AccountMenu />
        </Toolbar>
      </Container>
    </AppBar>
  );
};

const CustomAppLogo = styled(AppLogo)({
  h1: {
    marginBottom: 0,
    marginTop: 0,
  },
});

export default HeaderNavigation;

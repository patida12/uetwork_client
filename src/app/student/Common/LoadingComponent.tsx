import Box, { BoxProps } from '@mui/material/Box';
import CircularProgress from '@mui/material/CircularProgress';

interface LoadingComponentProps extends BoxProps {}

function LoadingComponent(props: LoadingComponentProps) {
  return (
    <Box display="flex" justifyContent="center" mt={2} {...props}>
      <CircularProgress />
    </Box>
  );
}

export default LoadingComponent;

import Button from '@mui/material/Button';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import { CustomCardButtonText } from 'app/constants/student.constants';
import styled from 'styled-components';

type CustomCardProps = {
  id?: string;
  subTitle?: string;
  mainTitle?: string;
  content?: string | JSX.Element;
  buttonText?: string | JSX.Element;
  action?: () => void;
  isTrimmedContent?: boolean;
};

function CustomCard({
  id,
  subTitle,
  mainTitle,
  content,
  buttonText,
  action,
  isTrimmedContent = false,
}: CustomCardProps) {
  // TODO: change `buttonText` prop to `cardState` or sth.
  return (
    <Card id={id} variant="outlined" sx={{ p: 2 }}>
      <CardContent>
        {subTitle && <Typography color="text.secondary">{subTitle}</Typography>}
        <Typography variant="h2" fontWeight={500} component="div" mb={3}>
          {mainTitle}
        </Typography>
        <CardContentWrapper isTrim={isTrimmedContent}>
          {typeof content === 'string' ? (
            <Typography fontSize={14} color="text.secondary" gutterBottom>
              {content}
            </Typography>
          ) : (
            content
          )}
        </CardContentWrapper>
      </CardContent>
      <CardActionsWrapper>
        {typeof buttonText === 'string' ? (
          <ButtonWrapper
            variant="contained"
            onClick={action}
            disabled={buttonText === CustomCardButtonText.EXPIRED}
            color={
              buttonText === CustomCardButtonText.CANCEL ? 'error' : 'primary'
            }
          >
            {buttonText}
          </ButtonWrapper>
        ) : (
          buttonText
        )}
      </CardActionsWrapper>
    </Card>
  );
}

const CardActionsWrapper = styled(CardActions)`
  display: flex;
  justify-content: end;
`;

const ButtonWrapper = styled(Button)`
  isolation: isolate;
  z-index: 3;
`;

const CardContentWrapper = styled.div<{ isTrim: boolean }>`
  display: ${props => (props.isTrim ? '-webkit-box' : 'block')};
  -webkit-line-clamp: 10;
  -webkit-box-orient: vertical;
  overflow: hidden;
  text-overflow: ellipsis;
`;

export default CustomCard;

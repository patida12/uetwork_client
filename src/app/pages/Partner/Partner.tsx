import {
  AccountCircleOutlined,
  HomeOutlined,
  PostAddOutlined,
  SchoolOutlined,
} from '@mui/icons-material';
import { Box } from '@mui/material';
import { styled } from '@mui/styles';
import { AppContent } from 'app/components/AppLayout/AppContent';
import { AppHeader } from 'app/components/AppLayout/AppHeader';
import { AppSider } from 'app/components/AppLayout/AppSider';
import { t } from 'locales/i18n';
import { RouteDef } from 'models';
import { PartnerDashboardPage } from './DashboardPage/Loadable';
import { messages } from './messages';
import { PassedStudentsPage } from './PassedStudentPage/Loadable';
import { PersonalInfoPage } from './PersonalInfoPage/Loadable';
import { PostListPage } from './PostListPage/PostListPage';
import { PostNewEditPage } from './PostNewEditPage/PostNewEditPage';
import { StudentApplicationsPage } from './StudentApplicationsPage/Loadable';

const routes: RouteDef[] = [
  {
    name: t(messages.home()),
    exact: true,
    path: '/home',
    component: PartnerDashboardPage,
    icon: HomeOutlined,
  },
  {
    name: t(messages.studentManagement()),
    icon: SchoolOutlined,
    path: '/student-management',
    component: PassedStudentsPage,
    children: [
      {
        name: t(messages.passedStudents()),
        exact: true,
        path: '/students',
        component: PassedStudentsPage,
      },
      {
        name: t(messages.studentApplications()),
        exact: true,
        path: '/applications',
        component: StudentApplicationsPage,
      },
    ],
  },
  {
    name: t(messages.editPersonalInfo()),
    icon: AccountCircleOutlined,
    path: '/account',
    exact: true,
    component: PersonalInfoPage,
    hide: true,
  },
  {
    name: t(messages.postManagement()),
    exact: true,
    path: '/posts',
    icon: PostAddOutlined,
    component: PersonalInfoPage,
    children: [
      {
        name: t(messages.postList()),
        exact: true,
        path: '/posts',
        component: PostListPage,
      },
      {
        name: t(messages.addPost()),
        exact: true,
        path: '/posts/new',
        component: PostNewEditPage,
      },
    ],
  },
  {
    name: t(messages.editPost()),
    exact: true,
    path: '/posts/:postId/edit',
    component: PostNewEditPage,
    hide: true,
    icon: PostAddOutlined,
  },
];

export const PartnerApp: React.FC = (): JSX.Element => {
  return (
    <StyledBox>
      <AppHeader />
      <AppSider routes={routes} open={true}></AppSider>
      <AppContent routes={routes} />
    </StyledBox>
  );
};

const StyledBox = styled(Box)({
  display: 'flex',
});

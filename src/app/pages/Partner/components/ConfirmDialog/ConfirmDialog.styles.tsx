import { DialogActions, DialogContent, DialogTitle } from '@mui/material';
import styled from 'styled-components';

export const Wrapper = styled.div`
  background-color: #ffffff;
  border-radius: 4px;
  padding: 20px;
`;

export const DialogTitleStyled = styled(DialogTitle)`
  padding: 0px;
`;

export const DialogContentStyled = styled(DialogContent)`
  padding: 0px;
`;

export const DialogActionStyled = styled(DialogActions)`
  padding: 0px;
  display: flex;
  justify-content: space-between;

  .btn_cancel {
    height: 40px;
    font-family: 'Roboto';
    font-style: normal;
    font-weight: 500;
    font-size: 18px;
    line-height: 24px;
    color: #4155e9;
    padding: 0px 12px;
  }

  .btn_confirm {
    height: 40px;
    font-family: 'Roboto';
    font-style: normal;
    font-weight: 500;
    font-size: 18px;
    line-height: 24px;
    color: #ffffff;
    background-color: #4155e9;
    border-radius: 4px;
    padding: 0px 12px;

    &:hover {
      background-color: #4155e9;
    }
  }
`;

export const Content = styled.p`
  margin: 15px 0px 40px;
`;

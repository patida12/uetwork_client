import { Button, Dialog } from '@mui/material';
import React from 'react';
import {
  Content,
  DialogActionStyled,
  DialogContentStyled,
  DialogTitleStyled,
  Wrapper,
} from './ConfirmDialog.styles';
import { t } from 'locales/i18n';
import { messages } from '../../messages';

interface Props {
  open: boolean;
  onClose: () => void;
  onConfirm: () => void;
  title: string;
  content: React.ReactNode;
  confirmText?: React.ReactNode;
  cancelText?: React.ReactNode;
}

const ConfirmDialog: React.FC<Props> = props => {
  const {
    open,
    onClose,
    onConfirm,
    title,
    content,
    confirmText = t(messages.confirm()),
    cancelText = t(messages.cancel()),
  } = props;

  return (
    <Dialog PaperComponent={Wrapper} open={open} onClose={onClose}>
      <DialogTitleStyled>{title}</DialogTitleStyled>
      <DialogContentStyled>
        <Content children={content} />
      </DialogContentStyled>
      <DialogActionStyled>
        <Button className="btn_cancel" onClick={onClose}>
          {cancelText}
        </Button>
        <Button className="btn_confirm" onClick={onConfirm}>
          {confirmText}
        </Button>
      </DialogActionStyled>
    </Dialog>
  );
};

export { ConfirmDialog };

import { AppDataGrid } from '../../../components/AppDataGrid/AppDataGrid';
import { AppContainer } from '../../../components/AppLayout/AppContainer';
import { messages } from '../messages';
import { t } from 'locales/i18n';
import { useAsync } from '../../../hooks/useAsync';
import { FilterState, PaginatedResultsMeta } from '../../../../models';
import { RecruitmentPost } from '../../../../models/internshipPost';
import { columns } from './columns';
import { GridSortModel } from '@mui/x-data-grid';
import {
  useQueryParams,
  withDefault,
  NumberParam,
  StringParam,
  ObjectParam,
} from 'use-query-params';
import { DEFAULT_PAGE, DEFAULT_PER_PAGE } from '../../../constants';
import { useEffect } from 'react';
import {
  GetPendingApplicationsQuery,
  getPosts,
  GetPostsFilterQuery,
  GetPostsQuery,
} from '../../../../api/partnerApi';
import { sortModelToQuery, isZeroValue, safeParseInt } from '../../../../utils';
import { CustomToolbar } from './components/CustomToolbar';

const PostListPage: React.FC = (): JSX.Element => {
  const { run, loading, data } = useAsync<{
    meta: PaginatedResultsMeta;
    posts: RecruitmentPost[];
  }>({
    data: {
      posts: [],
      meta: {
        currentPage: 0,
        perPage: 0,
        totalItems: 0,
        totalPages: 0,
      },
    },
  });

  const [query, setQuery] = useQueryParams({
    page: withDefault(NumberParam, DEFAULT_PAGE),
    perPage: withDefault(NumberParam, DEFAULT_PER_PAGE),
    q: withDefault(StringParam, ''),
    sort: withDefault(ObjectParam, {}),
    filter: withDefault(ObjectParam, {}),
  });

  useEffect(() => {
    run(getPosts(getQueryObject(query)));
  }, [run, query]);

  const { page, perPage, sort, q, filter } = query;

  const onPageChange = (page: number) => {
    setQuery({
      page,
    });
  };

  const onPageSizeChange = (pageSize: number) => {
    setQuery({
      perPage: pageSize,
      page: 1,
    });
  };

  const onSortModelChange = (model: GridSortModel) => {
    if (model.length === 0) {
      setQuery({
        sort: {},
      });
      return;
    }
    const firstElem = model[0];

    setQuery({
      sort: {
        ...sort,
        [firstElem.field]: [firstElem.sort],
      },
    });
  };

  const onFilterChange = (value: FilterState) => {
    setQuery({
      q: value.query,
      filter: value.filter,
    });
  };

  const initialFilters: FilterState = {
    query: q,
    filter: {
      termId: filter['termId'] ?? '',
    },
  };

  const { posts: rows, meta } = data!;

  return (
    <AppContainer title={t(messages.postList())}>
      <AppDataGrid
        sx={{ height: '100%' }}
        rows={rows ?? []}
        columns={columns}
        page={page}
        pageSize={perPage}
        rowCount={meta.totalItems}
        onPageChange={onPageChange}
        onPageSizeChange={onPageSizeChange}
        onSortModelChange={onSortModelChange}
        loading={loading}
        components={{ Toolbar: CustomToolbar }}
        componentsProps={{
          toolbar: {
            onFilterChange,
            initialFilters,
          },
        }}
      ></AppDataGrid>
    </AppContainer>
  );
};

const getQueryObject = (qObj: any): GetPendingApplicationsQuery => {
  const { page, perPage, sort, q, filter } = qObj;
  const gsq: GetPostsQuery = {
    page,
    perPage,
    q,
  };

  const gsqSort = sort == null ? [] : sortModelToQuery(sort);
  const gsqFilters: GetPostsFilterQuery = {};
  const { termId } = filter;
  if (!isZeroValue(termId)) {
    gsqFilters.termId = safeParseInt(termId);
  }

  gsq.sort = gsqSort;
  gsq.filter = gsqFilters;

  return gsq;
};

export { PostListPage };

import { EditOutlined } from '@mui/icons-material';
import { IconButton } from '@mui/material';
import {
  GridCellParams,
  GridColDef,
  GridValueGetterParams,
} from '@mui/x-data-grid';
import { Link } from 'react-router-dom';
import { t } from '../../../../locales/i18n';
import { formatDate, formatTimestamp } from '../../../../utils';
import { messages } from '../messages';

export const columns: GridColDef[] = [
  { field: 'id', hide: true },
  {
    field: 'title',
    headerName: t(messages.title()),
    width: 180,
  },
  {
    field: 'partnerContactName',
    headerName: t(messages.partnerContactName()),
    sortable: false,
    width: 150,
  },
  {
    field: 'partnerContactEmail',
    headerName: t(messages.partnerContactEmail()),
    sortable: false,
    width: 200,
  },
  {
    field: 'partnerContactPhoneNumber',
    headerName: t(messages.partnerContactPhoneNumber()),
    sortable: false,
    width: 200,
  },
  {
    field: 'jobCount',
    headerName: t(messages.jobCount()),
    sortable: false,
    width: 150,
  },
  {
    field: 'createdAt',
    headerName: t(messages.createdAt()),
    valueGetter: (params: GridValueGetterParams) => {
      return params.row.createdAt == null
        ? ''
        : formatTimestamp(params.row.createdAt);
    },
    sortable: false,
    width: 150,
  },
  {
    field: 'startRegAt',
    headerName: t(messages.startRegAt()),
    sortable: false,
    valueGetter: (params: GridValueGetterParams) => {
      return params.row.startRegAt == null
        ? ''
        : formatDate(params.row.startRegAt);
    },
    width: 150,
  },
  {
    field: 'endRegAt',
    headerName: t(messages.endRegAt()),
    sortable: false,
    valueGetter: (params: GridValueGetterParams) => {
      return params.row.endRegAt == null ? '' : formatDate(params.row.endRegAt);
    },
    width: 150,
  },
  {
    field: 'options',
    headerName: '',
    renderCell: (params: GridCellParams) => {
      return (
        <IconButton to={`posts/${params.row.id}/edit`} component={Link}>
          <EditOutlined color="primary" />
        </IconButton>
      );
    },
  },
];

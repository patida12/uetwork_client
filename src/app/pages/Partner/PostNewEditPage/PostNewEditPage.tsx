import {
  Button,
  Card,
  CardContent,
  Grid,
  MenuItem,
  styled as muiStyled,
  TextField,
  Typography,
} from '@mui/material';
import { useFormik } from 'formik';
import { AppContainer } from '../../../components/AppLayout/AppContainer';
import { messages } from '../messages';
import { t } from 'locales/i18n';
import { DateTimePicker, LoadingButton } from '@mui/lab';
import { Link, useHistory, useParams } from 'react-router-dom';
import 'react-quill/dist/quill.snow.css';
import { TextEditor } from '../../../components/TextEditor/TextEditor';
import { Delta as DeltaType } from 'quill';
import { Quill } from 'react-quill';
import { useEffect } from 'react';
import useIsMounted from '../../../hooks/useIsMounted';
import { useAsync } from '../../../hooks/useAsync';
import { useSnackbar } from 'notistack';
import * as Yup from 'yup';
import {
  createPost,
  updatePost,
  getPostById,
  getContacts,
} from 'api/partnerApi';
import { ContactInfo, TermLittleInfo } from '../../../../models';
import { getTerms } from '../../../../api/commonApi';

const Delta = Quill.import('delta');

interface PostInfo {
  title: string;
  jobCount: number;
  partnerContactId: number;
  startRegAt: Date;
  endRegAt: Date;
  content: DeltaType;
  termId: number;
}

interface PostNewEditPageProps {}

const PostNewEditPage: React.FC<PostNewEditPageProps> = (): JSX.Element => {
  const history = useHistory();
  const { postId } = useParams<{ postId?: string }>();
  const isEdit = postId != null;

  const { data: terms, run: runTerms } = useAsync<TermLittleInfo[]>({
    data: [],
  });

  const { data: contacts, run: runContacts } = useAsync<ContactInfo[]>({
    data: [],
  });

  useEffect(() => {
    runTerms(getTerms());
    runContacts(getContacts());
  }, [runTerms, runContacts]);

  let pageTitle = t(messages.addPost());
  if (postId != null) {
    pageTitle = t(messages.editPost());
  }

  const { enqueueSnackbar } = useSnackbar();
  const { run, loading } = useAsync();

  const isMounted = useIsMounted();

  const formik = useFormik<PostInfo>({
    initialValues: {
      title: '',
      jobCount: 1,
      partnerContactId: 0,
      startRegAt: new Date(),
      endRegAt: new Date(),
      content: new Delta(),
      termId: 0,
    },
    onSubmit: ({ content, ...values }) => {
      let createOrUpdatePostPromise: Promise<void>;
      if (postId == null) {
        createOrUpdatePostPromise = createPost({
          ...values,
          content: JSON.stringify(content),
        });
      } else {
        createOrUpdatePostPromise = updatePost(parseInt(postId), {
          ...values,
          content: JSON.stringify(content),
        });
      }

      run(
        createOrUpdatePostPromise
          .then(() => {
            enqueueSnackbar(t(messages.uploadPostSuccess()), {
              variant: 'success',
            });
            history.push('/partner/posts');
          })
          .catch(() => {
            enqueueSnackbar(t(messages.uploadPostFailed()), {
              variant: 'error',
            });
          }),
      );
    },
    validationSchema: PostInfoSchema,
  });

  useEffect(() => {
    if (postId == null) {
      return;
    }
    getPostById(parseInt(postId)).then(
      async ({
        title,
        content,
        startRegAt,
        endRegAt,
        jobCount,
        partnerContactId,
        termId,
      }) => {
        if (isMounted()) {
          formik.setValues({
            title,
            content: await parseJSONStringToDelta(content ?? ''),
            startRegAt: new Date(startRegAt),
            endRegAt: new Date(endRegAt),
            jobCount,
            partnerContactId,
            termId,
          });
        }
      },
    );
  }, [isMounted]);

  const {
    startRegAt,
    endRegAt,
    content,
    title,
    jobCount,
    partnerContactId,
    termId,
  } = formik.values;

  return (
    <StyledAppContainer title={pageTitle}>
      <StyledUploadPostCard
        variant="outlined"
        sx={{
          mb: 2,
          p: 2,
        }}
      >
        {/* TODO: go to posts listing screen when clicked */}
        <Button to="/partner/posts" component={Link} sx={{ mr: 1 }}>
          {t(messages.cancel())}
        </Button>
        <LoadingButton
          variant="contained"
          onClick={() => formik.handleSubmit()}
          loading={loading}
        >
          {t(messages.uploadPost())}
        </LoadingButton>
      </StyledUploadPostCard>
      <Card sx={{ mb: 2 }} variant="outlined">
        <CardContent>
          <Typography sx={{ mb: 4 }} variant="h2">
            {t(messages.basicInfo())}
          </Typography>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <TextField
                required
                name="title"
                value={title}
                onChange={formik.handleChange}
                label={t(messages.title())}
                fullWidth
                error={formik.touched.title && Boolean(formik.errors.title)}
                helperText={formik.touched.title && formik.errors.title}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                required
                name="termId"
                value={termId}
                onChange={formik.handleChange}
                label={t(messages.internshipTerm())}
                fullWidth
                error={formik.touched.termId && Boolean(formik.errors.termId)}
                helperText={formik.touched.termId && formik.errors.termId}
                select
                disabled={isEdit}
              >
                <MenuItem disabled value={0}>
                  <em>{t(messages.noneChosen())}</em>
                </MenuItem>
                {terms!.map(tm => (
                  <MenuItem key={tm.id} value={tm.id}>
                    {t(messages.termYear(), { year: tm.year, term: tm.term })}
                  </MenuItem>
                ))}
              </TextField>
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                required
                name="jobCount"
                value={jobCount}
                onChange={formik.handleChange}
                type="number"
                label={t(messages.jobCount())}
                fullWidth
                error={
                  formik.touched.jobCount && Boolean(formik.errors.jobCount)
                }
                helperText={formik.touched.jobCount && formik.errors.jobCount}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                required
                name="partnerContactId"
                value={partnerContactId}
                onChange={formik.handleChange}
                select
                label={t(messages.contact())}
                fullWidth
                error={
                  formik.touched.partnerContactId &&
                  Boolean(formik.errors.partnerContactId)
                }
                helperText={
                  formik.touched.partnerContactId &&
                  formik.errors.partnerContactId
                }
              >
                <MenuItem disabled value={0}>
                  <em>{t(messages.noneChosen())}</em>
                </MenuItem>
                {contacts!.map(c => (
                  <MenuItem key={c.id} value={c.id}>
                    {c.fullName} - {c.email}
                  </MenuItem>
                ))}
              </TextField>
            </Grid>
            <Grid item xs={12} sm={6}>
              <DateTimePicker
                label={t(messages.startRegAt())}
                value={startRegAt}
                onChange={value => {
                  formik.setFieldValue('startRegAt', value);
                }}
                renderInput={params => (
                  <TextField name="startRegAt" {...params} fullWidth />
                )}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <DateTimePicker
                label={t(messages.endRegAt())}
                value={endRegAt}
                onChange={value => {
                  formik.setFieldValue('endRegAt', value);
                }}
                renderInput={params => (
                  <TextField name="endRegAt" {...params} fullWidth />
                )}
              />
            </Grid>
          </Grid>
        </CardContent>
      </Card>
      <Card variant="outlined">
        <CardContent>
          <Typography sx={{ mb: 4 }} variant="h2">
            {t(messages.content())}
          </Typography>
          <TextEditor
            value={content}
            onChange={(value, delta, source, editor) => {
              // NOTE: perf will be better after build.
              formik.setFieldValue('content', editor.getContents());
            }}
          />
        </CardContent>
      </Card>
    </StyledAppContainer>
  );
};

const PostInfoSchema = Yup.object().shape({
  title: Yup.string().required(t(messages.required())),
  partnerContactId: Yup.number()
    .min(1, t(messages.required()))
    .required(t(messages.required())),
  jobCount: Yup.number()
    .min(1, t(messages.required()))
    .required(t(messages.required())),
  termId: Yup.number()
    .min(1, t(messages.required()))
    .required(t(messages.required())),
});

const parseJSONStringToDelta = async (jsonStr: string): Promise<DeltaType> => {
  try {
    const json = JSON.parse(jsonStr);

    return new Delta(json);
  } catch (e) {
    console.error(e);
    return new Delta();
  }
};

const StyledUploadPostCard = muiStyled(Card)(({ theme }) => {
  return {
    display: 'flex',
    justifyContent: 'flex-end',
  };
});

const StyledAppContainer = muiStyled(AppContainer)({
  height: 'auto !important',
});

export { PostNewEditPage };
export type { PostNewEditPageProps };

import { lazyLoad } from 'utils/loadable';
import { LoadingPage } from '../Common/LoadingPage/LoadingPage';

export const PartnerApp = lazyLoad(
  () => import('./Partner'),
  module => module.PartnerApp,
  {
    fallback: <LoadingPage />,
  },
);

import { styled } from '@mui/material';
import { t } from 'locales/i18n';
import { useEffect } from 'react';
import {
  getPartnerInfo,
  updatePartnerInfo,
  addContact,
  updateContact,
  deleteContact,
} from 'api/partnerApi';
import { PartnerInfo } from 'models';
import { AppContainer } from 'app/components/AppLayout/AppContainer';
import { useAsync } from 'app/hooks/useAsync';
import { messages } from '../messages';
import { ContactListCard } from './components/ContactListCard';
import { FormFields, PersonalInfoCard } from './components/PersonalInfoCard';
import { PartnerContactInfo } from './components/PartnerContactDialog';
import { ChangePasswordCard } from 'app/components/ChangePasswordCard/ChangePasswordCard';

const PersonalInfoPage: React.FC = (): JSX.Element => {
  const { run, data } = useAsync<PartnerInfo>({
    data: {
      id: 0,
      name: '',
      email: '',
      userId: 0,
      address: '',
      logoUrl: '',
      contacts: [],
      description: '',
      homepageUrl: '',
      phoneNumber: '',
    },
  });

  const onUpdateInfo = async (info: FormFields) => {
    await updatePartnerInfo(info);
    run(getPartnerInfo());
  };

  useEffect(() => {
    run(getPartnerInfo());
  }, [run]);

  const onAddContact = async (info: PartnerContactInfo) => {
    await addContact(info);
    run(getPartnerInfo());
  };
  const onUpdateContact = async (info: PartnerContactInfo) => {
    await updateContact({
      ...info,
      id: info.id!,
    });
    run(getPartnerInfo());
  };

  const onDeleteContact = async (id: number) => {
    await deleteContact(id);
    run(getPartnerInfo());
  };

  return (
    <StyledAppContainer title={t(messages.editPersonalInfo())}>
      <PersonalInfoCard initialValues={data} onUpdateInfo={onUpdateInfo} />
      <ContactListCard
        onAddContact={onAddContact}
        onUpdateContact={onUpdateContact}
        onDeleteContact={onDeleteContact}
        contacts={data!.contacts}
      />
      <ChangePasswordCard />
    </StyledAppContainer>
  );
};

const StyledAppContainer = styled(AppContainer)(({ theme }) => ({
  height: 'auto !important',
  '& > *': {
    marginBottom: theme.spacing(2),
  },
}));

export { PersonalInfoPage };

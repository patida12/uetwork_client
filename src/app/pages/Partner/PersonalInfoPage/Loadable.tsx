import { lazyLoad } from 'utils/loadable';
import { LoadingPage } from 'app/pages/Common/LoadingPage/LoadingPage';

export const PersonalInfoPage = lazyLoad(
  () => import('./PersonalInfo'),
  module => module.PersonalInfoPage,
  {
    fallback: <LoadingPage />,
  },
);

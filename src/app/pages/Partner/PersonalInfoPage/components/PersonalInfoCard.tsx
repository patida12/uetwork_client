import { messages } from '../../messages';
import { t } from 'locales/i18n';
import { LoadingButton } from '@mui/lab';
import {
  Card,
  TextField,
  CardActions,
  CardContent,
  styled,
  Typography,
  Grid,
} from '@mui/material';
import { useAsync } from 'app/hooks/useAsync';
import * as Yup from 'yup';
import { useFormik } from 'formik';
import { useSnackbar } from 'notistack';

interface FormFields {
  name: string;
  homepageUrl: string;
  description: string;
  phoneNumber: string;
  email: string;
  address: string;
  logoUrl: string;
}

interface PersonalInfoCardProps {
  className?: string;
  initialValues?: FormFields;
  onUpdateInfo: (info: FormFields) => Promise<void>;
}

const PersonalInfoCard: React.FC<PersonalInfoCardProps> = ({
  className,
  initialValues,
  onUpdateInfo,
}): JSX.Element => {
  const { enqueueSnackbar } = useSnackbar();
  const { run, loading } = useAsync();

  const formik = useFormik<FormFields>({
    initialValues: initialValues ?? {
      name: '',
      email: '',
      phoneNumber: '',
      address: '',
      homepageUrl: '',
      description: '',
      logoUrl: '',
    },
    validationSchema: PersonalInfoSchema,
    onSubmit: values => {
      run(
        onUpdateInfo(values)
          .then(() => {
            enqueueSnackbar(t(messages.saveChangesSuccess()), {
              variant: 'success',
            });
          })
          .catch(e => {
            enqueueSnackbar(t(messages.saveChangesFailed()), {
              variant: 'error',
            });
          }),
      );
    },
    enableReinitialize: true,
  });

  const partnerInfo = formik.values;

  return (
    <Card variant="outlined" className={className}>
      <CardContent>
        <StyledCardTitle variant="h2" component="h2">
          {t(messages.personalInfo())}
        </StyledCardTitle>

        <Grid container spacing={2}>
          <Grid item xs={12}>
            <TextField
              name="name"
              value={partnerInfo.name}
              label={t(messages.partnerName())}
              fullWidth
              onChange={formik.handleChange}
              error={formik.touched.name && Boolean(formik.errors.name)}
              helperText={formik.touched.name && formik.errors.name}
            />
          </Grid>
          <Grid item xs={12} md={6}>
            <TextField
              name="email"
              value={partnerInfo.email}
              disabled
              label={t(messages.email())}
              fullWidth
              onChange={formik.handleChange}
            />
          </Grid>
          <Grid item xs={12} md={6}>
            {/* TODO: Use specialized input for phone number */}
            <TextField
              name="phoneNumber"
              value={partnerInfo.phoneNumber}
              label={t(messages.phoneNumber())}
              fullWidth
              onChange={formik.handleChange}
              error={
                formik.touched.phoneNumber && Boolean(formik.errors.phoneNumber)
              }
              helperText={
                formik.touched.phoneNumber && formik.errors.phoneNumber
              }
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              name="address"
              value={partnerInfo.address}
              label={t(messages.address())}
              fullWidth
              onChange={formik.handleChange}
              error={formik.touched.address && Boolean(formik.errors.address)}
              helperText={formik.touched.address && formik.errors.address}
            />
          </Grid>
        </Grid>
      </CardContent>

      <StyledCardActions>
        <LoadingButton
          type="submit"
          variant="contained"
          disabled={Object.keys(formik.errors).length !== 0}
          loading={loading}
          onClick={() => formik.handleSubmit()}
        >
          {t(messages.saveChanges())}
        </LoadingButton>
      </StyledCardActions>
    </Card>
  );
};

const PersonalInfoSchema = Yup.object().shape({
  name: Yup.string().required(t(messages.required())),
  email: Yup.string().email(t(messages.emailNotValidMsg())),
  phoneNumber: Yup.string(),
});

const StyledCardActions = styled(CardActions)(({ theme }) => ({
  padding: theme.spacing(2),
  display: 'flex',
  justifyContent: 'end',
}));

const StyledCardTitle = styled(Typography)(({ theme }) => ({
  marginBottom: theme.spacing(4),
}));

export { PersonalInfoCard };
export type { FormFields };

import {
  Button,
  Dialog,
  DialogTitle,
  DialogActions,
  TextField,
  styled,
  ButtonProps,
} from '@mui/material';
import { useFormik } from 'formik';
import { useState } from 'react';
import { t } from '../../../../../locales/i18n';
import { messages } from '../../messages';
import * as Yup from 'yup';
import { LoadingButton } from '@mui/lab';

interface PartnerContactInfo {
  id?: number;
  fullName: string;
  phoneNumber: string;
  email: string;
}

interface PartnerContactDialogProps {
  initialValues?: PartnerContactInfo;
  onSubmit: (info: PartnerContactInfo) => void;
  onCancel?: () => void;
  buttonProps?: Omit<ButtonProps, 'onClick'>;
  title?: React.ReactNode;
  submitBtnTitle?: string;
  cancelBtnTitle?: string;
}

const PartnerContactDialog: React.FC<PartnerContactDialogProps> = ({
  buttonProps,
  onCancel,
  title,
  initialValues,
  onSubmit,
  submitBtnTitle,
  cancelBtnTitle,
}): JSX.Element => {
  const [open, setOpen] = useState(false);

  const formik = useFormik({
    initialValues: initialValues ?? {
      id: undefined,
      fullName: '',
      email: '',
      phoneNumber: '',
    },
    onSubmit: values => {
      onSubmit(values);
      setOpen(false);
    },
    validationSchema: PartnerContactInfoSchema,
  });

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
    onCancel?.();
  };

  const { fullName, email, phoneNumber } = formik.values;

  return (
    <>
      <Button onClick={handleClickOpen} {...buttonProps} />
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="partner-contact-dialog-title"
        aria-describedby="partner-contact-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{title}</DialogTitle>
        <FormWrapper>
          <TextField
            name="fullName"
            onChange={formik.handleChange}
            value={fullName}
            label={t(messages.fullName())}
            error={formik.touched.fullName && Boolean(formik.errors.fullName)}
            helperText={formik.touched.fullName && formik.errors.fullName}
          />
          <TextField
            name="email"
            onChange={formik.handleChange}
            value={email}
            label={t(messages.email())}
            error={formik.touched.email && Boolean(formik.errors.email)}
            helperText={formik.touched.email && formik.errors.email}
          />
          <TextField
            name="phoneNumber"
            onChange={formik.handleChange}
            value={phoneNumber}
            label={t(messages.phoneNumber())}
            error={
              formik.touched.phoneNumber && Boolean(formik.errors.phoneNumber)
            }
            helperText={formik.touched.phoneNumber && formik.errors.phoneNumber}
          />
        </FormWrapper>
        <DialogActions
          sx={{
            p: 2,
          }}
        >
          <Button onClick={handleClose}>
            {cancelBtnTitle ?? t(messages.cancel())}
          </Button>
          {/* TODO: button not disabled after submit*/}
          <LoadingButton
            variant="contained"
            onClick={() => formik.handleSubmit()}
            autoFocus
          >
            {submitBtnTitle ?? t(messages.create())}
          </LoadingButton>
        </DialogActions>
      </Dialog>
    </>
  );
};

const PartnerContactInfoSchema = Yup.object().shape({
  fullName: Yup.string().required(t(messages.required())),
  email: Yup.string()
    .email(t(messages.emailNotValidMsg()))
    .required(t(messages.required())),
  phoneNumber: Yup.string().required(t(messages.required())),
});

const FormWrapper = styled('form')(({ theme }) => ({
  display: 'flex',
  flexDirection: 'column',
  '& > .MuiTextField-root': {
    marginBottom: theme.spacing(2),
  },
  marginLeft: theme.spacing(2),
  marginRight: theme.spacing(2),
}));

export { PartnerContactDialog };
export type { PartnerContactInfo };

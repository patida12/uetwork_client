import {
  Box,
  Button,
  ButtonProps,
  Link,
  Paper,
  Typography,
} from '@mui/material';
import { DeleteOutlined, EditOutlined } from '@mui/icons-material';
import { t } from 'locales/i18n';
import { messages } from '../../messages';
import {
  PartnerContactDialog,
  PartnerContactInfo,
} from './PartnerContactDialog';
import { MouseEventHandler, useState } from 'react';

interface ContactInfoProps {
  className?: string;
  contact: PartnerContactInfo;
  onUpdateContact: (info: PartnerContactInfo) => void;
  onDeleteContact: (id: number) => void;
}

const ContactInfo: React.FC<ContactInfoProps> = ({
  contact,
  className,
  onUpdateContact,
  onDeleteContact,
}): JSX.Element => {
  const { fullName, email, phoneNumber, id } = contact;

  return (
    <Paper
      sx={{
        p: 2,
        width: 1,
        bgcolor: 'grey.200',
      }}
      className={className}
      elevation={0}
    >
      <Typography variant="subtitle1" gutterBottom>
        {fullName}
      </Typography>

      <Typography variant="body2" gutterBottom>
        <Typography
          variant="body2"
          component="span"
          sx={{ color: 'text.secondary' }}
        >
          {t(messages.email())}: &nbsp;
        </Typography>
        <Link href={'mailto:' + email}>{email}</Link>
      </Typography>

      <Typography variant="body2" gutterBottom>
        <Typography
          variant="body2"
          component="span"
          sx={{ color: 'text.secondary' }}
        >
          {t(messages.phoneNumber())}: &nbsp;
        </Typography>
        {phoneNumber}
      </Typography>

      <Box sx={{ mt: 1 }} display="flex" justifyContent="end">
        <ConfirmationButton
          color="error"
          startIcon={<DeleteOutlined />}
          onClick={() => {
            onDeleteContact(contact.id!);
          }}
          sx={{ mr: 1 }}
        >
          {t(messages.delete())}
        </ConfirmationButton>
        <PartnerContactDialog
          title={t(messages.editContact())}
          onSubmit={values => {
            onUpdateContact(values);
          }}
          buttonProps={{
            children: t(messages.edit()),
            startIcon: <EditOutlined />,
          }}
          initialValues={{ fullName, phoneNumber, email, id }}
          submitBtnTitle={t(messages.edit())}
        />
      </Box>
    </Paper>
  );
};

const ConfirmationButton: React.FC<ButtonProps> = ({
  onClick,
  children,
  ...props
}): JSX.Element => {
  const [clicked, setClicked] = useState(false);

  const onClickConfirmed: MouseEventHandler<HTMLButtonElement> = e => {
    if (clicked) {
      onClick?.(e);
      setClicked(false);
      return;
    }
    setClicked(true);
  };

  return (
    <Button
      {...props}
      children={clicked ? t(messages.areYouSure()) : children}
      onClick={onClickConfirmed}
    />
  );
};

export { ContactInfo };

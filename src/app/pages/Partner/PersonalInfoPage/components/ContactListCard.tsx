import { AddOutlined } from '@mui/icons-material';
import { Card, CardContent, Box, Grid, Typography } from '@mui/material';
import { t } from 'locales/i18n';
import { useSnackbar } from 'notistack';
import { ContactInfo as ContactInfoModel } from '../../../../../models';
import { useAsync } from '../../../../hooks/useAsync';
import { messages } from '../../messages';
import { ContactInfo } from './ContactInfo';
import {
  PartnerContactDialog,
  PartnerContactInfo,
} from './PartnerContactDialog';

interface ContactListCardProps {
  contacts: ContactInfoModel[];
  onAddContact: (info: PartnerContactInfo) => Promise<void>;
  onUpdateContact: (info: PartnerContactInfo) => Promise<void>;
  onDeleteContact: (id: number) => Promise<void>;
}

const ContactListCard: React.FC<ContactListCardProps> = ({
  contacts,
  onAddContact,
  onUpdateContact,
  onDeleteContact,
}): JSX.Element => {
  const { enqueueSnackbar } = useSnackbar();
  const { run } = useAsync();

  return (
    <Card variant="outlined">
      <CardContent>
        <Box
          display="flex"
          alignItems="center"
          justifyContent="space-between"
          sx={{
            marginBottom: 2,
          }}
        >
          <Typography variant="h2" component="h2">
            {t(messages.contactList())}
          </Typography>
          <PartnerContactDialog
            onSubmit={values => {
              run(
                onAddContact(values)
                  .then(() => {
                    enqueueSnackbar(t(messages.saveContactSuccess()), {
                      variant: 'success',
                    });
                  })
                  .catch(() => {
                    enqueueSnackbar(t(messages.saveContactFailed()), {
                      variant: 'error',
                    });
                  }),
              );
            }}
            title={t(messages.addContact())}
            buttonProps={{
              startIcon: <AddOutlined />,
              children: t(messages.add()),
            }}
          />
        </Box>
        <Grid container spacing={2}>
          {contacts.length === 0 ? (
            <Grid item>No contacts</Grid>
          ) : (
            contacts.map(c => (
              <Grid item xs={12} sm={6} lg={4} key={c.id}>
                <ContactInfo
                  onUpdateContact={values => {
                    run(
                      onUpdateContact(values)
                        .then(() => {
                          enqueueSnackbar(t(messages.saveContactSuccess()), {
                            variant: 'success',
                          });
                        })
                        .catch(() => {
                          enqueueSnackbar(t(messages.saveContactFailed()), {
                            variant: 'error',
                          });
                        }),
                    );
                  }}
                  onDeleteContact={id => {
                    run(
                      onDeleteContact(id)
                        .then(() => {
                          enqueueSnackbar(t(messages.deleteContactSuccess()), {
                            variant: 'success',
                          });
                        })
                        .catch(() => {
                          enqueueSnackbar(t(messages.deleteContactFailed()), {
                            variant: 'error',
                          });
                        }),
                    );
                  }}
                  contact={c}
                />
              </Grid>
            ))
          )}
        </Grid>
      </CardContent>
    </Card>
  );
};

export { ContactListCard };

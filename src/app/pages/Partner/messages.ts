import { translations } from 'locales/translations';
import { _t } from 'utils/messages';

export const messages = {
  home: () => _t(translations.common.app.home),
  studentManagement: () => _t(translations.partnerFeature.studentManagement),
  passedStudents: () => _t(translations.partnerFeature.passedStudents),
  studentApplications: () =>
    _t(translations.partnerFeature.studentApplications),
  postManagement: () => _t(translations.partnerFeature.postManagement),
  postList: () => _t(translations.partnerFeature.postList),
  personalInfo: () => _t(translations.accountFeature.personalInfo),
  editPersonalInfo: () => _t(translations.accountFeature.editPersonalInfo),
  contactList: () => _t(translations.partnerFeature.contactList),
  editContact: () => _t(translations.contactFeature.editContact),
  addContact: () => _t(translations.contactFeature.addContact),
  addPost: () => _t(translations.internship.postFeature.addPost),
  editPost: () => _t(translations.internship.postFeature.editPost),

  accept: () => _t(translations.common.accept),
  acceptDialogTitle: () =>
    _t(translations.partnerFeature.acceptStudentApplication),
  acceptDialogContent: () =>
    _t(translations.partnerFeature.acceptStudentApplicationMsg),
  reject: () => _t(translations.common.reject),
  rejectDialogTitle: () =>
    _t(translations.partnerFeature.rejectStudentApplication),
  rejectDialogContent: () =>
    _t(translations.partnerFeature.rejectStudentApplicationMsg),
  acceptApplicationSuccessMsg: () =>
    _t(translations.partnerFeature.acceptApplicationSuccessMsg),
  acceptApplicationFailedMsg: () =>
    _t(translations.partnerFeature.acceptApplicationFailedMsg),
  rejectApplicationSuccessMsg: () =>
    _t(translations.partnerFeature.rejectApplicationSuccessMsg),
  rejectApplicationFailedMsg: () =>
    _t(translations.partnerFeature.rejectApplicationFailedMsg),

  search: () => _t(translations.common.table.search),
  internshipTerm: () => _t(translations.common.app.internshipTerm),
  searchPlaceholder: () =>
    _t(translations.partnerFeature.studentSearchPlaceholder),
  all: () => _t(translations.common.all),
  noneChosen: () => _t(translations.common.noneChosen),

  partnerName: () => _t(translations.partnerFeature.name),
  address: () => _t(translations.partnerFeature.address),
  delete: () => _t(translations.common.delete),
  edit: () => _t(translations.common.edit),
  email: () => _t(translations.common.email),
  add: () => _t(translations.common.button.add),
  create: () => _t(translations.common.create),
  cancel: () => _t(translations.common.button.cancel),
  confirm: () => _t(translations.common.button.confirm),
  areYouSure: () => _t(translations.common.button.areYouSure),

  fullName: () => _t(translations.studentFeature.fullName),
  idNumber: () => _t(translations.studentFeature.idNumber),
  orgEmail: () => _t(translations.lecturerFeature.orgEmail),
  personalEmail: () => _t(translations.studentFeature.personalEmail),
  phoneNumber: () => _t(translations.studentFeature.phoneNumber),
  schoolClassName: () => _t(translations.studentFeature.schoolClass),
  supervisor: () => _t(translations.studentFeature.supervisor),
  registrationDate: () => _t(translations.studentFeature.registrationDate),
  registrationType: () => _t(translations.studentFeature.registrationType),
  selfRegistered: () => _t(translations.partnerFeature.selfRegistered),
  passedInterview: () => _t(translations.partnerFeature.passedInterview),
  termYear: () => _t(translations.internship.termYear),

  changePassword: () => _t(translations.authFeature.changePassword),
  currentPassword: () => _t(translations.authFeature.currentPassword),
  newPassword: () => _t(translations.authFeature.newPassword),
  retypeNewPassword: () => _t(translations.authFeature.retypeNewPassword),
  passwordDoNotMatchMsg: () =>
    _t(translations.common.validate.passwordDoNotMatchMsg),

  emailNotValidMsg: () => _t(translations.common.validate.invalidEmail),
  phoneNumberNotValidMsg: () =>
    _t(translations.common.validate.phoneNumberNotValidMsg),
  required: () => _t(translations.common.validate.required),
  saveChanges: () => _t(translations.common.saveChanges),
  saveChangesSuccess: () => _t(translations.common.saveChangesSuccess),
  saveChangesFailed: () => _t(translations.common.saveChangesFailed),

  // edit post page
  basicInfo: () => _t(translations.internship.postFeature.basicInfo),
  title: () => _t(translations.internship.postFeature.title),
  jobCount: () => _t(translations.internship.postFeature.jobCount),
  contact: () => _t(translations.internship.postFeature.contact),
  partnerContactName: () => _t(translations.partnerFeature.partnerContactName),
  partnerContactEmail: () =>
    _t(translations.partnerFeature.partnerContactEmail),
  partnerContactPhoneNumber: () =>
    _t(translations.partnerFeature.partnerContactPhoneNumber),
  startRegAt: () => _t(translations.internship.postFeature.startRegAt),
  endRegAt: () => _t(translations.internship.postFeature.endRegAt),
  content: () => _t(translations.internship.postFeature.content),
  uploadPost: () => _t(translations.internship.postFeature.uploadPost),
  uploadPostSuccess: () =>
    _t(translations.internship.postFeature.uploadPostSuccess),
  uploadPostFailed: () =>
    _t(translations.internship.postFeature.uploadPostFailed),
  // contact list card
  saveContactSuccess: () => _t(translations.contactFeature.saveContactSuccess),
  saveContactFailed: () => _t(translations.contactFeature.saveContactFailed),
  deleteContactSuccess: () =>
    _t(translations.contactFeature.deleteContactSuccess),
  deleteContactFailed: () =>
    _t(translations.contactFeature.deleteContactFailed),
  createdAt: () => _t(translations.userFeature.createdAt),
};

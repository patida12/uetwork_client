import {
  GridColDef,
  GridRenderCellParams,
  GridValueGetterParams,
} from '@mui/x-data-grid';
import { messages } from '../messages';
import { t } from 'locales/i18n';
import { Chip } from '@mui/material';
import { styled } from '@mui/styles';
import { formatDate } from 'utils';

export enum TableField {
  NAME = 'fullName',
  STUDENT_ID = 'studentIdNumber',
  SCHOOL_EMAIL = 'orgEmail',
  PERSONAL_EMAIL = 'personalEmail',
  PHONE_NUMBER = 'phoneNumber',
  CLASS = 'schoolClass',
  LECTURER = 'supervisor',
  DATE_REGISTERED = 'registeredAt',
  TYPE = 'isSelfRegistered',
}

export const columns: GridColDef[] = [
  { field: 'id', hide: true },
  {
    field: TableField.NAME,
    headerName: t(messages.fullName()),
    width: 180,
  },
  {
    field: TableField.STUDENT_ID,
    headerName: t(messages.idNumber()),
    sortable: false,
    width: 150,
  },
  {
    field: TableField.SCHOOL_EMAIL,
    headerName: t(messages.orgEmail()),
    sortable: false,
    width: 200,
  },
  {
    field: TableField.PERSONAL_EMAIL,
    headerName: t(messages.personalEmail()),
    sortable: false,
    width: 200,
  },
  {
    field: TableField.PHONE_NUMBER,
    headerName: t(messages.phoneNumber()),
    sortable: false,
    width: 150,
  },
  {
    field: TableField.CLASS,
    headerName: t(messages.schoolClassName()),
    valueGetter: (params: GridValueGetterParams) => {
      return params.row.schoolClass?.name ?? '';
    },
    sortable: false,
    width: 150,
  },
  {
    field: TableField.LECTURER,
    headerName: t(messages.supervisor()),
    valueGetter: (params: GridValueGetterParams) => {
      return params.row.supervisor?.name ?? '';
    },
    sortable: false,
    width: 180,
  },
  {
    field: TableField.DATE_REGISTERED,
    headerName: t(messages.registrationDate()),
    valueGetter: (params: GridValueGetterParams) => {
      return params.row.registeredAt == null
        ? ''
        : formatDate(params.row.registeredAt);
    },
    sortable: false,
    width: 150,
  },
  {
    field: TableField.TYPE,
    headerName: t(messages.registrationType()),
    sortable: false,
    width: 150,
    renderCell: (params: GridRenderCellParams) => {
      if (params.row.isSelfRegistered) {
        return (
          <StyledChip
            label={t(messages.selfRegistered())}
            className="warning"
          />
        );
      } else {
        return (
          <StyledChip
            label={t(messages.passedInterview())}
            className="success"
          />
        );
      }
    },
  },
];

export const StyledChip = styled(Chip)(({ theme }) => ({
  fontWeight: 500,
  '&.warning': {
    backgroundColor: theme.palette.warning.light,
    color: theme.palette.warning.main,
  },
  '&.success': {
    backgroundColor: theme.palette.success.light,
    color: theme.palette.success.main,
  },
}));

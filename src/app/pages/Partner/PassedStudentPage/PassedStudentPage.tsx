import { styled } from '@mui/styles';
import { GridSortModel } from '@mui/x-data-grid';
import { useEffect } from 'react';
import {
  useQueryParams,
  withDefault,
  NumberParam,
  StringParam,
  ObjectParam,
} from 'use-query-params';
import {
  getStudents,
  GetStudentsFilterQuery,
  GetStudentsQuery,
  GetStudentsResponseData,
} from 'api/partnerApi';
import { t } from '../../../../locales/i18n';
import { PaginatedResultsMeta, FilterState } from '../../../../models';
import { isZeroValue, safeParseInt, sortModelToQuery } from '../../../../utils';
import { AppDataGrid } from '../../../components/AppDataGrid/AppDataGrid';
import { AppContainer } from '../../../components/AppLayout/AppContainer';
import { DEFAULT_PAGE, DEFAULT_PER_PAGE } from '../../../constants';
import { useAsync } from '../../../hooks/useAsync';
import { messages } from '../messages';
import { columns } from './columns';
import { CustomToolbar } from './components/CustomToolbar';

const PassedStudentsPage: React.FC = (): JSX.Element => {
  const { loading, data, run } = useAsync<{
    students: GetStudentsResponseData[];
    meta: PaginatedResultsMeta;
  }>({
    data: {
      students: [],
      meta: {
        currentPage: 0,
        perPage: 0,
        totalItems: 0,
        totalPages: 0,
      },
    },
  });

  const [query, setQuery] = useQueryParams({
    page: withDefault(NumberParam, DEFAULT_PAGE),
    perPage: withDefault(NumberParam, DEFAULT_PER_PAGE),
    q: withDefault(StringParam, ''),
    sort: withDefault(ObjectParam, {}),
    filter: withDefault(ObjectParam, {}),
  });

  const { page, perPage, q, filter, sort } = query;

  const onPageChange = (page: number) => {
    setQuery({
      page,
    });
  };

  const onPageSizeChange = (pageSize: number) => {
    setQuery({
      perPage: pageSize,
      page: 1,
    });
  };

  const onSortModelChange = (model: GridSortModel) => {
    if (model.length === 0) {
      setQuery({
        sort: {},
      });
      return;
    }
    const firstElem = model[0];

    setQuery({
      sort: {
        ...sort,
        [firstElem.field]: [firstElem.sort],
      },
    });
  };

  const onFilterChange = (value: FilterState) => {
    setQuery({
      q: value.query,
      filter: value.filter,
    });
  };

  useEffect(() => {
    run(getStudents(getQueryObject(query)));
  }, [run, query]);

  const { students: rows, meta } = data!;

  const initialFilters: FilterState = {
    query: q,
    filter: {
      termId: filter['termId'] ?? '',
    },
  };

  return (
    <AppContainer title={t(messages.passedStudents())}>
      <StyledDiv>
        <AppDataGrid
          rows={rows ?? []}
          columns={columns}
          loading={loading}
          page={page}
          pageSize={perPage}
          rowCount={meta.totalItems}
          onPageChange={onPageChange}
          onPageSizeChange={onPageSizeChange}
          onSortModelChange={onSortModelChange}
          components={{ Toolbar: CustomToolbar }}
          componentsProps={{
            toolbar: {
              onFilterChange,
              initialFilters,
            },
          }}
        />
      </StyledDiv>
    </AppContainer>
  );
};

const getQueryObject = (qObj: any): GetStudentsQuery => {
  const { page, perPage, sort, q, filter } = qObj;
  const gsq: GetStudentsQuery = {
    page,
    perPage,
    q,
  };

  const gsqSort = sort == null ? [] : sortModelToQuery(sort);
  const gsqFilters: GetStudentsFilterQuery = {};
  const { termId } = filter;
  if (!isZeroValue(termId)) {
    gsqFilters.termId = safeParseInt(termId);
  }

  gsq.sort = gsqSort;
  gsq.filter = gsqFilters;

  return gsq;
};

const StyledDiv = styled('div')(({ theme }: any) => {
  return {
    height: '100%',
    width: '100%',
  };
});

export { PassedStudentsPage };

import { Button, MenuItem, TextField } from '@mui/material';
import { GridToolbarContainer } from '@mui/x-data-grid';
import { FilterState, TermLittleInfo } from 'models';
import { useEffect, useState } from 'react';
import { messages } from '../../messages';
import { t } from 'locales/i18n';
import styled from 'styled-components';
import { getTerms } from 'api/commonApi';
import { useAsync } from '../../../../hooks/useAsync';

interface CustomToolbarProps {
  onFilterChange: (value: FilterState) => void;
  initialFilters?: FilterState;
}

export const CustomToolbar: React.FC<CustomToolbarProps> = ({
  onFilterChange,
  initialFilters,
}): JSX.Element => {
  let initialValue: FilterState = {
    query: '',
    filter: {},
  };
  if (initialFilters != null) {
    initialValue.query = initialFilters.query ?? '';
    initialValue.filter = initialFilters.filter ?? {};
  }

  const [queryObj, setQueryObj] = useState<FilterState>(initialValue);

  const handleQueryChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setQueryObj(prev => ({
      ...prev,
      query: e.target.value,
    }));
  };

  const handleTermIdChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setQueryObj(prev => ({
      ...prev,
      filter: {
        ...prev.filter,
        termId: e.target.value,
      },
    }));
  };

  const { data: terms, run: runTerms } = useAsync<TermLittleInfo[]>({
    data: [],
  });

  useEffect(() => {
    runTerms(getTerms());
  }, [runTerms]);

  const { query, filter } = queryObj;

  const onSearch = () => {
    onFilterChange(queryObj);
  };

  return (
    <StyledGridToolbarContainer>
      <SearchField
        value={query}
        onChange={handleQueryChange}
        size="small"
        label={t(messages.search())}
      ></SearchField>
      <SelectField
        select
        label={t(messages.internshipTerm())}
        value={filter['termId']}
        onChange={handleTermIdChange}
        size="small"
      >
        <MenuItem value="">{t(messages.all())}</MenuItem>
        {terms!.map(tm => (
          <MenuItem key={tm.id} value={tm.id.toString()}>
            {t(messages.termYear(), { year: tm.year, term: tm.term })}
          </MenuItem>
        ))}
      </SelectField>
      <Button onClick={onSearch} variant="contained">
        {t(messages.search())}
      </Button>
    </StyledGridToolbarContainer>
  );
};

const StyledGridToolbarContainer = styled(GridToolbarContainer)({
  display: 'flex',
  padding: '16px',
  '& > *': {
    marginLeft: 8,
  },
});

const SearchField = styled(TextField)({
  flexGrow: 1,
  marginLeft: 0,
});

const SelectField = styled(TextField)({
  width: '200px',
});

import { lazyLoad } from 'utils/loadable';

export const PassedStudentsPage = lazyLoad(
  () => import('./PassedStudentPage'),
  module => module.PassedStudentsPage,
);

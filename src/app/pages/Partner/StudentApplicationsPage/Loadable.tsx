import { lazyLoad } from 'utils/loadable';

export const StudentApplicationsPage = lazyLoad(
  () => import('./StudentApplicationsPage'),
  module => module.StudentApplicationsPage,
);

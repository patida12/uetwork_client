import Button from '@mui/material/Button';
import { GridToolbarContainer, useGridApiContext } from '@mui/x-data-grid';
import { FilterState, TermLittleInfo } from '../../../../../models';
import { t } from 'locales/i18n';
import { messages } from '../../messages';
import { MenuItem, TextField } from '@mui/material';
import styled from 'styled-components';
import { useState, useEffect } from 'react';
import { getTerms } from 'api/commonApi';
import { useAsync } from '../../../../hooks/useAsync';
import { ConfirmDialog } from '../../components/ConfirmDialog/ConfirmDialog';
import { LoadingButton } from '@mui/lab';
import { batchUpdateRegistrationStatus } from '../../../../../api/partnerApi';
import { RegistrationStatus } from '../../../../../models/appliedStudent';
import { useSnackbar } from 'notistack';

interface CustomToolbarProps {
  onFilterChange: (value: FilterState) => void;
  initialFilters?: FilterState;
  refreshData: () => void;
}

const CustomToolbar: React.FC<CustomToolbarProps> = ({
  onFilterChange,
  initialFilters,
  refreshData,
}): JSX.Element => {
  let initialValue: FilterState = {
    query: '',
    filter: {},
  };
  if (initialFilters != null) {
    initialValue.query = initialFilters.query ?? '';
    initialValue.filter = initialFilters.filter ?? {};
  }

  const [queryObj, setQueryObj] = useState<FilterState>(initialValue);

  const handleQueryChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setQueryObj(prev => ({
      ...prev,
      query: e.target.value,
    }));
  };

  const handleTermIdChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setQueryObj(prev => ({
      ...prev,
      filter: {
        ...prev.filter,
        termId: e.target.value,
      },
    }));
  };

  const { data: terms, run: runTerms } = useAsync<TermLittleInfo[]>({
    data: [],
  });

  useEffect(() => {
    runTerms(getTerms());
  }, [runTerms]);

  const { query, filter } = queryObj;

  const onSearch = () => {
    onFilterChange(queryObj);
  };

  return (
    <StyledGridToolbarContainer>
      <FieldsWrapper>
        <SearchField
          value={query}
          onChange={handleQueryChange}
          size="small"
          label={t(messages.search())}
        ></SearchField>
        <SelectField
          select
          label={t(messages.internshipTerm())}
          value={filter['termId'] ?? ''}
          onChange={handleTermIdChange}
          size="small"
        >
          <MenuItem value="">{t(messages.all())}</MenuItem>
          {terms!.map(tm => (
            <MenuItem key={tm.id} value={tm.id.toString()}>
              {t(messages.termYear(), { year: tm.year, term: tm.term })}
            </MenuItem>
          ))}
        </SelectField>
        <Button onClick={onSearch} variant="contained">
          {t(messages.search())}
        </Button>
      </FieldsWrapper>
      <ToolbarActions refreshData={refreshData} />
    </StyledGridToolbarContainer>
  );
};

interface ToolbarActionsProps {
  refreshData: () => void;
}

const ToolbarActions: React.FC<ToolbarActionsProps> = ({ refreshData }) => {
  const apiRef = useGridApiContext();
  const selectedRows = apiRef.current.getSelectedRows();

  const [acceptDialogOpen, setAcceptDialogOpen] = useState(false);
  const [rejectDialogOpen, setRejectDialogOpen] = useState(false);

  const { run, loading } = useAsync();

  const { enqueueSnackbar } = useSnackbar();

  const onAcceptApplications = () => {
    const regIdsIterator = selectedRows.keys();

    run(
      batchUpdateRegistrationStatus({
        registrationIds: Array.from(regIdsIterator) as number[],
        status: RegistrationStatus.PASSED,
      })
        .then(() => {
          enqueueSnackbar(t(messages.acceptApplicationSuccessMsg()), {
            variant: 'success',
          });
          setAcceptDialogOpen(false);
          apiRef.current.setSelectionModel([]);
          refreshData();
        })
        .catch(e => {
          console.error(e);
          enqueueSnackbar(t(messages.acceptApplicationFailedMsg()), {
            variant: 'error',
          });
        }),
    );
  };

  const onRejectApplications = () => {
    const regIdsIterator = selectedRows.keys();

    run(
      batchUpdateRegistrationStatus({
        registrationIds: Array.from(regIdsIterator) as number[],
        status: RegistrationStatus.FAILED,
      })
        .then(() => {
          enqueueSnackbar(t(messages.rejectApplicationSuccessMsg()), {
            variant: 'success',
          });
          setRejectDialogOpen(false);
          apiRef.current.setSelectionModel([]);
          refreshData();
        })
        .catch(e => {
          console.error(e);
          enqueueSnackbar(t(messages.rejectApplicationFailedMsg()), {
            variant: 'error',
          });
        }),
    );
  };

  return (
    <>
      <FieldsWrapper>
        <LoadingButton
          color="success"
          disabled={selectedRows.size === 0}
          onClick={() => setAcceptDialogOpen(true)}
          loading={loading}
        >
          {t(messages.accept())}
        </LoadingButton>
        <LoadingButton
          color="error"
          disabled={selectedRows.size === 0}
          onClick={() => setRejectDialogOpen(true)}
        >
          {t(messages.reject())}
        </LoadingButton>
      </FieldsWrapper>
      <ConfirmDialog
        title={t(messages.acceptDialogTitle())}
        open={acceptDialogOpen}
        content={t(messages.acceptDialogContent())}
        onConfirm={onAcceptApplications}
        onClose={() => setAcceptDialogOpen(false)}
      />
      <ConfirmDialog
        title={t(messages.rejectDialogTitle())}
        open={rejectDialogOpen}
        content={t(messages.rejectDialogContent())}
        onConfirm={onRejectApplications}
        onClose={() => setRejectDialogOpen(false)}
      />
    </>
  );
};

const StyledGridToolbarContainer = styled(GridToolbarContainer)({
  display: 'flex',
  flexDirection: 'column',
  padding: '8px 16px',
});

const FieldsWrapper = styled('div')({
  width: '100%',
  display: 'flex',
  padding: '8px 0px',
  '& > *': {
    marginLeft: 8,
  },
});

const SearchField = styled(TextField)({
  flexGrow: 1,
  marginLeft: 0,
});

const SelectField = styled(TextField)({
  width: '200px',
});

export { CustomToolbar };

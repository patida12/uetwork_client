import { messages } from '../messages';
import { AppContainer } from '../../../components/AppLayout/AppContainer';
import { t } from 'locales/i18n';
import { GridSortModel } from '@mui/x-data-grid';
import { AppDataGrid } from '../../../components/AppDataGrid/AppDataGrid';
import {
  useQueryParams,
  withDefault,
  NumberParam,
  StringParam,
  ObjectParam,
} from 'use-query-params';
import { DEFAULT_PAGE, DEFAULT_PER_PAGE } from '../../../constants';
import { columns } from './columns';
import { StudentApplication } from '../../../../models/appliedStudent';
import { FilterState, PaginatedResultsMeta } from '../../../../models';
import { useEffect } from 'react';
import { getStudentApplications } from 'api/partnerApi';
import { useAsync } from '../../../hooks/useAsync';
import { CustomToolbar } from './components/CustomToolbar';
import {
  GetPendingApplicationsFilterQuery,
  GetPendingApplicationsQuery,
} from '../../../../api/partnerApi';
import { sortModelToQuery, isZeroValue, safeParseInt } from '../../../../utils';

const StudentApplicationsPage: React.FC = (): JSX.Element => {
  const { loading, data, run } = useAsync<{
    students: StudentApplication[];
    meta: PaginatedResultsMeta;
  }>({
    data: {
      students: [],
      meta: {
        currentPage: 0,
        perPage: 0,
        totalItems: 0,
        totalPages: 0,
      },
    },
  });

  const [query, setQuery] = useQueryParams({
    page: withDefault(NumberParam, DEFAULT_PAGE),
    perPage: withDefault(NumberParam, DEFAULT_PER_PAGE),
    q: withDefault(StringParam, ''),
    sort: withDefault(ObjectParam, {}),
    filter: withDefault(ObjectParam, {}),
  });

  const { page, perPage, q, filter, sort } = query;

  const onPageChange = (page: number) => {
    setQuery({
      page,
    });
  };

  const onPageSizeChange = (pageSize: number) => {
    setQuery({
      perPage: pageSize,
      page: 1,
    });
  };

  const onSortModelChange = (model: GridSortModel) => {
    if (model.length === 0) {
      setQuery({
        sort: {},
      });
      return;
    }
    const firstElem = model[0];

    setQuery({
      sort: {
        ...sort,
        [firstElem.field]: [firstElem.sort],
      },
    });
  };

  const onFilterChange = (value: FilterState) => {
    setQuery({
      q: value.query,
      filter: value.filter,
    });
  };

  useEffect(() => {
    run(getStudentApplications(getQueryObject(query)));
  }, [run, query]);

  const initialFilters: FilterState = {
    query: q,
    filter: {
      termId: filter['termId'] ?? '',
    },
  };

  const { students: rows, meta } = data!;

  return (
    <AppContainer title={t(messages.studentApplications())}>
      <AppDataGrid
        sx={{ height: '100%' }}
        rows={rows ?? []}
        columns={columns}
        page={page}
        pageSize={perPage}
        rowCount={meta.totalItems}
        onPageChange={onPageChange}
        onPageSizeChange={onPageSizeChange}
        onSortModelChange={onSortModelChange}
        loading={loading}
        checkboxSelection
        components={{ Toolbar: CustomToolbar }}
        componentsProps={{
          toolbar: {
            onFilterChange,
            initialFilters,
            refreshData: () =>
              run(getStudentApplications(getQueryObject(query))),
          },
        }}
      />
    </AppContainer>
  );
};

const getQueryObject = (qObj: any): GetPendingApplicationsQuery => {
  const { page, perPage, sort, q, filter } = qObj;
  const gsq: GetPendingApplicationsQuery = {
    page,
    perPage,
    q,
  };

  const gsqSort = sort == null ? [] : sortModelToQuery(sort);
  const gsqFilters: GetPendingApplicationsFilterQuery = {};
  const { termId } = filter;
  if (!isZeroValue(termId)) {
    gsqFilters.termId = safeParseInt(termId);
  }

  gsq.sort = gsqSort;
  gsq.filter = gsqFilters;

  return gsq;
};

export { StudentApplicationsPage };

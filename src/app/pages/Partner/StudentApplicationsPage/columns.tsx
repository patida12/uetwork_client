import { GridColDef, GridValueGetterParams } from '@mui/x-data-grid';
import { messages } from '../messages';
import { t } from 'locales/i18n';
import { formatDate } from 'utils';

export enum TableField {
  NAME = 'fullName',
  STUDENT_ID = 'studentIdNumber',
  SCHOOL_EMAIL = 'orgEmail',
  PERSONAL_EMAIL = 'personalEmail',
  PHONE_NUMBER = 'phoneNumber',
  CLASS = 'schoolClass',
  LECTURER = 'supervisor',
  DATE_REGISTERED = 'registeredAt',
}

export const columns: GridColDef[] = [
  { field: 'id', hide: true },
  {
    field: TableField.NAME,
    headerName: t(messages.fullName()),
    width: 180,
  },
  {
    field: TableField.STUDENT_ID,
    headerName: t(messages.idNumber()),
    width: 150,
  },
  {
    field: TableField.SCHOOL_EMAIL,
    headerName: t(messages.orgEmail()),
    sortable: false,
    width: 200,
  },
  {
    field: TableField.PERSONAL_EMAIL,
    headerName: t(messages.personalEmail()),
    sortable: false,
    width: 200,
  },
  {
    field: TableField.PHONE_NUMBER,
    headerName: t(messages.phoneNumber()),
    sortable: false,
    width: 150,
  },
  {
    field: TableField.DATE_REGISTERED,
    headerName: t(messages.registrationDate()),
    valueGetter: (params: GridValueGetterParams) => {
      return params.row.registeredAt == null
        ? ''
        : formatDate(params.row.registeredAt);
    },
    sortable: false,
    width: 150,
  },
];

import { translations } from 'locales/translations';
import { _t } from 'utils/messages';

export const messages = {
  partnerDisplayName: () => _t(translations.dashboard.partnerDisplayName),
  partnerIntroduction: () => _t(translations.dashboard.partnerIntroduction),
  totalStudents: () => _t(translations.dashboard.totalStudents),
  newPost: () => _t(translations.dashboard.newPost),
  listRequests: () => _t(translations.dashboard.listRequests),
  studentIdNumber: () =>
    _t(translations.internship.studentFeature.studentIdNumber),
  fullName: () => _t(translations.internship.studentFeature.fullName),
  orgEmail: () => _t(translations.internship.studentFeature.orgEmail),
  phoneNumber: () => _t(translations.internship.studentFeature.phoneNumber),
};

/**
 * Asynchronously loads the component for HomePage
 */

import { lazyLoad } from 'utils/loadable';

export const PartnerDashboardPage = lazyLoad(
  () => import('./index'),
  module => module.PartnerDashboardPage,
);

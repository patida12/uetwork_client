import { GridColDef } from '@mui/x-data-grid';
import { t } from 'locales/i18n';
import { messages } from './messages';

export const requestsColumns: GridColDef[] = [
  {
    field: 'fullName',
    headerName: t(messages.fullName()),
    width: 240,
    sortable: false,
    disableColumnMenu: true,
  },
  {
    field: 'studentIdNumber',
    headerName: t(messages.studentIdNumber()),
    width: 240,
    sortable: false,
    disableColumnMenu: true,
  },
  {
    field: 'orgEmail',
    headerName: t(messages.orgEmail()),
    width: 240,
    sortable: false,
    disableColumnMenu: true,
  },
  {
    field: 'phoneNumber',
    headerName: t(messages.phoneNumber()),
    width: 240,
    sortable: false,
    disableColumnMenu: true,
  },
];

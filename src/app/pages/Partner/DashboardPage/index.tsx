import { Container, Grid } from '@mui/material';
import dashboardApi from 'api/dashboardApi';
import { Results } from 'api/types';
import AppNewData from 'app/components/Dashboard/AppNewData';
import AppTotalSummary from 'app/components/Dashboard/AppTotalSummary';
import AppWelcome from 'app/components/Dashboard/AppWelcome';
import { PATH_NEW_POST_PAGE, PATH_REQUESTS_PAGE } from 'app/constants';
import { useAsync } from 'app/hooks/useAsync';
import studentImage from 'assets/undraw_exams_g-4-ow.svg';
import { t } from 'locales/i18n';
import { PartnerDashboard } from 'models/partnerDashboard';
import * as React from 'react';
import { requestsColumns } from './column';
import { messages } from './messages';

export function PartnerDashboardPage() {
  const { data, run } = useAsync<Results<PartnerDashboard>>({
    data: {
      codeResults: {
        code: 0,
        message: '',
      },
      item: defaultStat,
    },
  });

  const partnerAdminStat = data?.item ?? defaultStat;
  const isNotAvailable = partnerAdminStat === defaultStat;
  React.useEffect(() => {
    run(dashboardApi.getPartnerDashboard());
  }, [run]);
  return (
    <Container maxWidth={'xl'}>
      <Grid container spacing={3}>
        <Grid item xs={12} md={12}>
          <AppWelcome
            displayName={t(messages.partnerDisplayName())}
            introduction={t(messages.partnerIntroduction())}
            buttonLabel={t(messages.newPost())}
            buttonPath={PATH_NEW_POST_PAGE}
          />
        </Grid>

        <Grid item xs={12} md={8}>
          <AppNewData
            title={t(messages.listRequests())}
            rows={partnerAdminStat.recentApplications}
            columns={requestsColumns}
            buttonPath={PATH_REQUESTS_PAGE}
          />
        </Grid>
        <Grid item xs={12} md={4}>
          <AppTotalSummary
            title={t(messages.totalStudents())}
            total={partnerAdminStat.workingStudentsStat.total}
            srcImage={studentImage}
            color={'info'}
            isNotAvailable={isNotAvailable}
          />
        </Grid>
      </Grid>
    </Container>
  );
}

const defaultStat: PartnerDashboard = {
  workingStudentsStat: {
    total: 0,
  },
  recentApplications: [],
};

/**
 * Asynchronously loads the component for HomePage
 */

import { lazyLoad } from 'utils/loadable';

export const LecturerDashboardPage = lazyLoad(
  () => import('./index'),
  module => module.LecturerDashboardPage,
);

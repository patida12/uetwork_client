import { translations } from 'locales/translations';
import { _t } from 'utils/messages';

export const messages = {
  lecturerDisplayName: () => _t(translations.dashboard.lecturerDisplayName),
  lecturerIntroduction: () => _t(translations.dashboard.lecturerIntroduction),
  totalStudents: () => _t(translations.dashboard.totalStudents),
  listStudents: () => _t(translations.dashboard.listStudents),
  scored: () => _t(translations.dashboard.scored),
  notScored: () => _t(translations.dashboard.notScored),
  summited: () => _t(translations.dashboard.summited),
  notSummited: () => _t(translations.dashboard.notSummited),
  scoreStat: () => _t(translations.dashboard.scoreStat),
  reportStat: () => _t(translations.dashboard.reportStat),
};

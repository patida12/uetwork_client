import { Container, Grid } from '@mui/material';
import dashboardApi from 'api/dashboardApi';
import { Results } from 'api/types';
import AppPieChart from 'app/components/Dashboard/AppPieChart';
import AppWelcome from 'app/components/Dashboard/AppWelcome';
import { PATH_MY_STUDENTS_PAGE } from 'app/constants';
import { useAsync } from 'app/hooks/useAsync';
import { t } from 'locales/i18n';
import { LecturerDashboard } from 'models/lecturerDashboard';
import * as React from 'react';
import { messages } from './messages';

export function LecturerDashboardPage() {
  const { data, run } = useAsync<Results<LecturerDashboard>>({
    data: {
      codeResults: {
        code: 0,
        message: '',
      },
      item: defaultStat,
    },
  });

  const lecturerAdminStat = data?.item ?? defaultStat;
  const isNotAvailable = lecturerAdminStat === defaultStat;
  React.useEffect(() => {
    run(dashboardApi.getLecturerDashboard());
  }, [run]);
  return (
    <Container maxWidth={'xl'}>
      <Grid container spacing={3}>
        <Grid item xs={12} md={12}>
          <AppWelcome
            displayName={t(messages.lecturerDisplayName())}
            introduction={t(messages.lecturerIntroduction())}
            buttonLabel={t(messages.listStudents())}
            buttonPath={PATH_MY_STUDENTS_PAGE}
          />
        </Grid>
        <Grid item xs={12} md={6}>
          <AppPieChart
            title={t(messages.scoreStat())}
            chartData={[
              lecturerAdminStat.scoring.notScored,
              lecturerAdminStat.scoring.scored,
            ]}
            labels={[t(messages.notScored()), t(messages.scored())]}
            isNotAvailable={isNotAvailable}
          />
        </Grid>
        <Grid item xs={12} md={6}>
          <AppPieChart
            title={t(messages.reportStat())}
            chartData={[
              lecturerAdminStat.reporting.notSubmitted,
              lecturerAdminStat.reporting.submitted,
            ]}
            labels={[t(messages.notSummited()), t(messages.summited())]}
            isNotAvailable={isNotAvailable}
          />
        </Grid>
      </Grid>
    </Container>
  );
}

const defaultStat: LecturerDashboard = {
  scoring: {
    scored: 0,
    notScored: 0,
  },
  reporting: {
    submitted: 0,
    notSubmitted: 0,
  },
};

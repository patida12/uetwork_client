import { LoadingButton } from '@mui/lab';
import {
  Card,
  CardActions,
  CardContent,
  Grid,
  styled,
  TextField,
  Typography,
} from '@mui/material';
import { getLecturerInfo, updateLecturerInfo } from 'api/lecturerApi';
import { useFormik } from 'formik';
import { t } from 'locales/i18n';
import { useSnackbar } from 'notistack';
import { useEffect } from 'react';
import * as Yup from 'yup';
import { useAsync } from '../../../../hooks/useAsync';
import { messages } from '../../../../pages/Lecturer/messages';

interface PersonalInfoCardProps {
  className?: string;
}

const PersonalInfoCard: React.FC<PersonalInfoCardProps> = ({
  className,
}): JSX.Element => {
  const { enqueueSnackbar } = useSnackbar();
  const { loading, run } = useAsync();

  const formik = useFormik({
    initialValues: {
      fullName: '',
      personalEmail: '',
      phoneNumber: '',
      orgEmail: '',
    },
    validationSchema: PersonalInfoSchema,
    onSubmit: values => {
      run(
        updateLecturerInfo(values)
          .then(() => {
            enqueueSnackbar(t(messages.saveChangesSuccess()), {
              variant: 'success',
            });
          })
          .catch(e => {
            enqueueSnackbar(t(messages.saveChangesFailed()), {
              variant: 'error',
            });
          }),
      );
    },
  });

  useEffect(() => {
    getLecturerInfo().then(
      ({ orgEmail, phoneNumber, personalEmail, fullName }) => {
        formik.setValues({
          orgEmail,
          phoneNumber,
          personalEmail,
          fullName,
        });
      },
    );
  }, []);

  const lecturerInfo = formik.values;

  return (
    <Card variant="outlined" className={className}>
      <CardContent>
        <StyledCardTitle variant="h2" component="h2">
          {t(messages.personalInfo())}
        </StyledCardTitle>

        <Grid container spacing={2}>
          <Grid item xs={12} md={6}>
            <TextField
              name="fullName"
              value={lecturerInfo.fullName}
              label={t(messages.fullName())}
              fullWidth
              onChange={formik.handleChange}
              error={formik.touched.fullName && Boolean(formik.errors.fullName)}
              helperText={formik.touched.fullName && formik.errors.fullName}
            />
          </Grid>
          <Grid item xs={12} md={6}>
            {/* TODO: Use specialized input for phone number */}
            <TextField
              name="phoneNumber"
              value={lecturerInfo.phoneNumber}
              label={t(messages.phoneNumber())}
              fullWidth
              onChange={formik.handleChange}
              error={
                formik.touched.phoneNumber && Boolean(formik.errors.phoneNumber)
              }
              helperText={
                formik.touched.phoneNumber && formik.errors.phoneNumber
              }
            />
          </Grid>
          <Grid item xs={12} md={6}>
            <TextField
              name="orgEmail"
              value={lecturerInfo.orgEmail}
              disabled
              label={t(messages.orgEmail())}
              fullWidth
              onChange={formik.handleChange}
            />
          </Grid>
          <Grid item xs={12} md={6}>
            <TextField
              name="personalEmail"
              value={lecturerInfo.personalEmail}
              label={t(messages.personalEmail())}
              fullWidth
              onChange={formik.handleChange}
              error={
                formik.touched.personalEmail &&
                Boolean(formik.errors.personalEmail)
              }
              helperText={
                formik.touched.personalEmail && formik.errors.personalEmail
              }
            />
          </Grid>
        </Grid>
      </CardContent>

      <StyledCardActions>
        <LoadingButton
          type="submit"
          variant="contained"
          disabled={Object.keys(formik.errors).length !== 0}
          loading={loading}
          onClick={() => formik.handleSubmit()}
        >
          {t(messages.saveChanges())}
        </LoadingButton>
      </StyledCardActions>
    </Card>
  );
};

const PersonalInfoSchema = Yup.object().shape({
  fullName: Yup.string().required(t(messages.required())),
  personalEmail: Yup.string().email(t(messages.emailNotValidMsg())),
  phoneNumber: Yup.string(),
});

const StyledCardActions = styled(CardActions)(({ theme }) => ({
  padding: theme.spacing(2),
  display: 'flex',
  justifyContent: 'end',
}));

const StyledCardTitle = styled(Typography)(({ theme }) => ({
  marginBottom: theme.spacing(4),
}));

export { PersonalInfoCard };

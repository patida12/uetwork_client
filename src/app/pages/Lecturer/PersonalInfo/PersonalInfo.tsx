import { styled } from '@mui/material';
import { ChangePasswordCard } from 'app/components/ChangePasswordCard/ChangePasswordCard';
import { t } from 'locales/i18n';
import { AppContainer } from '../../../components/AppLayout/AppContainer';
import { messages } from '../../../pages/Lecturer/messages';
import { PersonalInfoCard } from './components/PersonalInfoCard';

const PersonalInfoPage: React.FC = (): JSX.Element => {
  return (
    <StyledAppContainer title={t(messages.editPersonalInfo())}>
      <PersonalInfoCard />
      <ChangePasswordCard />
    </StyledAppContainer>
  );
};

const StyledAppContainer = styled(AppContainer)(({ theme }) => ({
  '& > *': {
    marginBottom: theme.spacing(2),
  },
}));

export { PersonalInfoPage };

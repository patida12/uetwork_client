import { AccountCircleOutlined, HomeOutlined } from '@mui/icons-material';
import { t } from 'locales/i18n';
import { RouteDef } from 'models';
import { LecturerDashboardPage } from './DashboardPage/Loadable';
import { messages } from './messages';
import { MyStudentsPage } from './MyStudentsPage/Loadable';
import { PersonalInfoPage } from './PersonalInfo/PersonalInfo';

const routes: RouteDef[] = [
  {
    name: t(messages.home()),
    exact: true,
    path: '/home',
    component: LecturerDashboardPage,
    icon: HomeOutlined,
  },
  {
    name: t(messages.myStudents()),
    exact: true,
    path: '/my-students',
    component: MyStudentsPage,
    icon: AccountCircleOutlined,
  },
  {
    name: t(messages.editPersonalInfo()),
    exact: true,
    path: '/account',
    component: PersonalInfoPage,
    icon: AccountCircleOutlined,
    hide: true,
  },
];

export { routes };

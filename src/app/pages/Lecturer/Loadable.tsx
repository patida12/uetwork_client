import { lazyLoad } from 'utils/loadable';
import { LoadingPage } from '../../pages/Common/LoadingPage/LoadingPage';

export const LecturerApp = lazyLoad(
  () => import('./index'),
  module => module.LecturerApp,
  {
    fallback: <LoadingPage />,
  },
);

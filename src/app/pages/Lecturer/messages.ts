import { translations } from 'locales/translations';
import { _t } from 'utils/messages';

export const messages = {
  home: () => _t(translations.common.app.home),
  myStudents: () => _t(translations.lecturerFeature.myStudents),
  personalInfo: () => _t(translations.accountFeature.personalInfo),
  editPersonalInfo: () => _t(translations.accountFeature.editPersonalInfo),

  fullName: () => _t(translations.lecturerFeature.fullName),
  orgEmail: () => _t(translations.lecturerFeature.orgEmail),
  personalEmail: () => _t(translations.lecturerFeature.personalEmail),
  phoneNumber: () => _t(translations.lecturerFeature.phoneNumber),
  saveChanges: () => _t(translations.common.saveChanges),
  saveChangesSuccess: () => _t(translations.common.saveChangesSuccess),
  saveChangesFailed: () => _t(translations.common.saveChangesFailed),
  emailNotValidMsg: () => _t(translations.common.validate.invalidEmail),
  phoneNumberNotValidMsg: () =>
    _t(translations.common.validate.phoneNumberNotValidMsg),
  required: () => _t(translations.common.validate.required),
  passwordDoNotMatchMsg: () =>
    _t(translations.common.validate.passwordDoNotMatchMsg),

  changePassword: () => _t(translations.authFeature.changePassword),
  currentPassword: () => _t(translations.authFeature.currentPassword),
  newPassword: () => _t(translations.authFeature.newPassword),
  retypeNewPassword: () => _t(translations.authFeature.retypeNewPassword),
};

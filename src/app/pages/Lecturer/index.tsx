import { Box } from '@mui/material';
import { styled } from '@mui/styles';
import { AppContent } from 'app/components/AppLayout/AppContent';
import { AppHeader } from 'app/components/AppLayout/AppHeader';
import { AppSider } from 'app/components/AppLayout/AppSider';
import { routes } from './nav';

export const LecturerApp: React.FC = (): JSX.Element => {
  return (
    <StyledBox>
      <AppHeader />
      <AppSider routes={routes} open={true}></AppSider>
      <AppContent routes={routes} />
    </StyledBox>
  );
};

const StyledBox = styled(Box)({
  display: 'flex',
});

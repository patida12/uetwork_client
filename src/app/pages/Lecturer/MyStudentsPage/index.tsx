import { styled } from '@mui/styles';
import { GridSortModel } from '@mui/x-data-grid';
import {
  getStudents,
  GetStudentsFilterQuery,
  GetStudentsQuery,
} from 'api/lecturerApi';
import { AppContainer } from 'app/components/AppLayout/AppContainer';
import { useAsync } from 'app/hooks/useAsync';
import { messages } from 'app/pages/Lecturer/messages';
import { t } from 'locales/i18n';
import { FilterState, PaginatedResultsMeta, TermStudent } from 'models';
import { useEffect } from 'react';
import {
  NumberParam,
  ObjectParam,
  StringParam,
  useQueryParams,
  withDefault,
} from 'use-query-params';
import {
  isZeroValue,
  safeParseBool,
  safeParseInt,
  sortModelToQuery,
} from 'utils';
import { AppDataGrid } from '../../../components/AppDataGrid/AppDataGrid';
import { columns } from './columns';
import { CustomToolbar } from './components/CustomToolbar';

const DEFAULT_PAGE = 1;
const DEFAULT_PER_PAGE = 50;

const MyStudentsPage: React.FC = (): JSX.Element => {
  const { loading, data, run } = useAsync<{
    students: TermStudent.TermStudent[];
    meta: PaginatedResultsMeta;
  }>({
    data: {
      students: [],
      meta: {
        currentPage: 0,
        perPage: 0,
        totalItems: 0,
        totalPages: 0,
      },
    },
  });

  const [query, setQuery] = useQueryParams({
    page: withDefault(NumberParam, DEFAULT_PAGE),
    perPage: withDefault(NumberParam, DEFAULT_PER_PAGE),
    q: withDefault(StringParam, ''),
    sort: withDefault(ObjectParam, {}),
    filters: withDefault(ObjectParam, {}),
  });

  const { page, perPage, q, filters, sort } = query;

  const onPageChange = (page: number) => {
    setQuery({
      page,
    });
  };

  const onPageSizeChange = (pageSize: number) => {
    setQuery({
      perPage: pageSize,
      page: DEFAULT_PAGE,
    });
  };

  const onSortModelChange = (model: GridSortModel) => {
    if (model.length === 0) {
      setQuery({
        sort: {},
      });
      return;
    }
    const firstElem = model[0];

    setQuery({
      sort: {
        ...sort,
        [firstElem.field]: [firstElem.sort],
      },
    });
  };

  const onFilterChange = (value: FilterState) => {
    setQuery({
      q: value.query,
      filters: value.filter,
    });
  };

  useEffect(() => {
    run(getStudents(getQueryObject(query)));
  }, [run, query]);

  const { students: rows, meta } = data!;

  const initialFilters: FilterState = {
    query: q,
    filter: {
      reportSubmitted: filters['reportSubmitted'] ?? '',
      schoolClassId: filters['schoolClassId'] ?? '',
      scoreGiven: filters['scoreGiven'] ?? '',
      termId: filters['termId'] ?? '',
    },
  };

  return (
    <AppContainer title={t(messages.myStudents())}>
      <StyledDiv>
        <AppDataGrid
          rows={rows ?? []}
          columns={columns}
          loading={loading}
          page={page}
          pageSize={perPage}
          rowCount={meta.totalItems}
          onPageChange={onPageChange}
          onPageSizeChange={onPageSizeChange}
          onSortModelChange={onSortModelChange}
          getRowId={row => row.termId.toString() + ':' + row.id.toString()}
          components={{ Toolbar: CustomToolbar }}
          componentsProps={{
            toolbar: {
              onFilterChange,
              initialFilters,
            },
          }}
        />
      </StyledDiv>
    </AppContainer>
  );
};

const getQueryObject = (qObj: any): GetStudentsQuery => {
  const { page, perPage, sort, q, filters } = qObj;
  const gsq: GetStudentsQuery = {
    page,
    perPage,
    q,
  };

  const gsqSort: string[] = sortModelToQuery(sort);

  const gsqFilters: GetStudentsFilterQuery = {};
  const { reportSubmitted, scoreGiven, termId, schoolClassId } = filters;
  if (!isZeroValue(reportSubmitted)) {
    gsqFilters.reportSubmitted = safeParseBool(reportSubmitted);
  }
  if (!isZeroValue(scoreGiven)) {
    gsqFilters.scoreGiven = safeParseBool(scoreGiven);
  }
  if (!isZeroValue(termId)) {
    gsqFilters.termId = safeParseInt(termId);
  }
  if (!isZeroValue(schoolClassId)) {
    gsqFilters.schoolClassId = safeParseInt(schoolClassId);
  }

  gsq.sort = gsqSort;
  gsq.filter = gsqFilters;

  return gsq;
};

const StyledDiv = styled('div')(({ theme }: any) => {
  return {
    height: '100%',
    width: '100%',
  };
});
export { MyStudentsPage };

import { lazyLoad } from 'utils/loadable';

export const MyStudentsPage = lazyLoad(
  () => import('./index'),
  module => module.MyStudentsPage,
);

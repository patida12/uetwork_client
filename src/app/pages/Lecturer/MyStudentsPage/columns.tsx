import { Link } from '@mui/material';
import {
  GridCellParams,
  GridColDef,
  GridValueGetterParams,
} from '@mui/x-data-grid';
import { t } from 'locales/i18n';
import { formatScore } from 'utils';
import { downloadStudentReport } from 'api/lecturerApi';
import { AddScoreDialog } from './components/AddScoreDialog';
import { messages } from './messages';

const columns: GridColDef[] = [
  {
    field: 'id',
    sortable: false,
    hide: true,
  },
  {
    field: 'fullName',
    headerName: t(messages.name()),
    width: 200,
  },
  {
    field: 'studentIdNumber',
    headerName: t(messages.studentIdNumber()),
    sortable: false,
    width: 150,
  },
  {
    field: 'orgEmail',
    headerName: t(messages.orgEmail()),
    sortable: false,
    width: 200,
  },
  {
    field: 'personalEmail',
    headerName: t(messages.personalEmail()),
    sortable: false,
    width: 200,
  },
  {
    field: 'schoolClassName',
    headerName: t(messages.schoolClassName()),
    valueGetter: (params: GridValueGetterParams) => {
      return params.row.schoolClass?.name ?? '';
    },
    sortable: false,
    width: 200,
  },
  {
    field: 'selectedPartnerName',
    headerName: t(messages.selectedPartnerName()),
    valueGetter: (params: GridValueGetterParams) => {
      return params.row.selectedPartner?.name ?? '';
    },
    sortable: false,
    width: 200,
  },
  {
    field: 'reportDownloadLink',
    headerName: t(messages.finalReport()),
    renderCell: (params: GridCellParams) => {
      if (!params.row.reportSubmitted) {
        return t(messages.notPresent());
      }

      const studentId = params.row.id;
      const termId = params.row.termId;

      const onClick = () => {
        downloadStudentReport(termId, studentId).then(url => {
          window.open(url, '_blank');
        });
      };

      return (
        <Link href="#" onClick={onClick}>
          {t(messages.download())}
        </Link>
      );
    },
    sortable: false,
    width: 150,
  },
  {
    field: 'score',
    headerName: t(messages.score()),
    renderCell: (params: GridCellParams) => {
      if (params.row.score == null) {
        return t(messages.notScored());
      }
      return formatScore(params.row.score);
    },
    sortable: false,
  },
  {
    field: 'options',
    headerName: t(messages.option()),
    renderCell: (params: GridCellParams) => {
      const { id, termId, score } = params.row;
      return <AddScoreDialog studentId={id} termId={termId} score={score} />;
    },
    sortable: false,
    width: 150,
  },
];

export { columns };

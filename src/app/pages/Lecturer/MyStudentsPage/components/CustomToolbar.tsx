import { Button, MenuItem, TextField } from '@mui/material';
import { styled } from '@mui/styles';
import { GridToolbarContainer } from '@mui/x-data-grid';
import { getTerms } from 'api/commonApi';
import { useAsync } from 'app/hooks/useAsync';
import { t } from 'locales/i18n';
import { FilterState, TermLittleInfo } from 'models';
import { useEffect, useState } from 'react';
import { messages } from '../messages';

interface CustomToolbarProps {
  onFilterChange: (value: FilterState) => void;
  initialFilters?: FilterState;
}

const CustomToolbar: React.FC<CustomToolbarProps> = ({
  onFilterChange,
  initialFilters,
}): JSX.Element => {
  let initialValue: FilterState = {
    query: '',
    filter: {},
  };

  if (initialFilters != null) {
    initialValue.query = initialFilters.query ?? '';
    initialValue.filter = initialFilters.filter ?? {};
  }

  const [query, setQuery] = useState<FilterState>(initialValue);

  const { data: terms, run: runTerms } = useAsync<TermLittleInfo[]>({
    data: [],
  });

  useEffect(() => {
    runTerms(getTerms());
  }, [runTerms]);

  const handleQueryChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setQuery(prev => ({
      ...prev,
      query: e.target.value,
    }));
  };

  const handleTermIdChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setQuery(prev => ({
      ...prev,
      filter: {
        ...prev.filter,
        termId: e.target.value,
      },
    }));
  };

  const handleReportStatusChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setQuery(prev => ({
      ...prev,
      filter: {
        ...prev.filter,
        reportSubmitted: e.target.value,
      },
    }));
  };

  const handleScoreStatusChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setQuery(prev => ({
      ...prev,
      filter: {
        ...prev.filter,
        scoreGiven: e.target.value,
      },
    }));
  };

  const onSearch = () => {
    onFilterChange(query);
  };

  return (
    <StyledGridToolbarContainer>
      <Wrapper>
        <StyledTextField
          value={query.query}
          onChange={handleQueryChange}
          size="small"
          label={t(messages.search())}
          placeholder={t(messages.searchPlaceholder())}
        ></StyledTextField>
      </Wrapper>
      <SecondWrapper>
        <TextField
          select
          label={t(messages.term())}
          placeholder={t(messages.selectTermPlaceholder())}
          size="small"
          value={query.filter['termId']}
          onChange={handleTermIdChange}
        >
          <MenuItem value="">{t(messages.all())}</MenuItem>
          {terms!.map(term => (
            <MenuItem key={term.id} value={term.id.toString()}>
              {t(messages.termYear(), { term: term.term, year: term.year })}
            </MenuItem>
          ))}
        </TextField>
        {/* <TextField */}
        {/*   label={t(messages.schoolClass())} */}
        {/*   placeholder={t(messages.selectSchoolClassPlaceholder())} */}
        {/*   size="small" */}
        {/*   value={query.filter['schoolClassId']} */}
        {/*   onChange={handleClassIdChange} */}
        {/*   select */}
        {/* > */}
        {/*   <MenuItem value="">{t(messages.all())}</MenuItem> */}
        {/*   {schoolClasses!.map(t => ( */}
        {/*     <MenuItem key={t.id} value={t.id.toString()}> */}
        {/*       {t.name} */}
        {/*     </MenuItem> */}
        {/*   ))} */}
        {/* </TextField> */}
        <TextField
          select
          label={t(messages.reportStatus())}
          size="small"
          onChange={handleReportStatusChange}
          value={query.filter['reportSubmitted']}
        >
          <MenuItem value="">{t(messages.all())}</MenuItem>
          <MenuItem value={'true'}>{t(messages.reportSubmitted())}</MenuItem>
          <MenuItem value={'false'}>
            {t(messages.reportNotSubmitted())}
          </MenuItem>
        </TextField>
        <TextField
          select
          label={t(messages.scoreStatus())}
          size="small"
          value={query.filter['scoreGiven']}
          onChange={handleScoreStatusChange}
        >
          <MenuItem value="">{t(messages.all())}</MenuItem>
          <MenuItem value={'true'}>{t(messages.scoreGiven())}</MenuItem>
          <MenuItem value={'false'}>{t(messages.scoreNotGiven())}</MenuItem>
        </TextField>
        <StyledButton onClick={onSearch} variant="contained">
          {t(messages.search())}
        </StyledButton>
      </SecondWrapper>
    </StyledGridToolbarContainer>
  );
};

const Wrapper = styled('div')({
  width: '100%',
  margin: '4px',
});

const SecondWrapper = styled(Wrapper)({
  display: 'flex',
  flexDirection: 'row',
  '& > *': {
    marginRight: '8px',
    width: '200px',
    flexShrink: 0,
  },
});

const StyledButton = styled(Button)({
  marginRight: 0,
  width: '150px',
  flexShrink: 0,
});

const StyledTextField = styled(TextField)({
  width: '100%',
});

const StyledGridToolbarContainer = styled(GridToolbarContainer)({
  padding: '12px 16px',
  display: 'flex',
  flexDirection: 'column',
});

export { CustomToolbar };
export type { CustomToolbarProps };

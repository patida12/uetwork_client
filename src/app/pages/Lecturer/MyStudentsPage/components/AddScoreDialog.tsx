import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Link,
  TextField,
} from '@mui/material';
import { useFormik } from 'formik';
import { t } from 'locales/i18n';
import { useState } from 'react';
import { messages } from '../messages';
import * as Yup from 'yup';
import { LoadingButton } from '@mui/lab';
import { assignScore } from 'api/lecturerApi';
import { useAsync } from 'app/hooks/useAsync';
import { useHistory } from 'react-router-dom';

interface AddScoreDialogProps {
  studentId: number;
  termId: number;
  score?: number;
}

const AddScoreDialog: React.FC<AddScoreDialogProps> = ({
  studentId,
  termId,
  score,
}): JSX.Element => {
  const history = useHistory();

  const [open, setOpen] = useState(false);
  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const { loading, run } = useAsync();

  const formik = useFormik({
    initialValues: {
      score: score ?? 0,
    },
    onSubmit: ({ score }) => {
      run(
        assignScore({ score, studentId, termId }).then(() => {
          handleClose();
          // TODO: we should figure out how to refresh data from here. Maybe use context?
          history.go(0);
        }),
      );
    },
    validationSchema: Yup.object().shape({
      score: Yup.number()
        .required(t(messages.required()))
        .min(0, t(messages.scoreNotValidMsg()))
        .max(10, t(messages.scoreNotValidMsg())),
    }),
  });

  return (
    <>
      <Link href="#" underline="hover" onClick={handleClickOpen}>
        {t(messages.addScore())}
      </Link>
      <Dialog open={open} onClose={handleClose}>
        <DialogTitle id="partner-contact-dialog-title">
          {t(messages.addScore())}
        </DialogTitle>
        <DialogContent>
          <TextField
            name="score"
            fullWidth
            autoFocus
            margin="normal"
            label={t(messages.score())}
            type="number"
            value={formik.values.score}
            onChange={formik.handleChange}
            error={formik.touched.score && Boolean(formik.errors.score)}
            helperText={formik.touched.score && formik.errors.score}
          />
        </DialogContent>
        <DialogActions sx={{ p: 3 }}>
          <Button onClick={handleClose}>{t(messages.cancel())}</Button>
          <LoadingButton
            onClick={() => formik.handleSubmit()}
            type="submit"
            variant="contained"
            loading={loading}
          >
            {t(messages.confirm())}
          </LoadingButton>
        </DialogActions>
      </Dialog>
    </>
  );
};

export { AddScoreDialog };
export type { AddScoreDialogProps };

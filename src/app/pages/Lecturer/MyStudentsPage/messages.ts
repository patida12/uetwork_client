import { translations } from 'locales/translations';
import { _t } from 'utils/messages';

export const messages = {
  name: () => _t(translations.lecturerFeature.fullName),
  studentIdNumber: () => _t(translations.lecturerFeature.studentIdNumber),
  orgEmail: () => _t(translations.lecturerFeature.orgEmail),
  personalEmail: () => _t(translations.lecturerFeature.personalEmail),
  phoneNumber: () => _t(translations.lecturerFeature.phoneNumber),
  schoolClassName: () => _t(translations.lecturerFeature.schoolClassName),
  selectedPartnerName: () =>
    _t(translations.lecturerFeature.selectedPartnerName),
  finalReport: () => _t(translations.lecturerFeature.finalReport),
  score: () => _t(translations.lecturerFeature.score),
  download: () => _t(translations.common.download),
  notPresent: () => _t(translations.common.notPresent),
  addScore: () => _t(translations.lecturerFeature.addScore),
  notScored: () => _t(translations.lecturerFeature.notScored),
  option: () => _t(translations.common.table.option),
  search: () => _t(translations.common.table.search),
  searchPlaceholder: () => _t(translations.lecturerFeature.searchPlaceholder),
  term: () => _t(translations.termFeature.term),
  selectTermPlaceholder: () =>
    _t(translations.lecturerFeature.selectTermPlaceholder),
  schoolClass: () => _t(translations.studentFeature.schoolClass),
  selectSchoolClassPlaceholder: () =>
    _t(translations.lecturerFeature.selectSchoolClassPlaceholder),
  reportStatus: () => _t(translations.lecturerFeature.reportStatus),
  reportSubmitted: () => _t(translations.lecturerFeature.reportSubmitted),
  reportNotSubmitted: () => _t(translations.lecturerFeature.reportNotSubmitted),
  scoreStatus: () => _t(translations.lecturerFeature.scoreStatus),
  scoreGiven: () => _t(translations.lecturerFeature.scoreGiven),
  scoreNotGiven: () => _t(translations.lecturerFeature.scoreNotGiven),
  all: () => _t(translations.common.all),

  termYear: () => _t(translations.internship.termYear),

  // My students page - add score dialog
  required: () => _t(translations.common.validate.required),
  scoreNotValidMsg: () => _t(translations.lecturerFeature.scoreNotValidMsg),
  cancel: () => _t(translations.common.button.cancel),
  confirm: () => _t(translations.common.button.confirm),
};

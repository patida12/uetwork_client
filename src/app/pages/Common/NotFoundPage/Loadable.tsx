/**
 * Asynchronously loads the component for NotFoundPage
 */

import LazyLoading from 'app/components/LazyLoading/lazyLoading.component';
import { lazyLoad } from 'utils/loadable';

export const NotFoundPage = lazyLoad(
  () => import('./index'),
  module => module.NotFoundPage,
  {
    fallback: <LazyLoading />,
  },
);

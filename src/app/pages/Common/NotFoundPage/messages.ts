import { translations } from 'locales/translations';
import { _t } from 'utils/messages';

export const messages = {
  areYouLost: () => _t(translations.notFoundFeature.areYouLost),
  takeMeHome: () => _t(translations.notFoundFeature.takeMeHome),
  notFound: () => _t(translations.notFoundFeature.notFound),
  notFoundMsg: () => _t(translations.notFoundFeature.notFoundMsg),
};

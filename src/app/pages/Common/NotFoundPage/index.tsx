import { Box, Button, Typography } from '@mui/material';
import * as React from 'react';
import { Helmet } from 'react-helmet-async';
import styled from 'styled-components/macro';
import { t } from 'locales/i18n';
import { messages } from './messages';
import pageNotFoundIllustration from 'assets/undraw_page_not_found.svg';
import { Link as RouterLink } from 'react-router-dom';

export function NotFoundPage() {
  return (
    <>
      <Helmet>
        <title>{t(messages.notFound())}</title>
        <meta name="description" content="Page not found" />
      </Helmet>
      <Wrapper>
        <Typography variant="h3" gutterBottom>
          {t(messages.areYouLost())}
        </Typography>
        <Typography color="text.secondary">
          {t(messages.notFoundMsg())}
        </Typography>
        <Box sx={{ height: 260, my: { x: 5, sm: 10 } }}>
          <img
            src={pageNotFoundIllustration}
            alt="page not found illustration"
            height="100%"
          />
        </Box>
        <Button component={RouterLink} to="/home" variant="contained">
          {t(messages.takeMeHome())}
        </Button>
      </Wrapper>
    </>
  );
}

const Wrapper = styled.div`
  height: 80vh;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  min-height: 320px;
`;

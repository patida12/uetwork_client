import {
  Grid,
  Box,
  TextField,
  Theme,
  FormHelperText,
  Divider,
} from '@mui/material';
import { AppLogo } from 'app/components/AppLogo';
import { styled } from '@mui/styles';
import workingImage from 'assets/undraw_remotely.svg';
import { t } from 'locales/i18n';
import { messages } from '../messages';
import { Link, useHistory } from 'react-router-dom';
import googleIcon from 'assets/google.svg';
import { useAsync } from '../../../hooks/useAsync';
import { login, loginWithGoogle } from 'api/authApi';
import { LoadingButton } from '@mui/lab';
import { useContext } from 'react';
import { UserContext } from 'contexts/UserContext';
import { useSnackbar } from 'notistack';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { Helmet } from 'react-helmet-async';
import { PATH_AUTH } from 'app/constants/path.constants';
import { GoogleLogin, GoogleLoginResponse } from 'react-google-login';

export const LoginPage: React.FC = (): JSX.Element => {
  const history = useHistory();
  const { run: runLoginEmail, loading: loadingLoginEmail } = useAsync();
  const { run: runLoginGoogle, loading: loadingLoginGoogle } = useAsync();

  const { enqueueSnackbar } = useSnackbar();

  const { setCurrentUser } = useContext(UserContext);

  const formik = useFormik({
    initialValues: {
      email: '',
      password: '',
    },
    onSubmit: values => {
      runLoginEmail(
        login(values)
          .then(res => {
            setCurrentUser(res.user);
            history.push('/home');
          })
          .catch(e => {
            enqueueSnackbar(e.response?.data.message, { variant: 'error' });
          }),
      );
    },
    validationSchema: Yup.object().shape({
      email: Yup.string()
        .email(t(messages.emailNotValidMsg()))
        .required(t(messages.required())),
      password: Yup.string().required(t(messages.required())),
    }),
  });

  const onLoginWithGoogle = (res: GoogleLoginResponse) => {
    runLoginGoogle(
      loginWithGoogle(res.tokenId)
        .then(res => {
          setCurrentUser(res.user);
          history.push('/home');
        })
        .catch(e => {
          enqueueSnackbar(e.response?.data.message, { variant: 'error' });
        }),
    );
  };

  const { email, password } = formik.values;

  return (
    <>
      <Helmet>
        <title>{t(messages.login())}</title>
      </Helmet>
      <Wrapper container>
        <StyledHugeImage item xs={0} sm={0} md={6}>
          <Box
            sx={{
              display: { xs: 'none', md: 'flex' },
              justifyContent: 'center',
            }}
          >
            <Illustration src={workingImage} alt="working illustration" />
          </Box>
        </StyledHugeImage>
        <FormWrapper item xs={12} sm={12} md={6}>
          <FormContainer>
            <AppLogo />
            <Box
              sx={{ mt: 1 }}
              component="form"
              onSubmit={e => {
                e.preventDefault();
                formik.handleSubmit();
              }}
            >
              <TextField
                margin="normal"
                required
                fullWidth
                id="email"
                label={t(messages.email())}
                name="email"
                autoComplete="email"
                autoFocus
                placeholder="18020117@vnu.edu.vn"
                error={formik.touched.email && Boolean(formik.errors.email)}
                helperText={formik.touched.email && formik.errors.email}
                value={email}
                onChange={formik.handleChange}
              />
              <TextField
                margin="normal"
                required
                fullWidth
                name="password"
                label={t(messages.password())}
                type="password"
                id="password"
                autoComplete="current-password"
                error={
                  formik.touched.password && Boolean(formik.errors.password)
                }
                helperText={formik.touched.password && formik.errors.password}
                value={password}
                onChange={formik.handleChange}
              />
              <HelperTextWrapper>
                <FormHelperText>
                  {t(messages.notHaveAccountMsg())}
                  <Link to={PATH_AUTH.register} className="link">
                    {t(messages.register())}
                  </Link>
                </FormHelperText>
                <FormHelperText>
                  <Link to={PATH_AUTH.resetPassword} className="link">
                    {t(messages.forgotPasswordMsg())}
                  </Link>
                </FormHelperText>
              </HelperTextWrapper>
              <StyledButton
                type="submit"
                fullWidth
                variant="contained"
                size="large"
                loading={loadingLoginEmail}
                disabled={loadingLoginGoogle}
              >
                {t(messages.login())}
              </StyledButton>
              <Divider />
              <GoogleLogin
                clientId={process.env.REACT_APP_GOOGLE_CLIENT_ID || ''}
                onSuccess={onLoginWithGoogle as any} // ignore GoogleLoginResponseOffline
                responseType="id_token"
                disabled={loadingLoginEmail}
                render={({ onClick, disabled }) => {
                  return (
                    <StyledButton
                      fullWidth
                      variant="outlined"
                      size="large"
                      onClick={onClick}
                      disabled={disabled}
                      loading={loadingLoginGoogle}
                    >
                      <img
                        src={googleIcon}
                        alt="google icon"
                        className="icon"
                      />
                      {t(messages.loginWithGoogle())}
                    </StyledButton>
                  );
                }}
              />
            </Box>
          </FormContainer>
        </FormWrapper>
      </Wrapper>
    </>
  );
};

const StyledButton = styled(LoadingButton)({
  marginTop: '15px',
  marginBottom: '15px',
  '& .icon': {
    height: '23px',
    marginRight: '4px',
  },
});

const Wrapper = styled(Grid)({
  height: '100vh',
});

const FormWrapper = styled(Grid)({
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
});

const FormContainer = styled(Box)({
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  padding: 16,
  maxWidth: '500px',
  marginBottom: '100px',
});

const Illustration = styled('img')({
  width: '80%',
});

const HelperTextWrapper = styled('div')(({ theme }) => ({
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'space-between',
  '& .link': {
    textDecoration: 'none',
    marginLeft: 4,
    fontWeight: 500,
    color: theme.palette.primary.main,
  },
}));

const StyledHugeImage = styled(Grid)(({ theme }: { theme: Theme }) => {
  return {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(65, 85, 233, 0.05)',
  };
});

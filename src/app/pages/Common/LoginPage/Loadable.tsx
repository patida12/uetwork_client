import { lazyLoad } from 'utils/loadable';
import { LoadingPage } from '../LoadingPage/LoadingPage';

export const LoginPage = lazyLoad(
  () => import('./LoginPage'),
  module => module.LoginPage,
  {
    fallback: <LoadingPage />,
  },
);

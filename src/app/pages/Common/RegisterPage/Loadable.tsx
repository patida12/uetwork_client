import { lazyLoad } from 'utils/loadable';
import { LoadingPage } from '../LoadingPage/LoadingPage';

export const RegisterPage = lazyLoad(
  () => import('./RegisterPage'),
  module => module.RegisterPage,
  {
    fallback: <LoadingPage />,
  },
);

import {
  Grid,
  Box,
  TextField,
  Theme,
  FormHelperText,
  MenuItem,
  InputAdornment,
  Typography,
} from '@mui/material';
import { styled } from '@mui/styles';
import workingImage from 'assets/undraw_remotely.svg';
import { t } from 'locales/i18n';
import { messages } from '../messages';
import { Link, useHistory } from 'react-router-dom';
import { LoadingButton } from '@mui/lab';
import { useSnackbar } from 'notistack';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { useAsync } from 'app/hooks/useAsync';
import { getOrgs } from 'api/mocks/authApi';
import { OrganizationInfo } from 'models/organization';
import { useEffect } from 'react';
import { register } from 'api/authApi';

const VNU_EMAIL_DOMAIN = '@vnu.edu.vn';
const STUDENT_ID_NUMBER_LENGTH = 8;

export const RegisterPage: React.FC = (): JSX.Element => {
  const history = useHistory();
  const { run: runLoadOrgs, data: orgs } = useAsync<OrganizationInfo[]>({
    data: [],
  });
  const { run, loading } = useAsync();
  const { enqueueSnackbar } = useSnackbar();

  const formik = useFormik({
    initialValues: {
      email: '',
      fullName: '',
      organizationId: 0,
    },
    onSubmit: async values => {
      run(
        register({
          ...values,
          email: email + VNU_EMAIL_DOMAIN,
        })
          .then(() => {
            enqueueSnackbar(t(messages.registerSuccess()), {
              variant: 'success',
            });
            history.push('/login');
          })
          .catch(e => {
            enqueueSnackbar(t(messages.registerFailed()), {
              variant: 'error',
            });
          }),
      );
    },
    validationSchema: Yup.object().shape({
      email: Yup.string()
        .transform(value => value + VNU_EMAIL_DOMAIN)
        .length(
          STUDENT_ID_NUMBER_LENGTH + VNU_EMAIL_DOMAIN.length,
          t(messages.emailNotValidMsg()),
        ) // limit first part of VNU email (student id number) to 8 characters.
        .email(t(messages.emailNotValidMsg()))
        .required(t(messages.required())),
      fullName: Yup.string().required(t(messages.required())),
      organizationId: Yup.number().min(1, t(messages.required())),
    }),
  });

  useEffect(() => {
    runLoadOrgs(getOrgs());
  }, [runLoadOrgs]);

  const { email, fullName, organizationId } = formik.values;

  return (
    <Wrapper container>
      <StyledHugeImage item xs={0} sm={0} md={6}>
        <Box
          sx={{ display: { xs: 'none', md: 'flex' }, justifyContent: 'center' }}
        >
          <Illustration src={workingImage} alt="working illustration" />
        </Box>
      </StyledHugeImage>
      <FormWrapper item xs={12} sm={12} md={6}>
        <FormContainer>
          <Typography variant="h2" component="h2" sx={{ mb: 1 }}>
            {t(messages.createStudentAccount())}
          </Typography>
          <Box sx={{ mt: 1 }}>
            <TextField
              margin="normal"
              required
              fullWidth
              id="email"
              label={t(messages.orgEmail())}
              name="email"
              autoFocus
              placeholder="18020117"
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    {VNU_EMAIL_DOMAIN}
                  </InputAdornment>
                ),
              }}
              error={formik.touched.email && Boolean(formik.errors.email)}
              helperText={formik.touched.email && formik.errors.email}
              value={email}
              onChange={formik.handleChange}
            />
            <TextField
              margin="normal"
              required
              fullWidth
              name="fullName"
              placeholder="Lê Sinh Viên"
              label={t(messages.fullName())}
              error={formik.touched.fullName && Boolean(formik.errors.fullName)}
              helperText={formik.touched.fullName && formik.errors.fullName}
              value={fullName}
              onChange={formik.handleChange}
            />
            <TextField
              margin="normal"
              required
              select
              fullWidth
              name="organizationId"
              label={t(messages.organization())}
              error={
                formik.touched.organizationId &&
                Boolean(formik.errors.organizationId)
              }
              helperText={
                formik.touched.organizationId && formik.errors.organizationId
              }
              value={organizationId}
              onChange={formik.handleChange}
            >
              <MenuItem value={0}>
                <em>{t(messages.noneChosen())}</em>
              </MenuItem>
              {orgs!.map(o => (
                <MenuItem key={o.id} value={o.id}>
                  {o.name}
                </MenuItem>
              ))}
            </TextField>
            <HelperTextWrapper>
              <FormHelperText>
                {t(messages.alreadyHaveAccountMsg())}
                <Link to="/login" className="link">
                  {t(messages.login())}
                </Link>
              </FormHelperText>
            </HelperTextWrapper>
            <StyledButton
              type="submit"
              fullWidth
              variant="contained"
              size="large"
              loading={loading}
              onClick={() => formik.handleSubmit()}
            >
              {t(messages.register())}
            </StyledButton>
            <FormHelperText>{t(messages.registerHelperText())}</FormHelperText>
          </Box>
        </FormContainer>
      </FormWrapper>
    </Wrapper>
  );
};

const StyledButton = styled(LoadingButton)({
  marginTop: '15px',
  marginBottom: '15px',
  '& .icon': {
    height: '23px',
    marginRight: '4px',
  },
});

const Wrapper = styled(Grid)({
  height: '100vh',
});

const FormWrapper = styled(Grid)({
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
});

const FormContainer = styled(Box)({
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  padding: 16,
  maxWidth: '500px',
  marginBottom: '100px',
});

const Illustration = styled('img')({
  width: '80%',
});

const HelperTextWrapper = styled('div')(({ theme }) => ({
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'space-between',
  '& .link': {
    textDecoration: 'none',
    marginLeft: 4,
    fontWeight: 500,
    color: theme.palette.primary.main,
  },
}));

const StyledHugeImage = styled(Grid)(({ theme }: { theme: Theme }) => {
  return {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(65, 85, 233, 0.05)',
  };
});

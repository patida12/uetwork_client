import { Helmet } from 'react-helmet-async';
import { t } from 'locales/i18n';
import { messages } from '../messages';
import {
  Box,
  Button,
  Container,
  Stack,
  styled,
  TextField,
  Typography,
} from '@mui/material';
import { Link as RouterLink, useHistory } from 'react-router-dom';
import { PATH_AUTH } from 'app/constants/path.constants';
import { useSnackbar } from 'notistack';
import { LoadingButton } from '@mui/lab';
import * as Yup from 'yup';
import { useQuery } from '../../../hooks/useQuery';
import { useAsync } from '../../../hooks/useAsync';
import { completeResetPassword } from '../../../../api/authApi';
import { useFormik } from 'formik';

const CompleteResetPasswordPage: React.FC = (): JSX.Element => {
  const { run, loading } = useAsync();
  const { enqueueSnackbar } = useSnackbar();
  const query = useQuery();
  const history = useHistory();

  const resetToken = query.get('resetToken');
  const requestId = query.get('requestId');

  const formik = useFormik({
    initialValues: {
      resetToken: resetToken ?? '',
      requestId: parseInt(requestId ?? '0') ?? 0,
      newPassword: '',
      retypeNewPassword: '',
    },
    onSubmit: ({ resetToken, requestId, newPassword }) => {
      run(
        completeResetPassword(resetToken, requestId, newPassword)
          .then(() => {
            enqueueSnackbar(t(messages.resetPasswordSuccess()), {
              variant: 'success',
            });
            history.push(PATH_AUTH.login);
          })
          .catch(e => {
            enqueueSnackbar(e.response?.data.message, { variant: 'error' });
          }),
      );
    },
    validationSchema: ResetPasswordSchema,
  });

  const { newPassword, retypeNewPassword } = formik.values;

  return (
    <>
      <Helmet>
        <title>{t(messages.resetPassword())}</title>
      </Helmet>
      <Wrapper>
        <Container>
          <Box sx={{ maxWidth: 480, mx: 'auto' }}>
            <Typography variant="h3" paragraph>
              {t(messages.resetPassword())}
            </Typography>
            <Typography sx={{ color: 'text.secondary', mb: 5 }}>
              {t(messages.completeResetPasswordInstructionMsg())}
            </Typography>

            <Stack spacing={3}>
              <TextField
                name="newPassword"
                label={t(messages.newPassword())}
                value={newPassword}
                onChange={formik.handleChange}
                error={
                  formik.touched.newPassword &&
                  Boolean(formik.errors.newPassword)
                }
                helperText={
                  formik.touched.newPassword && formik.errors.newPassword
                }
                type="password"
                autoComplete="password"
                required
              />

              <TextField
                name="retypeNewPassword"
                label={t(messages.retypeNewPassword())}
                value={retypeNewPassword}
                onChange={formik.handleChange}
                error={
                  formik.touched.retypeNewPassword &&
                  Boolean(formik.errors.retypeNewPassword)
                }
                helperText={
                  formik.touched.retypeNewPassword &&
                  formik.errors.retypeNewPassword
                }
                type="password"
                autoComplete="password"
                required
              />

              <LoadingButton
                fullWidth
                size="large"
                type="submit"
                variant="contained"
                loading={loading}
                onClick={() => formik.handleSubmit()}
              >
                {t(messages.resetPassword())}
              </LoadingButton>
            </Stack>

            <Button
              fullWidth
              size="large"
              component={RouterLink}
              to={PATH_AUTH.login}
              sx={{ mt: 1 }}
            >
              {t(messages.back())}
            </Button>
          </Box>
        </Container>
      </Wrapper>
    </>
  );
};

const Wrapper = styled('div')(({ theme }) => ({
  display: 'flex',
  minHeight: '100vh',
  alignItems: 'center',
  justifyContent: 'center',
  padding: theme.spacing(12, 0),
}));

const ResetPasswordSchema = Yup.object().shape({
  newPassword: Yup.string().required(t(messages.required())),
  retypeNewPassword: Yup.string()
    .required(t(messages.required()))
    .oneOf([Yup.ref('newPassword'), ''], t(messages.passwordDoNotMatchMsg())),
});

export { CompleteResetPasswordPage };

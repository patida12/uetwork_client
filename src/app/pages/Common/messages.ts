import { translations } from 'locales/translations';
import { _t } from 'utils/messages';

export const messages = {
  // login page
  login: () => _t(translations.authFeature.login),
  loginWithGoogle: () => _t(translations.authFeature.loginWithGoogle),
  email: () => _t(translations.authFeature.email),
  password: () => _t(translations.authFeature.password),
  notHaveAccountMsg: () => _t(translations.authFeature.notHaveAccountMsg),
  register: () => _t(translations.authFeature.register),
  forgotPasswordMsg: () => _t(translations.authFeature.forgotPasswordMsg),
  required: () => _t(translations.common.validate.required),
  emailNotValidMsg: () => _t(translations.common.validate.invalidEmail),

  // register page
  fullName: () => _t(translations.studentFeature.fullName),
  orgEmail: () => _t(translations.lecturerFeature.orgEmail),
  organization: () => _t(translations.common.app.organization),

  registerSuccess: () => _t(translations.authFeature.registerSuccess),
  registerFailed: () => _t(translations.authFeature.registerFailed),
  registerHelperText: () => _t(translations.authFeature.registerHelperText),
  alreadyHaveAccountMsg: () =>
    _t(translations.authFeature.alreadyHaveAccountMsg),
  noneChosen: () => _t(translations.common.noneChosen),
  createStudentAccount: () => _t(translations.authFeature.createStudentAccount),

  // forgot password page
  forgotPassword: () => _t(translations.authFeature.forgotPassword),
  resetPassword: () => _t(translations.authFeature.resetPassword),
  back: () => _t(translations.common.back),
  resetPasswordInstructionMsg: () =>
    _t(translations.authFeature.resetPasswordInstructionMsg),
  resetPasswordSuccessMsg: () =>
    _t(translations.authFeature.resetPasswordSuccessMsg),
  resetPasswordSuccess: () => _t(translations.authFeature.resetPasswordSuccess),
  resetPasswordFailed: () => _t(translations.authFeature.resetPasswordFailed),

  completeResetPasswordInstructionMsg: () =>
    _t(translations.authFeature.completeResetPasswordInstructionMsg),
  newPassword: () => _t(translations.authFeature.newPassword),
  retypeNewPassword: () => _t(translations.authFeature.retypeNewPassword),
  resetPasswordRequestSent: () =>
    _t(translations.authFeature.resetPasswordRequestSent),
  passwordDoNotMatchMsg: () =>
    _t(translations.common.validate.passwordDoNotMatchMsg),
};

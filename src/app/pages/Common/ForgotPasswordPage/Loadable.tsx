import { lazyLoad } from 'utils/loadable';
import { LoadingPage } from '../LoadingPage/LoadingPage';

export const ForgotPasswordPage = lazyLoad(
  () => import('./ForgotPasswordPage'),
  module => module.ForgotPasswordPage,
  {
    fallback: <LoadingPage />,
  },
);

import { Helmet } from 'react-helmet-async';
import { t } from 'locales/i18n';
import { messages } from '../messages';
import { Box, Button, Container, styled, Typography } from '@mui/material';
import { Link as RouterLink } from 'react-router-dom';
import { useState } from 'react';
import { ResetPasswordForm } from './components/ResetPasswordForm';
import { PATH_AUTH } from 'app/constants/path.constants';
import mailSentIllustration from 'assets/undraw_mail_sent.svg';
import { useSnackbar } from 'notistack';
import { Trans } from 'react-i18next';
import { resetPassword } from 'api/authApi';

const ForgotPasswordPage: React.FC = (): JSX.Element => {
  const [email, setEmail] = useState('');
  const [sent, setSent] = useState(false);

  const { enqueueSnackbar } = useSnackbar();

  const notSentPage = (
    <>
      <Typography variant="h3" paragraph>
        {t(messages.forgotPasswordMsg())}
      </Typography>
      <Typography sx={{ color: 'text.secondary', mb: 5 }}>
        {t(messages.resetPasswordInstructionMsg())}
      </Typography>

      <ResetPasswordForm
        onGetEmail={async email => {
          try {
            await resetPassword(email);
            setSent(true);
            setEmail(email);
          } catch (e) {
            enqueueSnackbar('f', { variant: 'error' });
          }
        }}
      />

      <Button
        fullWidth
        size="large"
        component={RouterLink}
        to={PATH_AUTH.login}
        sx={{ mt: 1 }}
      >
        {t(messages.back())}
      </Button>
    </>
  );

  const sentSuccessPage = (
    <Box sx={{ textAlign: 'center' }}>
      <Box sx={{ mb: 5, mx: 'auto', height: 160 }}>
        <img
          src={mailSentIllustration}
          alt="mail sent illustration"
          height="100%"
        />
      </Box>

      <Typography variant="h3" gutterBottom>
        {t(messages.resetPasswordRequestSent())}
      </Typography>
      <Typography>
        <Trans
          i18nKey={messages.resetPasswordSuccessMsg()[0]}
          values={{ email }}
        ></Trans>
      </Typography>

      <Button
        size="large"
        variant="contained"
        component={RouterLink}
        to={PATH_AUTH.login}
        sx={{ mt: 5 }}
      >
        {t(messages.back())}
      </Button>
    </Box>
  );

  return (
    <>
      <Helmet>
        <title>{t(messages.forgotPassword())}</title>
      </Helmet>
      <Wrapper>
        <Container>
          <Box sx={{ maxWidth: 480, mx: 'auto' }}>
            {sent ? sentSuccessPage : notSentPage}
          </Box>
        </Container>
      </Wrapper>
    </>
  );
};

const Wrapper = styled('div')(({ theme }) => ({
  display: 'flex',
  minHeight: '100vh',
  alignItems: 'center',
  justifyContent: 'center',
  padding: theme.spacing(12, 0),
}));

export { ForgotPasswordPage };

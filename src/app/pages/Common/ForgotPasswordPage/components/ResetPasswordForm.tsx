import { useFormik } from 'formik';
import * as Yup from 'yup';
import { messages } from '../../messages';
import { t } from 'locales/i18n';
import { LoadingButton } from '@mui/lab';
import { Stack, TextField } from '@mui/material';
import { useAsync } from 'app/hooks/useAsync';

interface ResetPasswordFormProps {
  onGetEmail: (email: string) => Promise<void>;
}

const ResetPasswordForm: React.FC<ResetPasswordFormProps> = ({
  onGetEmail,
}): JSX.Element => {
  const { run, loading } = useAsync();

  const formik = useFormik({
    initialValues: {
      email: '',
    },
    onSubmit: ({ email }) => {
      run(onGetEmail(email));
    },
    validationSchema: ResetPasswordFormSchema,
  });

  const { email } = formik.values;
  return (
    <Stack spacing={3}>
      <TextField
        name="email"
        label={t(messages.email())}
        value={email}
        onChange={formik.handleChange}
        error={formik.touched.email && Boolean(formik.errors.email)}
        helperText={formik.touched.email && formik.errors.email}
        type="email"
        autoComplete="email"
        required
      />

      <LoadingButton
        fullWidth
        size="large"
        type="submit"
        variant="contained"
        loading={loading}
        onClick={() => formik.handleSubmit()}
      >
        {t(messages.resetPassword())}
      </LoadingButton>
    </Stack>
  );
};

const ResetPasswordFormSchema = Yup.object().shape({
  email: Yup.string()
    .email(t(messages.emailNotValidMsg()))
    .required(t(messages.required())),
});

export { ResetPasswordForm };
export type { ResetPasswordFormProps };

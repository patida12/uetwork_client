import CloseIcon from '@mui/icons-material/Close';
import { Alert, IconButton, Snackbar } from '@mui/material';
import React from 'react';

interface CustomSnackbarProps {
  code: number;
  message: string;
  isOpen: boolean;
  onClose: (
    event: React.SyntheticEvent | Event,
    reason?: string | undefined,
  ) => void;
}

function CustomSnackbar({
  code,
  message,
  isOpen,
  onClose,
}: CustomSnackbarProps) {
  return (
    <Snackbar
      open={isOpen}
      autoHideDuration={6000}
      onClose={onClose}
      anchorOrigin={{
        vertical: 'top',
        horizontal: 'right',
      }}
    >
      <Alert
        severity={code === 200 ? 'success' : 'error'}
        sx={{ width: '100%' }}
      >
        {message}
        <IconButton
          size="small"
          aria-label="close"
          color="inherit"
          onClick={onClose}
        >
          <CloseIcon fontSize="small" />
        </IconButton>
      </Alert>
    </Snackbar>
  );
}

export default React.memo(CustomSnackbar);

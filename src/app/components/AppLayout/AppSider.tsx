import { ExpandLess, ExpandMore } from '@mui/icons-material';
import {
  Collapse,
  Divider,
  Drawer,
  List,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  styled,
} from '@mui/material';
import { ChildRouteDef, RouteDef } from 'models';
import { useState } from 'react';
import { Link, useRouteMatch } from 'react-router-dom';
import { AppLogo } from '../AppLogo';

const PREFIX = 'app-sider';
const classes = {
  logoText: `${PREFIX}-logoText`,
  logo: `${PREFIX}-logo`,
  activeItem: `${PREFIX}-activeItem`,
  itemIcon: `${PREFIX}-itemIcon`,
  nestedItem: `${PREFIX}-nestedItem`,
  nestedActiveItem: `${PREFIX}-nestedActiveItem`,
  drawer: `${PREFIX}-drawer`,
};

export const drawerWidth = 280;

export interface AppSiderProps {
  open: boolean;
  routes: RouteDef[];
}

export const AppSider: React.FC<AppSiderProps> = ({
  routes,
  open,
}): JSX.Element => {
  const { path: parentPath } = useRouteMatch();

  const filteredRoutes = routes.filter(route => route.hide !== true);

  const listItems = filteredRoutes.map(route => {
    if (route.children == null || route.children.length === 0) {
      return (
        <Link
          key={route.path}
          to={parentPath + route.path}
          className={classes.nestedItem}
        >
          <ListItemButton>
            <ListItemIcon className={classes.itemIcon}>
              <route.icon />
            </ListItemIcon>
            <ListItemText>{route.name}</ListItemText>
          </ListItemButton>
        </Link>
      );
    } else {
      const { children } = route;
      return (
        <ListItemWithSubmenu
          childRoutes={children}
          parentPath={parentPath}
          name={route.name}
          icon={route.icon}
          key={route.path}
        ></ListItemWithSubmenu>
      );
    }
  });

  return (
    <StyledDrawer variant="permanent" className={classes.drawer}>
      <Root>
        <StyledLink to={parentPath}>
          <AppLogo />
        </StyledLink>
        <Divider />
        <StyledList component="nav">{listItems}</StyledList>
      </Root>
    </StyledDrawer>
  );
};

interface ListItemWithSubmenuProps {
  icon: React.ComponentType<{ className?: string }>;
  name: string;
  childRoutes: ChildRouteDef[];
  parentPath: string;
  className?: string;
}

const ListItemWithSubmenu: React.FC<ListItemWithSubmenuProps> = (
  props,
): JSX.Element => {
  const { childRoutes, name, parentPath, className } = props;
  const [open, setOpen] = useState(false);

  const toggleOpen = () => {
    setOpen(!open);
  };

  return (
    <>
      <ListItemButton onClick={toggleOpen} className={className}>
        <ListItemIcon className={classes.itemIcon}>
          <props.icon />
        </ListItemIcon>
        <ListItemText>{name}</ListItemText>
        {open ? <ExpandLess /> : <ExpandMore />}
      </ListItemButton>
      <Collapse in={open} unmountOnExit>
        <List component="div">
          {childRoutes.map(cr => (
            <Link
              key={cr.path}
              to={parentPath + cr.path}
              className={classes.nestedItem}
            >
              <ListItemButton sx={{ pl: 7 }}>
                <ListItemText>{cr.name}</ListItemText>
              </ListItemButton>
            </Link>
          ))}
        </List>
      </Collapse>
    </>
  );
};

const Root = styled('div')(({ theme }) => ({
  [`& .${classes.logoText}`]: {
    textAlign: 'center',
    paddingTop: theme.spacing(2),
  },
  [`& .${classes.logo}`]: {
    paddingBottom: theme.spacing(1),
  },
  [`& .${classes.activeItem}`]: {
    color: theme.palette.primary.main,
    '& .MuiListItemIcon-root': {
      color: theme.palette.primary.main,
    },
  },
  [`& .${classes.itemIcon}`]: {
    minWidth: 'auto',
    marginRight: theme.spacing(2),
  },
  [`& .${classes.nestedItem}`]: {
    color: theme.palette.common.black,
    textDecoration: 'none',
  },
  [`& .${classes.nestedActiveItem}`]: {
    color: theme.palette.primary.main,
    textDecoration: 'none',
  },
}));

const StyledDrawer = styled(Drawer)(({ theme }) => ({
  '& .MuiDrawer-paper': {
    position: 'relative',
    boxSizing: 'border-box',
    width: drawerWidth,
    backgroundColor: '#f5f5f5',
  },
}));

const StyledList = styled(List)({
  backgroundColor: '#f5f5f5',
  width: '100%',
  maxWidth: 360,
});

const StyledLink = styled(Link)({
  textDecoration: 'none',
});

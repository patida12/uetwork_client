import { Typography } from '@mui/material';
import { styled } from '@mui/styles';

export interface AppContainerProps {
  title: React.ReactNode;
  className?: string;
}

export const AppContainer: React.FC<AppContainerProps> = ({
  title,
  children,
  className,
}): JSX.Element => {
  let titleElem = title;
  if (typeof title === 'string') {
    titleElem = <Typography variant="h1">{title}</Typography>;
  }

  return (
    <StyledDiv className={className}>
      <TitleWrapper>{titleElem}</TitleWrapper>
      {children != null && <>{children}</>}
    </StyledDiv>
  );
};

const StyledDiv = styled('div')({
  width: '100%',
  height: '100%',
  padding: '16px',
  display: 'flex',
  flexDirection: 'column',
});

const TitleWrapper = styled('div')(({ theme }) => ({
  marginTop: theme.spacing(2),
  marginBottom: theme.spacing(2),
}));

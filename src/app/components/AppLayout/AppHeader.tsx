import { AccountCircleOutlined } from '@mui/icons-material';
import {
  AppBar,
  IconButton,
  Menu,
  MenuItem,
  styled,
  Toolbar,
} from '@mui/material';
import { LanguageSwitcher } from 'app/admin/components/LanguageSwitcher';
import { t } from 'locales/i18n';
import { MouseEvent, useState } from 'react';
import { useHistory, useRouteMatch } from 'react-router-dom';
import { useUserInfo } from '../../hooks/useUserInfo';
import { drawerWidth } from './AppSider';
import { messages } from './messages';

export interface AppHeaderProps {}

/**
 * AppHeader is a common header for
 * @returns
 */
export const AppHeader: React.FC<AppHeaderProps> = (): JSX.Element => {
  return (
    <StyledAppBar position="absolute" elevation={0}>
      <StyledToolbar>
        <div></div>
        <div>
          <LanguageSwitcher />
          <AccountMenu />
        </div>
      </StyledToolbar>
    </StyledAppBar>
  );
};

interface AccountMenuProps {}

/**
 * AccountMenu is a Material Menu with `Account Info` and `Log out` options.
 * @returns
 */
export const AccountMenu: React.FC<AccountMenuProps> = (): JSX.Element => {
  const [anchorEl, setAnchorEl] = useState<HTMLElement | null>(null);
  const { logout } = useUserInfo();

  const menuOpen = Boolean(anchorEl);
  const handleClick = (e: MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(e.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const history = useHistory();
  const { path } = useRouteMatch();

  const handleAccount = () => {
    history.push(path + '/account');
    handleClose();
  };

  const handleLogOut = () => {
    logout();
  };

  return (
    <>
      <IconButton onClick={handleClick}>
        <AccountCircleOutlined />
      </IconButton>
      <Menu open={menuOpen} anchorEl={anchorEl} onClose={handleClose}>
        <MenuItem onClick={handleAccount}>
          {t(messages.personalInfo())}
        </MenuItem>
        <MenuItem onClick={handleLogOut}>{t(messages.logOut())}</MenuItem>
      </Menu>
    </>
  );
};

const PREFIX = 'app-header';

const classes = {
  appBar: `${PREFIX}-appBar`,
  menuButton: `${PREFIX}-menuButton`,
  toolbar: `${PREFIX}-toolbar`,
  grow: `${PREFIX}-grow`,
  sectionDesktop: `${PREFIX}-sectionDesktop`,
  sectionMobile: `${PREFIX}-sectionMobile`,
};

const StyledAppBar = styled(AppBar)(({ theme }) => ({
  [theme.breakpoints.up('sm')]: {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: drawerWidth,
  },
  backgroundColor: theme.palette.background.default,

  [`& .${classes.menuButton}`]: {
    marginRight: theme.spacing(2),
    [theme.breakpoints.up('sm')]: {
      display: 'none',
    },
  },

  [`& .${classes.toolbar}`]: theme.mixins.toolbar,

  [`& .${classes.grow}`]: {
    flexGrow: 1,
  },
}));

const StyledToolbar = styled(Toolbar)({
  display: 'flex',
  justifyContent: 'space-between',
});

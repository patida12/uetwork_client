import { translations } from 'locales/translations';
import { _t } from 'utils/messages';

export const messages = {
  personalInfo: () => _t(translations.accountFeature.personalInfo),
  logOut: () => _t(translations.accountFeature.logOut),
  changePassword: () => _t(translations.accountFeature.changePassword),
};

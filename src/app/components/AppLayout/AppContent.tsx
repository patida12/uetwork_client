import { Box, Toolbar } from '@mui/material';
import { styled } from '@mui/styles';
import { ChildRouteDef, RouteDef } from 'models';
import { Route, Switch, useRouteMatch } from 'react-router-dom';

export interface AppContentProps {
  routes: RouteDef[];
}

export const AppContent: React.FC<AppContentProps> = ({
  routes,
}): JSX.Element => {
  const { path } = useRouteMatch();

  const childRoutes = flattenRoutes(routes);
  const routeItems = childRoutes.map(props => {
    return (
      <Route
        {...props}
        key={props.path}
        component={props.component}
        path={path + props.path}
      ></Route>
    );
  });

  return (
    <StyledBox component="main">
      <Toolbar />
      <Switch>{routeItems}</Switch>
    </StyledBox>
  );
};

const StyledBox = styled(Box)({
  height: '100vh',
  display: 'flex',
  flexDirection: 'column',
  width: '100%',
  overflowY: 'scroll',
});

/**
 * Turn a list of routes definitions into child routes definition.
 * @param routes
 * @returns
 */
const flattenRoutes = (routes: RouteDef[]): ChildRouteDef[] => {
  const childRouteDefs: ChildRouteDef[] = [];

  for (let route of routes) {
    if (route.children != null) {
      childRouteDefs.push(...route.children);
    } else {
      childRouteDefs.push(route);
    }
  }

  return childRouteDefs;
};

import { styled } from '@mui/styles';

export interface AppLogoProps {
  className?: string;
}

const Wrapper = styled('div')({
  padding: '0.5rem',
  margin: 'auto',
  textAlign: 'center',
});

export const AppLogo: React.FC<AppLogoProps> = ({ className }): JSX.Element => {
  return (
    <Wrapper className={className}>
      <h1>
        uet<span style={{ color: '#4155e9' }}>.</span>work
      </h1>
    </Wrapper>
  );
};

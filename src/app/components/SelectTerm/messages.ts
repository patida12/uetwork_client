import { translations } from 'locales/translations';
import { _t } from 'utils/messages';

export const messages = {
  internshipTerm: () => _t(translations.termFeature.internshipTerm),
  term: () => _t(translations.termFeature.term),
  warningTermUndefined: () => _t(translations.termFeature.warningTermUndefined),
};

import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import Select from '@mui/material/Select';
import { styled } from '@mui/material/styles';
import { PATH_TERMS_PAGE } from 'app/constants';
import { TermContext } from 'contexts/TermProvider';
import { t } from 'locales/i18n';
import { TermIdParam } from 'models';
import { useSnackbar } from 'notistack';
import React, { useContext, useEffect } from 'react';
import { useHistory, useParams, useRouteMatch } from 'react-router-dom';
import { ChangeTermIdParam } from 'utils/ChangeTermIdParam';
import { messages } from './messages';

const PREFIX = 'SelectTerm';

const classes = {
  formControl: `${PREFIX}-formControl`,
};

const Root = styled('div')(({ theme }) => ({
  [`& .${classes.formControl}`]: {
    minWidth: 200,
  },
}));

function SelectTerm() {
  let { path } = useRouteMatch();
  const history = useHistory();
  const { termId } = useParams<TermIdParam>();
  const { listTerms, currentTerm, set_current_term } = useContext(TermContext);
  const { enqueueSnackbar } = useSnackbar();

  useEffect(() => {
    var termIdNumber = Number(termId);
    var term =
      listTerms.length > 0
        ? listTerms.find(term => term.id === termIdNumber)
        : undefined;
    if (term === undefined) {
      history.push(PATH_TERMS_PAGE);
      enqueueSnackbar(t(messages.warningTermUndefined()), {
        variant: 'warning',
      });
    } else {
      set_current_term(term);
    }
  }, [termId]);

  const handleChangeTerm = (termId: number) => {
    const term =
      listTerms.length > 0
        ? listTerms.find(term => term.id === termId)
        : undefined;
    if (term === undefined) {
      history.push(PATH_TERMS_PAGE);
      enqueueSnackbar(t(messages.warningTermUndefined()), {
        variant: 'warning',
      });
    } else {
      set_current_term(term);
      var newPath = ChangeTermIdParam(path, term.id.toString());
      history.push(newPath);
      history.go(0);
    }
  };

  return (
    <Root>
      {currentTerm && (
        <FormControl
          variant="outlined"
          className={classes.formControl}
          size="small"
        >
          <InputLabel id="select-term">
            {t(messages.internshipTerm())}
          </InputLabel>
          <Select
            id="select-term"
            value={currentTerm.id}
            onChange={event => handleChangeTerm(Number(event.target.value))}
            label={t(messages.internshipTerm())}
          >
            {listTerms.length > 0 &&
              listTerms.map(internshipTerm => (
                <MenuItem key={internshipTerm.id} value={internshipTerm.id}>
                  {t(messages.term())} {internshipTerm.term} -{' '}
                  {internshipTerm.year}
                </MenuItem>
              ))}
          </Select>
        </FormControl>
      )}
    </Root>
  );
}

export default SelectTerm;

import { translations } from 'locales/translations';
import { _t } from 'utils/messages';

export const messages = {
  editorPlaceholder: () => _t(translations.common.quill.placeholder),

  // change password card
  changePassword: () => _t(translations.authFeature.changePassword),
  currentPassword: () => _t(translations.authFeature.currentPassword),
  newPassword: () => _t(translations.authFeature.newPassword),
  retypeNewPassword: () => _t(translations.authFeature.retypeNewPassword),
  passwordDoNotMatchMsg: () =>
    _t(translations.common.validate.passwordDoNotMatchMsg),
  required: () => _t(translations.common.validate.required),
  saveChanges: () => _t(translations.common.saveChanges),
  saveChangesSuccess: () => _t(translations.common.saveChangesSuccess),
  saveChangesFailed: () => _t(translations.common.saveChangesFailed),
};

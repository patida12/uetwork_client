import { DataGrid, DataGridProps } from '@mui/x-data-grid';
import {
  CustomLoadingOverlay,
  CustomNoRowsOverlay,
  CustomPagination,
} from '../../admin/components/DataGrid';

interface AppDataGridProps extends DataGridProps {
  page: number;
  pageSize: number;
  rowCount: number;
}

export const AppDataGrid: React.FC<AppDataGridProps> = ({
  page,
  components,
  componentsProps,
  onPageChange,
  onPageSizeChange,
  pageSize,
  rowCount,
  ...rest
}): JSX.Element => {
  const pageCount = Math.ceil(rowCount / pageSize);

  return (
    <DataGrid
      paginationMode="server"
      sortingMode="server"
      filterMode="server"
      pagination
      disableSelectionOnClick
      disableColumnMenu
      className={classes.datagrid}
      components={{
        LoadingOverlay: CustomLoadingOverlay,
        NoRowsOverlay: CustomNoRowsOverlay,
        Pagination: CustomPagination,
        ...components,
      }}
      componentsProps={{
        pagination: {
          pageSize,
          onPageSizeChange,
          page,
          pageCount,
          onPageChange,
        },
        ...componentsProps,
      }}
      page={page - 1}
      pageSize={pageSize}
      rowCount={rowCount}
      {...rest}
    />
  );
};

const PREFIX = 'index';

const classes = {
  root: `${PREFIX}-root`,
  noRowsOverlay: `${PREFIX}-noRowsOverlay`,
  label: `${PREFIX}-label`,
  datagrid: `${PREFIX}-datagrid`,
};

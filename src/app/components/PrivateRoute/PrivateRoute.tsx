import { useEffect } from 'react';
import { Redirect, Route, RouteProps } from 'react-router-dom';
import { PATH_AUTH } from '../../constants/path.constants';
import { AppRole } from '../../constants/role.constants';
import { useUserInfo } from '../../hooks/useUserInfo';

interface PrivateRouteProps extends RouteProps {
  requiredRoles: AppRole[];
}

const PrivateRoute: React.FC<PrivateRouteProps> = ({
  requiredRoles,
  ...rest
}): JSX.Element => {
  const { currentUser, getInfo, getInfoLoading } = useUserInfo();

  useEffect(() => {
    getInfo();
  }, [getInfo]);

  if (getInfoLoading) {
    return <div></div>;
  }

  if (currentUser == null) {
    // TODO: improvement, save the url of user before they got redirected.
    return <Redirect to={PATH_AUTH.login} />;
  } else if (!requiredRoles.includes(currentUser.role)) {
    return <Redirect to="/404" />;
  } else {
    // TODO: we should use render function to re-authenticates every time.
    return <Route {...rest} />;
  }
};

export { PrivateRoute };

import { styled } from '@mui/styles';
import { deltaToHTML } from '../../../utils';

export interface DeltaRendererProps {
  deltaStr: string;
  className?: string;
}

export const DeltaRenderer: React.FC<DeltaRendererProps> = ({
  deltaStr,
  className,
}): JSX.Element => {
  const htmlString = deltaToHTML(deltaStr);

  return (
    <Wrapper
      className={className}
      dangerouslySetInnerHTML={{ __html: htmlString }}
    />
  );
};

// Style the text block
const Wrapper = styled('div')(({ theme }) => ({
  '& code,pre': {
    fontFamily: `source-code-pro,Menlo,Monaco,Consolas,Courier New,monospace`,
  },
  '& pre': {
    backgroundColor: theme.palette.grey[200],
    padding: theme.spacing(2),
  },
  '& img': {
    maxWidth: '100%',
  },
}));

import { Card, CardHeader } from '@mui/material';
import { styled, useTheme } from '@mui/material/styles';
import { NOT_AVAILABLE } from 'app/constants';
import merge from 'lodash/merge';
import PropTypes from 'prop-types';
import ReactApexChart from 'react-apexcharts';
import { fNumber } from 'utils/formatNumber';
import BaseOptionChart from './BaseOptionChart';

const CHART_HEIGHT = 392;
const LEGEND_HEIGHT = 72;

const ChartWrapperStyle = styled('div')(({ theme }) => ({
  height: CHART_HEIGHT,
  marginTop: theme.spacing(5),
  '& .apexcharts-canvas svg': { height: CHART_HEIGHT },
  '& .apexcharts-canvas svg,.apexcharts-canvas foreignObject': {
    overflow: 'visible',
  },
  '& .apexcharts-legend': {
    height: LEGEND_HEIGHT,
    alignContent: 'center',
    position: 'relative !important',
    borderTop: `solid 1px ${theme.palette.divider}`,
    top: `calc(${CHART_HEIGHT - LEGEND_HEIGHT}px) !important`,
  },
}));

AppPieChart.propTypes = {
  chartData: PropTypes.arrayOf(PropTypes.number).isRequired,
  title: PropTypes.string.isRequired,
  labels: PropTypes.arrayOf(PropTypes.string).isRequired,
  isNotAvailable: PropTypes.bool.isRequired,
};

export default function AppPieChart({
  title,
  chartData,
  labels,
  isNotAvailable,
}) {
  const theme = useTheme();

  const chartOptions = merge(BaseOptionChart(), {
    colors: [
      theme.palette.primary.light,
      theme.palette.primary.darker,
      theme.palette.primary.main,
    ],
    labels: isNotAvailable ? [] : labels,
    stroke: { colors: [theme.palette.background.paper] },
    legend: { floating: true, horizontalAlign: 'center' },
    tooltip: {
      fillSeriesColor: false,
      y: {
        formatter: seriesName =>
          isNotAvailable ? NOT_AVAILABLE : fNumber(seriesName),
        title: {
          formatter: seriesName =>
            isNotAvailable ? NOT_AVAILABLE : `${seriesName}`,
        },
      },
    },
    plotOptions: {
      pie: {
        donut: {
          size: '90%',
          labels: {
            value: {
              formatter: val => (isNotAvailable ? NOT_AVAILABLE : fNumber(val)),
            },
            total: {
              formatter: w => {
                const sum = w.globals.seriesTotals.reduce((a, b) => a + b, 0);
                return isNotAvailable ? NOT_AVAILABLE : fNumber(sum);
              },
            },
          },
        },
      },
    },
  });

  return (
    <Card>
      <CardHeader title={title} />
      <ChartWrapperStyle dir="ltr">
        <ReactApexChart
          type="donut"
          series={isNotAvailable ? [0, 0, 0] : chartData}
          options={chartOptions}
          height={280}
        />
      </ChartWrapperStyle>
    </Card>
  );
}

import { Box, Card, Stack, Typography } from '@mui/material';
import { alpha, styled, useTheme } from '@mui/material/styles';
import { NOT_AVAILABLE } from 'app/constants';
import PropTypes from 'prop-types';
import { fNumber, fPercent } from 'utils/formatNumber';
import Iconify from '../Iconify';

const IconWrapperStyle = styled('div')(({ theme }) => ({
  width: 24,
  height: 24,
  display: 'flex',
  borderRadius: '50%',
  alignItems: 'center',
  justifyContent: 'center',
  color: theme.palette.success.main,
  backgroundColor: alpha(theme.palette.success.main, 0.16),
}));

AppWidgetSummary.propTypes = {
  title: PropTypes.string.isRequired,
  lastTotal: PropTypes.number.isRequired,
  currentTotal: PropTypes.number.isRequired,
  srcImage: PropTypes.string.isRequired,
  isNotAvailable: PropTypes.bool.isRequired,
};

export default function AppWidgetSummary({
  title,
  lastTotal,
  currentTotal,
  srcImage,
  isNotAvailable,
}) {
  const theme = useTheme();
  const percent = isNotAvailable
    ? NOT_AVAILABLE
    : ((currentTotal - lastTotal) * 100) / lastTotal;

  return (
    <Card sx={{ display: 'flex', alignItems: 'center', p: 3 }}>
      <Box sx={{ flexGrow: 1 }}>
        <Typography variant="subtitle2">{title}</Typography>

        <Stack
          direction="row"
          alignItems="center"
          spacing={1}
          sx={{ mt: 2, mb: 1 }}
        >
          <IconWrapperStyle
            sx={{
              ...(percent < 0 && {
                color: 'error.main',
                bgcolor: alpha(theme.palette.error.main, 0.16),
              }),
            }}
          >
            <Iconify
              width={16}
              height={16}
              icon={
                percent >= 0 ? 'eva:trending-up-fill' : 'eva:trending-down-fill'
              }
              sx={{}}
            />
          </IconWrapperStyle>
          <Typography component="span" variant="subtitle2">
            {percent > 0 && '+'}
            {isNotAvailable ? NOT_AVAILABLE : fPercent(percent)}
          </Typography>
        </Stack>

        <Typography variant="h3">
          {isNotAvailable ? NOT_AVAILABLE : fNumber(currentTotal)}
        </Typography>
      </Box>
      <img src={srcImage} alt={title} width="20%" height="20%" />
    </Card>
  );
}

import EmojiEventsIcon from '@mui/icons-material/EmojiEvents';
import {
  Avatar,
  Box,
  Card,
  CardHeader,
  Chip,
  Stack,
  Typography,
} from '@mui/material';
import { NOT_AVAILABLE, TOP_DATA_COLOR } from 'app/constants';
import PropTypes from 'prop-types';
import { fShortenNumber } from 'utils/formatNumber';

export default function AppTopData({
  title,
  list,
  nameQuantity,
  isNotAvailable,
}) {
  let index = 0;
  return (
    <Card>
      <CardHeader title={title} />
      <Stack spacing={3} sx={{ p: 3, pr: 0 }}>
        {isNotAvailable
          ? NOT_AVAILABLE
          : list.map(item => (
              <DataItem
                key={item.id}
                item={item}
                nameQuantity={nameQuantity}
                rank={index++}
              />
            ))}
      </Stack>
    </Card>
  );
}

DataItem.propTypes = {
  item: PropTypes.shape({
    name: PropTypes.string,
    logoUrl: PropTypes.string,
    studentsCount: PropTypes.number,
  }),
  nameQuantity: PropTypes.string,
  rank: PropTypes.number,
  isNotAvailable: PropTypes.bool,
};

function DataItem({ item, nameQuantity, rank }) {
  const { logoUrl, name, studentsCount } = item;

  return (
    <Stack direction="row" alignItems="center" spacing={2}>
      <Box
        sx={{
          width: 48,
          height: 48,
          flexShrink: 0,
          display: 'flex',
          borderRadius: 1.5,
          alignItems: 'center',
          justifyContent: 'center',
          bgcolor: 'background.neutral',
        }}
      >
        <Avatar alt="Partner Logo" src={logoUrl} />
      </Box>

      <Box sx={{ flexGrow: 1, minWidth: 160 }}>
        <Typography variant="subtitle2">{name}</Typography>
      </Box>

      <Stack alignItems="flex-end" sx={{ pr: 3 }}>
        <Chip
          label={TOP_DATA_COLOR.at(rank)?.value}
          color={TOP_DATA_COLOR.at(rank)?.color}
          size="small"
          variant="outlined"
          icon={<EmojiEventsIcon />}
        />
        <Typography variant="caption" sx={{ mt: 0.5, color: 'text.secondary' }}>
          {fShortenNumber(studentsCount)}&nbsp;{nameQuantity}
        </Typography>
      </Stack>
    </Stack>
  );
}

import { translations } from 'locales/translations';
import { _t } from 'utils/messages';

export const messages = {
  welcomeBack: () => _t(translations.dashboard.welcomeBack),
  viewAll: () => _t(translations.dashboard.viewAll),
};

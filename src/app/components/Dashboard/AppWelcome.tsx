import { Button, Card, CardContent, Typography } from '@mui/material';
import { styled } from '@mui/material/styles';
import SeoIllustration from 'assets/illustration_seo';
import { t } from 'locales/i18n';
import PropTypes from 'prop-types';
import { Link as RouterLink } from 'react-router-dom';
import { messages } from './messages';

const RootStyle = styled(Card)(({ theme }) => ({
  boxShadow: 'none',
  textAlign: 'center',
  backgroundColor: theme.palette.primary.lighter,
  [theme.breakpoints.up('md')]: {
    height: '100%',
    display: 'flex',
    textAlign: 'left',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
}));

AppWelcome.propTypes = {
  displayName: PropTypes.string,
  introduction: PropTypes.string,
  buttonLabel: PropTypes.string,
  buttonPath: PropTypes.string,
};

export default function AppWelcome({
  displayName,
  introduction,
  buttonLabel,
  buttonPath,
}) {
  return (
    <RootStyle>
      <CardContent
        sx={{
          p: { md: 0 },
          pl: { md: 5 },
          color: 'grey.800',
        }}
      >
        <Typography gutterBottom variant="h2">
          {t(messages.welcomeBack())}
          <br /> {!displayName ? '...' : displayName}!
        </Typography>

        <Typography
          variant="body1"
          sx={{ pb: { xs: 3, xl: 5 }, maxWidth: 480, mx: 'auto' }}
        >
          {introduction}
        </Typography>

        <Button variant="contained" to={buttonPath} component={RouterLink}>
          {buttonLabel}
        </Button>
      </CardContent>

      <SeoIllustration
        sx={{
          p: 3,
          width: 360,
          margin: { xs: 'auto', md: 'inherit' },
        }}
      />
    </RootStyle>
  );
}

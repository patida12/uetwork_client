import { Card, Typography } from '@mui/material';
import { alpha, styled } from '@mui/material/styles';
import { NOT_AVAILABLE } from 'app/constants';
import PropTypes from 'prop-types';
import { fShortenNumber } from 'utils/formatNumber';

const RootStyle = styled(Card)(({ theme }) => ({
  boxShadow: 'none',
  textAlign: 'center',
  padding: theme.spacing(3, 0),
}));

const IconWrapperStyle = styled('div')(({ theme }) => ({
  margin: 'auto',
  display: 'flex',
  borderRadius: '50%',
  alignItems: 'center',
  width: theme.spacing(12),
  height: theme.spacing(12),
  justifyContent: 'center',
  marginBottom: theme.spacing(3),
}));

AppTotalSummary.propTypes = {
  color: PropTypes.oneOf([
    'primary',
    'secondary',
    'info',
    'success',
    'warning',
    'error',
  ]),
  srcImage: PropTypes.string,
  title: PropTypes.string,
  total: PropTypes.number,
  isNotAvailable: PropTypes.bool,
};

export default function AppTotalSummary({
  title,
  total,
  srcImage,
  color = 'primary',
  isNotAvailable,
}) {
  return (
    <RootStyle
      sx={{
        color: theme => theme.palette[color].dark,
        bgcolor: theme => theme.palette[color].lighter,
      }}
    >
      <IconWrapperStyle
        sx={{
          color: theme => theme.palette[color].dark,
          backgroundImage: theme =>
            `linear-gradient(135deg, ${alpha(
              theme.palette[color].dark,
              0,
            )} 0%, ${alpha(theme.palette[color].dark, 0.24)} 100%)`,
        }}
      >
        <img src={srcImage} alt={title} width={'80%'} height={'80%'} />
      </IconWrapperStyle>
      <Typography variant="h3">
        {isNotAvailable ? NOT_AVAILABLE : fShortenNumber(total)}
      </Typography>
      <Typography variant="h4" sx={{ opacity: 0.72 }}>
        {title}
      </Typography>
    </RootStyle>
  );
}

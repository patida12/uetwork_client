import {
  Box,
  Button,
  Card,
  CardHeader,
  Divider,
  TableContainer,
} from '@mui/material';
import { DataGrid, GridColDef } from '@mui/x-data-grid';
import { CustomNoRowsOverlay } from 'app/admin/components/DataGrid';
import { t } from 'locales/i18n';
import { Link as RouterLink } from 'react-router-dom';
import Iconify from '../Iconify';
import { messages } from './messages';

interface AppNewDataProps {
  columns: GridColDef[];
  title: string;
  rows: any[];
  buttonPath: string;
}

export default function AppNewData({
  title,
  columns,
  rows,
  buttonPath,
}: AppNewDataProps) {
  return (
    <Card>
      <CardHeader title={title} />
      <TableContainer sx={{ minWidth: 720, height: 400 }}>
        <DataGrid
          hideFooter
          rows={rows}
          columns={columns}
          components={{
            NoRowsOverlay: CustomNoRowsOverlay,
          }}
          disableSelectionOnClick
        />
      </TableContainer>

      <Divider />

      <Box sx={{ p: 2, textAlign: 'right' }}>
        <Button
          to={buttonPath}
          component={RouterLink}
          size="small"
          color="inherit"
          endIcon={<Iconify sx={{}} icon={'eva:arrow-ios-forward-fill'} />}
        >
          {t(messages.viewAll())}
        </Button>
      </Box>
    </Card>
  );
}

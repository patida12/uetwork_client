import Box from '@mui/material/Box';
import CircularProgress, {
  circularProgressClasses,
  CircularProgressProps,
} from '@mui/material/CircularProgress';
import * as React from 'react';
import { useTranslation } from 'react-i18next';
import styled from 'styled-components/macro';
import { messages } from './messages';

export default function LazyLoading(props: CircularProgressProps) {
  const { t } = useTranslation();

  return (
    <>
      <Wrapper>
        <Progress>
          <Box>
            <CircularProgress
              variant="indeterminate"
              disableShrink
              sx={{
                color: theme => theme.palette.primary.main,
                animationDuration: '550ms',
                left: 0,
                [`& .${circularProgressClasses.circle}`]: {
                  strokeLinecap: 'round',
                },
              }}
              size={40}
              thickness={4}
              {...props}
            />
          </Box>
        </Progress>
        <Box>{t(messages.loading())}</Box>
      </Wrapper>
    </>
  );
}

const Wrapper = styled.div`
  height: 80vh;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  min-height: 320px;
`;

const Progress = styled.div`
  margin-top: -8vh;
  font-weight: bold;
  color: black;
  font-size: 3.375rem;

  span {
    font-size: 3.125rem;
  }
`;

import {
  Card,
  CardActions,
  CardContent,
  styled,
  TextField,
  Typography,
} from '@mui/material';
import { messages } from '../messages';
import { t } from 'locales/i18n';
import { LoadingButton } from '@mui/lab';
import { useFormik } from 'formik';
import { useAsync } from 'app/hooks/useAsync';
import { useSnackbar } from 'notistack';
import * as Yup from 'yup';
import { updatePassword } from 'api/authApi';

interface ChangePasswordCardProps {
  className?: string;
}

const ChangePasswordCard: React.FC<ChangePasswordCardProps> = ({
  className,
}): JSX.Element => {
  const { enqueueSnackbar } = useSnackbar();
  const { loading, run } = useAsync();

  const formik = useFormik({
    initialValues: {
      currentPassword: '',
      newPassword: '',
      retypeNewPassword: '',
    },
    onSubmit: ({ newPassword, currentPassword }, { resetForm }) => {
      run(
        updatePassword(currentPassword, newPassword)
          .then(() => {
            enqueueSnackbar(t(messages.saveChangesSuccess()), {
              variant: 'success',
            });
            resetForm();
          })
          .catch(e => {
            enqueueSnackbar(t(messages.saveChangesFailed()), {
              variant: 'error',
            });
          }),
      );
    },
    validationSchema: PasswordInfoSchema,
  });

  const passwordInfo = formik.values;

  return (
    <StyledCard variant="outlined" className={className}>
      <CardContent>
        <StyledCardTitle variant="h2" component="h2">
          {t(messages.changePassword())}
        </StyledCardTitle>
        <TextField
          name="currentPassword"
          className="mb-2"
          required
          value={passwordInfo.currentPassword}
          label={t(messages.currentPassword())}
          fullWidth
          onChange={formik.handleChange}
          error={
            formik.touched.currentPassword &&
            Boolean(formik.errors.currentPassword)
          }
          helperText={
            formik.touched.currentPassword && formik.errors.currentPassword
          }
          type="password"
          autoComplete="current-password"
        />
        <TextField
          name="newPassword"
          className="mb-2"
          required
          value={passwordInfo.newPassword}
          label={t(messages.newPassword())}
          fullWidth
          onChange={formik.handleChange}
          error={
            formik.touched.newPassword && Boolean(formik.errors.newPassword)
          }
          helperText={formik.touched.newPassword && formik.errors.newPassword}
          type="password"
          autoComplete="new-password"
        />
        <TextField
          name="retypeNewPassword"
          required
          value={passwordInfo.retypeNewPassword}
          label={t(messages.retypeNewPassword())}
          fullWidth
          onChange={formik.handleChange}
          error={
            formik.touched.retypeNewPassword &&
            Boolean(formik.errors.retypeNewPassword)
          }
          helperText={
            formik.touched.retypeNewPassword && formik.errors.retypeNewPassword
          }
          type="password"
          autoComplete="new-password"
        />
      </CardContent>
      <StyledCardActions>
        <LoadingButton
          type="submit"
          variant="contained"
          loading={loading}
          onClick={() => formik.handleSubmit()}
        >
          {t(messages.saveChanges())}
        </LoadingButton>
      </StyledCardActions>
    </StyledCard>
  );
};

const PasswordInfoSchema = Yup.object().shape({
  currentPassword: Yup.string().required(t(messages.required())),
  newPassword: Yup.string().required(t(messages.required())),
  retypeNewPassword: Yup.string()
    .required(t(messages.required()))
    .oneOf([Yup.ref('newPassword'), ''], t(messages.passwordDoNotMatchMsg())),
});

const StyledCard = styled(Card)(({ theme }) => ({
  '& .mb-2': {
    marginBottom: theme.spacing(2),
  },
}));

const StyledCardTitle = styled(Typography)(({ theme }) => ({
  marginBottom: theme.spacing(4),
}));

const StyledCardActions = styled(CardActions)(({ theme }) => ({
  padding: theme.spacing(2),
  display: 'flex',
  justifyContent: 'end',
}));

export { ChangePasswordCard };

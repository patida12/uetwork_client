import styled from 'styled-components/macro';
import PhoneInput from 'react-phone-number-input';

export const PhoneInputCustom = styled(PhoneInput)`
  .PhoneInput {
    display: flex;
    align-items: center;
  }

  .PhoneInputInput {
    min-width: 0;
    height: 4ch;
    border-radius: 5px;
    border: solid 1px #ccc;
  }
  .PhoneInputInput:focus {
    outline: none !important;
    border-radius: 5px;
    border: solid 2px #015198;
  }
  .PhoneInputInput:hover:not(:focus) {
    outline: none !important;
    border-radius: 5px;
    border: solid 1px #000;
  }
`;

import ReactQuill, { ReactQuillProps, Quill } from 'react-quill';
import 'react-quill/dist/quill.snow.css';
import styled from 'styled-components';
import { t } from 'locales/i18n';
import { messages } from '../messages';
import { modules } from './editorModules';
import { ImageUploader } from './image-uploader.module';

Quill.register('modules/imageUploader', ImageUploader);

const TextEditor: React.FC<ReactQuillProps> = (props): JSX.Element => {
  return (
    <EditorWrapper data-text-editor="ql">
      <ReactQuill
        theme="snow"
        placeholder={t(messages.editorPlaceholder())}
        bounds={`[data-text-editor="ql"]`} // prevent tooltip overflow
        modules={{
          ...modules,
        }}
        {...props}
      />
    </EditorWrapper>
  );
};

const EditorWrapper = styled.div`
  text-size-adjust: 100%;
  -webkit-font-smoothing: antialiased;
  font-size: 1rem;
  font-weight: 400;
  line-height: 1.5;
  font-family: Inter, -apple-system, BlinkMacSystemFont, 'Segoe UI', Helvetica,
    Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji';
  color: rgb(18, 24, 40);
  box-sizing: border-box;
  border: 1px solid rgb(230, 232, 240);
  border-radius: 8px;
  display: flex;
  flex-direction: column;
  overflow: hidden;
  height: 330px;
  margin-top: 24px;

  .quill {
    text-size-adjust: 100%;
    -webkit-font-smoothing: antialiased;
    font-size: 1rem;
    font-weight: 400;
    line-height: 1.5;
    font-family: Inter, -apple-system, BlinkMacSystemFont, 'Segoe UI', Helvetica,
      Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji';
    color: rgb(18, 24, 40);
    box-sizing: border-box;
    display: flex;
    flex: 1 1 0%;
    flex-direction: column;
    overflow: hidden;

    .ql-editor {
      text-size-adjust: 100%;
      -webkit-font-smoothing: antialiased;
      font-weight: 400;
      -webkit-user-modify: read-write;
      -webkit-line-break: after-white-space;
      line-height: 1.42;
      outline: none;
      tab-size: 4;
      text-align: left;
      white-space: pre-wrap;
      word-wrap: break-word;
      box-sizing: border-box;
      color: rgb(18, 24, 40);
      flex: 1 1 0%;
      font-family: Inter, -apple-system, BlinkMacSystemFont, 'Segoe UI',
        Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji';
      font-size: 1rem;
      height: auto;
      overflow-y: auto;
      padding: 16px;
      &.ql-blank::before {
        font-style: normal;
      }
    }

    .ql-toolbar {
      cursor: default;
      tab-size: 4;
      -webkit-tap-highlight-color: transparent /* 4 */;
      word-break: break-word;
      text-size-adjust: 100%;
      -webkit-font-smoothing: antialiased;
      font-size: 1rem;
      font-weight: 400;
      line-height: 1.5;
      color: rgb(18, 24, 40);
      &.ql-snow {
        border-bottom-color: rgb(230, 232, 240);
        border-left: none;
        border-right: none;
        border-top: none;
        box-sizing: border-box;
        font-family: 'Inter', 'Helvetica', 'Arial', sans-serif;
        padding: 8px;
      }
    }

    .ql-container {
      text-size-adjust: 100%;
      -webkit-font-smoothing: antialiased;
      font-weight: 400;
      line-height: 1.5;
      color: rgb(18, 24, 40);
      font-family: Helvetica, Arial, sans-serif;
      font-size: 13px;
      margin: 0;
      position: relative;
      box-sizing: border-box;
      border: 1px solid #ccc;
      border-bottom: none rgb(230, 232, 240);
      border-left: none;
      border-right: none;
      display: flex;
      flex: 1 1 0%;
      flex-direction: column;
      height: auto;
      overflow: hidden;
      border-top: 0;
    }
  }
`;

export { TextEditor };

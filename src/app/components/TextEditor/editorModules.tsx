import { StringMap } from 'quill';
import storage from '../../../firebase/firebase';
import { getDownloadURL, ref, uploadBytesResumable } from 'firebase/storage';

const modules: StringMap = {
  toolbar: {
    container: [
      [{ header: [1, 2, false] }],
      ['bold', 'italic', 'underline', 'strike', 'blockquote'],
      [
        { list: 'ordered' },
        { list: 'bullet' },
        { indent: '-1' },
        { indent: '+1' },
      ],
      ['link', 'image'],
    ],
    clipboard: {
      matchVisual: false,
    },
  },
  imageUploader: {
    upload: async (file: File): Promise<string> => {
      var formData = new FormData();
      formData.append('image', file);

      var currentdate = new Date();
      var fileNamePredecessor =
        currentdate.getDate().toString() +
        currentdate.getMonth().toString() +
        currentdate.getFullYear().toString() +
        currentdate.getTime().toString();

      var fileName = fileNamePredecessor + file.name;

      const metadata = {
        contentType: 'image/jpeg',
      };

      const storageRef = ref(storage, fileName);
      const uploadTask = uploadBytesResumable(storageRef, file, metadata);

      const snapshot = await uploadTask;
      return getDownloadURL(snapshot.ref);
    },
  },
};

export { modules };

import { translations } from 'locales/translations';
import { _t } from 'utils/messages';

export const messages = {
  fullName: () => _t(translations.lecturerFeature.fullName),
  personalEmail: () => _t(translations.lecturerFeature.personalEmail),
  orgEmail: () => _t(translations.lecturerFeature.orgEmail),
  phoneNumber: () => _t(translations.lecturerFeature.phoneNumber),
  search: () => _t(translations.common.table.search),
  option: () => _t(translations.common.table.option),
  add: () => _t(translations.common.button.add),
  edit: () => _t(translations.common.button.edit),
  detail: () => _t(translations.common.button.detail),
  save: () => _t(translations.common.button.save),
  close: () => _t(translations.common.button.close),
  addDialogTitle: () => _t(translations.lecturerFeature.addLecturer),
  editDialogTitle: () => _t(translations.lecturerFeature.editLecturer),
  detailDialogTitle: () => _t(translations.lecturerFeature.detailLecturer),
  required: () => _t(translations.common.validate.required),
  tooShort: () => _t(translations.common.validate.tooShort),
  tooLong: () => _t(translations.common.validate.tooLong),
  invalidEmail: () => _t(translations.common.validate.invalidEmail),
  keyword: () => _t(translations.common.table.keyword),
  editSuccess: () => _t(translations.lecturerFeature.editSuccess),
};

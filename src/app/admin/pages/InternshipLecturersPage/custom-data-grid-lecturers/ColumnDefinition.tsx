import { GridCellParams, GridColumns } from '@mui/x-data-grid';
import GridCellExpand from 'app/components/GridCellExpand';
import { t } from 'locales/i18n';
import { messages } from '../messages';

export const getColumns = (): GridColumns => [
  {
    field: 'id',
    sortable: false,
    disableColumnMenu: true,
    hide: true,
  },
  {
    field: 'fullName',
    headerName: t(messages.fullName()),
    width: 250,
    renderCell: (params: GridCellParams) => (
      <GridCellExpand
        value={params.value ? params.value.toString() : ''}
        width={params.colDef.width}
      />
    ),
    disableColumnMenu: true,
  },
  {
    field: 'orgEmail',
    headerName: t(messages.orgEmail()),
    width: 200,
    renderCell: (params: GridCellParams) => (
      <GridCellExpand
        value={params.value ? params.value.toString() : ''}
        width={params.colDef.width}
      />
    ),
    disableColumnMenu: true,
    sortable: false,
  },
  {
    field: 'personalEmail',
    headerName: t(messages.personalEmail()),
    width: 200,
    renderCell: (params: GridCellParams) => (
      <GridCellExpand
        value={params.value ? params.value.toString() : ''}
        width={params.colDef.width}
      />
    ),
    disableColumnMenu: true,
    sortable: false,
  },
  {
    field: 'numOfSupervisedStudents',
    headerName: t(messages.numOfSupervisedStudents()),
    width: 300,
    renderCell: (params: GridCellParams) => (
      <GridCellExpand
        value={params.value ? params.value.toString() : '0'}
        width={params.colDef.width}
      />
    ),
    disableColumnMenu: true,
    sortable: false,
  },
];

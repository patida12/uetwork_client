import { GridSortModel } from '@mui/x-data-grid';
import internshipLecturerApi from 'api/internship/internshipLecturerApi';
import { PaginatedResults, QueryParameters } from 'api/types';
import { DEFAULT_PAGE, DEFAULT_ROWS_PER_PAGE } from 'app/constants';
import { useAsync } from 'app/hooks/useAsync';
import { FilterState, TermIdParam } from 'models/common';
import { InternshipLecturer } from 'models/internshipLecturer';
import React from 'react';
import { useParams } from 'react-router-dom';
import theme from 'styles/theme';
import {
  NumberParam,
  ObjectParam,
  StringParam,
  useQueryParams,
  withDefault,
} from 'use-query-params';
import { sortModelToQuery } from 'utils';
import DataTable from '../../components/DataGrid';
import { getColumns } from './custom-data-grid-lecturers/ColumnDefinition';
import CustomToolbar from './custom-data-grid-lecturers/CustomToolbar';

export function InternshipLecturersPage() {
  const { termId } = useParams<TermIdParam>();
  const { loading, data, run } = useAsync<PaginatedResults<InternshipLecturer>>(
    {
      data: {
        totalItems: 0,
        currentPage: 1,
        totalPages: 0,
        perPage: 0,
        items: [],
      },
    },
  );

  const [query, setQuery] = useQueryParams({
    page: withDefault(NumberParam, DEFAULT_PAGE),
    perPage: withDefault(NumberParam, DEFAULT_ROWS_PER_PAGE),
    q: withDefault(StringParam, ''),
    sort: withDefault(ObjectParam, {}),
    filter: withDefault(ObjectParam, {}),
  });

  const { page, perPage, sort } = query;

  const onPageChange = (page: number) => {
    setQuery({
      page,
    });
  };

  const onPageSizeChange = (pageSize: number) => {
    setQuery({
      perPage: pageSize,
      page: DEFAULT_PAGE,
    });
  };

  const handleSortModelChange = (model: GridSortModel) => {
    if (model.length === 0) {
      setQuery({
        sort: {},
      });
      return;
    }
    const firstElem = model[0];

    setQuery({
      sort: {
        ...sort,
        [firstElem.field]: [firstElem.sort],
      },
    });
  };

  const handleFilterChange = (newFilter: FilterState) => {
    setQuery({
      q: newFilter.query,
      filter: newFilter.filter,
    });
  };

  const columns = React.useMemo(() => getColumns(), []);

  const rows: InternshipLecturer[] = data?.items ?? [];
  const totalItems: number = data?.totalItems ?? 0;

  React.useEffect(() => {
    run(
      internshipLecturerApi.getAllInternshipLecturers(
        getQueryObject(query),
        Number(termId),
      ),
    );
  }, [query]);

  return (
    <div style={{ height: 700, width: '100%', marginTop: theme.spacing(4) }}>
      <DataTable
        rows={rows}
        columns={columns}
        pageSize={perPage}
        rowCount={totalItems}
        loading={loading}
        page={page}
        customToolbar={CustomToolbar}
        onPageSizeChange={onPageSizeChange}
        onPageChange={onPageChange}
        onSortModelChange={handleSortModelChange}
        onFilterChange={handleFilterChange}
      />
    </div>
  );
}

const getQueryObject = (qObj: any): QueryParameters => {
  const { page, perPage, sort, q, filter } = qObj;
  const gsq: QueryParameters = {
    page,
    perPage,
    q,
    filter,
  };
  const gsqSort: string[] = sortModelToQuery(sort);
  gsq.sort = gsqSort;
  return gsq;
};

import { translations } from 'locales/translations';
import { _t } from 'utils/messages';

export const messages = {
  fullName: () => _t(translations.internship.lecturerFeature.fullName),
  orgEmail: () => _t(translations.internship.lecturerFeature.orgEmail),
  personalEmail: () =>
    _t(translations.internship.lecturerFeature.personalEmail),
  numOfSupervisedStudents: () =>
    _t(translations.internship.lecturerFeature.numOfSupervisedStudents),
  search: () => _t(translations.common.table.search),
  keyword: () => _t(translations.common.table.keyword),
  option: () => _t(translations.common.table.option),
};

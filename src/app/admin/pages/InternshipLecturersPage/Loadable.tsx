import LazyLoading from 'app/components/LazyLoading/lazyLoading.component';
import { lazyLoad } from 'utils/loadable';

export const InternshipLecturersPage = lazyLoad(
  () => import('./index'),
  module => module.InternshipLecturersPage,
  {
    fallback: <LazyLoading />,
  },
);

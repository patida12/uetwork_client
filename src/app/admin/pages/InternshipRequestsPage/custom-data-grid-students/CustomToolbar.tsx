import SearchIcon from '@mui/icons-material/Search';
import {
  Button,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
} from '@mui/material';
import Grid from '@mui/material/Grid';
import { styled } from '@mui/material/styles';
import TextField from '@mui/material/TextField';
import { GridToolbarContainer } from '@mui/x-data-grid';
import {
  INTERNSHIP_TYPE,
  SENT_MAIL_STATUS,
} from 'app/constants/status.constants';
import { useFormik } from 'formik';
import { t } from 'locales/i18n';
import { CustomToolbarProps, FilterState } from 'models';
import React from 'react';
import OptionsInternshipRequest from '../components/OptionsInternshipRequest';
import { messages } from '../messages';

const PREFIX = 'CustomToolbar';

const classes = {
  textField: `${PREFIX}-textField`,
};

const StyledGridToolbarContainer = styled(GridToolbarContainer)(
  ({ theme }) => ({
    [`& .${classes.textField}`]: {
      width: '25ch',
    },
  }),
);

function CustomToolbar({
  onFilterChange,
  selectionModel,
  refreshData,
}: CustomToolbarProps) {
  const formik = useFormik<FilterState>({
    initialValues: {
      query: '',
      filter: {
        internshipType: '',
        isMailSent: '',
      },
    },
    onSubmit: values => {
      var filters: FilterState = {
        query: values.query,
        filter: {
          internshipType:
            INTERNSHIP_TYPE.find(
              type => type.key === values.filter.internshipType,
            )?.value ?? '',
          isMailSent:
            SENT_MAIL_STATUS.find(
              status => status.key === values.filter.isMailSent,
            )?.value ?? '',
        },
      };
      onFilterChange(filters);
    },
  });

  return (
    <form onSubmit={formik.handleSubmit}>
      <StyledGridToolbarContainer style={{ margin: 10 }}>
        <Grid
          container
          direction="row"
          justifyContent="space-between"
          spacing={1}
        >
          <Grid
            item
            xs={12}
            sm={8}
            md={8}
            direction="row"
            container
            alignItems="center"
            justifyContent="flex-start"
            spacing={1}
          >
            <Grid item>
              <FormControl
                variant="outlined"
                style={{ minWidth: 250 }}
                size="small"
              >
                <InputLabel id="select-internshipType-label">
                  {t(messages.filterCompany())}
                </InputLabel>
                <Select
                  id="filter.internshipType"
                  name="filter.internshipType"
                  label={t(messages.filterCompany())}
                  value={formik.values.filter.internshipType}
                  onChange={formik.handleChange}
                >
                  <MenuItem
                    key={INTERNSHIP_TYPE[0].key}
                    value={INTERNSHIP_TYPE[0].key}
                  >
                    {t(messages.allCompanies())}
                  </MenuItem>
                  <MenuItem
                    key={INTERNSHIP_TYPE[1].key}
                    value={INTERNSHIP_TYPE[1].key}
                  >
                    {t(messages.associatedCompanies())}
                  </MenuItem>
                  <MenuItem
                    key={INTERNSHIP_TYPE[2].key}
                    value={INTERNSHIP_TYPE[2].key}
                  >
                    {t(messages.otherCompanies())}
                  </MenuItem>
                </Select>
              </FormControl>
            </Grid>
            <Grid item>
              <FormControl
                variant="outlined"
                style={{ minWidth: 250 }}
                size="small"
              >
                <InputLabel>{t(messages.filterIsMailSent())}</InputLabel>
                <Select
                  id="filter.isMailSent"
                  name="filter.isMailSent"
                  label={t(messages.filterIsMailSent())}
                  value={formik.values.filter.isMailSent}
                  onChange={formik.handleChange}
                >
                  <MenuItem
                    key={SENT_MAIL_STATUS[0].key}
                    value={SENT_MAIL_STATUS[0].key}
                  >
                    {t(messages.all())}
                  </MenuItem>
                  <MenuItem
                    key={SENT_MAIL_STATUS[1].key}
                    value={SENT_MAIL_STATUS[1].key}
                  >
                    {t(messages.isSent())}
                  </MenuItem>
                  <MenuItem
                    key={SENT_MAIL_STATUS[2].key}
                    value={SENT_MAIL_STATUS[2].key}
                  >
                    {t(messages.notSent())}
                  </MenuItem>
                </Select>
              </FormControl>
            </Grid>
            <Grid item>
              <TextField
                value={formik.values.query}
                onChange={formik.handleChange}
                id="query"
                className={classes.textField}
                size="small"
                variant="outlined"
                rows={1}
                label={t(messages.keyword())}
              />
            </Grid>
            <Grid item>
              <Button
                variant="contained"
                type="submit"
                startIcon={<SearchIcon />}
              >
                {t(messages.search())}
              </Button>
            </Grid>
          </Grid>
          <Grid item>
            <OptionsInternshipRequest
              rowIds={selectionModel ?? []}
              refreshData={refreshData!}
            />
          </Grid>
        </Grid>
      </StyledGridToolbarContainer>
    </form>
  );
}

export default React.memo(CustomToolbar);

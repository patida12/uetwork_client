import { Chip } from '@mui/material';
import { GridCellParams, GridColumns } from '@mui/x-data-grid';
import GridCellExpand from 'app/components/GridCellExpand';
import { TERM_STATUS } from 'app/constants/status.constants';
import { t } from 'locales/i18n';
import { formatDate } from 'utils';
import { messages } from '../messages';

export const getColumns = (): GridColumns => [
  {
    field: 'id',
    sortable: false,
    disableColumnMenu: true,
    hide: true,
  },
  {
    field: 'studentIdNumber',
    headerName: t(messages.studentIdNumber()),
    width: 250,
    renderCell: (params: GridCellParams) => (
      <GridCellExpand
        value={params.value ? params.value.toString() : ''}
        width={params.colDef.width}
      />
    ),
    disableColumnMenu: true,
  },
  {
    field: 'fullName',
    headerName: t(messages.fullName()),
    width: 250,
    renderCell: (params: GridCellParams) => (
      <GridCellExpand
        value={params.value ? params.value.toString() : ''}
        width={params.colDef.width}
      />
    ),
    disableColumnMenu: true,
  },
  {
    field: 'orgEmail',
    headerName: t(messages.orgEmail()),
    width: 200,
    renderCell: (params: GridCellParams) => (
      <GridCellExpand
        value={params.value ? params.value.toString() : ''}
        width={params.colDef.width}
      />
    ),
    disableColumnMenu: true,
    sortable: false,
  },
  {
    field: 'phoneNumber',
    headerName: t(messages.phoneNumber()),
    width: 200,
    renderCell: (params: GridCellParams) => (
      <GridCellExpand
        value={params.value ? params.value.toString() : ''}
        width={params.colDef.width}
      />
    ),
    disableColumnMenu: true,
    sortable: false,
  },
  {
    field: 'internshipType',
    headerName: t(messages.internshipType()),
    width: 200,
    renderCell: (params: GridCellParams) => (
      <GridCellExpand
        value={params.value ? params.value.toString() : ''}
        width={params.colDef.width}
      />
    ),
    disableColumnMenu: true,
    sortable: false,
  },
  {
    field: 'partnerName',
    headerName: t(messages.partnerName()),
    width: 250,
    renderCell: (params: GridCellParams) => (
      <GridCellExpand
        value={params.value ? params.value.toString() : ''}
        width={params.colDef.width}
      />
    ),
    disableColumnMenu: true,
    sortable: false,
  },
  {
    field: 'registeredAt',
    headerName: t(messages.registeredAt()),
    width: 200,
    renderCell: (params: GridCellParams) => (
      <GridCellExpand
        value={params.value ? formatDate(params.value.toString()) : ''}
        width={params.colDef.width}
      />
    ),
    disableColumnMenu: true,
    sortable: false,
  },
  {
    field: 'status',
    headerName: t(messages.status()),
    width: 150,
    sortable: false,
    disableColumnMenu: true,
    renderCell: (params: GridCellParams) => (
      <Chip
        label={TERM_STATUS.find(status => status.key === params.value)?.value}
        color={TERM_STATUS.find(status => status.key === params.value)?.color}
        variant="outlined"
        size="small"
      />
    ),
  },
];

import { BuildCircleOutlined } from '@mui/icons-material';
import { Button, MenuItem } from '@mui/material';
import Menu from '@mui/material/Menu';
import { GridRowId } from '@mui/x-data-grid';
import internshipRequestApi from 'api/internship/internshipRequestApi';
import { TERM_STATUS } from 'app/constants/status.constants';
import { t } from 'locales/i18n';
import { TermIdParam } from 'models';
import { useSnackbar } from 'notistack';
import React from 'react';
import { useParams } from 'react-router-dom';
import { messages } from '../messages';

interface OptionsProps {
  rowIds: GridRowId[];
  refreshData: () => void;
}

function OptionsInternshipRequest({ rowIds, refreshData }: OptionsProps) {
  const { termId } = useParams<TermIdParam>();
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);
  const { enqueueSnackbar } = useSnackbar();

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleMarkAsPassedOrFailed = (status: string) => {
    var registrationIds: number[] = rowIds.map(id => Number(id));
    internshipRequestApi
      .markAsPassedOrFailed(registrationIds, status, Number(termId))
      .then(res => {
        enqueueSnackbar(t(messages.markSuccess()), {
          variant: 'success',
        });
        refreshData();
      })
      .catch(e => {
        enqueueSnackbar(e.response?.data.message, { variant: 'error' });
      });
  };

  const handleSendNotice = () => {
    var registrationIds: number[] = rowIds.map(id => Number(id));
    internshipRequestApi
      .sendNotice(registrationIds, Number(termId))
      .then(res => {
        enqueueSnackbar(t(messages.sentNoticeSuccess()), {
          variant: 'success',
        });
        refreshData();
      })
      .catch(e => {
        enqueueSnackbar(e.response?.data.message, { variant: 'error' });
      });
  };

  return (
    <div>
      <Button
        variant="contained"
        onClick={handleClick}
        startIcon={<BuildCircleOutlined />}
        disabled={rowIds.length <= 0 ? true : false}
      >
        {t(messages.option())}
      </Button>
      <Menu anchorEl={anchorEl} open={open} onClose={handleClose}>
        <MenuItem
          onClick={() => handleMarkAsPassedOrFailed(TERM_STATUS[2].value)}
        >
          {t(messages.markAsPassed())}
        </MenuItem>
        <MenuItem
          onClick={() => handleMarkAsPassedOrFailed(TERM_STATUS[1].value)}
        >
          {t(messages.markAsFailed())}
        </MenuItem>
        <MenuItem onClick={handleSendNotice}>
          {t(messages.sendNotice())}
        </MenuItem>
      </Menu>
    </div>
  );
}

export default React.memo(OptionsInternshipRequest);

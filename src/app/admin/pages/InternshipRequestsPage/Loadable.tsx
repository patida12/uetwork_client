import LazyLoading from 'app/components/LazyLoading/lazyLoading.component';
import { lazyLoad } from 'utils/loadable';

export const InternshipRequestsPage = lazyLoad(
  () => import('./index'),
  module => module.InternshipRequestsPage,
  {
    fallback: <LazyLoading />,
  },
);

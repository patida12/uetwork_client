import { translations } from 'locales/translations';
import { _t } from 'utils/messages';

export const messages = {
  studentIdNumber: () =>
    _t(translations.internship.studentFeature.studentIdNumber),
  fullName: () => _t(translations.internship.studentFeature.fullName),
  orgEmail: () => _t(translations.internship.studentFeature.orgEmail),
  phoneNumber: () => _t(translations.internship.studentFeature.phoneNumber),
  internshipType: () =>
    _t(translations.internship.studentFeature.internshipType),
  partnerName: () => _t(translations.partnerFeature.name),
  registeredAt: () => _t(translations.internship.studentFeature.registeredAt),
  status: () => _t(translations.internship.studentFeature.status),
  search: () => _t(translations.common.table.search),
  keyword: () => _t(translations.common.table.keyword),
  filterCompany: () => _t(translations.common.app.company),
  allCompanies: () => _t(translations.internship.studentFeature.allPartners),
  associatedCompanies: () =>
    _t(translations.internship.studentFeature.associatePartners),
  otherCompanies: () =>
    _t(translations.internship.studentFeature.otherPartners),
  school: () => _t(translations.internship.studentFeature.school),
  option: () => _t(translations.common.table.option),
  markAsPassed: () => _t(translations.internship.studentFeature.markAsPassed),
  markAsFailed: () => _t(translations.internship.studentFeature.markAsFailed),
  sendNotice: () => _t(translations.internship.studentFeature.sendNotice),
  filterIsMailSent: () =>
    _t(translations.internship.studentFeature.filterIsMailSent),
  all: () => _t(translations.internship.studentFeature.all),
  isSent: () => _t(translations.internship.studentFeature.isSent),
  notSent: () => _t(translations.internship.studentFeature.notSent),
  markSuccess: () => _t(translations.internship.studentFeature.markSuccess),
  sentNoticeSuccess: () =>
    _t(translations.internship.studentFeature.sentNoticeSuccess),
};

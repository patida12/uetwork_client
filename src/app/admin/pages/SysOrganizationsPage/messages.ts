import { translations } from 'locales/translations';
import { _t } from 'utils/messages';

export const messages = {
  search: () => _t(translations.common.table.search),
  option: () => _t(translations.common.table.option),
  add: () => _t(translations.common.button.add),
  edit: () => _t(translations.common.button.edit),
  detail: () => _t(translations.common.button.detail),
  save: () => _t(translations.common.button.save),
  close: () => _t(translations.common.button.close),
  required: () => _t(translations.common.validate.required),
  tooShort: () => _t(translations.common.validate.tooShort),
  tooLong: () => _t(translations.common.validate.tooLong),
  invalidEmail: () => _t(translations.common.validate.invalidEmail),
  keyword: () => _t(translations.common.table.keyword),
  organizationName: () => _t(translations.sysAdminFeature.organizationName),
  adminEmail: () => _t(translations.sysAdminFeature.adminEmail),
  createdAt: () => _t(translations.sysAdminFeature.createdAt),
  addDialogTitle: () => _t(translations.sysAdminFeature.addOrg),
  editDialogTitle: () => _t(translations.sysAdminFeature.editOrg),
  addOrgSuccess: () => _t(translations.sysAdminFeature.addOrgSuccess),
  editOrgSuccess: () => _t(translations.sysAdminFeature.editOrgSuccess),
};

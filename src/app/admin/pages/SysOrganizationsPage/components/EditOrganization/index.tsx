import { EditOutlined } from '@mui/icons-material';
import { IconButton, Tooltip } from '@mui/material';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import Grid from '@mui/material/Grid';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import orgApi from 'api/orgApi';
import {
  DialogActions,
  DialogContent,
  DialogTitle,
} from 'app/components/CustomDialog';
import { useFormik } from 'formik';
import { t } from 'locales/i18n';
import { Organization, UpdateOrg } from 'models/organization';
import { useSnackbar } from 'notistack';
import React from 'react';
import theme from 'styles/theme';
import * as Yup from 'yup';
import { messages } from '../../messages';

interface EditOrgProps {
  refreshData: () => void;
  org: Organization;
}

function EditOrgDialog({ org, refreshData }: EditOrgProps) {
  const [open, setOpen] = React.useState(false);
  const edit = t(messages.edit());
  const { enqueueSnackbar } = useSnackbar();

  const EditOrgSchema = Yup.object().shape({
    name: Yup.string()
      .min(2, t(messages.tooShort()))
      .max(50, t(messages.tooLong()))
      .required(t(messages.required())),
  });

  const formik = useFormik({
    initialValues: {
      name: org.name,
    },
    validationSchema: EditOrgSchema,
    onSubmit: values => {
      const newOrg: UpdateOrg = {
        name: values.name,
      };
      orgApi
        .editOrganization(org.id, newOrg)
        .then(res => {
          enqueueSnackbar(t(messages.editOrgSuccess()), {
            variant: 'success',
          });
          handleClose();
          refreshData();
        })
        .catch(e => {
          enqueueSnackbar(e.response?.data.message, { variant: 'error' });
        });
    },
  });

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Tooltip title={edit}>
        <IconButton
          style={{
            color: theme.palette.primary.light,
          }}
          aria-label="edit"
          onClick={handleClickOpen}
          size="large"
        >
          <EditOutlined />
        </IconButton>
      </Tooltip>
      <Dialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
        scroll="paper"
        maxWidth={'md'}
        style={{ marginTop: 60 }}
      >
        <DialogTitle id="customized-dialog-title" onClose={handleClose}>
          {t(messages.editDialogTitle())}
        </DialogTitle>
        <div>
          <form onSubmit={formik.handleSubmit}>
            <DialogContent dividers>
              <Grid
                direction="column"
                container
                alignItems="center"
                justifyContent="center"
                spacing={1}
                style={{ marginRight: 60 }}
              >
                <Grid
                  item
                  direction="row"
                  container
                  alignItems="center"
                  justifyContent="center"
                >
                  <Grid item xs={2}>
                    <Typography>{t(messages.organizationName())}:</Typography>
                  </Grid>
                  <Grid item xs={10}>
                    <TextField
                      size="small"
                      variant="outlined"
                      style={{ width: '50ch' }}
                      id="name"
                      name="name"
                      value={formik.values.name}
                      onChange={formik.handleChange}
                      error={formik.touched.name && Boolean(formik.errors.name)}
                      helperText={formik.touched.name && formik.errors.name}
                    />
                  </Grid>
                </Grid>
              </Grid>
            </DialogContent>
            <DialogActions>
              <Button autoFocus variant="contained" type="submit">
                {t(messages.save())}
              </Button>
            </DialogActions>
          </form>
        </div>
      </Dialog>
    </div>
  );
}
export default React.memo(EditOrgDialog);

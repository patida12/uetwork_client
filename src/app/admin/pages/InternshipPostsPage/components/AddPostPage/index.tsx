import { DateTimePicker } from '@mui/lab';
import { Card, CardContent, styled } from '@mui/material';
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import internshipPostApi from 'api/internship/internshipPostApi';
import { TextEditor } from 'app/components/TextEditor/TextEditor';
import { parseISO } from 'date-fns';
import { useFormik } from 'formik';
import { t } from 'locales/i18n';
import { TermIdParam } from 'models';
import { InternshipPost } from 'models/internshipPost';
import { useSnackbar } from 'notistack';
import React, { useState } from 'react';
import 'react-phone-number-input/style.css';
import 'react-quill/dist/quill.snow.css';
import { Link, useParams } from 'react-router-dom';
import theme from 'styles/theme';
import { convertUTCDateToLocalDate } from 'utils/convertTime';
import * as Yup from 'yup';
import { messages } from '../../messages';
import { DefaultPost } from '../EditPostDialog';
import SelectContact from '../SelectContact';
import SelectPartner from '../SelectPartner';

export const defaultPost: InternshipPost = {
  id: 0,
  termId: 0,
  partnerId: 0,
  partnerName: '',
  partnerContactId: 0,
  partnerContactName: '',
  partnerContactEmail: '',
  partnerContactPhoneNumber: '',
  title: '',
  createdAt: new Date().toISOString(),
  startRegAt: new Date().toISOString(),
  endRegAt: new Date().toISOString(),
  jobCount: 0,
  content: '',
};

function AddPostPage() {
  const { termId } = useParams<TermIdParam>();
  const {
    title,
    partnerId,
    partnerContactId,
    startRegAt,
    endRegAt,
    jobCount,
    content,
  } = defaultPost;
  const [selectedPartnerId, setSelectedPartnerId] = useState<number>();
  const [selectedContactId, setSelectedContactId] = useState<number>();

  const { enqueueSnackbar } = useSnackbar();

  const AddPostSchema = Yup.object().shape({
    title: Yup.string()
      .min(2, t(messages.tooShort()))
      .max(50, t(messages.tooLong()))
      .required(t(messages.required())),
    startRegAt: Yup.date().required(t(messages.required())),
    endRegAt: Yup.date().required(t(messages.required())),
    jobCount: Yup.number().integer().min(1).required(t(messages.required())),
  });

  const formik = useFormik<DefaultPost>({
    initialValues: {
      title: title,
      startRegAt: parseISO(startRegAt),
      endRegAt: parseISO(endRegAt),
      jobCount: jobCount,
      content: content,
      partnerId: partnerId,
      partnerContactId: partnerContactId,
    },
    validationSchema: AddPostSchema,
    onSubmit: values => {
      internshipPostApi
        .addPost(Number(termId), {
          ...values,
          startRegAt: convertUTCDateToLocalDate(new Date(values.startRegAt)),
          endRegAt: convertUTCDateToLocalDate(new Date(values.endRegAt)),
          jobCount: Number(values.jobCount),
          partnerId: selectedPartnerId,
          partnerContactId: selectedContactId || 0,
        })
        .then(res => {
          enqueueSnackbar(t(messages.addPostSuccess()), {
            variant: 'success',
          });
        })
        .catch(e => {
          enqueueSnackbar(e.response?.data.message, { variant: 'error' });
        });
    },
  });

  const handleChangePartner = (partnerId: number) => {
    setSelectedPartnerId(partnerId);
  };

  const handleChangeContact = (contactId: number) => {
    setSelectedContactId(contactId);
  };

  return (
    <div style={{ marginTop: theme.spacing(4) }}>
      <form onSubmit={formik.handleSubmit}>
        <StyledUploadPostCard
          variant="outlined"
          sx={{
            mb: 2,
            p: 2,
          }}
        >
          <Button to="posts" component={Link} sx={{ mr: 1 }}>
            {t(messages.cancel())}
          </Button>
          <Button autoFocus variant="contained" type="submit">
            {t(messages.save())}
          </Button>
        </StyledUploadPostCard>
        <Card sx={{ mb: 2 }} variant="outlined">
          <CardContent>
            <Typography sx={{ mb: 4 }} variant="h2">
              {t(messages.basicInfo())}
            </Typography>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={6}>
                <TextField
                  variant="outlined"
                  id="title"
                  name="title"
                  value={formik.values.title}
                  onChange={formik.handleChange}
                  label={t(messages.title())}
                  fullWidth
                  error={formik.touched.title && Boolean(formik.errors.title)}
                  helperText={formik.touched.title && formik.errors.title}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <DateTimePicker
                  value={formik.values.startRegAt}
                  onChange={value => {
                    formik.setFieldValue('startRegAt', value);
                  }}
                  renderInput={params => (
                    <TextField
                      {...params}
                      label={t(messages.startRegAt())}
                      fullWidth
                      error={
                        formik.touched.startRegAt &&
                        Boolean(formik.errors.startRegAt)
                      }
                      helperText={
                        formik.touched.startRegAt && formik.errors.startRegAt
                      }
                    />
                  )}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <SelectPartner handleChangePartner={handleChangePartner} />
              </Grid>
              <Grid item xs={12} sm={6}>
                <DateTimePicker
                  value={formik.values.endRegAt}
                  onChange={value => {
                    formik.setFieldValue('endRegAt', value);
                  }}
                  minDate={formik.values.startRegAt}
                  renderInput={params => (
                    <TextField
                      {...params}
                      label={t(messages.endRegAt())}
                      fullWidth
                      error={
                        formik.touched.endRegAt &&
                        Boolean(formik.errors.endRegAt)
                      }
                      helperText={
                        formik.touched.endRegAt && formik.errors.endRegAt
                      }
                    />
                  )}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <SelectContact
                  handleChangeContact={handleChangeContact}
                  partnerId={selectedPartnerId}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  variant="outlined"
                  id="jobCount"
                  name="jobCount"
                  value={formik.values.jobCount}
                  inputProps={{ inputMode: 'numeric', min: '1' }}
                  onChange={formik.handleChange}
                  label={t(messages.jobCount())}
                  fullWidth
                  error={
                    formik.touched.jobCount && Boolean(formik.errors.jobCount)
                  }
                  helperText={formik.touched.jobCount && formik.errors.jobCount}
                />
              </Grid>
            </Grid>
          </CardContent>
        </Card>
        <Card variant="outlined">
          <CardContent>
            <Typography sx={{ mb: 4 }} variant="h2">
              {t(messages.content())}
            </Typography>
            <TextEditor
              defaultValue={formik.values.content}
              onChange={(value, delta, source, editor) => {
                formik.setFieldValue(
                  'content',
                  JSON.stringify(editor.getContents()),
                );
              }}
            />
          </CardContent>
        </Card>
      </form>
    </div>
  );
}

const StyledUploadPostCard = styled(Card)(({ theme }) => {
  return {
    display: 'flex',
    justifyContent: 'flex-end',
  };
});
export default React.memo(AddPostPage);

import { Box } from '@mui/material';
import Autocomplete from '@mui/material/Autocomplete';
import CircularProgress from '@mui/material/CircularProgress';
import TextField from '@mui/material/TextField';
import internshipPartnerApi from 'api/internship/internshipPartnerApi';
import { Results } from 'api/types';
import { useAsync } from 'app/hooks/useAsync';
import { t } from 'locales/i18n';
import { ContactInfo } from 'models';
import { useSnackbar } from 'notistack';
import * as React from 'react';
import { messages } from '../../messages';

const defaultContactListing: ContactInfo = {
  id: 0,
  fullName: '',
  email: '',
  phoneNumber: '',
  partnerId: 0,
};

interface SelectContactProps {
  partnerId: number | undefined;
  handleChangeContact: (id: number) => void;
}

function SelectContact({ partnerId, handleChangeContact }: SelectContactProps) {
  const [selectedContact, setContact] = React.useState<ContactInfo>(
    defaultContactListing,
  );
  const { enqueueSnackbar } = useSnackbar();

  var { data, run, loading } = useAsync<Results<ContactInfo[]>>({
    data: {
      codeResults: {
        code: 0,
        message: '',
      },
      item: [],
    },
  });

  const contactsList: ContactInfo[] = data?.item ?? [];

  React.useEffect(() => {
    if (partnerId)
      run(internshipPartnerApi.getContactsListing(Number(partnerId)));
  }, [enqueueSnackbar, run, partnerId]);

  React.useEffect(() => {
    if (selectedContact) {
      var contact = contactsList.find(
        contact => contact.id === selectedContact.id,
      );
      if (contact) handleChangeContact(contact.id);
    }
  }, [handleChangeContact, selectedContact]);

  function handleChange(contact: ContactInfo) {
    setContact(contact);
  }

  // if (contactsList === []) {
  //   return <CircularProgress size={20} />;
  // }

  return (
    <Autocomplete
      disabled={partnerId ? false : true}
      id="select-contact"
      onChange={(
        event: React.SyntheticEvent<Element, Event>,
        value: ContactInfo | null,
      ) => {
        if (value) {
          handleChange(value);
        }
      }}
      getOptionLabel={option => option.fullName}
      options={contactsList}
      renderOption={(props, option) => (
        <Box
          component="li"
          sx={{ '& > img': { mr: 2, flexShrink: 0 } }}
          {...props}
          key={option.id}
        >
          {option.fullName}
        </Box>
      )}
      renderInput={params => (
        <TextField
          {...params}
          placeholder={t(messages.partnerContactName())}
          InputProps={{
            ...params.InputProps,
            endAdornment: (
              <React.Fragment>
                {loading ? (
                  <CircularProgress color="inherit" size={20} />
                ) : null}
                {params.InputProps.endAdornment}
              </React.Fragment>
            ),
          }}
          fullWidth
          error={
            selectedContact === undefined &&
            Boolean(selectedContact === undefined)
          }
          helperText={selectedContact === undefined && t(messages.required())}
        />
      )}
    />
  );
}
export default React.memo(SelectContact);

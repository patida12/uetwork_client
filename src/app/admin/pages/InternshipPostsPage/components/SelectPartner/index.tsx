import { Box, Grid } from '@mui/material';
import Autocomplete from '@mui/material/Autocomplete';
import CircularProgress from '@mui/material/CircularProgress';
import TextField from '@mui/material/TextField';
import internshipPartnerApi from 'api/internship/internshipPartnerApi';
import { Results } from 'api/types';
import { IsAssociated } from 'app/admin/pages/ProfilePartnersPage/custom-data-grid-partners/ColumnDefinition';
import { useAsync } from 'app/hooks/useAsync';
import { t } from 'locales/i18n';
import { TermIdParam } from 'models';
import { PartnerListing } from 'models/internshipPartner';
import { useSnackbar } from 'notistack';
import * as React from 'react';
import { useParams } from 'react-router-dom';
import { messages } from '../../messages';

const defaultPartnerListing: PartnerListing = {
  id: 0,
  name: '',
  termStatus: '',
  isAssociate: false,
  logoUrl: '',
};

interface SelectPartnerProps {
  handleChangePartner: (id: number) => void;
}

function SelectPartner({ handleChangePartner }: SelectPartnerProps) {
  const { termId } = useParams<TermIdParam>();
  const { enqueueSnackbar } = useSnackbar();

  var { data, run, loading } = useAsync<Results<PartnerListing[]>>({
    data: {
      codeResults: {
        code: 0,
        message: '',
      },
      item: [],
    },
  });

  const partnersList: PartnerListing[] = data?.item ?? [];
  const [selectedPartner, setPartner] = React.useState<PartnerListing>(
    partnersList.length > 0 ? partnersList[0] : defaultPartnerListing,
  );

  React.useEffect(() => {
    run(internshipPartnerApi.getListPartners(Number(termId)));
  }, [enqueueSnackbar, run]);

  React.useEffect(() => {
    if (selectedPartner) {
      var partner = partnersList.find(
        partner => partner.id === selectedPartner.id,
      );
      if (partner) handleChangePartner(partner.id);
    }
  }, [handleChangePartner, selectedPartner]);

  function handleChange(partner: PartnerListing) {
    setPartner(partner);
  }

  return (
    <Autocomplete
      id="select-partner"
      onChange={(
        event: React.SyntheticEvent<Element, Event>,
        value: PartnerListing | null,
      ) => {
        if (value) {
          handleChange(value);
        }
      }}
      getOptionLabel={option => option.name}
      options={partnersList}
      renderOption={(props, option) => (
        <Box
          component="li"
          sx={{ '& > img': { mr: 2, flexShrink: 0 } }}
          {...props}
          key={option.id}
        >
          <Grid container>
            <Grid item xs={11}>
              {option.name}
            </Grid>
            <IsAssociated isAssociate={Boolean(option.isAssociate)} />
          </Grid>
        </Box>
      )}
      renderInput={params => (
        <TextField
          {...params}
          label={t(messages.partnerName())}
          InputProps={{
            ...params.InputProps,
            endAdornment: (
              <React.Fragment>
                {loading ? (
                  <CircularProgress color="inherit" size={20} />
                ) : null}
                {params.InputProps.endAdornment}
              </React.Fragment>
            ),
          }}
          fullWidth
          error={
            selectedPartner === undefined &&
            Boolean(selectedPartner === undefined)
          }
          helperText={selectedPartner === undefined && t(messages.required())}
        />
      )}
    />
  );
}
export default React.memo(SelectPartner);

import { QuillDeltaToHtmlConverter } from 'quill-delta-to-html';

export const deltaToHTML = (content: string): string => {
  var html = '';
  if (content !== '') {
    var deltaOps;
    try {
      deltaOps = JSON.parse(content).ops;
    } catch (e) {
      deltaOps = JSON.parse('{"defaultContent": 1}');
    }
    var converter = new QuillDeltaToHtmlConverter(deltaOps, {});
    html = converter.convert();
  }
  return html;
};

import { DateTimePicker } from '@mui/lab';
import { Card, CardContent, styled } from '@mui/material';
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import internshipPostApi from 'api/internship/internshipPostApi';
import { Results } from 'api/types';
import { TextEditor } from 'app/components/TextEditor/TextEditor';
import { useAsync } from 'app/hooks/useAsync';
import { LoadingPage } from 'app/pages/Common/LoadingPage/LoadingPage';
import { NotFoundPage } from 'app/pages/Common/NotFoundPage';
import { parseISO } from 'date-fns';
import { useFormik } from 'formik';
import { t } from 'locales/i18n';
import { PostIdParam, TermIdParam } from 'models';
import { InternshipPost } from 'models/internshipPost';
import { useSnackbar } from 'notistack';
import React, { useState } from 'react';
import 'react-phone-number-input/style.css';
import 'react-quill/dist/quill.snow.css';
import { Link, useParams } from 'react-router-dom';
import theme from 'styles/theme';
import { convertUTCDateToLocalDate } from 'utils/convertTime';
import * as Yup from 'yup';
import { messages } from '../../messages';
import { defaultPost } from '../AddPostPage';
import { deltaToHTML } from '../deltaToHTML';
import SelectContact from '../SelectContact';

export interface DefaultPost {
  title: string;
  partnerId?: number;
  partnerContactId: number;
  startRegAt: Date;
  endRegAt: Date;
  jobCount: number;
  content: string;
}

function EditPostPage() {
  const { termId } = useParams<TermIdParam>();
  const { postId } = useParams<PostIdParam>();
  const [selectedContactId, setSelectedContactId] = useState<number>();
  const { enqueueSnackbar } = useSnackbar();

  const { data, run, loading } = useAsync<Results<InternshipPost>>({
    data: {
      codeResults: {
        code: 0,
        message: '',
      },
      item: defaultPost,
    },
  });

  React.useEffect(() => {
    run(internshipPostApi.getDetailPost(Number(postId), Number(termId)));
  }, [run, postId]);

  const post: InternshipPost = data?.item ?? defaultPost;
  const {
    id,
    title,
    partnerId,
    partnerContactId,
    startRegAt,
    endRegAt,
    jobCount,
    content,
  } = post;

  const EditPostSchema = Yup.object().shape({
    title: Yup.string()
      .min(2, t(messages.tooShort()))
      .max(50, t(messages.tooLong()))
      .required(t(messages.required())),
    startRegAt: Yup.date().required(t(messages.required())),
    endRegAt: Yup.date().required(t(messages.required())),
    jobCount: Yup.number().integer().min(1).required(t(messages.required())),
  });

  const formik = useFormik<DefaultPost>({
    initialValues: {
      title: title,
      startRegAt: parseISO(startRegAt),
      endRegAt: parseISO(endRegAt),
      jobCount: jobCount,
      content: deltaToHTML(content),
      partnerContactId: partnerContactId,
    },
    validationSchema: EditPostSchema,
    onSubmit: values => {
      internshipPostApi
        .updatePost(id, Number(termId), {
          ...values,
          startRegAt: convertUTCDateToLocalDate(new Date(values.startRegAt)),
          endRegAt: convertUTCDateToLocalDate(new Date(values.endRegAt)),
          jobCount: Number(values.jobCount),
          partnerContactId: selectedContactId!,
        })
        .then(res => {
          enqueueSnackbar(t(messages.editPostSuccess()), {
            variant: 'success',
          });
        })
        .catch(e => {
          enqueueSnackbar(e.response?.data.message, { variant: 'error' });
        });
    },
    enableReinitialize: true,
  });

  const handleChangeContact = (contactId: number) => {
    setSelectedContactId(contactId);
  };

  if (loading === true) {
    return <LoadingPage />;
  }

  if (post.id === defaultPost.id) {
    return <NotFoundPage />;
  }

  return (
    <div style={{ marginTop: theme.spacing(4) }}>
      <form onSubmit={formik.handleSubmit}>
        <StyledUploadPostCard
          variant="outlined"
          sx={{
            mb: 2,
            p: 2,
          }}
        >
          <Button
            to={`/admin/org/terms/${termId}/posts`}
            component={Link}
            sx={{ mr: 1 }}
          >
            {t(messages.cancel())}
          </Button>
          <Button autoFocus variant="contained" type="submit">
            {t(messages.save())}
          </Button>
        </StyledUploadPostCard>
        <Card sx={{ mb: 2 }} variant="outlined">
          <CardContent>
            <Typography sx={{ mb: 4 }} variant="h2">
              {t(messages.basicInfo())}
            </Typography>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={6}>
                <TextField
                  variant="outlined"
                  id="title"
                  name="title"
                  value={formik.values.title}
                  onChange={formik.handleChange}
                  label={t(messages.title())}
                  fullWidth
                  error={formik.touched.title && Boolean(formik.errors.title)}
                  helperText={formik.touched.title && formik.errors.title}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <DateTimePicker
                  value={formik.values.startRegAt}
                  onChange={value => {
                    formik.setFieldValue('startRegAt', value);
                  }}
                  renderInput={params => (
                    <TextField
                      {...params}
                      label={t(messages.startRegAt())}
                      fullWidth
                      error={
                        formik.touched.startRegAt &&
                        Boolean(formik.errors.startRegAt)
                      }
                      helperText={
                        formik.touched.startRegAt && formik.errors.startRegAt
                      }
                    />
                  )}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <SelectContact
                  handleChangeContact={handleChangeContact}
                  partnerId={partnerId}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <DateTimePicker
                  value={formik.values.endRegAt}
                  onChange={value => {
                    formik.setFieldValue('endRegAt', value);
                  }}
                  minDate={formik.values.startRegAt}
                  renderInput={params => (
                    <TextField
                      {...params}
                      label={t(messages.endRegAt())}
                      fullWidth
                      error={
                        formik.touched.endRegAt &&
                        Boolean(formik.errors.endRegAt)
                      }
                      helperText={
                        formik.touched.endRegAt && formik.errors.endRegAt
                      }
                    />
                  )}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  variant="outlined"
                  id="jobCount"
                  name="jobCount"
                  value={formik.values.jobCount}
                  inputProps={{ inputMode: 'numeric', min: '1' }}
                  onChange={formik.handleChange}
                  label={t(messages.jobCount())}
                  fullWidth
                  error={
                    formik.touched.jobCount && Boolean(formik.errors.jobCount)
                  }
                  helperText={formik.touched.jobCount && formik.errors.jobCount}
                />
              </Grid>
            </Grid>
          </CardContent>
        </Card>
        <Card variant="outlined">
          <CardContent>
            <Typography sx={{ mb: 4 }} variant="h2">
              {t(messages.content())}
            </Typography>
            <TextEditor
              defaultValue={deltaToHTML(content)}
              onChange={(value, delta, source, editor) => {
                formik.setFieldValue(
                  'content',
                  JSON.stringify(editor.getContents()),
                );
              }}
            />
          </CardContent>
        </Card>
      </form>
    </div>
  );
}

const StyledUploadPostCard = styled(Card)(({ theme }) => {
  return {
    display: 'flex',
    justifyContent: 'flex-end',
  };
});
export default React.memo(EditPostPage);

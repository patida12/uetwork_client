import { InfoOutlined } from '@mui/icons-material';
import { IconButton, Stack, Tooltip } from '@mui/material';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import internshipPostApi from 'api/internship/internshipPostApi';
import { Results } from 'api/types';
import {
  DialogActions,
  DialogContent,
  DialogTitle,
} from 'app/components/CustomDialog';
import { useAsync } from 'app/hooks/useAsync';
import { t } from 'locales/i18n';
import { TermIdParam } from 'models';
import { InternshipPost } from 'models/internshipPost';
import React from 'react';
import 'react-phone-number-input/style.css';
import 'react-quill/dist/quill.bubble.css';
import { useParams } from 'react-router-dom';
import styled from 'styled-components';
import theme from 'styles/theme';
import { formatDate } from 'utils';
import { messages } from '../../messages';
import { defaultPost } from '../AddPostDialog';
import { deltaToHTML } from '../deltaToHTML';
import EditPostDialog from '../EditPostDialog';

interface DetailPostDialogProps {
  postId: number;
  refreshData: () => void;
}

function ViewDetailsPostDialog({ postId, refreshData }: DetailPostDialogProps) {
  const { termId } = useParams<TermIdParam>();
  const [open, setOpen] = React.useState(false);
  const edit = t(messages.detail());

  const { data, run } = useAsync<Results<InternshipPost>>({
    data: {
      codeResults: {
        code: 0,
        message: '',
      },
      item: defaultPost,
    },
  });

  const post: InternshipPost = data?.item ?? defaultPost;
  const html = deltaToHTML(post.content);

  React.useEffect(() => {
    run(internshipPostApi.getDetailPost(postId, Number(termId)));
  }, [run, postId]);

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Tooltip title={edit}>
        <IconButton
          style={{
            color: theme.palette.primary.light,
          }}
          aria-label="edit"
          onClick={handleClickOpen}
          size="large"
        >
          <InfoOutlined />
        </IconButton>
      </Tooltip>
      <Dialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
        scroll="paper"
        maxWidth={'md'}
        style={{ marginTop: 60 }}
      >
        <DialogTitle id="customized-dialog-title" onClose={handleClose}>
          {t(messages.detailDialogTitle())}
        </DialogTitle>
        <div>
          <DialogContent dividers>
            <Grid
              direction="column"
              container
              alignItems="center"
              justifyContent="center"
              spacing={3}
              style={{ marginRight: 60 }}
            >
              <Grid
                item
                direction="row"
                container
                alignItems="center"
                justifyContent="center"
              >
                <Grid item xs={3}>
                  <Typography>{t(messages.title())}:</Typography>
                </Grid>
                <Grid item xs={9}>
                  <Typography sx={{ width: '50ch' }}>{post.title}</Typography>
                </Grid>
              </Grid>
              <Grid
                item
                direction="row"
                container
                alignItems="center"
                justifyContent="center"
              >
                <Grid item xs={3}>
                  <Typography>{t(messages.partnerName())}:</Typography>
                </Grid>
                <Grid item xs={9}>
                  <Typography sx={{ width: '50ch' }}>
                    {post.partnerName}
                  </Typography>
                </Grid>
              </Grid>
              <Grid
                item
                direction="row"
                container
                alignItems="center"
                justifyContent="center"
              >
                <Grid item xs={3}>
                  <Typography>{t(messages.createdAt())}:</Typography>
                </Grid>
                <Grid item xs={9}>
                  <Typography sx={{ width: '50ch' }}>
                    {formatDate(post.createdAt)}
                  </Typography>
                </Grid>
              </Grid>
              <Grid
                item
                direction="row"
                container
                alignItems="center"
                justifyContent="center"
              >
                <Grid item xs={3}>
                  <Typography>{t(messages.startRegAt())}:</Typography>
                </Grid>
                <Grid item xs={9}>
                  <Typography sx={{ width: '50ch' }}>
                    {formatDate(post.startRegAt)}
                  </Typography>
                </Grid>
              </Grid>
              <Grid
                item
                direction="row"
                container
                alignItems="center"
                justifyContent="center"
              >
                <Grid item xs={3}>
                  <Typography>{t(messages.endRegAt())}:</Typography>
                </Grid>
                <Grid item xs={9}>
                  <Typography sx={{ width: '50ch' }}>
                    {formatDate(post.endRegAt)}
                  </Typography>
                </Grid>
              </Grid>
              <Grid
                item
                direction="row"
                container
                alignItems="center"
                justifyContent="center"
              >
                <Grid item xs={3}>
                  <Typography>{t(messages.jobCount())}:</Typography>
                </Grid>
                <Grid item xs={9}>
                  <Typography sx={{ width: '50ch' }}>
                    {post.jobCount}
                  </Typography>
                </Grid>
              </Grid>
              <Grid
                item
                direction="row"
                container
                alignItems="start"
                justifyContent="center"
              >
                <Grid item xs={3}>
                  <Typography>{t(messages.content())}:</Typography>
                </Grid>
                <Grid item xs={9}>
                  <ReactQuillWrapper>
                    <div
                      dangerouslySetInnerHTML={{ __html: html }}
                      className="react-quill-style"
                    ></div>
                  </ReactQuillWrapper>
                </Grid>
              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions>
            <Stack direction="row" spacing={1}>
              <EditPostDialog post={post} refreshData={refreshData} />
              <Button autoFocus onClick={handleClose} variant="contained">
                {t(messages.close())}
              </Button>
            </Stack>
          </DialogActions>
        </div>
      </Dialog>
    </div>
  );
}
export default React.memo(ViewDetailsPostDialog);

const ReactQuillWrapper = styled.div`
  width: '50ch';
  height: '40ch';
  .react-quill-style img {
    height: 100%;
    width: 100%;
    object-fit: contain;
  }
`;

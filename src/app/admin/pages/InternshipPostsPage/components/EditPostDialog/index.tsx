import Edit from '@mui/icons-material/Edit';
import { DateTimePicker } from '@mui/lab';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import Grid from '@mui/material/Grid';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import internshipPostApi from 'api/internship/internshipPostApi';
import {
  DialogActions,
  DialogContent,
  DialogTitle,
} from 'app/components/CustomDialog';
import { TextEditor } from 'app/components/TextEditor/TextEditor';
import { parseISO } from 'date-fns';
import { useFormik } from 'formik';
import { t } from 'locales/i18n';
import { TermIdParam } from 'models';
import { InternshipPost } from 'models/internshipPost';
import { useSnackbar } from 'notistack';
import React, { useState } from 'react';
import 'react-phone-number-input/style.css';
import 'react-quill/dist/quill.snow.css';
import { useParams } from 'react-router-dom';
import { convertUTCDateToLocalDate } from 'utils/convertTime';
import * as Yup from 'yup';
import { messages } from '../../messages';
import { deltaToHTML } from '../deltaToHTML';
import SelectContact from '../SelectContact';

export interface DefaultPost {
  title: string;
  partnerId?: number;
  partnerContactId: number;
  startRegAt: Date;
  endRegAt: Date;
  jobCount: number;
  content: string;
}

interface EditPostDialogProps {
  post: InternshipPost;
  refreshData: () => void;
}

function EditPostDialog({ post, refreshData }: EditPostDialogProps) {
  const { termId } = useParams<TermIdParam>();
  const [open, setOpen] = React.useState(false);
  const {
    id,
    title,
    partnerId,
    partnerContactId,
    startRegAt,
    endRegAt,
    jobCount,
    content,
  } = post;
  const [selectedContactId, setSelectedContactId] = useState<number>();

  const { enqueueSnackbar } = useSnackbar();
  const html = deltaToHTML(content);

  const EditPostSchema = Yup.object().shape({
    title: Yup.string()
      .min(2, t(messages.tooShort()))
      .max(50, t(messages.tooLong()))
      .required(t(messages.required())),
    startRegAt: Yup.date().required(t(messages.required())),
    endRegAt: Yup.date().required(t(messages.required())),
    jobCount: Yup.number().integer().min(1).required(t(messages.required())),
  });

  const formik = useFormik<DefaultPost>({
    initialValues: {
      title: title,
      startRegAt: parseISO(startRegAt),
      endRegAt: parseISO(endRegAt),
      jobCount: jobCount,
      content: html,
      partnerContactId: partnerContactId,
    },
    validationSchema: EditPostSchema,
    onSubmit: values => {
      internshipPostApi
        .updatePost(id, Number(termId), {
          ...values,
          startRegAt: convertUTCDateToLocalDate(new Date(values.startRegAt)),
          endRegAt: convertUTCDateToLocalDate(new Date(values.endRegAt)),
          jobCount: Number(values.jobCount),
          partnerContactId: selectedContactId!,
        })
        .then(res => {
          enqueueSnackbar(t(messages.editPostSuccess()), {
            variant: 'success',
          });
          handleClose();
          refreshData();
        })
        .catch(e => {
          enqueueSnackbar(e.response?.data.message, { variant: 'error' });
        });
    },
  });

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  const handleChangeContact = (contactId: number) => {
    setSelectedContactId(contactId);
  };

  return (
    <div>
      <Button
        variant="outlined"
        size="medium"
        onClick={handleClickOpen}
        startIcon={<Edit />}
      >
        {t(messages.edit())}
      </Button>
      <Dialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
        scroll="paper"
        maxWidth={'md'}
        style={{ marginTop: 60 }}
      >
        <DialogTitle id="customized-dialog-title" onClose={handleClose}>
          {t(messages.editDialogTitle())}
        </DialogTitle>
        <div>
          <form onSubmit={formik.handleSubmit}>
            <DialogContent dividers>
              <Grid
                direction="column"
                container
                alignItems="center"
                justifyContent="center"
                spacing={1}
                style={{ marginRight: 60 }}
              >
                <Grid
                  item
                  direction="row"
                  container
                  alignItems="center"
                  justifyContent="center"
                >
                  <Grid item xs={3}>
                    <Typography>{t(messages.title())}:</Typography>
                  </Grid>
                  <Grid item xs={9}>
                    <TextField
                      size="small"
                      variant="outlined"
                      style={{ width: '50ch' }}
                      id="title"
                      name="title"
                      value={formik.values.title}
                      onChange={formik.handleChange}
                      error={
                        formik.touched.title && Boolean(formik.errors.title)
                      }
                      helperText={formik.touched.title && formik.errors.title}
                    />
                  </Grid>
                </Grid>
                <Grid
                  item
                  direction="row"
                  container
                  alignItems="center"
                  justifyContent="center"
                >
                  <Grid item xs={3}>
                    <Typography>{t(messages.partnerContactName())}:</Typography>
                  </Grid>
                  <Grid item xs={9}>
                    <SelectContact
                      handleChangeContact={handleChangeContact}
                      partnerId={partnerId}
                    />
                  </Grid>
                </Grid>
                <Grid
                  item
                  direction="row"
                  container
                  alignItems="center"
                  justifyContent="center"
                >
                  <Grid item xs={3}>
                    <Typography>{t(messages.startRegAt())}:</Typography>
                  </Grid>
                  <Grid item xs={9}>
                    <DateTimePicker
                      value={formik.values.startRegAt}
                      onChange={value => {
                        formik.setFieldValue('startRegAt', value);
                      }}
                      maxDate={formik.values.endRegAt}
                      renderInput={params => (
                        <TextField
                          {...params}
                          size="small"
                          style={{ width: '50ch' }}
                          placeholder={t(messages.startRegAt())}
                          error={
                            formik.touched.startRegAt &&
                            Boolean(formik.errors.startRegAt)
                          }
                          helperText={
                            formik.touched.startRegAt &&
                            formik.errors.startRegAt
                          }
                        />
                      )}
                    />
                  </Grid>
                </Grid>
                <Grid
                  item
                  direction="row"
                  container
                  alignItems="center"
                  justifyContent="center"
                >
                  <Grid item xs={3}>
                    <Typography>{t(messages.endRegAt())}:</Typography>
                  </Grid>
                  <Grid item xs={9}>
                    <DateTimePicker
                      value={formik.values.endRegAt}
                      onChange={value => {
                        formik.setFieldValue('endRegAt', value);
                      }}
                      minDate={formik.values.startRegAt}
                      renderInput={params => (
                        <TextField
                          {...params}
                          size="small"
                          style={{ width: '50ch' }}
                          placeholder={t(messages.endRegAt())}
                          error={
                            formik.touched.endRegAt &&
                            Boolean(formik.errors.endRegAt)
                          }
                          helperText={
                            formik.touched.endRegAt && formik.errors.endRegAt
                          }
                        />
                      )}
                    />
                  </Grid>
                </Grid>
                <Grid
                  item
                  direction="row"
                  container
                  alignItems="center"
                  justifyContent="center"
                >
                  <Grid item xs={3}>
                    <Typography>{t(messages.jobCount())}:</Typography>
                  </Grid>
                  <Grid item xs={9}>
                    <TextField
                      size="small"
                      variant="outlined"
                      style={{ width: '50ch' }}
                      id="jobCount"
                      name="jobCount"
                      value={formik.values.jobCount}
                      onChange={formik.handleChange}
                      error={
                        formik.touched.jobCount &&
                        Boolean(formik.errors.jobCount)
                      }
                      helperText={
                        formik.touched.jobCount && formik.errors.jobCount
                      }
                    />
                  </Grid>
                </Grid>
                <Grid
                  item
                  direction="row"
                  container
                  alignItems="start"
                  justifyContent="center"
                >
                  <Grid item xs={3}>
                    <Typography>{t(messages.content())}:</Typography>
                  </Grid>
                  <Grid item xs={9}>
                    <div style={{ width: '50ch' }}>
                      <TextEditor
                        defaultValue={formik.values.content}
                        onChange={(value, delta, source, editor) => {
                          formik.setFieldValue(
                            'content',
                            JSON.stringify(editor.getContents()),
                          );
                        }}
                      />
                    </div>
                  </Grid>
                </Grid>
              </Grid>
            </DialogContent>
            <DialogActions>
              <Button autoFocus variant="contained" type="submit">
                {t(messages.save())}
              </Button>
            </DialogActions>
          </form>
        </div>
      </Dialog>
    </div>
  );
}
export default React.memo(EditPostDialog);

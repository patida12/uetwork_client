import LazyLoading from 'app/components/LazyLoading/lazyLoading.component';
import { lazyLoad } from 'utils/loadable';

export const InternshipPostsPage = lazyLoad(
  () => import('./index'),
  module => module.InternshipPostsPage,
  {
    fallback: <LazyLoading />,
  },
);

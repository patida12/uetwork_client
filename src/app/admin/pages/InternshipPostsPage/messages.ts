import { translations } from 'locales/translations';
import { _t } from 'utils/messages';

export const messages = {
  title: () => _t(translations.internship.postFeature.title),
  partnerName: () => _t(translations.partnerFeature.name),
  partnerContactName: () => _t(translations.partnerFeature.partnerContactName),
  partnerContactEmail: () =>
    _t(translations.partnerFeature.partnerContactEmail),
  partnerContactPhoneNumber: () =>
    _t(translations.partnerFeature.partnerContactPhoneNumber),
  createdAt: () => _t(translations.internship.postFeature.createdAt),
  startRegAt: () => _t(translations.internship.postFeature.startRegAt),
  endRegAt: () => _t(translations.internship.postFeature.endRegAt),
  jobCount: () => _t(translations.internship.postFeature.jobCount),
  numOfApplications: () =>
    _t(translations.internship.postFeature.numOfApplications),
  search: () => _t(translations.common.table.search),
  option: () => _t(translations.common.table.option),
  content: () => _t(translations.internship.postFeature.content),
  add: () => _t(translations.common.button.add),
  edit: () => _t(translations.common.button.edit),
  detail: () => _t(translations.common.button.detail),
  delete: () => _t(translations.common.button.delete),
  save: () => _t(translations.common.button.save),
  close: () => _t(translations.common.button.close),
  addDialogTitle: () => _t(translations.internship.postFeature.addPost),
  editDialogTitle: () => _t(translations.internship.postFeature.editPost),
  detailDialogTitle: () => _t(translations.internship.postFeature.detailPost),
  deleteDialogTitle: () => _t(translations.internship.postFeature.deletePost),
  required: () => _t(translations.common.validate.required),
  tooShort: () => _t(translations.common.validate.tooShort),
  tooLong: () => _t(translations.common.validate.tooLong),
  invalidEmail: () => _t(translations.common.validate.invalidEmail),
  placeholderQuill: () => _t(translations.common.quill.placeholder),
  keyword: () => _t(translations.common.table.keyword),
  editPostSuccess: () =>
    _t(translations.internship.postFeature.editPostSuccess),
  addPostSuccess: () => _t(translations.internship.postFeature.addPostSuccess),
  cancel: () => _t(translations.common.button.cancel),
  basicInfo: () => _t(translations.internship.postFeature.basicInfo),
};

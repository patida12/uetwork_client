import { GridSortModel } from '@mui/x-data-grid';
import internshipPostApi from 'api/internship/internshipPostApi';
import { PaginatedResults, QueryParameters } from 'api/types';
import { DEFAULT_PAGE, DEFAULT_ROWS_PER_PAGE } from 'app/constants';
import { useAsync } from 'app/hooks/useAsync';
import { FilterState, TermIdParam } from 'models';
import { InternshipPost } from 'models/internshipPost';
import * as React from 'react';
import { useParams } from 'react-router-dom';
import theme from 'styles/theme';
import {
  NumberParam,
  ObjectParam,
  StringParam,
  useQueryParams,
  withDefault,
} from 'use-query-params';
import { sortModelToQuery } from 'utils';
import DataTable from '../../components/DataGrid';
import { getColumns } from './custom-data-grid-posts/ColumnDefinition';
import CustomToolbar from './custom-data-grid-posts/CustomToolbar';

export function InternshipPostsPage() {
  const { termId } = useParams<TermIdParam>();
  const { loading, data, run } = useAsync<PaginatedResults<InternshipPost>>({
    data: {
      totalItems: 0,
      currentPage: 1,
      totalPages: 0,
      perPage: 0,
      items: [],
    },
  });

  const [query, setQuery] = useQueryParams({
    page: withDefault(NumberParam, DEFAULT_PAGE),
    perPage: withDefault(NumberParam, DEFAULT_ROWS_PER_PAGE),
    q: withDefault(StringParam, ''),
    sort: withDefault(ObjectParam, {}),
    filter: withDefault(ObjectParam, {}),
  });

  const { page, perPage, sort } = query;

  const onPageChange = (page: number) => {
    setQuery({
      page,
    });
  };

  const onPageSizeChange = (pageSize: number) => {
    setQuery({
      perPage: pageSize,
      page: DEFAULT_PAGE,
    });
  };

  const handleSortModelChange = (model: GridSortModel) => {
    if (model.length === 0) {
      setQuery({
        sort: {},
      });
      return;
    }
    const firstElem = model[0];

    setQuery({
      sort: {
        ...sort,
        [firstElem.field]: [firstElem.sort],
      },
    });
  };

  const handleFilterChange = (newFilter: FilterState) => {
    setQuery({
      q: newFilter.query,
      filter: newFilter.filter,
    });
  };

  const refreshData = (): void => {
    run(internshipPostApi.getAllPosts(getQueryObject(query), Number(termId)));
  };
  const columns = React.useMemo(() => getColumns(refreshData), [refreshData]);

  const rows: InternshipPost[] = data?.items ?? [];
  const totalItems: number = data?.totalItems ?? 0;

  React.useEffect(() => {
    run(internshipPostApi.getAllPosts(getQueryObject(query), Number(termId)));
  }, [query]);

  return (
    <div style={{ height: 700, width: '100%', marginTop: theme.spacing(4) }}>
      <DataTable
        rows={rows}
        columns={columns}
        pageSize={perPage}
        rowCount={totalItems}
        loading={loading}
        page={page}
        customToolbar={CustomToolbar}
        onPageSizeChange={onPageSizeChange}
        onPageChange={onPageChange}
        onSortModelChange={handleSortModelChange}
        onFilterChange={handleFilterChange}
        refreshData={refreshData}
      />
    </div>
  );
}

const getQueryObject = (qObj: any): QueryParameters => {
  const { page, perPage, sort, q, filter } = qObj;
  const gsq: QueryParameters = {
    page,
    perPage,
    q,
    filter,
  };
  const gsqSort: string[] = sortModelToQuery(sort);
  gsq.sort = gsqSort;
  return gsq;
};

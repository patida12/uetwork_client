import { EditOutlined } from '@mui/icons-material';
import { IconButton } from '@mui/material';
import { GridCellParams, GridColumns } from '@mui/x-data-grid';
import GridCellExpand from 'app/components/GridCellExpand';
import { t } from 'locales/i18n';
import { Link } from 'react-router-dom';
import theme from 'styles/theme';
import { formatDate } from 'utils';
import { messages } from '../messages';

export const getColumns = (refreshData?: () => void): GridColumns => [
  {
    field: 'id',
    sortable: false,
    disableColumnMenu: true,
    hide: true,
  },
  {
    field: 'title',
    headerName: t(messages.title()),
    width: 250,
    renderCell: (params: GridCellParams) => (
      <GridCellExpand
        value={params.value ? params.value.toString() : ''}
        width={params.colDef.width}
      />
    ),
    disableColumnMenu: true,
  },
  {
    field: 'partnerName',
    headerName: t(messages.partnerName()),
    width: 250,
    renderCell: (params: GridCellParams) => (
      <GridCellExpand
        value={params.value ? params.value.toString() : ''}
        width={params.colDef.width}
      />
    ),
    disableColumnMenu: true,
    sortable: false,
  },
  {
    field: 'partnerContactName',
    headerName: t(messages.partnerContactName()),
    width: 250,
    renderCell: (params: GridCellParams) => (
      <GridCellExpand
        value={params.value ? params.value.toString() : ''}
        width={params.colDef.width}
      />
    ),
    disableColumnMenu: true,
    sortable: false,
  },
  {
    field: 'partnerContactEmail',
    headerName: t(messages.partnerContactEmail()),
    width: 250,
    renderCell: (params: GridCellParams) => (
      <GridCellExpand
        value={params.value ? params.value.toString() : ''}
        width={params.colDef.width}
      />
    ),
    disableColumnMenu: true,
    sortable: false,
  },
  {
    field: 'partnerContactPhoneNumber',
    headerName: t(messages.partnerContactPhoneNumber()),
    width: 250,
    renderCell: (params: GridCellParams) => (
      <GridCellExpand
        value={params.value ? params.value.toString() : ''}
        width={params.colDef.width}
      />
    ),
    disableColumnMenu: true,
    sortable: false,
  },
  {
    field: 'createdAt',
    headerName: t(messages.createdAt()),
    width: 200,
    renderCell: (params: GridCellParams) => (
      <GridCellExpand
        value={params.value ? formatDate(params.value.toString()) : ''}
        width={params.colDef.width}
      />
    ),
    disableColumnMenu: true,
  },
  {
    field: 'startRegAt',
    headerName: t(messages.startRegAt()),
    width: 200,
    renderCell: (params: GridCellParams) => (
      <GridCellExpand
        value={params.value ? formatDate(params.value.toString()) : ''}
        width={params.colDef.width}
      />
    ),
    disableColumnMenu: true,
    sortable: false,
  },
  {
    field: 'endRegAt',
    headerName: t(messages.endRegAt()),
    width: 200,
    renderCell: (params: GridCellParams) => (
      <GridCellExpand
        value={params.value ? formatDate(params.value.toString()) : ''}
        width={params.colDef.width}
      />
    ),
    disableColumnMenu: true,
    sortable: false,
  },
  {
    field: 'jobCount',
    headerName: t(messages.jobCount()),
    width: 200,
    renderCell: (params: GridCellParams) => (
      <GridCellExpand
        value={params.value ? params.value.toString() : '0'}
        width={params.colDef.width}
      />
    ),
    disableColumnMenu: true,
    sortable: false,
  },
  {
    field: 'options',
    headerName: t(messages.option()),
    width: 150,
    sortable: false,
    disableColumnMenu: true,
    renderCell: (params: GridCellParams) => (
      <IconButton
        style={{
          color: theme.palette.primary.light,
        }}
        size="large"
        component={Link}
        to={`edit-post/${params.row.id}`}
      >
        <EditOutlined />
      </IconButton>
    ),
  },
];

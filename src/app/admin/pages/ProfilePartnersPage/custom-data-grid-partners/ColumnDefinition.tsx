import { CheckCircle } from '@mui/icons-material';
import { Tooltip } from '@mui/material';
import Grid from '@mui/material/Grid';
import Link from '@mui/material/Link';
import { GridCellParams, GridColumns } from '@mui/x-data-grid';
import GridCellExpand from 'app/components/GridCellExpand';
import { t } from 'locales/i18n';
import * as React from 'react';
import theme from 'styles/theme';
import ShowContactsDialog from '../components/ShowContactsDialog';
import ViewDetailsPartnerDialog from '../components/ViewDetailsPartnerDialog';
import { messages } from '../messages';
interface IsAssociatedProps {
  isAssociate: boolean;
}

export function IsAssociated(props: IsAssociatedProps) {
  const associated = t(messages.associatedCompanies());

  if (props.isAssociate === true) {
    return (
      <Tooltip title={associated}>
        <CheckCircle
          fontSize="small"
          style={{ color: theme.palette.success.main }}
        />
      </Tooltip>
    );
  }
  return <></>;
}

function getColumns(): GridColumns {
  return [
    {
      field: 'id',
      sortable: false,
      disableColumnMenu: true,
      hide: true,
      filterable: false,
    },
    {
      field: 'name',
      headerName: t(messages.partnerName()),
      width: 450,
      renderCell: (params: GridCellParams) => (
        <Grid container>
          <Grid item xs={11}>
            <GridCellExpand
              value={params.value ? params.value.toString() : ''}
              width={params.colDef.width}
            />
          </Grid>
          <Grid>
            <IsAssociated isAssociate={Boolean(params.row.isAssociate)} />
          </Grid>
        </Grid>
      ),
      filterable: true,
    },
    {
      field: 'homepageUrl',
      headerName: t(messages.homepageUrl()),
      width: 350,
      renderCell: (params: GridCellParams) => (
        <Link
          href={`${params.value}`}
          target="_blank"
          rel="noreferrer"
          variant="body2"
        >
          {params.value}
        </Link>
      ),
      filterable: true,
      sortable: false,
    },
    {
      field: 'isAssociate',
      headerName: t(messages.status()),
      width: 0,
      sortable: false,
      disableColumnMenu: true,
      filterable: false,
      hide: true,
    },
    {
      field: 'contactsList',
      headerName: t(messages.contact()),
      width: 150,
      renderCell: (params: GridCellParams) => (
        <strong>
          <ShowContactsDialog partner={params.row} />
        </strong>
      ),
      sortable: false,
      disableColumnMenu: true,
      filterable: false,
    },
    {
      field: 'options',
      headerName: t(messages.option()),
      width: 150,
      sortable: false,
      filterable: false,
      disableColumnMenu: true,
      renderCell: (params: GridCellParams) => (
        <ViewDetailsPartnerDialog partner={params.row} />
      ),
    },
  ];
}

export { getColumns };

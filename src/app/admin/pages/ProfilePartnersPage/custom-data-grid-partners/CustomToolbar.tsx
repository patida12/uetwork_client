import SearchIcon from '@mui/icons-material/Search';
import { Button } from '@mui/material';
import FormControl from '@mui/material/FormControl';
import Grid from '@mui/material/Grid';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import Select from '@mui/material/Select';
import { styled } from '@mui/material/styles';
import TextField from '@mui/material/TextField';
import { GridToolbarContainer } from '@mui/x-data-grid';
import { TRUE_FALSE_OPTIONS } from 'app/constants';
import { useFormik } from 'formik';
import { t } from 'locales/i18n';
import { CustomToolbarProps, FilterState } from 'models';
import React from 'react';
import { messages } from '../messages';

const PREFIX = 'CustomToolbar';

const classes = {
  textField: `${PREFIX}-textField`,
};

const StyledGridToolbarContainer = styled(GridToolbarContainer)(
  ({ theme }) => ({
    [`& .${classes.textField}`]: {
      width: '25ch',
    },
  }),
);

function CustomToolbar({ onFilterChange, refreshData }: CustomToolbarProps) {
  const formik = useFormik<FilterState>({
    initialValues: {
      query: '',
      filter: {
        isAssociate: '',
      },
    },
    onSubmit: values => {
      var filters: FilterState = {
        query: values.query,
        filter: {
          isAssociate:
            TRUE_FALSE_OPTIONS.find(
              type => type.key === values.filter.isAssociate,
            )?.value ?? '',
        },
      };
      onFilterChange(filters);
    },
  });
  return (
    <form onSubmit={formik.handleSubmit}>
      <StyledGridToolbarContainer style={{ margin: 10 }}>
        <Grid
          direction="row"
          container
          alignContent="flex-end"
          justifyContent="flex-start"
          spacing={1}
        >
          <Grid item>
            <FormControl
              variant="outlined"
              style={{ minWidth: 250 }}
              size="small"
            >
              <InputLabel id="select-partners-label">
                {t(messages.filterCompany())}
              </InputLabel>
              <Select
                labelId="select-partners-label"
                id="filter.isAssociate"
                name="filter.isAssociate"
                label={t(messages.filterCompany())}
                value={formik.values.filter.isAssociate}
                onChange={formik.handleChange}
              >
                <MenuItem
                  key={TRUE_FALSE_OPTIONS[0].key}
                  value={TRUE_FALSE_OPTIONS[0].key}
                >
                  {t(messages.allCompanies())}
                </MenuItem>
                <MenuItem
                  key={TRUE_FALSE_OPTIONS[1].key}
                  value={TRUE_FALSE_OPTIONS[1].key}
                >
                  {t(messages.associatedCompanies())}
                </MenuItem>
                <MenuItem
                  key={TRUE_FALSE_OPTIONS[2].key}
                  value={TRUE_FALSE_OPTIONS[2].key}
                >
                  {t(messages.otherCompanies())}
                </MenuItem>
              </Select>
            </FormControl>
          </Grid>
          <Grid item>
            <TextField
              value={formik.values.query}
              onChange={formik.handleChange}
              id="query"
              className={classes.textField}
              size="small"
              variant="outlined"
              rows={1}
              label={t(messages.keyword())}
            />
          </Grid>
          <Grid item>
            <Button
              variant="contained"
              type="submit"
              startIcon={<SearchIcon />}
            >
              {t(messages.search())}
            </Button>
          </Grid>
        </Grid>
      </StyledGridToolbarContainer>
    </form>
  );
}
export default React.memo(CustomToolbar);

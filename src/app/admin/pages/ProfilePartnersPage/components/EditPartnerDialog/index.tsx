import { EditOutlined } from '@mui/icons-material';
import CloseIcon from '@mui/icons-material/Close';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import MuiDialogActions from '@mui/material/DialogActions';
import MuiDialogContent from '@mui/material/DialogContent';
import MuiDialogTitle from '@mui/material/DialogTitle';
import Grid from '@mui/material/Grid';
import IconButton from '@mui/material/IconButton';
import { Theme } from '@mui/material/styles';
import TextField from '@mui/material/TextField';
import Tooltip from '@mui/material/Tooltip';
import Typography from '@mui/material/Typography';
import { WithStyles } from '@mui/styles';
import createStyles from '@mui/styles/createStyles';
import withStyles from '@mui/styles/withStyles';
import { IdProps } from 'models';
import React from 'react';
import { useTranslation } from 'react-i18next';
import theme from 'styles/theme';
import { messages } from '../../messages';

const styles = (theme: Theme) =>
  createStyles({
    root: {
      margin: 0,
      padding: theme.spacing(2),
    },
    closeButton: {
      position: 'absolute',
      right: theme.spacing(1),
      top: theme.spacing(1),
      color: theme.palette.primary.main,
    },
  });

export interface DialogTitleProps extends WithStyles<typeof styles> {
  id: string;
  children: React.ReactNode;
  onClose: () => void;
}

const DialogTitle = withStyles(styles)((props: DialogTitleProps) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle className={classes.root} {...other}>
      <div>
        <Typography variant="h6">{children}</Typography>
      </div>
      {onClose ? (
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          onClick={onClose}
          size="large"
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme: Theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

const DialogActions = withStyles((theme: Theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);

function EditPartnerDialog(props: IdProps) {
  // const id = props.id; //TODO: use id to call api
  const row = {
    id: 1,
    name: 'aCong ty An Ninh Mang Viettel',
    homepageUrl: 'https://viettelcybersecurity.com',
    isAssociate: true,
    description: 'La cong ty an ninh mang ...',
    contacts: [
      {
        id: 0,
        fullName: 'string',
        phoneNumber: 'string',
      },
    ],
    address: 'Landmark 72 Pham Hung',
  };
  const [open, setOpen] = React.useState(false);

  const { t } = useTranslation();
  const edit = t(messages.edit());

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Tooltip title={edit}>
        <IconButton
          style={{
            color: theme.palette.primary.light,
          }}
          aria-label="edit"
          onClick={handleClickOpen}
          size="large"
        >
          <EditOutlined />
        </IconButton>
      </Tooltip>
      <Dialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
        scroll="paper"
        maxWidth={'md'}
        style={{ marginTop: 60 }}
      >
        <DialogTitle id="customized-dialog-title" onClose={handleClose}>
          {t(messages.editDialogTitle())}
        </DialogTitle>
        <DialogContent dividers>
          <Grid
            direction="column"
            container
            alignContent="center"
            justifyContent="center"
            spacing={1}
            style={{ marginRight: 60 }}
          >
            <Grid
              item
              direction="row"
              container
              alignContent="flex-start"
              justifyContent="center"
            >
              <Grid item xs={3}>
                <Typography>{t(messages.partnerName())}:</Typography>
              </Grid>
              <Grid item xs={9}>
                <TextField
                  size="small"
                  variant="outlined"
                  style={{ width: '50ch' }}
                  defaultValue={row.name}
                />
              </Grid>
            </Grid>
            <Grid
              item
              direction="row"
              container
              alignContent="flex-start"
              justifyContent="center"
            >
              <Grid item xs={3}>
                <Typography>{t(messages.homepageUrl())}:</Typography>
              </Grid>
              <Grid item xs={9}>
                <TextField
                  size="small"
                  variant="outlined"
                  style={{ width: '50ch' }}
                  defaultValue={row.homepageUrl}
                />
              </Grid>
            </Grid>
            <Grid
              item
              direction="row"
              container
              alignContent="flex-start"
              justifyContent="center"
            >
              <Grid item xs={3}>
                <Typography>{t(messages.description())}:</Typography>
              </Grid>
              <Grid item xs={9}>
                <TextField
                  size="small"
                  variant="outlined"
                  style={{ width: '50ch' }}
                  multiline
                  defaultValue={row.description}
                />
              </Grid>
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button autoFocus onClick={handleClose} variant="contained">
            {t(messages.save())}
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
export default React.memo(EditPartnerDialog);

import { InfoOutlined } from '@mui/icons-material';
import { Link } from '@mui/material';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import Grid from '@mui/material/Grid';
import IconButton from '@mui/material/IconButton';
import Tooltip from '@mui/material/Tooltip';
import Typography from '@mui/material/Typography';
import { DialogTitle } from 'app/components/CustomDialog';
import { t } from 'locales/i18n';
import { ProfilePartner } from 'models';
import React from 'react';
import theme from 'styles/theme';
import { formatDate } from 'utils';
import { messages } from '../../messages';

export const defaultPartner: ProfilePartner = {
  id: 0,
  name: '',
  homepageUrl: '',
  logoUrl: '',
  description: '',
  contacts: [],
  address: '',
  userId: 0,
  isAssociate: false,
};

interface ViewPartnerProps {
  partner: ProfilePartner;
}

function ViewDetailsPartnerDialog({ partner }: ViewPartnerProps) {
  const detail = t(messages.detail());
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Tooltip title={detail}>
        <IconButton
          style={{
            color: theme.palette.primary.light,
          }}
          aria-label="info"
          onClick={handleClickOpen}
          size="large"
        >
          <InfoOutlined />
        </IconButton>
      </Tooltip>
      <Dialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
        scroll="paper"
        maxWidth={'md'}
        style={{ marginTop: 60 }}
      >
        <DialogTitle id="customized-dialog-title" onClose={handleClose}>
          {t(messages.detailDialogTitle())}
        </DialogTitle>
        <DialogContent dividers>
          <Grid
            direction="column"
            container
            alignContent="center"
            justifyContent="center"
            spacing={2}
          >
            <Grid
              item
              direction="row"
              container
              alignContent="flex-start"
              justifyContent="center"
            >
              <Grid item xs={4}>
                <Typography variant="subtitle1">
                  {t(messages.partnerName())}:
                </Typography>
              </Grid>
              <Grid item xs={8}>
                <Typography sx={{ width: '40ch' }}>{partner.name}</Typography>
              </Grid>
            </Grid>
            <Grid
              item
              direction="row"
              container
              alignContent="flex-start"
              justifyContent="center"
            >
              <Grid item xs={4}>
                <Typography variant="subtitle1">
                  {t(messages.homepageUrl())}:
                </Typography>
              </Grid>
              <Grid item xs={8}>
                <Link
                  href={`${partner.homepageUrl}`}
                  target="_blank"
                  rel="noreferrer"
                  variant="body2"
                  sx={{ width: '40ch' }}
                >
                  {partner.homepageUrl}
                </Link>
              </Grid>
            </Grid>
            {partner.isAssociate && (
              <Grid
                item
                direction="row"
                container
                alignContent="flex-start"
                justifyContent="center"
              >
                <Grid item xs={4}>
                  <Typography variant="subtitle1">
                    {t(messages.associateExpirationDate())}:
                  </Typography>
                </Grid>
                <Grid item xs={8}>
                  <Typography sx={{ width: '40ch' }}>
                    {partner.associateExpirationDate
                      ? formatDate(partner.associateExpirationDate)
                      : ''}
                  </Typography>
                </Grid>
              </Grid>
            )}
            <Grid
              item
              direction="row"
              container
              alignContent="flex-start"
              justifyContent="center"
            >
              <Grid item xs={4}>
                <Typography variant="subtitle1">
                  {t(messages.description())}:
                </Typography>
              </Grid>
              <Grid item xs={8}>
                <Typography sx={{ width: '40ch' }}>
                  {partner.description}
                </Typography>
              </Grid>
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button
            autoFocus
            onClick={handleClose}
            variant="contained"
            sx={{ m: 1 }}
          >
            {t(messages.close())}
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
export default React.memo(ViewDetailsPartnerDialog);

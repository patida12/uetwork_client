import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import { Drawer, Grid } from '@mui/material';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import Tooltip from '@mui/material/Tooltip';
import Typography from '@mui/material/Typography';
import { t } from 'locales/i18n';
import { ProfilePartner } from 'models';
import { ContactInfo } from 'models/contact';
import React from 'react';
import DataTableContact from './component/DataTableContact';
import { messages } from './message';

interface ShowContactProps {
  partner: ProfilePartner;
}
function ShowContactsDialog({ partner }: ShowContactProps) {
  const contacts: ContactInfo[] = partner.contacts;

  const [state, setState] = React.useState(false);

  const showContactTitle = t(messages.showContactTitle());

  const toggleDrawer = () => (
    event: React.KeyboardEvent | React.MouseEvent,
  ) => {
    if (
      event &&
      event.type === 'keydown' &&
      ((event as React.KeyboardEvent).key === 'Tab' ||
        (event as React.KeyboardEvent).key === 'Shift')
    ) {
      return;
    }

    setState(!state);
  };

  return (
    <div>
      <React.Fragment>
        <Tooltip title={showContactTitle}>
          <Button aria-label="show" size="small" onClick={toggleDrawer()}>
            {t(messages.show())}
          </Button>
        </Tooltip>
        <Drawer anchor={'right'} open={state} onClose={toggleDrawer()}>
          <div style={{ margin: 12 }}>
            <Grid container>
              <Grid>
                <IconButton
                  edge="start"
                  color="inherit"
                  onClick={toggleDrawer()}
                  aria-label="close"
                  size="large"
                >
                  <ArrowBackIcon />
                </IconButton>
              </Grid>
              <Grid>
                <Typography variant="h6" component="h6">
                  <Box fontWeight="fontWeightMedium" mt={1}>
                    {partner.name}
                  </Box>
                </Typography>
              </Grid>
            </Grid>

            <DataTableContact contacts={contacts} />
          </div>
        </Drawer>
      </React.Fragment>
    </div>
  );
}
export default React.memo(ShowContactsDialog);

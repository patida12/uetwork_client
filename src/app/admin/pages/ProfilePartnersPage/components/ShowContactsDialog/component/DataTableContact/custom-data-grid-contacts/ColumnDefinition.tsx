import { GridCellParams, GridColumns } from '@mui/x-data-grid';
import GridCellExpand from 'app/components/GridCellExpand';
import { GRID_EXPAND_CELL_WIDTH } from 'app/constants';
import { t } from 'locales/i18n';
import React from 'react';
import { messages } from '../../../message';
import EditContactDialog from '../../EditContactDialog';

export const getColumns = (): GridColumns => [
  {
    field: 'id',
    sortable: false,
    disableColumnMenu: true,
    hide: true,
  },
  {
    field: 'fullName',
    headerName: t(messages.fullName()),
    width: 200,
    renderCell: (params: GridCellParams) => (
      <GridCellExpand
        value={params.value ? params.value.toString() : ''}
        width={params.colDef.width ?? GRID_EXPAND_CELL_WIDTH}
      />
    ),
  },
  {
    field: 'phoneNumber',
    headerName: t(messages.phoneNumber()),
    width: 200,
    renderCell: (params: GridCellParams) => (
      <GridCellExpand
        value={params.value ? params.value.toString() : ''}
        width={params.colDef.width ?? GRID_EXPAND_CELL_WIDTH}
      />
    ),
    sortable: false,
    disableColumnMenu: true,
  },
  {
    field: 'email',
    headerName: t(messages.workEmail()),
    width: 200,
    renderCell: (params: GridCellParams) => (
      <GridCellExpand
        value={params.value ? params.value.toString() : ''}
        width={params.colDef.width ?? GRID_EXPAND_CELL_WIDTH}
      />
    ),
    sortable: false,
    disableColumnMenu: true,
  },
  {
    field: 'options',
    headerName: t(messages.option()),
    width: 150,
    sortable: false,
    disableColumnMenu: true,
    renderCell: (params: GridCellParams) => (
      <strong>
        <EditContactDialog contact={params.row} />
      </strong>
    ),
  },
];

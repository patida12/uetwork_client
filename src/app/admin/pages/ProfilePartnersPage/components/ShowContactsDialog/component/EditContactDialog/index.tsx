import { EditOutlined } from '@mui/icons-material';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import Grid from '@mui/material/Grid';
import IconButton from '@mui/material/IconButton';
import TextField from '@mui/material/TextField';
import Tooltip from '@mui/material/Tooltip';
import Typography from '@mui/material/Typography';
import {
  DialogActions,
  DialogContent,
  DialogTitle,
} from 'app/components/CustomDialog';
import { PhoneInputCustom } from 'app/components/PhoneInput';
import { useFormik } from 'formik';
import { t } from 'locales/i18n';
import { ContactInfo } from 'models';
import React from 'react';
import 'react-phone-number-input/style.css';
import theme from 'styles/theme';
import * as Yup from 'yup';
import { messages } from '../../message';

interface EditContactProps {
  contact: ContactInfo;
}

function EditContactDialog({ contact }: EditContactProps) {
  const [open, setOpen] = React.useState(false);
  const edit = t(messages.edit());

  const EditContactSchema = Yup.object({
    fullName: Yup.string()
      .min(2, t(messages.tooShort()))
      .max(50, t(messages.tooLong()))
      .required(t(messages.required())),
    email: Yup.string()
      .email(t(messages.invalidEmail()))
      .required(t(messages.required())),
    phoneNumber: Yup.string().max(50, t(messages.tooLong())),
  });

  const formik = useFormik({
    initialValues: contact,
    validationSchema: EditContactSchema,
    onSubmit: values => {
      alert(JSON.stringify(values, null, 2)); //TODO:integrate api
    },
  });

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Tooltip title={edit}>
        <IconButton
          style={{
            color: theme.palette.primary.light,
          }}
          aria-label="edit"
          onClick={handleClickOpen}
          size="large"
        >
          <EditOutlined />
        </IconButton>
      </Tooltip>
      <Dialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
        scroll="paper"
        maxWidth={'md'}
        style={{ marginTop: 60 }}
      >
        <DialogTitle id="customized-dialog-title" onClose={handleClose}>
          {t(messages.editDialogTitle())}
        </DialogTitle>
        <div>
          <form onSubmit={formik.handleSubmit}>
            <DialogContent dividers>
              <Grid
                direction="column"
                container
                alignContent="center"
                justifyContent="center"
                spacing={1}
                style={{ marginRight: 60 }}
              >
                <Grid
                  item
                  direction="row"
                  container
                  alignContent="flex-start"
                  justifyContent="center"
                >
                  <Grid item xs={3}>
                    <Typography>{t(messages.fullName())}:</Typography>
                  </Grid>
                  <Grid item xs={9}>
                    <TextField
                      size="small"
                      variant="outlined"
                      style={{ width: '50ch' }}
                      id="fullName"
                      name="fullName"
                      value={formik.values.fullName}
                      onChange={formik.handleChange}
                      error={
                        formik.touched.fullName &&
                        Boolean(formik.errors.fullName)
                      }
                      helperText={
                        formik.touched.fullName && formik.errors.fullName
                      }
                    />
                  </Grid>
                </Grid>
                <Grid
                  item
                  direction="row"
                  container
                  alignContent="flex-start"
                  justifyContent="center"
                >
                  <Grid item xs={3}>
                    <Typography>{t(messages.phoneNumber())}:</Typography>
                  </Grid>
                  <Grid item xs={9}>
                    <PhoneInputCustom
                      international
                      style={{
                        width: '50ch',
                      }}
                      id="phoneNumber"
                      name="phoneNumber"
                      value={formik.values.phoneNumber}
                      onChange={(e: string) =>
                        formik.setFieldValue('phoneNumber', e)
                      }
                    />
                  </Grid>
                </Grid>
                <Grid
                  item
                  direction="row"
                  container
                  alignContent="flex-start"
                  justifyContent="center"
                >
                  <Grid item xs={3}>
                    <Typography>{t(messages.workEmail())}:</Typography>
                  </Grid>
                  <Grid item xs={9}>
                    <TextField
                      size="small"
                      variant="outlined"
                      style={{ width: '50ch' }}
                      id="workEmail"
                      name="workEmail"
                      value={formik.values.email}
                      onChange={formik.handleChange}
                      error={
                        formik.touched.email && Boolean(formik.errors.email)
                      }
                      helperText={formik.touched.email && formik.errors.email}
                      type="email"
                    />
                  </Grid>
                </Grid>
              </Grid>
            </DialogContent>
            <DialogActions>
              <Button autoFocus variant="contained" type="submit">
                {t(messages.save())}
              </Button>
            </DialogActions>
          </form>
        </div>
      </Dialog>
    </div>
  );
}
export default React.memo(EditContactDialog);

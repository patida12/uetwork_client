import { DataGrid } from '@mui/x-data-grid/DataGrid';
import { ContactInfo } from 'models/contact';
import * as React from 'react';
import theme from 'styles/theme';
import { getColumns } from './custom-data-grid-contacts/ColumnDefinition';
import QuickSearchToolbar from './custom-data-grid-contacts/CustomToolbar';

function escapeRegExp(value: string): string {
  return value.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&');
}

interface DataContactsProps {
  contacts: ContactInfo[];
}

export default function DataTableContact({ contacts }: DataContactsProps) {
  const columns = getColumns();
  const [searchText, setSearchText] = React.useState('');
  const [rows, setRows] = React.useState<any[]>(contacts);

  const requestSearch = (searchValue: string) => {
    setSearchText(searchValue);
    const searchRegex = new RegExp(escapeRegExp(searchValue), 'i');
    const filteredRows = contacts.filter((row: any) => {
      return Object.keys(row).some((field: any) => {
        return searchRegex.test(row[field].toString());
      });
    });
    setRows(filteredRows);
  };

  React.useEffect(() => {
    setRows(contacts);
  }, [contacts]);

  return (
    <div style={{ height: 700, width: 700, marginTop: theme.spacing(4) }}>
      <DataGrid
        hideFooter
        rows={rows}
        columns={columns}
        components={{ Toolbar: QuickSearchToolbar }}
        componentsProps={{
          toolbar: {
            value: searchText,
            onChange: (event: React.ChangeEvent<HTMLInputElement>) =>
              requestSearch(event.target.value),
            clearSearch: () => requestSearch(''),
          },
        }}
      />
    </div>
  );
}

import ClearIcon from '@mui/icons-material/Clear';
import SearchIcon from '@mui/icons-material/Search';
import { IconButton, styled } from '@mui/material';
import Grid from '@mui/material/Grid';
import TextField from '@mui/material/TextField';
import { GridToolbarContainer } from '@mui/x-data-grid';
import { t } from 'locales/i18n';
import * as React from 'react';
import { messages } from '../../../message';
import AddContactDialog from '../../AddContactDialog';

const PREFIX = 'CustomToolbar';

const classes = {
  textField: `${PREFIX}-textField`,
};

const StyledGridToolbarContainer = styled(GridToolbarContainer)(
  ({ theme: Theme }) => ({
    [`& .${classes.textField}`]: {
      width: '50ch',
    },
  }),
);

interface QuickSearchToolbarProps {
  clearSearch: () => void;
  onChange: () => void;
  value: string;
}

function QuickSearchToolbar(props: QuickSearchToolbarProps) {
  return (
    <StyledGridToolbarContainer style={{ margin: 8 }}>
      <Grid
        container
        direction="row"
        justifyContent="space-between"
        alignContent="flex-end"
        spacing={1}
      >
        <Grid item>
          <TextField
            value={props.value}
            onChange={props.onChange}
            id="query"
            className={classes.textField}
            size="small"
            variant="outlined"
            rows={1}
            label={t(messages.keyword())}
            InputProps={{
              startAdornment: <SearchIcon fontSize="small" />,
              endAdornment: (
                <IconButton
                  title="Clear"
                  aria-label="Clear"
                  size="small"
                  style={{ visibility: props.value ? 'visible' : 'hidden' }}
                  onClick={props.clearSearch}
                >
                  <ClearIcon fontSize="small" />
                </IconButton>
              ),
            }}
          />
        </Grid>
        <Grid item>
          <AddContactDialog />
        </Grid>
      </Grid>
    </StyledGridToolbarContainer>
  );
}
export default React.memo(QuickSearchToolbar);

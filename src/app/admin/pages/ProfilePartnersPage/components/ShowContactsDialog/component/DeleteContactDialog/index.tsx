import { Delete } from '@mui/icons-material';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import React from 'react';
import { useTranslation } from 'react-i18next';
import theme from 'styles/theme';
import { messages } from '../../message';

function DeleteContactDialog() {
  const [open, setOpen] = React.useState(false);

  const { t } = useTranslation();

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Button
        variant="contained"
        size="small"
        style={{
          backgroundColor: theme.palette.error.main,
        }}
        onClick={handleClickOpen}
        startIcon={<Delete />}
      >
        {t(messages.delete())}
      </Button>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          <Delete
            fontSize="small"
            style={{
              color: theme.palette.error.main,
              marginRight: theme.spacing(1),
              marginBottom: theme.spacing(0.5),
            }}
          />
          {t(messages.deleteDialogTitle())}
        </DialogTitle>
        <DialogContent dividers>
          <DialogContentText id="alert-dialog-description">
            {t(messages.confirmDelete())}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button
            onClick={handleClose}
            style={{ color: theme.palette.error.main }}
          >
            {t(messages.delete())}
          </Button>
          <Button onClick={handleClose} variant="contained" autoFocus>
            {t(messages.cancel())}
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
export default React.memo(DeleteContactDialog);

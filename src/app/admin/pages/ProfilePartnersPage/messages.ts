import { translations } from 'locales/translations';
import { _t } from 'utils/messages';

export const messages = {
  partnerId: () => _t(translations.partnerFeature.id),
  partnerName: () => _t(translations.partnerFeature.name),
  homepageUrl: () => _t(translations.partnerFeature.homepageUrl),
  status: () => _t(translations.partnerFeature.status),
  contact: () => _t(translations.partnerFeature.contact),
  option: () => _t(translations.common.table.option),
  description: () => _t(translations.partnerFeature.description),
  search: () => _t(translations.common.table.search),
  filterCompany: () => _t(translations.common.app.company),
  allCompanies: () => _t(translations.partnerFeature.allCompanies),
  associatedCompanies: () =>
    _t(translations.partnerFeature.associatedCompanies),
  otherCompanies: () => _t(translations.partnerFeature.otherCompanies),
  add: () => _t(translations.common.button.add),
  edit: () => _t(translations.common.button.edit),
  detail: () => _t(translations.common.button.detail),
  save: () => _t(translations.common.button.save),
  close: () => _t(translations.common.button.close),
  addDialogTitle: () => _t(translations.partnerFeature.addPartner),
  editDialogTitle: () => _t(translations.partnerFeature.editPartner),
  detailDialogTitle: () => _t(translations.partnerFeature.detailPartner),
  required: () => _t(translations.common.validate.required),
  tooShort: () => _t(translations.common.validate.tooShort),
  tooLong: () => _t(translations.common.validate.tooLong),
  invalidEmail: () => _t(translations.common.validate.invalidEmail),
  keyword: () => _t(translations.common.table.keyword),
  associateExpirationDate: () =>
    _t(translations.partnerFeature.associateExpirationDate),
};

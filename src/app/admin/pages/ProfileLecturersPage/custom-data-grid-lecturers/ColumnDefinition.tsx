import { GridCellParams, GridColumns } from '@mui/x-data-grid';
import GridCellExpand from 'app/components/GridCellExpand';
import { t } from 'locales/i18n';
import { messages } from '../messages';

export const getColumns = (): GridColumns => [
  {
    field: 'id',
    sortable: false,
    disableColumnMenu: true,
    hide: true,
  },
  {
    field: 'fullName',
    headerName: t(messages.fullName()),
    width: 250,
    renderCell: (params: GridCellParams) => (
      <GridCellExpand
        value={params.value ? params.value.toString() : ''}
        width={params.colDef.width}
      />
    ),
  },
  {
    field: 'orgEmail',
    headerName: t(messages.orgEmail()),
    width: 250,
    renderCell: (params: GridCellParams) => (
      <GridCellExpand
        value={params.value ? params.value.toString() : ''}
        width={params.colDef.width}
      />
    ),
    sortable: false,
    disableColumnMenu: true,
  },
  {
    field: 'personalEmail',
    headerName: t(messages.personalEmail()),
    width: 250,
    renderCell: (params: GridCellParams) => (
      <GridCellExpand
        value={params.value ? params.value.toString() : ''}
        width={params.colDef.width}
      />
    ),
    sortable: false,
    disableColumnMenu: true,
  },
  {
    field: 'phoneNumber',
    headerName: t(messages.phoneNumber()),
    width: 250,
    renderCell: (params: GridCellParams) => (
      <GridCellExpand
        value={params.value ? params.value.toString() : ''}
        width={params.colDef.width}
      />
    ),
    sortable: false,
    disableColumnMenu: true,
  },
];

import LazyLoading from 'app/components/LazyLoading/lazyLoading.component';
import { lazyLoad } from 'utils/loadable';

export const LecturersPage = lazyLoad(
  () => import('./index'),
  module => module.default,
  {
    fallback: <LazyLoading />,
  },
);

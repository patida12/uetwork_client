import { AddCircle, AddCircleOutlined } from '@mui/icons-material';
import CloseIcon from '@mui/icons-material/Close';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import MuiDialogActions from '@mui/material/DialogActions';
import MuiDialogContent from '@mui/material/DialogContent';
import MuiDialogTitle from '@mui/material/DialogTitle';
import Grid from '@mui/material/Grid';
import IconButton from '@mui/material/IconButton';
import { Theme } from '@mui/material/styles';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import { WithStyles } from '@mui/styles';
import createStyles from '@mui/styles/createStyles';
import withStyles from '@mui/styles/withStyles';
import { PhoneInputCustom } from 'app/components/PhoneInput';
import { useFormik } from 'formik';
import React from 'react';
import { useTranslation } from 'react-i18next';
import 'react-phone-number-input/style.css';
import theme from 'styles/theme';
import * as Yup from 'yup';
import { messages } from '../../messages';

const styles = (theme: Theme) =>
  createStyles({
    root: {
      margin: 0,
      padding: theme.spacing(2),
    },
    closeButton: {
      position: 'absolute',
      right: theme.spacing(1),
      top: theme.spacing(1),
      color: theme.palette.primary.main,
    },
  });

export interface DialogTitleProps extends WithStyles<typeof styles> {
  id: string;
  children: React.ReactNode;
  onClose: () => void;
}

const DialogTitle = withStyles(styles)((props: DialogTitleProps) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle className={classes.root} {...other}>
      <div>
        <Typography variant="h6">{children}</Typography>
      </div>
      {onClose ? (
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          onClick={onClose}
          size="large"
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme: Theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

const DialogActions = withStyles((theme: Theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);

function AddLecturerDialog() {
  const [open, setOpen] = React.useState(false);

  const { t } = useTranslation();

  const AddLecturerSchema = Yup.object().shape({
    fullName: Yup.string()
      .min(2, t(messages.tooShort()))
      .max(50, t(messages.tooLong()))
      .required(t(messages.required())),
    personalEmail: Yup.string().email(t(messages.invalidEmail())),
    phoneNumber: Yup.string().max(50, t(messages.tooLong())),
  });
  const formik = useFormik({
    initialValues: {
      fullName: '',
      personalEmail: '',
      phoneNumber: '',
    },
    validationSchema: AddLecturerSchema,
    onSubmit: values => {
      alert(JSON.stringify(values, null, 2));
    },
  });

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Button
        variant="contained"
        size="small"
        onClick={handleClickOpen}
        startIcon={<AddCircleOutlined />}
      >
        {t(messages.add())}
      </Button>
      <Dialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
        scroll="paper"
        maxWidth={'md'}
        style={{ marginTop: 60 }}
      >
        <DialogTitle id="customized-dialog-title" onClose={handleClose}>
          <AddCircle
            fontSize="small"
            style={{
              color: theme.palette.primary.main,
              marginRight: theme.spacing(1),
              marginBottom: theme.spacing(0.5),
            }}
          />
          {t(messages.addDialogTitle())}
        </DialogTitle>
        <div>
          <form onSubmit={formik.handleSubmit}>
            <DialogContent dividers>
              <Grid
                direction="column"
                container
                alignContent="center"
                justifyContent="center"
                spacing={1}
                style={{ marginRight: 60 }}
              >
                <Grid
                  item
                  direction="row"
                  container
                  alignContent="flex-start"
                  justifyContent="center"
                >
                  <Grid item xs={3}>
                    <Typography>{t(messages.fullName())}:</Typography>
                  </Grid>
                  <Grid item xs={9}>
                    <TextField
                      size="small"
                      variant="outlined"
                      style={{ width: '50ch' }}
                      id="fullName"
                      name="fullName"
                      value={formik.values.fullName}
                      onChange={formik.handleChange}
                      error={
                        formik.touched.fullName &&
                        Boolean(formik.errors.fullName)
                      }
                      helperText={
                        formik.touched.fullName && formik.errors.fullName
                      }
                    />
                  </Grid>
                </Grid>
                <Grid
                  item
                  direction="row"
                  container
                  alignContent="flex-start"
                  justifyContent="center"
                >
                  <Grid item xs={3}>
                    <Typography>{t(messages.personalEmail())}:</Typography>
                  </Grid>
                  <Grid item xs={9}>
                    <TextField
                      size="small"
                      variant="outlined"
                      style={{ width: '50ch' }}
                      id="personalEmail"
                      name="personalEmail"
                      value={formik.values.personalEmail}
                      onChange={formik.handleChange}
                      error={
                        formik.touched.personalEmail &&
                        Boolean(formik.errors.personalEmail)
                      }
                      helperText={
                        formik.touched.personalEmail &&
                        formik.errors.personalEmail
                      }
                      type="email"
                    />
                  </Grid>
                </Grid>
                <Grid
                  item
                  direction="row"
                  container
                  alignContent="flex-start"
                  justifyContent="center"
                >
                  <Grid item xs={3}>
                    <Typography>{t(messages.phoneNumber())}:</Typography>
                  </Grid>
                  <Grid item xs={9}>
                    <PhoneInputCustom
                      defaultCountry="VN"
                      international
                      style={{
                        width: '50ch',
                      }}
                      id="phoneNumber"
                      name="phoneNumber"
                      value={formik.values.phoneNumber}
                      onChange={(e: string) =>
                        formik.setFieldValue('phoneNumber', e)
                      }
                    />
                  </Grid>
                </Grid>
              </Grid>
            </DialogContent>
            <DialogActions>
              <Button autoFocus variant="contained" type="submit">
                {t(messages.save())}
              </Button>
            </DialogActions>
          </form>
        </div>
      </Dialog>
    </div>
  );
}
export default React.memo(AddLecturerDialog);

import { Edit } from '@mui/icons-material';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import Grid from '@mui/material/Grid';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import profileLecturerApi from 'api/profileLecturerApi';
import {
  DialogActions,
  DialogContent,
  DialogTitle,
} from 'app/components/CustomDialog';
import { PhoneInputCustom } from 'app/components/PhoneInput';
import { useFormik } from 'formik';
import { t } from 'locales/i18n';
import { Lecturer } from 'models';
import { useSnackbar } from 'notistack';
import React from 'react';
import 'react-phone-number-input/style.css';
import * as Yup from 'yup';
import { messages } from '../../messages';

interface EditLecturerProps {
  lecturer: Lecturer;
}

function EditLecturerDialog({ lecturer }: EditLecturerProps) {
  const { enqueueSnackbar } = useSnackbar();

  const EditLecturerSchema = Yup.object().shape({
    fullName: Yup.string()
      .min(2, t(messages.tooShort()))
      .max(50, t(messages.tooLong()))
      .required(t(messages.required())),
    personalEmail: Yup.string().email(t(messages.invalidEmail())),
    phoneNumber: Yup.string().max(50, t(messages.tooLong())),
  });

  const formik = useFormik({
    initialValues: lecturer,
    validationSchema: EditLecturerSchema,
    onSubmit: values => {
      profileLecturerApi
        .updateLecturer(lecturer.id, { ...values })
        .then(res => {
          enqueueSnackbar(t(messages.editSuccess()), { variant: 'success' });
          handleClose();
        })
        .catch(e => {
          enqueueSnackbar(e.response?.data.message, { variant: 'error' });
        });
    },
  });

  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Button
        variant="outlined"
        size="medium"
        onClick={handleClickOpen}
        startIcon={<Edit />}
      >
        {t(messages.edit())}
      </Button>
      <Dialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
        scroll="paper"
        maxWidth={'md'}
        style={{ marginTop: 60 }}
      >
        <DialogTitle id="customized-dialog-title" onClose={handleClose}>
          {t(messages.editDialogTitle())}
        </DialogTitle>
        <div>
          <form onSubmit={formik.handleSubmit}>
            <DialogContent dividers>
              <Grid
                direction="column"
                container
                alignContent="center"
                justifyContent="center"
                spacing={1}
                style={{ marginRight: 60 }}
              >
                <Grid
                  item
                  direction="row"
                  container
                  alignContent="flex-start"
                  justifyContent="center"
                >
                  <Grid item xs={3}>
                    <Typography>{t(messages.fullName())}:</Typography>
                  </Grid>
                  <Grid item xs={9}>
                    <TextField
                      size="small"
                      variant="outlined"
                      style={{ width: '50ch' }}
                      id="fullName"
                      name="fullName"
                      value={formik.values.fullName}
                      onChange={formik.handleChange}
                      error={
                        formik.touched.fullName &&
                        Boolean(formik.errors.fullName)
                      }
                      helperText={
                        formik.touched.fullName && formik.errors.fullName
                      }
                    />
                  </Grid>
                </Grid>
                <Grid
                  item
                  direction="row"
                  container
                  alignContent="flex-start"
                  justifyContent="center"
                >
                  <Grid item xs={3}>
                    <Typography>{t(messages.personalEmail())}:</Typography>
                  </Grid>
                  <Grid item xs={9}>
                    <TextField
                      size="small"
                      variant="outlined"
                      style={{ width: '50ch' }}
                      id="personalEmail"
                      name="personalEmail"
                      value={formik.values.personalEmail}
                      onChange={formik.handleChange}
                      error={
                        formik.touched.personalEmail &&
                        Boolean(formik.errors.personalEmail)
                      }
                      helperText={
                        formik.touched.personalEmail &&
                        formik.errors.personalEmail
                      }
                      type="email"
                    />
                  </Grid>
                </Grid>
                <Grid
                  item
                  direction="row"
                  container
                  alignContent="flex-start"
                  justifyContent="center"
                >
                  <Grid item xs={3}>
                    <Typography>{t(messages.phoneNumber())}:</Typography>
                  </Grid>
                  <Grid item xs={9}>
                    <PhoneInputCustom
                      international
                      style={{
                        width: '50ch',
                      }}
                      id="phoneNumber"
                      name="phoneNumber"
                      value={formik.values.phoneNumber}
                      onChange={(e: string) =>
                        formik.setFieldValue('phoneNumber', e)
                      }
                    />
                  </Grid>
                </Grid>
              </Grid>
            </DialogContent>
            <DialogActions>
              <Button autoFocus variant="contained" type="submit">
                {t(messages.save())}
              </Button>
            </DialogActions>
          </form>
        </div>
      </Dialog>
    </div>
  );
}
export default React.memo(EditLecturerDialog);

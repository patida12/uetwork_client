import { InfoOutlined } from '@mui/icons-material';
import { Stack } from '@mui/material';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import Grid from '@mui/material/Grid';
import IconButton from '@mui/material/IconButton';
import Tooltip from '@mui/material/Tooltip';
import Typography from '@mui/material/Typography';
import {
  DialogActions,
  DialogContent,
  DialogTitle,
} from 'app/components/CustomDialog';
import { t } from 'locales/i18n';
import { Lecturer } from 'models';
import React from 'react';
import theme from 'styles/theme';
import { messages } from '../../messages';
import EditLecturerDialog from '../EditLecturerDialog';

export const defaultLecturer: Lecturer = {
  id: 0,
  fullName: '',
  phoneNumber: '',
  personalEmail: '',
  orgEmail: '',
  userId: 0,
  organizationId: 0,
};

interface ViewLecturerProps {
  lecturer: Lecturer;
}

function ViewDetailsLecturerDialog({ lecturer }: ViewLecturerProps) {
  const detail = t(messages.detail());
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Tooltip title={detail}>
        <IconButton
          style={{
            color: theme.palette.primary.light,
          }}
          aria-label="info"
          onClick={handleClickOpen}
          size="large"
        >
          <InfoOutlined />
        </IconButton>
      </Tooltip>
      <Dialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
        scroll="paper"
        maxWidth={'md'}
        style={{ marginTop: 60 }}
      >
        <DialogTitle id="customized-dialog-title" onClose={handleClose}>
          {t(messages.detailDialogTitle())}
        </DialogTitle>
        <DialogContent dividers>
          <Grid
            direction="column"
            container
            alignContent="center"
            justifyContent="center"
            spacing={1}
            style={{ marginRight: 60 }}
          >
            <Grid
              item
              direction="row"
              container
              alignContent="flex-start"
              justifyContent="center"
            >
              <Grid item xs={3}>
                <Typography>{t(messages.fullName())}:</Typography>
              </Grid>
              <Grid item xs={9}>
                <Typography sx={{ width: '50ch' }}>
                  {lecturer.fullName}
                </Typography>
              </Grid>
            </Grid>
            <Grid
              item
              direction="row"
              container
              alignContent="flex-start"
              justifyContent="center"
            >
              <Grid item xs={3}>
                <Typography>{t(messages.personalEmail())}:</Typography>
              </Grid>
              <Grid item xs={9}>
                <Typography sx={{ width: '50ch' }}>
                  {lecturer.personalEmail}
                </Typography>
              </Grid>
            </Grid>
            <Grid
              item
              direction="row"
              container
              alignContent="flex-start"
              justifyContent="center"
            >
              <Grid item xs={3}>
                <Typography>{t(messages.phoneNumber())}:</Typography>
              </Grid>
              <Grid item xs={9}>
                <Typography sx={{ width: '50ch' }}>
                  {lecturer.phoneNumber}
                </Typography>
              </Grid>
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions>
          <Stack direction="row" spacing={1}>
            <EditLecturerDialog lecturer={lecturer} />
            <Button autoFocus onClick={handleClose} variant="contained">
              {t(messages.close())}
            </Button>
          </Stack>
        </DialogActions>
      </Dialog>
    </div>
  );
}
export default React.memo(ViewDetailsLecturerDialog);

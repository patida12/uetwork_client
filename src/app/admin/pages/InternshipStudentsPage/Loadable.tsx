import LazyLoading from 'app/components/LazyLoading/lazyLoading.component';
import { lazyLoad } from 'utils/loadable';

export const InternshipStudentsPage = lazyLoad(
  () => import('./index'),
  module => module.InternshipStudentsPage,
  {
    fallback: <LazyLoading />,
  },
);

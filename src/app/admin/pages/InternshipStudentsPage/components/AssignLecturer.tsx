import { MenuItem } from '@mui/material';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import { GridRowId } from '@mui/x-data-grid';
import internshipStudentApi from 'api/internship/internshipScoreApi';
import {
  DialogActions,
  DialogContent,
  DialogTitle,
} from 'app/components/CustomDialog';
import { t } from 'locales/i18n';
import { TermIdParam } from 'models';
import { useSnackbar } from 'notistack';
import React, { useState } from 'react';
import 'react-phone-number-input/style.css';
import 'react-quill/dist/quill.snow.css';
import { useParams } from 'react-router-dom';
import { messages } from '../messages';
import SelectLecturerComponent from './SelectLecturerComponent';

interface AssignLecturerProps {
  rowIds: GridRowId[];
  refreshData: () => void;
}

function AssignLecturer({ rowIds, refreshData }: AssignLecturerProps) {
  const { termId } = useParams<TermIdParam>();
  const [open, setOpen] = React.useState(false);
  const { enqueueSnackbar } = useSnackbar();
  const [lecturerId, setLecturerId] = useState<number>();

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  const handleChangeSupervisor = (id: string | number) => {
    setLecturerId(Number(id));
  };

  const handleChoose = () => {
    if (rowIds.length <= 0 || lecturerId === undefined) {
      enqueueSnackbar(t(messages.preventAssign()), { variant: 'warning' });
    } else {
      var studentIds: number[] = rowIds.map(id => Number(id));
      internshipStudentApi
        .assignSupervisor(Number(lecturerId), studentIds, Number(termId))
        .then(res => {
          enqueueSnackbar(t(messages.assignSuccess()), {
            variant: 'success',
          });
          refreshData();
        })
        .catch(e => {
          enqueueSnackbar(e.response?.data.message, { variant: 'error' });
        });
    }
  };

  return (
    <div>
      <MenuItem onClick={handleClickOpen}>{t(messages.assign())}</MenuItem>
      <Dialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
        scroll="paper"
        maxWidth={'md'}
        style={{ marginTop: 60 }}
      >
        <DialogTitle id="customized-dialog-title" onClose={handleClose}>
          {t(messages.assign())}
        </DialogTitle>
        <div>
          <DialogContent dividers>
            <SelectLecturerComponent
              handleChangeSupervisor={handleChangeSupervisor}
              customWidth={'50ch'}
            />
          </DialogContent>
          <DialogActions>
            <Button
              autoFocus
              variant="contained"
              onClick={handleChoose}
              sx={{ m: 1 }}
            >
              {t(messages.assignButton())}
            </Button>
          </DialogActions>
        </div>
      </Dialog>
    </div>
  );
}
export default React.memo(AssignLecturer);

import { BuildCircleOutlined } from '@mui/icons-material';
import { Button, MenuItem } from '@mui/material';
import Menu from '@mui/material/Menu';
import { GridRowId } from '@mui/x-data-grid';
import { t } from 'locales/i18n';
import React from 'react';
import { messages } from '../messages';
import AssignLecturer from './AssignLecturer';

interface OptionsProps {
  rowIds: GridRowId[];
  refreshData: () => void;
}

function OptionsInternshipStudent({ rowIds, refreshData }: OptionsProps) {
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  //TODO
  const handleImport = () => {};
  const handleExport = () => {};

  return (
    <div>
      <Button
        variant="contained"
        onClick={handleClick}
        startIcon={<BuildCircleOutlined />}
      >
        {t(messages.option())}
      </Button>
      <Menu anchorEl={anchorEl} open={open} onClose={handleClose}>
        <AssignLecturer rowIds={rowIds} refreshData={refreshData} />
        <MenuItem onClick={() => handleImport()}>
          {t(messages.import())}
        </MenuItem>
        <MenuItem onClick={() => handleExport()}>
          {t(messages.export())}
        </MenuItem>
      </Menu>
    </div>
  );
}

export default React.memo(OptionsInternshipStudent);

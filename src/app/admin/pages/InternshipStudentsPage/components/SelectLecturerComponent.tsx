import Autocomplete from '@mui/material/Autocomplete';
import CircularProgress from '@mui/material/CircularProgress';
import TextField from '@mui/material/TextField';
import { getLecturerListing } from 'api/commonApi';
import { ALL_CLASSES_EN, ALL_CLASSES_VI } from 'app/constants';
import { useAsync } from 'app/hooks/useAsync';
import { t } from 'locales/i18n';
import { TermIdParam } from 'models';
import * as React from 'react';
import { useParams } from 'react-router-dom';
import { messages } from '../messages';

interface SelectLecturersProps {
  handleChangeSupervisor: (is: string | number) => void;
  customWidth?: string;
}

function SelectLecturers({
  handleChangeSupervisor,
  customWidth,
}: SelectLecturersProps) {
  const { termId } = useParams<TermIdParam>();
  const [open, setOpen] = React.useState(false);
  const [superVisor, setSuperVisor] = React.useState<string>('');
  const { loading, data, run } = useAsync<{ id: number; fullName: string }[]>({
    data: [],
  });

  const lecturersList = data ?? [];

  React.useEffect(() => {
    run(getLecturerListing(Number(termId)));
  }, [run]);

  React.useEffect(() => {
    var lecturer = lecturersList.find(
      lecturer => lecturer.fullName === superVisor,
    );
    var id = lecturer ? lecturer.id : '';
    handleChangeSupervisor(id);
  }, [handleChangeSupervisor, superVisor]);

  function handleChange(value: string) {
    setSuperVisor(value);
  }

  return (
    <Autocomplete
      id="select-supervisor"
      value={superVisor}
      onChange={(
        event: React.SyntheticEvent<Element, Event>,
        value: string | null,
      ) => {
        handleChange(value!);
      }}
      sx={customWidth ? { width: customWidth } : {}}
      open={open}
      onOpen={() => {
        setOpen(true);
      }}
      onClose={() => {
        setOpen(false);
      }}
      isOptionEqualToValue={(option, value) =>
        value === ALL_CLASSES_EN || ALL_CLASSES_VI ? true : false
      }
      options={lecturersList.map(supervisor => {
        return supervisor.fullName;
      })}
      getOptionLabel={option => option}
      loading={loading}
      renderInput={params => (
        <TextField
          {...params}
          label={t(messages.supervisor())}
          InputProps={{
            ...params.InputProps,
            endAdornment: (
              <React.Fragment>
                {loading ? (
                  <CircularProgress color="inherit" size={20} />
                ) : null}
                {params.InputProps.endAdornment}
              </React.Fragment>
            ),
          }}
        />
      )}
    />
  );
}
export default React.memo(SelectLecturers);

import { GridCellParams, GridColumns } from '@mui/x-data-grid';
import GridCellExpand from 'app/components/GridCellExpand';
import { t } from 'locales/i18n';
import { formatDate } from 'utils';
import { messages } from '../messages';

export const getColumns = (): GridColumns => [
  {
    field: 'id',
    sortable: false,
    disableColumnMenu: true,
    hide: true,
  },
  {
    field: 'fullName',
    headerName: t(messages.fullName()),
    width: 250,
    renderCell: (params: GridCellParams) => (
      <GridCellExpand
        value={params.value ? params.value.toString() : ''}
        width={params.colDef.width}
      />
    ),
    disableColumnMenu: true,
  },
  {
    field: 'studentIdNumber',
    headerName: t(messages.studentIdNumber()),
    width: 250,
    renderCell: (params: GridCellParams) => (
      <GridCellExpand
        value={params.value ? params.value.toString() : ''}
        width={params.colDef.width}
      />
    ),
    disableColumnMenu: true,
  },
  {
    field: 'orgEmail',
    headerName: t(messages.orgEmail()),
    width: 200,
    renderCell: (params: GridCellParams) => (
      <GridCellExpand
        value={params.value ? params.value.toString() : ''}
        width={params.colDef.width}
      />
    ),
    disableColumnMenu: true,
    sortable: false,
  },
  {
    field: 'phonenumber',
    sortable: false,
    disableColumnMenu: true,
    hide: true,
  },
  {
    field: 'personalEmail',
    sortable: false,
    disableColumnMenu: true,
    hide: true,
  },
  {
    field: 'organizationId',
    sortable: false,
    disableColumnMenu: true,
    hide: true,
  },
  {
    field: 'userId',
    sortable: false,
    disableColumnMenu: true,
    hide: true,
  },
  {
    field: 'selectedAt',
    renderCell: (params: GridCellParams) => (
      <GridCellExpand
        value={params.value ? formatDate(params.value.toString()) : ''}
        width={params.colDef.width}
      />
    ),
    sortable: false,
    disableColumnMenu: true,
    hide: true,
  },
  {
    field: 'internshipType',
    headerName: t(messages.internshipType()),
    width: 200,
    renderCell: (params: GridCellParams) => (
      <GridCellExpand
        value={params.value ? params.value.toString() : ''}
        width={params.colDef.width}
      />
    ),
    disableColumnMenu: true,
    sortable: false,
  },
  {
    field: 'reportSubmitted',
    sortable: false,
    disableColumnMenu: true,
    hide: true,
  },
  {
    field: 'schoolClass',
    headerName: t(messages.schoolClass()),
    width: 200,
    renderCell: (params: GridCellParams) => (
      <GridCellExpand
        value={params.value ? params.value.name.toString() : ''}
        width={params.colDef.width}
      />
    ),
    disableColumnMenu: true,
    sortable: false,
  },
  {
    field: 'supervisor',
    headerName: t(messages.supervisor()),
    width: 250,
    renderCell: (params: GridCellParams) => (
      <GridCellExpand
        value={params.value ? params.value.name.toString() : ''}
        width={params.colDef.width}
      />
    ),
    disableColumnMenu: true,
    sortable: false,
  },
  {
    field: 'selectedPartner',
    headerName: t(messages.selectedPartner()),
    width: 250,
    renderCell: (params: GridCellParams) => (
      <GridCellExpand
        value={params.value ? params.value.name.toString() : ''}
        width={params.colDef.width}
      />
    ),
    disableColumnMenu: true,
    sortable: false,
  },
  {
    field: 'score',
    headerName: t(messages.score()),
    width: 150,
    sortable: false,
    disableColumnMenu: true,
    renderCell: (params: GridCellParams) => (
      <GridCellExpand
        value={params.value ? params.value.toFixed(2) : ''}
        width={params.colDef.width}
      />
    ),
  },
];

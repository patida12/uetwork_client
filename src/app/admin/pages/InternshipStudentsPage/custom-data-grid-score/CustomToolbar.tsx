import SearchIcon from '@mui/icons-material/Search';
import {
  Button,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
} from '@mui/material';
import Grid from '@mui/material/Grid';
import { styled } from '@mui/material/styles';
import TextField from '@mui/material/TextField';
import { GridToolbarContainer } from '@mui/x-data-grid';
import { TRUE_FALSE_OPTIONS } from 'app/constants/status.constants';
import { useFormik } from 'formik';
import { t } from 'locales/i18n';
import { CustomToolbarProps, FilterState } from 'models';
import React from 'react';
import OptionsInternshipStudent from '../components/OptionsInternshipStudent';
import SelectLecturers from '../components/SelectLecturerComponent';
import { messages } from '../messages';

const PREFIX = 'CustomToolbar';

const classes = {
  textField: `${PREFIX}-textField`,
};

const StyledGridToolbarContainer = styled(GridToolbarContainer)(
  ({ theme }) => ({
    [`& .${classes.textField}`]: {
      width: '25ch',
    },
  }),
);

function CustomToolbar({
  onFilterChange,
  selectionModel,
  refreshData,
}: CustomToolbarProps) {
  const [superVisorId, setSuperVisorId] = React.useState<string | number>('');
  const formik = useFormik<FilterState>({
    initialValues: {
      query: '',
      filter: {
        partnerSelected: '',
        supervisorAssigned: '',
        reportSubmitted: '',
        scoreGiven: '',
      },
    },
    onSubmit: values => {
      var filters: FilterState = {
        query: values.query,
        filter: {
          partnerSelected:
            TRUE_FALSE_OPTIONS.find(
              type => type.key === values.filter.partnerSelected,
            )?.value ?? '',
          supervisorAssigned:
            TRUE_FALSE_OPTIONS.find(
              type => type.key === values.filter.supervisorAssigned,
            )?.value ?? '',
          reportSubmitted:
            TRUE_FALSE_OPTIONS.find(
              type => type.key === values.filter.reportSubmitted,
            )?.value ?? '',
          scoreGiven:
            TRUE_FALSE_OPTIONS.find(
              type => type.key === values.filter.scoreGiven,
            )?.value ?? '',
          superVisorId: superVisorId,
        },
      };
      onFilterChange(filters);
    },
  });

  function handleChangeSupervisor(id: string | number) {
    setSuperVisorId(id);
  }

  return (
    <form onSubmit={formik.handleSubmit}>
      <StyledGridToolbarContainer style={{ margin: 10 }}>
        <Grid
          container
          direction="row"
          justifyContent="space-between"
          spacing={1}
        >
          <Grid
            item
            xs={12}
            sm={11}
            md={11}
            direction="row"
            container
            alignItems="center"
            justifyContent="flex-start"
            spacing={1}
          >
            <Grid item>
              <FormControl
                variant="outlined"
                style={{ minWidth: 250 }}
                size="small"
              >
                <InputLabel id="partnerSelected">
                  {t(messages.partnerSelected())}
                </InputLabel>
                <Select
                  id="filter.partnerSelected"
                  name="filter.partnerSelected"
                  label={t(messages.partnerSelected())}
                  value={formik.values.filter.partnerSelected}
                  onChange={formik.handleChange}
                >
                  <MenuItem
                    key={TRUE_FALSE_OPTIONS[0].key}
                    value={TRUE_FALSE_OPTIONS[0].key}
                  >
                    {t(messages.all())}
                  </MenuItem>
                  <MenuItem
                    key={TRUE_FALSE_OPTIONS[1].key}
                    value={TRUE_FALSE_OPTIONS[1].key}
                  >
                    {t(messages.selected())}
                  </MenuItem>
                  <MenuItem
                    key={TRUE_FALSE_OPTIONS[2].key}
                    value={TRUE_FALSE_OPTIONS[2].key}
                  >
                    {t(messages.notSelected())}
                  </MenuItem>
                </Select>
              </FormControl>
            </Grid>
            <Grid item>
              <FormControl
                variant="outlined"
                style={{ minWidth: 250 }}
                size="small"
              >
                <InputLabel id="supervisorAssigned">
                  {t(messages.supervisorAssigned())}
                </InputLabel>
                <Select
                  id="filter.supervisorAssigned"
                  name="filter.supervisorAssigned"
                  label={t(messages.supervisorAssigned())}
                  value={formik.values.filter.supervisorAssigned}
                  onChange={formik.handleChange}
                >
                  <MenuItem
                    key={TRUE_FALSE_OPTIONS[0].key}
                    value={TRUE_FALSE_OPTIONS[0].key}
                  >
                    {t(messages.all())}
                  </MenuItem>
                  <MenuItem
                    key={TRUE_FALSE_OPTIONS[1].key}
                    value={TRUE_FALSE_OPTIONS[1].key}
                  >
                    {t(messages.assigned())}
                  </MenuItem>
                  <MenuItem
                    key={TRUE_FALSE_OPTIONS[2].key}
                    value={TRUE_FALSE_OPTIONS[2].key}
                  >
                    {t(messages.notAssigned())}
                  </MenuItem>
                </Select>
              </FormControl>
            </Grid>
            <Grid item>
              <FormControl
                variant="outlined"
                style={{ minWidth: 250 }}
                size="small"
              >
                <InputLabel id="reportSubmitted">
                  {t(messages.reportSubmitted())}
                </InputLabel>
                <Select
                  id="filter.reportSubmitted"
                  name="filter.reportSubmitted"
                  label={t(messages.reportSubmitted())}
                  value={formik.values.filter.reportSubmitted}
                  onChange={formik.handleChange}
                >
                  <MenuItem
                    key={TRUE_FALSE_OPTIONS[0].key}
                    value={TRUE_FALSE_OPTIONS[0].key}
                  >
                    {t(messages.all())}
                  </MenuItem>
                  <MenuItem
                    key={TRUE_FALSE_OPTIONS[1].key}
                    value={TRUE_FALSE_OPTIONS[1].key}
                  >
                    {t(messages.submitted())}
                  </MenuItem>
                  <MenuItem
                    key={TRUE_FALSE_OPTIONS[2].key}
                    value={TRUE_FALSE_OPTIONS[2].key}
                  >
                    {t(messages.notSubmitted())}
                  </MenuItem>
                </Select>
              </FormControl>
            </Grid>
            <Grid item>
              <FormControl
                variant="outlined"
                style={{ minWidth: 250 }}
                size="small"
              >
                <InputLabel id="scoreGiven">
                  {t(messages.scoreGiven())}
                </InputLabel>
                <Select
                  id="filter.scoreGiven"
                  name="filter.scoreGiven"
                  label={t(messages.scoreGiven())}
                  value={formik.values.filter.scoreGiven}
                  onChange={formik.handleChange}
                >
                  <MenuItem
                    key={TRUE_FALSE_OPTIONS[0].key}
                    value={TRUE_FALSE_OPTIONS[0].key}
                  >
                    {t(messages.all())}
                  </MenuItem>
                  <MenuItem
                    key={TRUE_FALSE_OPTIONS[1].key}
                    value={TRUE_FALSE_OPTIONS[1].key}
                  >
                    {t(messages.given())}
                  </MenuItem>
                  <MenuItem
                    key={TRUE_FALSE_OPTIONS[2].key}
                    value={TRUE_FALSE_OPTIONS[2].key}
                  >
                    {t(messages.notGiven())}
                  </MenuItem>
                </Select>
              </FormControl>
            </Grid>
            <Grid item>
              <FormControl
                variant="outlined"
                style={{ minWidth: 250 }}
                size="small"
              >
                <SelectLecturers
                  handleChangeSupervisor={handleChangeSupervisor}
                />
              </FormControl>
            </Grid>
            <Grid item>
              <TextField
                value={formik.values.query}
                onChange={formik.handleChange}
                id="query"
                className={classes.textField}
                size="small"
                variant="outlined"
                rows={1}
                label={t(messages.keyword())}
                style={{ minWidth: 250 }}
              />
            </Grid>
            <Grid item>
              <Button
                variant="contained"
                type="submit"
                startIcon={<SearchIcon />}
              >
                {t(messages.search())}
              </Button>
            </Grid>
          </Grid>
          <Grid item>
            <OptionsInternshipStudent
              rowIds={selectionModel ?? []}
              refreshData={refreshData!}
            />
          </Grid>
        </Grid>
      </StyledGridToolbarContainer>
    </form>
  );
}

export default React.memo(CustomToolbar);

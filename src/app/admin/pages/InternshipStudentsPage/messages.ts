import { translations } from 'locales/translations';
import { _t } from 'utils/messages';

export const messages = {
  studentIdNumber: () =>
    _t(translations.internship.studentFeature.studentIdNumber),
  fullName: () => _t(translations.internship.studentFeature.fullName),
  orgEmail: () => _t(translations.internship.studentFeature.orgEmail),
  phoneNumber: () => _t(translations.internship.studentFeature.phoneNumber),
  internshipType: () =>
    _t(translations.internship.studentFeature.internshipType),
  schoolClass: () => _t(translations.internship.scoreFeature.schoolClass),
  supervisor: () => _t(translations.internship.scoreFeature.supervisor),
  selectedPartner: () =>
    _t(translations.internship.scoreFeature.selectedPartner),
  score: () => _t(translations.internship.scoreFeature.score),
  all: () => _t(translations.internship.scoreFeature.all),
  partnerSelected: () =>
    _t(translations.internship.scoreFeature.partnerSelected),
  selected: () => _t(translations.internship.scoreFeature.selected),
  notSelected: () => _t(translations.internship.scoreFeature.notSelected),
  supervisorAssigned: () =>
    _t(translations.internship.scoreFeature.supervisorAssigned),
  assigned: () => _t(translations.internship.scoreFeature.assigned),
  notAssigned: () => _t(translations.internship.scoreFeature.notAssigned),
  reportSubmitted: () =>
    _t(translations.internship.scoreFeature.reportSubmitted),
  submitted: () => _t(translations.internship.scoreFeature.submitted),
  notSubmitted: () => _t(translations.internship.scoreFeature.notSubmitted),
  scoreGiven: () => _t(translations.internship.scoreFeature.scoreGiven),
  given: () => _t(translations.internship.scoreFeature.given),
  notGiven: () => _t(translations.internship.scoreFeature.notGiven),
  search: () => _t(translations.common.table.search),
  keyword: () => _t(translations.common.table.keyword),
  option: () => _t(translations.common.table.option),
  help: () => _t(translations.internship.scoreFeature.help),
  assign: () => _t(translations.internship.scoreFeature.assign),
  import: () => _t(translations.internship.scoreFeature.import),
  export: () => _t(translations.internship.scoreFeature.export),
  preventAssign: () => _t(translations.internship.scoreFeature.preventAssign),
  chooseLecturer: () => _t(translations.internship.scoreFeature.chooseLecturer),
  assignButton: () => _t(translations.internship.scoreFeature.assignButton),
  assignSuccess: () => _t(translations.internship.scoreFeature.assignSuccess),
};

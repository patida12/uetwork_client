import { translations } from 'locales/translations';
import { _t } from 'utils/messages';

export const messages = {
  name: () => _t(translations.internship.partnerFeature.name),
  termStatus: () => _t(translations.internship.partnerFeature.termStatus),
  email: () => _t(translations.internship.partnerFeature.email),
  homepageUrl: () => _t(translations.internship.partnerFeature.homepageUrl),
  joinedAt: () => _t(translations.internship.partnerFeature.joinedAt),
  isAssociate: () => _t(translations.internship.partnerFeature.isAssociate),
  search: () => _t(translations.common.table.search),
  keyword: () => _t(translations.common.table.keyword),
  option: () => _t(translations.common.table.option),
  markAsApproved: () =>
    _t(translations.internship.partnerFeature.markAsApproved),
  markAsRejected: () =>
    _t(translations.internship.partnerFeature.markAsRejected),
  filterCompany: () => _t(translations.common.app.company),
  allCompanies: () => _t(translations.internship.partnerFeature.allPartners),
  waitingCompanies: () =>
    _t(translations.internship.partnerFeature.waitingCompanies),
  approvedCompanies: () =>
    _t(translations.internship.partnerFeature.approvedCompanies),
  rejectedCompanies: () =>
    _t(translations.internship.partnerFeature.rejectedCompanies),
  success: () => _t(translations.internship.partnerFeature.success),
  addPartnersToTerm: () => _t(translations.partnerFeature.addPartnersToTerm),
  addToTermSuccess: () => _t(translations.partnerFeature.addToTermSuccess),
  save: () => _t(translations.common.button.save),
  partner: () => _t(translations.header.partner),
  required: () => _t(translations.common.validate.required),
  addPartnersToTermDialog: () =>
    _t(translations.partnerFeature.addPartnersToTermDialog),
};

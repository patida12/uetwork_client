import { Chip, Grid, Link } from '@mui/material';
import { GridCellParams, GridColumns } from '@mui/x-data-grid';
import GridCellExpand from 'app/components/GridCellExpand';
import { PARTNER_STATUS } from 'app/constants/status.constants';
import { t } from 'locales/i18n';
import { formatDate } from 'utils';
import { IsAssociated } from '../../ProfilePartnersPage/custom-data-grid-partners/ColumnDefinition';
import { messages } from '../messages';

export const getColumns = (): GridColumns => [
  {
    field: 'id',
    sortable: false,
    disableColumnMenu: true,
    hide: true,
  },
  {
    field: 'name',
    headerName: t(messages.name()),
    width: 450,
    renderCell: (params: GridCellParams) => (
      <Grid container>
        <Grid item xs={11}>
          <GridCellExpand
            value={params.value ? params.value.toString() : ''}
            width={params.colDef.width}
          />
        </Grid>
        <Grid>
          <IsAssociated isAssociate={Boolean(params.row.isAssociate)} />
        </Grid>
      </Grid>
    ),
    disableColumnMenu: true,
  },
  {
    field: 'email',
    headerName: t(messages.email()),
    width: 250,
    renderCell: (params: GridCellParams) => (
      <GridCellExpand
        value={params.value ? params.value.toString() : ''}
        width={params.colDef.width}
      />
    ),
    disableColumnMenu: true,
    sortable: false,
  },
  {
    field: 'homepageUrl',
    headerName: t(messages.homepageUrl()),
    width: 250,
    renderCell: (params: GridCellParams) => (
      <Link
        href={`${params.value}`}
        target="_blank"
        rel="noreferrer"
        variant="body2"
      >
        {params.value}
      </Link>
    ),
    disableColumnMenu: true,
    sortable: false,
  },
  {
    field: 'joinedAt',
    headerName: t(messages.joinedAt()),
    width: 250,
    renderCell: (params: GridCellParams) => (
      <GridCellExpand
        value={params.value ? formatDate(params.value.toString()) : ''}
        width={params.colDef.width}
      />
    ),
    disableColumnMenu: true,
  },
  {
    field: 'isAssociate',
    headerName: t(messages.isAssociate()),
    width: 0,
    sortable: false,
    disableColumnMenu: true,
    filterable: false,
    hide: true,
  },
  {
    field: 'status',
    headerName: t(messages.termStatus()),
    width: 150,
    sortable: false,
    disableColumnMenu: true,
    renderCell: (params: GridCellParams) => (
      <Chip
        label={
          PARTNER_STATUS.find(status => status.key === params.value)?.value
        }
        color={
          PARTNER_STATUS.find(status => status.key === params.value)?.color
        }
        variant="outlined"
        size="small"
      />
    ),
  },
];

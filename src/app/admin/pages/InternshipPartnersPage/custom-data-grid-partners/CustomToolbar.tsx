import SearchIcon from '@mui/icons-material/Search';
import {
  Button,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
} from '@mui/material';
import Grid from '@mui/material/Grid';
import { styled } from '@mui/material/styles';
import TextField from '@mui/material/TextField';
import { GridToolbarContainer } from '@mui/x-data-grid';
import { PARTNER_STATUS } from 'app/constants/status.constants';
import { useFormik } from 'formik';
import { t } from 'locales/i18n';
import { CustomToolbarProps, FilterState } from 'models';
import React from 'react';
import AddPartnersToTerm from '../components/AddPartnersToTerm';
import OptionsInternshipPartner from '../components/OptionsInternshipPartner';
import { messages } from '../messages';

const PREFIX = 'CustomToolbar';

const classes = {
  textField: `${PREFIX}-textField`,
};

const StyledGridToolbarContainer = styled(GridToolbarContainer)(
  ({ theme }) => ({
    [`& .${classes.textField}`]: {
      width: '25ch',
    },
  }),
);

function CustomToolbar({
  onFilterChange,
  selectionModel,
  refreshData,
}: CustomToolbarProps) {
  const formik = useFormik<FilterState>({
    initialValues: {
      query: '',
      filter: {
        status: '',
      },
    },
    onSubmit: values => {
      var filters: FilterState = {
        query: values.query,
        filter: {
          status:
            PARTNER_STATUS.find(st => st.key === values.filter.status)?.value ??
            '',
        },
      };
      onFilterChange(filters);
    },
  });

  return (
    <form onSubmit={formik.handleSubmit}>
      <StyledGridToolbarContainer style={{ margin: 10 }}>
        <Grid
          container
          direction="row"
          justifyContent="space-between"
          spacing={1}
        >
          <Grid
            item
            xs={12}
            sm={10}
            md={10}
            direction="row"
            container
            alignItems="center"
            justifyContent="flex-start"
            spacing={1}
          >
            <Grid item>
              <FormControl
                variant="outlined"
                style={{ minWidth: 250 }}
                size="small"
              >
                <InputLabel id="select-status-label">
                  {t(messages.filterCompany())}
                </InputLabel>
                <Select
                  id="filter.status"
                  name="filter.status"
                  label={t(messages.filterCompany())}
                  value={formik.values.filter.status}
                  onChange={formik.handleChange}
                >
                  <MenuItem
                    key={PARTNER_STATUS[0].key}
                    value={PARTNER_STATUS[0].key}
                  >
                    {t(messages.allCompanies())}
                  </MenuItem>
                  <MenuItem
                    key={PARTNER_STATUS[1].key}
                    value={PARTNER_STATUS[1].key}
                  >
                    {t(messages.waitingCompanies())}
                  </MenuItem>
                  <MenuItem
                    key={PARTNER_STATUS[2].key}
                    value={PARTNER_STATUS[2].key}
                  >
                    {t(messages.approvedCompanies())}
                  </MenuItem>
                  <MenuItem
                    key={PARTNER_STATUS[3].key}
                    value={PARTNER_STATUS[3].key}
                  >
                    {t(messages.rejectedCompanies())}
                  </MenuItem>
                </Select>
              </FormControl>
            </Grid>
            <Grid item>
              <TextField
                value={formik.values.query}
                onChange={formik.handleChange}
                id="query"
                className={classes.textField}
                size="small"
                variant="outlined"
                rows={1}
                label={t(messages.keyword())}
              />
            </Grid>
            <Grid item>
              <Button
                variant="contained"
                type="submit"
                startIcon={<SearchIcon />}
              >
                {t(messages.search())}
              </Button>
            </Grid>
          </Grid>
          <Grid
            container
            item
            xs={12}
            sm={2}
            md={2}
            spacing={1}
            direction="row"
            justifyContent="flex-end"
          >
            <Grid item>
              <AddPartnersToTerm refreshData={refreshData!} />
            </Grid>
            <Grid item>
              <OptionsInternshipPartner
                rowIds={selectionModel ?? []}
                refreshData={refreshData!}
              />
            </Grid>
          </Grid>
        </Grid>
      </StyledGridToolbarContainer>
    </form>
  );
}

export default React.memo(CustomToolbar);

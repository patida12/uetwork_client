import { BuildCircleOutlined } from '@mui/icons-material';
import { Button, MenuItem } from '@mui/material';
import Menu from '@mui/material/Menu';
import { GridRowId } from '@mui/x-data-grid';
import internshipPartnerApi from 'api/internship/internshipPartnerApi';
import { PARTNER_STATUS } from 'app/constants/status.constants';
import { t } from 'locales/i18n';
import { TermIdParam } from 'models/common';
import { useSnackbar } from 'notistack';
import React from 'react';
import { useParams } from 'react-router-dom';
import { messages } from '../messages';

interface OptionsProps {
  rowIds: GridRowId[];
  refreshData: () => void;
}

function OptionsInternshipPartner({ rowIds, refreshData }: OptionsProps) {
  const { termId } = useParams<TermIdParam>();
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);
  const { enqueueSnackbar } = useSnackbar();

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleAcceptOrReject = (status: string) => {
    var partnerIds: number[] = rowIds.map(id => Number(id));
    internshipPartnerApi
      .acceptOrReject(partnerIds, status, Number(termId))
      .then(res => {
        enqueueSnackbar(t(messages.success()), {
          variant: 'success',
        });
        handleClose();
        refreshData();
      })
      .catch(e => {
        enqueueSnackbar(e.response?.data.message, { variant: 'error' });
      });
  };

  return (
    <div>
      <Button
        variant="contained"
        onClick={handleClick}
        startIcon={<BuildCircleOutlined />}
        disabled={rowIds.length <= 0 ? true : false}
      >
        {t(messages.option())}
      </Button>
      <Menu anchorEl={anchorEl} open={open} onClose={handleClose}>
        <MenuItem onClick={() => handleAcceptOrReject(PARTNER_STATUS[2].value)}>
          {t(messages.markAsApproved())}
        </MenuItem>
        <MenuItem onClick={() => handleAcceptOrReject(PARTNER_STATUS[3].value)}>
          {t(messages.markAsRejected())}
        </MenuItem>
      </Menu>
    </div>
  );
}

export default React.memo(OptionsInternshipPartner);

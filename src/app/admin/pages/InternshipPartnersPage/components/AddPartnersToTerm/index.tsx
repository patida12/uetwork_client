import { AddCircleOutline } from '@mui/icons-material';
import {
  Autocomplete,
  Box,
  CircularProgress,
  Dialog,
  DialogActions,
  DialogContent,
  Grid,
  TextField,
} from '@mui/material';
import Button from '@mui/material/Button';
import internshipPartnerApi from 'api/internship/internshipPartnerApi';
import { Results } from 'api/types';
import { IsAssociated } from 'app/admin/pages/ProfilePartnersPage/custom-data-grid-partners/ColumnDefinition';
import { DialogTitle } from 'app/components/CustomDialog';
import { useAsync } from 'app/hooks/useAsync';
import { t } from 'locales/i18n';
import { TermIdParam } from 'models';
import { PartnerListing } from 'models/internshipPartner';
import { useSnackbar } from 'notistack';
import React from 'react';
import 'react-phone-number-input/style.css';
import 'react-quill/dist/quill.snow.css';
import { useParams } from 'react-router-dom';
import { messages } from '../../messages';

interface AddPartnersToTermProps {
  refreshData: () => void;
}

function AddPartnersToTerm({ refreshData }: AddPartnersToTermProps) {
  const [open, setOpen] = React.useState(false);
  const { enqueueSnackbar } = useSnackbar();
  const { termId } = useParams<TermIdParam>();

  var { data, run, loading } = useAsync<Results<PartnerListing[]>>({
    data: {
      codeResults: {
        code: 0,
        message: '',
      },
      item: [],
    },
  });

  const partnersList: PartnerListing[] = data?.item ?? [];
  const [selectedPartner, setPartner] = React.useState<PartnerListing[]>([]);

  React.useEffect(() => {
    run(internshipPartnerApi.getListPartners(Number(termId)));
  }, [run]);

  function handleChange(partners: PartnerListing[]) {
    setPartner(partners);
  }

  const handleAddToTerm = () => {
    if (selectedPartner !== undefined && selectedPartner.length > 0) {
      var partnerIds: number[] = selectedPartner.map(partner => partner.id);
      console.log(partnerIds);
      internshipPartnerApi
        .addPartnersToTerm(Number(termId), partnerIds)
        .then(res => {
          enqueueSnackbar(t(messages.addToTermSuccess()), {
            variant: 'success',
          });
          refreshData();
          handleClose();
        })
        .catch(e => {
          enqueueSnackbar(e.response?.data.message, { variant: 'error' });
        });
    }
  };

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Button
        variant="contained"
        onClick={handleClickOpen}
        startIcon={<AddCircleOutline />}
      >
        {t(messages.addPartnersToTerm())}
      </Button>
      <Dialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
        scroll="paper"
        maxWidth={'xl'}
        style={{ marginTop: 60 }}
      >
        <DialogTitle id="customized-dialog-title" onClose={handleClose}>
          {t(messages.addPartnersToTermDialog())}
        </DialogTitle>
        <DialogContent dividers>
          <Autocomplete
            multiple
            filterSelectedOptions
            id="select-partner"
            onChange={(
              event: React.SyntheticEvent<Element, Event>,
              value: PartnerListing[] | null,
            ) => {
              if (value) {
                handleChange(value);
              }
            }}
            getOptionLabel={option => option.name}
            options={partnersList}
            value={selectedPartner}
            renderOption={(props, option) => (
              <Box
                component="li"
                sx={{ '& > img': { mr: 2, flexShrink: 0 } }}
                {...props}
                key={option.id}
              >
                <Grid container>
                  <Grid item xs={11}>
                    {option.name}
                  </Grid>
                  <IsAssociated isAssociate={Boolean(option.isAssociate)} />
                </Grid>
              </Box>
            )}
            sx={{ width: '50ch' }}
            renderInput={params => (
              <TextField
                {...params}
                label={t(messages.partner())}
                style={{ width: '50ch' }}
                InputProps={{
                  ...params.InputProps,
                  endAdornment: (
                    <React.Fragment>
                      {loading ? (
                        <CircularProgress color="inherit" size={20} />
                      ) : null}
                      {params.InputProps.endAdornment}
                    </React.Fragment>
                  ),
                }}
                error={
                  selectedPartner === undefined &&
                  Boolean(selectedPartner === undefined)
                }
                helperText={
                  selectedPartner === undefined && t(messages.required())
                }
              />
            )}
          />
        </DialogContent>
        <DialogActions>
          <Button
            autoFocus
            variant="contained"
            onClick={handleAddToTerm}
            sx={{ m: 1 }}
          >
            {t(messages.save())}
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
export default React.memo(AddPartnersToTerm);

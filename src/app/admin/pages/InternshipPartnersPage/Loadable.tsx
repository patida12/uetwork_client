import LazyLoading from 'app/components/LazyLoading/lazyLoading.component';
import { lazyLoad } from 'utils/loadable';

export const InternshipPartnersPage = lazyLoad(
  () => import('./index'),
  module => module.InternshipPartnersPage,
  {
    fallback: <LazyLoading />,
  },
);

import { translations } from 'locales/translations';
import { _t } from 'utils/messages';

export const messages = {
  sysDisplayName: () => _t(translations.dashboard.sysDisplayName),
  sysIntroduction: () => _t(translations.dashboard.sysIntroduction),
  totalStudents: () => _t(translations.dashboard.totalStudents),
  totalLecturers: () => _t(translations.dashboard.totalLecturers),
  totalCompanies: () => _t(translations.dashboard.totalCompanies),
  totalOrgs: () => _t(translations.dashboard.totalOrgs),
  totalClasses: () => _t(translations.dashboard.totalClasses),
  orgs: () => _t(translations.dashboard.orgs),
};

import { Container, Grid } from '@mui/material';
import dashboardApi from 'api/dashboardApi';
import { Results } from 'api/types';
import AppTotalSummary from 'app/components/Dashboard/AppTotalSummary';
import AppWelcome from 'app/components/Dashboard/AppWelcome';
import { PATH_ORGS_PAGE } from 'app/constants';
import { useAsync } from 'app/hooks/useAsync';
import partnerImage from 'assets/undraw_agreement_re_d4dv.svg';
import studentImage from 'assets/undraw_exams_g-4-ow.svg';
import orgImage from 'assets/undraw_mind_map_re_nlb6.svg';
import lecturerImage from 'assets/undraw_teacher_re_sico.svg';
import classImage from 'assets/undraw_team_page_re_cffb.svg';
import { t } from 'locales/i18n';
import { SysAdminDashboard } from 'models/sysAdminDashboard';
import * as React from 'react';
import { messages } from './messages';

export function SysAdminDashboardPage() {
  const { data, run } = useAsync<Results<SysAdminDashboard>>({
    data: {
      codeResults: {
        code: 0,
        message: '',
      },
      item: defaultStat,
    },
  });

  const sysAdminStat = data?.item ?? defaultStat;
  const isNotAvailable = sysAdminStat === defaultStat;
  React.useEffect(() => {
    run(dashboardApi.getSysAdminDashboard());
  }, [run]);
  return (
    <Container maxWidth={'xl'}>
      <Grid container spacing={3}>
        <Grid item xs={12} md={12}>
          <AppWelcome
            displayName={t(messages.sysDisplayName())}
            introduction={t(messages.sysIntroduction())}
            buttonLabel={t(messages.orgs())}
            buttonPath={PATH_ORGS_PAGE}
          />
        </Grid>
        <Grid item xs={12} md={6}>
          <AppTotalSummary
            title={t(messages.totalOrgs())}
            total={sysAdminStat.orgsStat.total}
            srcImage={orgImage}
            color={'info'}
            isNotAvailable={isNotAvailable}
          />
        </Grid>
        <Grid item xs={12} md={6}>
          <AppTotalSummary
            title={t(messages.totalClasses())}
            total={sysAdminStat.classesStat.total}
            srcImage={classImage}
            color={'success'}
            isNotAvailable={isNotAvailable}
          />
        </Grid>
        <Grid item xs={12} md={4}>
          <AppTotalSummary
            title={t(messages.totalStudents())}
            total={sysAdminStat.studentsStat.total}
            srcImage={studentImage}
            color={'warning'}
            isNotAvailable={isNotAvailable}
          />
        </Grid>
        <Grid item xs={12} md={4}>
          <AppTotalSummary
            title={t(messages.totalCompanies())}
            total={sysAdminStat.partnersStat.total}
            srcImage={partnerImage}
            color={'error'}
            isNotAvailable={isNotAvailable}
          />
        </Grid>
        <Grid item xs={12} md={4}>
          <AppTotalSummary
            title={t(messages.totalLecturers())}
            total={sysAdminStat.lecturersStat.total}
            srcImage={lecturerImage}
            color={'primary'}
            isNotAvailable={isNotAvailable}
          />
        </Grid>
      </Grid>
    </Container>
  );
}

const defaultStat: SysAdminDashboard = {
  orgsStat: {
    total: 0,
  },
  classesStat: {
    total: 0,
  },
  studentsStat: {
    total: 0,
  },
  partnersStat: {
    total: 0,
  },
  lecturersStat: {
    total: 0,
  },
};

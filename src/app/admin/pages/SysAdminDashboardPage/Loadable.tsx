/**
 * Asynchronously loads the component for HomePage
 */

import { lazyLoad } from 'utils/loadable';

export const SysAdminDashboardPage = lazyLoad(
  () => import('./index'),
  module => module.SysAdminDashboardPage,
);

import { Container, Grid } from '@mui/material';
import dashboardApi from 'api/dashboardApi';
import { Results } from 'api/types';
import AppPieChart from 'app/components/Dashboard/AppPieChart';
import AppTopData from 'app/components/Dashboard/AppTopData';
import AppWelcome from 'app/components/Dashboard/AppWelcome';
import AppWidgetSummary from 'app/components/Dashboard/AppWidgetSummary';
import { PATH_TERMS_PAGE } from 'app/constants';
import { useAsync } from 'app/hooks/useAsync';
import partnerImage from 'assets/undraw_agreement_re_d4dv.svg';
import studentImage from 'assets/undraw_exams_g-4-ow.svg';
import lecturerImage from 'assets/undraw_teacher_re_sico.svg';
import { t } from 'locales/i18n';
import { OrgAdminDashboard } from 'models/orgAdminDashboard';
import * as React from 'react';
import { messages } from './messages';

export function OrgAdminDashboardPage() {
  const { data, run } = useAsync<Results<OrgAdminDashboard>>({
    data: {
      codeResults: {
        code: 0,
        message: '',
      },
      item: defaultStat,
    },
  });

  const orgAdminStat = data?.item ?? defaultStat;
  const isNotAvailable = orgAdminStat === defaultStat;
  React.useEffect(() => {
    run(dashboardApi.getOrgAdminDashboard());
  }, [run]);
  return (
    <Container maxWidth={'xl'}>
      <Grid container spacing={3}>
        <Grid item xs={12} md={12}>
          <AppWelcome
            displayName={t(messages.displayName())}
            introduction={t(messages.introduction())}
            buttonLabel={t(messages.terms())}
            buttonPath={PATH_TERMS_PAGE}
          />
        </Grid>
        <Grid item xs={12} md={4}>
          <AppWidgetSummary
            title={t(messages.totalStudents())}
            lastTotal={orgAdminStat.termStudentsStat.lastTotal}
            currentTotal={orgAdminStat.termStudentsStat.currentTotal}
            srcImage={studentImage}
            isNotAvailable={isNotAvailable}
          />
        </Grid>
        <Grid item xs={12} md={4}>
          <AppWidgetSummary
            title={t(messages.totalCompanies())}
            lastTotal={orgAdminStat.termPartnersStat.lastTotal}
            currentTotal={orgAdminStat.termPartnersStat.currentTotal}
            srcImage={partnerImage}
            isNotAvailable={isNotAvailable}
          />
        </Grid>
        <Grid item xs={12} md={4}>
          <AppWidgetSummary
            title={t(messages.totalLecturers())}
            lastTotal={orgAdminStat.termLecturersStat.lastTotal}
            currentTotal={orgAdminStat.termLecturersStat.currentTotal}
            srcImage={lecturerImage}
            isNotAvailable={isNotAvailable}
          />
        </Grid>
        <Grid item xs={12} md={6}>
          <AppPieChart
            title={t(messages.company())}
            chartData={[
              orgAdminStat.termPartnersStatusStat.totalPending,
              orgAdminStat.termPartnersStatusStat.totalAccepted,
              orgAdminStat.termPartnersStatusStat.totalRejected,
            ]}
            labels={['Pending', 'Accepted', 'Rejected']}
            isNotAvailable={isNotAvailable}
          />
        </Grid>

        <Grid item xs={12} md={6}>
          <AppTopData
            title={t(messages.top5Companies())}
            nameQuantity={t(messages.nameQuantity())}
            list={orgAdminStat.mostPopularPartnersStat[0].partners}
            isNotAvailable={isNotAvailable}
          />
        </Grid>
      </Grid>
    </Container>
  );
}

const defaultStat: OrgAdminDashboard = {
  termStudentsStat: {
    lastTotal: 0,
    currentTotal: 0,
  },
  termPartnersStat: {
    lastTotal: 0,
    currentTotal: 0,
  },
  termLecturersStat: {
    lastTotal: 0,
    currentTotal: 0,
  },
  termPartnersStatusStat: {
    totalPending: 0,
    totalAccepted: 0,
    totalRejected: 0,
  },
  mostPopularPartnersStat: [
    {
      partners: [
        {
          id: 0,
          name: '',
          studentsCount: 0,
          logoUrl: '',
        },
      ],
    },
  ],
};

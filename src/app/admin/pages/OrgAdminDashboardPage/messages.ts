import { translations } from 'locales/translations';
import { _t } from 'utils/messages';

export const messages = {
  displayName: () => _t(translations.dashboard.displayName),
  introduction: () => _t(translations.dashboard.introduction),
  totalStudents: () => _t(translations.dashboard.totalStudents),
  totalLecturers: () => _t(translations.dashboard.totalLecturers),
  totalCompanies: () => _t(translations.dashboard.totalCompanies),
  company: () => _t(translations.dashboard.company),
  top5Companies: () => _t(translations.dashboard.top5Companies),
  nameQuantity: () => _t(translations.dashboard.nameQuantity),
  terms: () => _t(translations.dashboard.terms),
};

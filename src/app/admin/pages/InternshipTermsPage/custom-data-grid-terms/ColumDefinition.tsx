import { GridCellParams, GridColumns } from '@mui/x-data-grid';
import GridCellExpand from 'app/components/GridCellExpand';
import { t } from 'locales/i18n';
import { InternshipTerm } from 'models/internshipTerm';
import { formatDate } from 'utils';
import EditTermDialog from '../components/EditTermDialog';
import { messages } from '../messages';

export const getColumns = (refreshData?: () => void): GridColumns => [
  {
    field: 'id',
    sortable: false,
    disableColumnMenu: true,
    hide: true,
  },
  {
    field: 'year',
    sortable: false,
    disableColumnMenu: true,
    hide: true,
  },
  {
    field: 'term',
    sortable: false,
    disableColumnMenu: true,
    hide: true,
  },

  {
    field: 'nameOfTerm',
    headerName: t(messages.nameOfTerm()),
    width: 200,
    renderCell: (params: GridCellParams) => (
      <GridCellExpand
        value={
          t(messages.term()) +
          ' ' +
          params.row.term.toString() +
          ' - ' +
          params.row.year
        }
        width={params.colDef.width}
      />
    ),
    disableColumnMenu: true,
    sortable: false,
  },
  {
    field: 'createdAt',
    headerName: t(messages.createdAt()),
    width: 200,
    renderCell: (params: GridCellParams) => (
      <GridCellExpand
        value={params.value ? formatDate(params.value.toString()) : ''}
        width={params.colDef.width}
      />
    ),
    disableColumnMenu: true,
  },
  {
    field: 'startDate',
    headerName: t(messages.startDate()),
    width: 200,
    renderCell: (params: GridCellParams) => (
      <GridCellExpand
        value={params.value ? formatDate(params.value.toString()) : ''}
        width={params.colDef.width}
      />
    ),
    disableColumnMenu: true,
    sortable: false,
  },
  {
    field: 'endDate',
    headerName: t(messages.endDate()),
    width: 200,
    renderCell: (params: GridCellParams) => (
      <GridCellExpand
        value={params.value ? formatDate(params.value.toString()) : ''}
        width={params.colDef.width}
      />
    ),
    disableColumnMenu: true,
    sortable: false,
  },
  {
    field: 'startRegAt',
    headerName: t(messages.startRegAt()),
    width: 200,
    renderCell: (params: GridCellParams) => (
      <GridCellExpand
        value={params.value ? formatDate(params.value.toString()) : ''}
        width={params.colDef.width}
      />
    ),
    disableColumnMenu: true,
    sortable: false,
  },
  {
    field: 'endRegAt',
    headerName: t(messages.endRegAt()),
    width: 200,
    renderCell: (params: GridCellParams) => (
      <GridCellExpand
        value={params.value ? formatDate(params.value.toString()) : ''}
        width={params.colDef.width}
      />
    ),
    disableColumnMenu: true,
    sortable: false,
  },

  {
    field: 'organizationId',
    sortable: false,
    disableColumnMenu: true,
    hide: true,
  },
  {
    field: 'options',
    headerName: t(messages.option()),
    width: 150,
    sortable: false,
    disableColumnMenu: true,
    renderCell: (params: GridCellParams) => (
      <EditTermDialog
        internshipTerm={params.row as InternshipTerm}
        isEditting={true}
        refreshData={refreshData!}
      />
    ),
  },
];

import SearchIcon from '@mui/icons-material/Search';
import { Button } from '@mui/material';
import Grid from '@mui/material/Grid';
import { styled } from '@mui/material/styles';
import TextField from '@mui/material/TextField';
import { GridToolbarContainer } from '@mui/x-data-grid';
import { useFormik } from 'formik';
import { t } from 'locales/i18n';
import { CustomToolbarProps, FilterState } from 'models';
import React from 'react';
import * as Yup from 'yup';
import AddTermDialog from '../components/AddTermDialog';
import { messages } from '../messages';

const PREFIX = 'CustomToolbar';

const classes = {
  textField: `${PREFIX}-textField`,
};

const StyledGridToolbarContainer = styled(GridToolbarContainer)(
  ({ theme }) => ({
    [`& .${classes.textField}`]: {
      width: '25ch',
    },
  }),
);

function CustomToolbar({
  onFilterChange,
  selectionModel,
  refreshData,
}: CustomToolbarProps) {
  const SearchTermSchema = Yup.object().shape({
    filter: Yup.object().shape({
      year: Yup.number().min(1970),
    }),
  });
  const formik = useFormik<FilterState>({
    initialValues: {
      query: '',
      filter: {
        year: '',
      },
    },
    validationSchema: SearchTermSchema,
    onSubmit: values => {
      var filters: FilterState = {
        query: '',
        filter: {
          year: values.filter.year === '' ? '' : Number(values.filter.year),
        },
      };
      onFilterChange(filters);
    },
  });

  return (
    <form onSubmit={formik.handleSubmit}>
      <StyledGridToolbarContainer style={{ margin: 10 }}>
        <Grid
          container
          direction="row"
          justifyContent="space-between"
          spacing={1}
        >
          <Grid
            item
            xs={12}
            sm={8}
            md={8}
            direction="row"
            container
            alignItems="center"
            justifyContent="flex-start"
            spacing={1}
          >
            <Grid item>
              <TextField
                value={formik.values.filter.year}
                onChange={formik.handleChange}
                id="filter.year"
                className={classes.textField}
                size="small"
                variant="outlined"
                rows={1}
                label={t(messages.year())}
              />
            </Grid>
            <Grid item>
              <Button
                variant="contained"
                type="submit"
                startIcon={<SearchIcon />}
              >
                {t(messages.search())}
              </Button>
            </Grid>
          </Grid>
          <Grid item>
            <AddTermDialog refreshData={refreshData} />
          </Grid>
        </Grid>
      </StyledGridToolbarContainer>
    </form>
  );
}

export default React.memo(CustomToolbar);

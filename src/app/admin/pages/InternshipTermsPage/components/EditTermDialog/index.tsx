import { AddCircleOutlined, EditOutlined } from '@mui/icons-material';
import { DatePicker, DateTimePicker } from '@mui/lab';
import { IconButton, Tooltip } from '@mui/material';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import Grid from '@mui/material/Grid';
import TextField from '@mui/material/TextField';
import internshipTermApi from 'api/internship/internshipTermApi';
import {
  DialogActions,
  DialogContent,
  DialogTitle,
} from 'app/components/CustomDialog';
import { TermContext } from 'contexts/TermProvider';
import { parseISO } from 'date-fns';
import dayjs from 'dayjs';
import { useFormik } from 'formik';
import { t } from 'locales/i18n';
import { InternshipTerm, NewTerm } from 'models/internshipTerm';
import { useSnackbar } from 'notistack';
import React, { useContext } from 'react';
import 'react-phone-number-input/style.css';
import 'react-quill/dist/quill.snow.css';
import theme from 'styles/theme';
import { convertUTCDateToLocalDate } from 'utils/convertTime';
import * as Yup from 'yup';
import { messages } from '../../messages';
export interface DefaultTerm {
  term: number;
  year: number;
  startRegAt: Date;
  endRegAt: Date;
  startDate: Date;
  endDate: Date;
}

interface EditTermDialogProps {
  internshipTerm: InternshipTerm;
  isEditting: boolean;
  refreshData: () => void;
}

function EditTermDialog({
  internshipTerm,
  isEditting,
  refreshData,
}: EditTermDialogProps) {
  const [open, setOpen] = React.useState(false);
  const {
    id,
    year,
    term,
    startDate,
    endDate,
    startRegAt,
    endRegAt,
  } = internshipTerm;
  const { set_list_terms } = useContext(TermContext);

  const edit = t(messages.edit());
  const { enqueueSnackbar } = useSnackbar();

  const EditTermSchema = Yup.object().shape({
    year: Yup.number().integer().required(t(messages.required())),
    term: Yup.number().integer().min(1).required(t(messages.required())),
    startDate: Yup.date().required(t(messages.required())),
    endDate: Yup.date().required(t(messages.required())),
    startRegAt: Yup.date().required(t(messages.required())),
    endRegAt: Yup.date().required(t(messages.required())),
  });
  const formik = useFormik<DefaultTerm>({
    initialValues: {
      year: year,
      term: term,
      startDate: parseISO(startDate),
      endDate: parseISO(endDate),
      startRegAt: parseISO(startRegAt),
      endRegAt: parseISO(endRegAt),
    },
    validationSchema: EditTermSchema,
    onSubmit: values => {
      var newTerm: NewTerm = {
        ...values,
        year: Number(values.year),
        startDate: convertUTCDateToLocalDate(new Date(values.startDate)).slice(
          0,
          10,
        ),
        endDate: convertUTCDateToLocalDate(new Date(values.endDate)).slice(
          0,
          10,
        ),
        startRegAt: convertUTCDateToLocalDate(new Date(values.startRegAt)),
        endRegAt: convertUTCDateToLocalDate(new Date(values.endRegAt)),
      };
      var termId = isEditting ? id : undefined;
      internshipTermApi
        .upsertTerm(newTerm, termId)
        .then(resp => {
          internshipTermApi
            .getTermListing()
            .then(res => set_list_terms(res.item))
            .catch(e => set_list_terms([]));

          enqueueSnackbar(
            isEditting ? t(messages.editSuccess()) : t(messages.addSuccess()),
            { variant: 'success' },
          );
          handleClose();
          refreshData();
        })
        .catch(async e => {
          enqueueSnackbar(e.response?.data.message, { variant: 'error' });
        });
    },
  });

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      {isEditting ? (
        <Tooltip title={edit}>
          <IconButton
            style={{
              color: theme.palette.primary.light,
            }}
            aria-label="edit"
            onClick={handleClickOpen}
            size="large"
          >
            <EditOutlined />
          </IconButton>
        </Tooltip>
      ) : (
        <Button
          variant={'contained'}
          onClick={handleClickOpen}
          startIcon={<AddCircleOutlined />}
        >
          {t(messages.add())}
        </Button>
      )}
      <Dialog
        aria-labelledby="customized-dialog-title"
        open={open}
        scroll="paper"
        maxWidth={'lg'}
        style={{ marginTop: 60 }}
      >
        <DialogTitle id="customized-dialog-title" onClose={handleClose}>
          {isEditting
            ? t(messages.editDialogTitle())
            : t(messages.addDialogTitle())}
        </DialogTitle>
        <div>
          <form onSubmit={formik.handleSubmit}>
            <DialogContent dividers>
              <Grid
                direction="column"
                container
                alignItems="center"
                justifyContent="center"
                spacing={2}
              >
                <Grid
                  item
                  direction="row"
                  container
                  alignItems="center"
                  justifyContent="space-around"
                  spacing={2}
                >
                  <Grid item>
                    <DatePicker
                      views={['year']}
                      value={formik.values.year.toString()}
                      onChange={value => {
                        formik.setFieldValue(
                          'year',
                          dayjs(value).format('YYYY').toString(),
                        );
                      }}
                      renderInput={params => (
                        <TextField
                          {...params}
                          style={{ width: '50ch' }}
                          placeholder={t(messages.year())}
                          error={
                            formik.touched.year && Boolean(formik.errors.year)
                          }
                          helperText={formik.touched.year && formik.errors.year}
                          label={t(messages.year())}
                        />
                      )}
                    />
                  </Grid>
                  <Grid item>
                    <TextField
                      value={formik.values.term}
                      onChange={formik.handleChange}
                      id="term"
                      type="number"
                      style={{ width: '50ch' }}
                      InputProps={{
                        inputProps: {
                          min: 1,
                        },
                      }}
                      label={t(messages.term())}
                    />
                  </Grid>
                </Grid>
                <Grid
                  item
                  direction="row"
                  container
                  alignItems="center"
                  justifyContent="space-around"
                  spacing={2}
                >
                  <Grid item>
                    <DatePicker
                      value={formik.values.startDate}
                      onChange={value => {
                        formik.setFieldValue('startDate', value);
                      }}
                      renderInput={params => (
                        <TextField
                          {...params}
                          style={{ width: '50ch' }}
                          placeholder={t(messages.startDate())}
                          error={
                            formik.touched.startDate &&
                            Boolean(formik.errors.startDate)
                          }
                          helperText={
                            formik.touched.startDate && formik.errors.startDate
                          }
                          label={t(messages.startDate())}
                        />
                      )}
                    />
                  </Grid>
                  <Grid item>
                    <DatePicker
                      value={formik.values.endDate}
                      onChange={value => {
                        formik.setFieldValue('endDate', value);
                      }}
                      minDate={formik.values.startDate}
                      renderInput={params => (
                        <TextField
                          {...params}
                          style={{ width: '50ch' }}
                          placeholder={t(messages.endDate())}
                          error={
                            formik.touched.endDate &&
                            Boolean(formik.errors.endDate)
                          }
                          helperText={
                            formik.touched.endDate && formik.errors.endDate
                          }
                          label={t(messages.endDate())}
                        />
                      )}
                    />
                  </Grid>
                </Grid>
                <Grid
                  item
                  direction="row"
                  container
                  alignItems="center"
                  justifyContent="space-around"
                  spacing={2}
                >
                  <Grid item>
                    <DateTimePicker
                      value={formik.values.startRegAt}
                      onChange={value => {
                        formik.setFieldValue('startRegAt', value);
                      }}
                      minDate={formik.values.startDate}
                      renderInput={params => (
                        <TextField
                          {...params}
                          style={{ width: '50ch' }}
                          placeholder={t(messages.startRegAt())}
                          error={
                            formik.touched.startRegAt &&
                            Boolean(formik.errors.startRegAt)
                          }
                          helperText={
                            formik.touched.startRegAt &&
                            formik.errors.startRegAt
                          }
                          label={t(messages.startRegAt())}
                        />
                      )}
                    />
                  </Grid>
                  <Grid item>
                    <DateTimePicker
                      value={formik.values.endRegAt}
                      onChange={value => {
                        formik.setFieldValue('endRegAt', value);
                      }}
                      minDate={formik.values.startRegAt}
                      renderInput={params => (
                        <TextField
                          {...params}
                          style={{ width: '50ch' }}
                          placeholder={t(messages.endRegAt())}
                          error={
                            formik.touched.endRegAt &&
                            Boolean(formik.errors.endRegAt)
                          }
                          helperText={
                            formik.touched.endRegAt && formik.errors.endRegAt
                          }
                          label={t(messages.endRegAt())}
                        />
                      )}
                    />
                  </Grid>
                </Grid>
              </Grid>
            </DialogContent>
            <DialogActions>
              <Button autoFocus variant="contained" type="submit" sx={{ m: 1 }}>
                {t(messages.save())}
              </Button>
            </DialogActions>
          </form>
        </div>
      </Dialog>
    </div>
  );
}
export default React.memo(EditTermDialog);

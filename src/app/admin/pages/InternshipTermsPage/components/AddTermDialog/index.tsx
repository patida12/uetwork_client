import { InternshipTerm } from 'models/internshipTerm';
import React from 'react';
import EditTermDialog from '../EditTermDialog';

var newTerm: InternshipTerm = {
  id: 0,
  term: 1,
  year: new Date().getFullYear(),
  startRegAt: new Date().toISOString(),
  endRegAt: new Date().toISOString(),
  startDate: new Date().toISOString(),
  endDate: new Date().toISOString(),
  createdAt: new Date().toISOString(),
  organizationId: 0,
};

interface AddTermProps {
  refreshData?: () => void;
}

function AddTermDialog({ refreshData }: AddTermProps) {
  return (
    <EditTermDialog
      internshipTerm={newTerm}
      isEditting={false}
      refreshData={refreshData!}
    />
  );
}

export default React.memo(AddTermDialog);

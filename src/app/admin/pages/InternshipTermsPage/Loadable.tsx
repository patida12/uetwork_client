import LazyLoading from 'app/components/LazyLoading/lazyLoading.component';
import { lazyLoad } from 'utils/loadable';

export const InternshipTermsPage = lazyLoad(
  () => import('./index'),
  module => module.InternshipTermsPage,
  {
    fallback: <LazyLoading />,
  },
);

import { translations } from 'locales/translations';
import { _t } from 'utils/messages';

export const messages = {
  nameOfTerm: () => _t(translations.internship.termFeature.nameOfTerm),
  startRegAt: () => _t(translations.internship.termFeature.startRegAt),
  endRegAt: () => _t(translations.internship.termFeature.endRegAt),
  startDate: () => _t(translations.internship.termFeature.startDate),
  endDate: () => _t(translations.internship.termFeature.endDate),
  createdAt: () => _t(translations.internship.termFeature.createdAt),
  term: () => _t(translations.internship.termFeature.term),
  year: () => _t(translations.internship.termFeature.year),
  search: () => _t(translations.common.table.search),
  required: () => _t(translations.common.validate.required),
  add: () => _t(translations.common.button.add),
  edit: () => _t(translations.common.button.edit),
  detail: () => _t(translations.common.button.detail),
  delete: () => _t(translations.common.button.delete),
  save: () => _t(translations.common.button.save),
  close: () => _t(translations.common.button.close),
  option: () => _t(translations.common.table.option),
  addDialogTitle: () => _t(translations.internship.termFeature.addTerm),
  editDialogTitle: () => _t(translations.internship.termFeature.editTerm),
  addSuccess: () => _t(translations.internship.termFeature.addSuccess),
  editSuccess: () => _t(translations.internship.termFeature.editSuccess),
};

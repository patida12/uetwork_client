import { AddCircle, AddCircleOutlined } from '@mui/icons-material';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import Grid from '@mui/material/Grid';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import profilePartnerApi from 'api/profilePartnerApi';
import {
  DialogActions,
  DialogContent,
  DialogTitle,
} from 'app/components/CustomDialog';
import { useFormik } from 'formik';
import { t } from 'locales/i18n';
import { useSnackbar } from 'notistack';
import React from 'react';
import theme from 'styles/theme';
import * as Yup from 'yup';
import { messages } from '../../messages';

function AddPartnerDialog() {
  const [open, setOpen] = React.useState(false);
  const { enqueueSnackbar } = useSnackbar();

  const AddPartnerSchema = Yup.object().shape({
    name: Yup.string().required(t(messages.required())),
    homepageUrl: Yup.string().required(t(messages.required())),
    logoUrl: Yup.string().required(t(messages.required())),
    description: Yup.string().required(t(messages.required())),
    orgEmail: Yup.string().email(t(messages.emailNotValidMsg())),
  });

  const formik = useFormik({
    initialValues: {
      name: '',
      homepageUrl: '',
      logoUrl: '',
      description: '',
      orgEmail: '',
    },
    validationSchema: AddPartnerSchema,
    onSubmit: values => {
      profilePartnerApi
        .addAccountPartner(values)
        .then(res => {
          enqueueSnackbar(t(messages.addAccountSuccess()), {
            variant: 'success',
          });
          handleClose();
        })
        .catch(e => {
          enqueueSnackbar(e.response?.data.message, { variant: 'error' });
        });
    },
  });

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Button
        variant="contained"
        onClick={handleClickOpen}
        startIcon={<AddCircleOutlined />}
      >
        {t(messages.add())}
      </Button>
      <Dialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
        scroll="paper"
        maxWidth={'md'}
        style={{ marginTop: 60 }}
      >
        <DialogTitle id="customized-dialog-title" onClose={handleClose}>
          <AddCircle
            fontSize="small"
            style={{
              color: theme.palette.primary.main,
              marginRight: theme.spacing(1),
              marginBottom: theme.spacing(0.5),
            }}
          />
          {t(messages.addDialogTitle())}
        </DialogTitle>
        <div>
          <form onSubmit={formik.handleSubmit}>
            <DialogContent dividers>
              <Grid
                direction="column"
                container
                alignContent="center"
                justifyContent="center"
                spacing={1}
              >
                <Grid
                  item
                  direction="row"
                  container
                  alignContent="flex-start"
                  justifyContent="center"
                  sx={{ width: '55ch' }}
                >
                  <Grid item xs={3}>
                    <Typography>{t(messages.partnerName())}:</Typography>
                  </Grid>
                  <Grid item xs={9}>
                    <TextField
                      size="small"
                      variant="outlined"
                      style={{ width: '40ch' }}
                      id="name"
                      name="name"
                      value={formik.values.name}
                      onChange={formik.handleChange}
                      error={formik.touched.name && Boolean(formik.errors.name)}
                      helperText={formik.touched.name && formik.errors.name}
                    />
                  </Grid>
                </Grid>
                <Grid
                  item
                  direction="row"
                  container
                  alignContent="flex-start"
                  justifyContent="center"
                >
                  <Grid item xs={3}>
                    <Typography>{t(messages.homepageUrl())}:</Typography>
                  </Grid>
                  <Grid item xs={9}>
                    <TextField
                      size="small"
                      variant="outlined"
                      style={{ width: '40ch' }}
                      id="homepageUrl"
                      name="homepageUrl"
                      value={formik.values.homepageUrl}
                      onChange={formik.handleChange}
                      error={
                        formik.touched.homepageUrl &&
                        Boolean(formik.errors.homepageUrl)
                      }
                      helperText={
                        formik.touched.homepageUrl && formik.errors.homepageUrl
                      }
                    />
                  </Grid>
                </Grid>
                <Grid
                  item
                  direction="row"
                  container
                  alignContent="flex-start"
                  justifyContent="center"
                >
                  <Grid item xs={3}>
                    <Typography>{t(messages.description())}:</Typography>
                  </Grid>
                  <Grid item xs={9}>
                    <TextField
                      size="small"
                      variant="outlined"
                      style={{ width: '40ch' }}
                      multiline
                      id="description"
                      name="description"
                      value={formik.values.description}
                      onChange={formik.handleChange}
                      error={
                        formik.touched.description &&
                        Boolean(formik.errors.description)
                      }
                      helperText={
                        formik.touched.description && formik.errors.description
                      }
                    />
                  </Grid>
                </Grid>
                <Grid
                  item
                  direction="row"
                  container
                  alignContent="flex-start"
                  justifyContent="center"
                >
                  <Grid item xs={3}>
                    <Typography>{t(messages.logoUrl())}:</Typography>
                  </Grid>
                  <Grid item xs={9}>
                    <TextField
                      size="small"
                      variant="outlined"
                      style={{ width: '40ch' }}
                      id="logoUrl"
                      name="logoUrl"
                      value={formik.values.logoUrl}
                      onChange={formik.handleChange}
                      error={
                        formik.touched.logoUrl && Boolean(formik.errors.logoUrl)
                      }
                      helperText={
                        formik.touched.logoUrl && formik.errors.logoUrl
                      }
                    />
                  </Grid>
                </Grid>
                <Grid
                  item
                  direction="row"
                  container
                  alignContent="flex-start"
                  justifyContent="center"
                >
                  <Grid item xs={3}>
                    <Typography>{t(messages.orgEmail())}:</Typography>
                  </Grid>
                  <Grid item xs={9}>
                    <TextField
                      size="small"
                      variant="outlined"
                      style={{ width: '40ch' }}
                      id="orgEmail"
                      name="orgEmail"
                      value={formik.values.orgEmail}
                      onChange={formik.handleChange}
                      error={
                        formik.touched.orgEmail &&
                        Boolean(formik.errors.orgEmail)
                      }
                      helperText={
                        formik.touched.orgEmail && formik.errors.orgEmail
                      }
                    />
                  </Grid>
                </Grid>
              </Grid>
            </DialogContent>
            <DialogActions>
              <Button autoFocus variant="contained" type="submit">
                {t(messages.save())}
              </Button>
            </DialogActions>
          </form>
        </div>
      </Dialog>
    </div>
  );
}
export default React.memo(AddPartnerDialog);

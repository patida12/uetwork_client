import SearchIcon from '@mui/icons-material/Search';
import { Button } from '@mui/material';
import Grid from '@mui/material/Grid';
import { styled } from '@mui/material/styles';
import TextField from '@mui/material/TextField';
import { GridToolbarContainer } from '@mui/x-data-grid';
import { useFormik } from 'formik';
import { t } from 'locales/i18n';
import { CustomToolbarProps, FilterState } from 'models';
import React from 'react';
import AddPartnerDialog from '../components/AddPartnerDialog';
import { messages } from '../messages';

const PREFIX = 'CustomToolbar';

const classes = {
  textField: `${PREFIX}-textField`,
};

const StyledGridToolbarContainer = styled(GridToolbarContainer)(
  ({ theme }) => ({
    [`& .${classes.textField}`]: {
      width: '25ch',
    },
  }),
);

function CustomToolbar({ onFilterChange, selectionModel }: CustomToolbarProps) {
  const formik = useFormik<FilterState>({
    initialValues: {
      query: '',
      filter: {},
    },
    onSubmit: values => {
      var filters: FilterState = {
        query: values.query,
        filter: {},
      };
      onFilterChange(filters);
    },
  });
  return (
    <form onSubmit={formik.handleSubmit}>
      <StyledGridToolbarContainer style={{ margin: 10 }}>
        <Grid
          container
          direction="row"
          justifyContent="space-between"
          spacing={1}
        >
          <Grid
            item
            xs={12}
            sm={10}
            md={10}
            direction="row"
            container
            alignContent="flex-end"
            justifyContent="flex-start"
            spacing={1}
          >
            <Grid item>
              <TextField
                value={formik.values.query}
                onChange={formik.handleChange}
                id="query"
                className={classes.textField}
                size="small"
                variant="outlined"
                rows={1}
                label={t(messages.keyword())}
              />
            </Grid>
            <Grid item>
              <Button
                variant="contained"
                type="submit"
                startIcon={<SearchIcon />}
              >
                {t(messages.search())}
              </Button>
            </Grid>
          </Grid>
          <Grid item>
            <AddPartnerDialog />
          </Grid>
        </Grid>
      </StyledGridToolbarContainer>
    </form>
  );
}
export default React.memo(CustomToolbar);

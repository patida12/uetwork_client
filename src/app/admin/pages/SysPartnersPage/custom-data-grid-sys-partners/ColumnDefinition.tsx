import Link from '@mui/material/Link';
import { GridCellParams, GridColumns } from '@mui/x-data-grid';
import GridCellExpand from 'app/components/GridCellExpand';
import { t } from 'locales/i18n';
import * as React from 'react';
import ShowContactsDialog from '../../ProfilePartnersPage/components/ShowContactsDialog';
import { messages } from '../messages';

function getColumns(): GridColumns {
  return [
    {
      field: 'id',
      sortable: false,
      disableColumnMenu: true,
      hide: true,
      filterable: false,
    },
    {
      field: 'name',
      headerName: t(messages.partnerName()),
      width: 450,
      renderCell: (params: GridCellParams) => (
        <GridCellExpand
          value={params.value ? params.value.toString() : ''}
          width={params.colDef.width}
        />
      ),
      filterable: true,
    },
    {
      field: 'homepageUrl',
      headerName: t(messages.homepageUrl()),
      width: 350,
      renderCell: (params: GridCellParams) => (
        <Link
          href={`${params.value}`}
          target="_blank"
          rel="noreferrer"
          variant="body2"
        >
          {params.value}
        </Link>
      ),
      filterable: true,
      sortable: false,
    },
    {
      field: 'isAssociate',
      headerName: t(messages.status()),
      width: 0,
      sortable: false,
      disableColumnMenu: true,
      filterable: false,
      hide: true,
    },
    {
      field: 'contactsList',
      headerName: t(messages.contact()),
      width: 150,
      renderCell: (params: GridCellParams) => (
        <strong>
          <ShowContactsDialog partner={params.row} />
        </strong>
      ),
      sortable: false,
      disableColumnMenu: true,
      filterable: false,
    },
  ];
}

export { getColumns };

import Link from '@mui/material/Link';
import { GridCellParams, GridColumns } from '@mui/x-data-grid';
import profileStudentApi from 'api/profileStudentApi';
import GridCellExpand from 'app/components/GridCellExpand';
import { t } from 'locales/i18n';
import React from 'react';
import { messages } from '../messages';

const getColumns = (): GridColumns => [
  {
    field: 'id',
    headerName: 'ID',
    sortable: false,
    disableColumnMenu: true,
    hide: true,
  },
  {
    field: 'studentIdNumber',
    headerName: t(messages.studentIdNumber()),
    width: 200,
    renderCell: (params: GridCellParams) => (
      <GridCellExpand
        value={params.value ? params.value.toString() : ''}
        width={params.colDef.width}
      />
    ),
    sortable: false,
  },
  {
    field: 'fullName',
    headerName: t(messages.fullName()),
    width: 250,
    renderCell: (params: GridCellParams) => (
      <GridCellExpand
        value={params.value ? params.value.toString() : ''}
        width={params.colDef.width}
      />
    ),
  },
  {
    field: 'orgEmail',
    headerName: t(messages.orgEmail()),
    width: 250,
    renderCell: (params: GridCellParams) => (
      <GridCellExpand
        value={params.value ? params.value.toString() : ''}
        width={params.colDef.width}
      />
    ),
    sortable: false,
    disableColumnMenu: true,
  },
  {
    field: 'personalEmail',
    headerName: t(messages.personalEmail()),
    width: 250,
    renderCell: (params: GridCellParams) => (
      <GridCellExpand
        value={params.value ? params.value.toString() : ''}
        width={params.colDef.width}
      />
    ),
    sortable: false,
    disableColumnMenu: true,
  },
  {
    field: 'phoneNumber',
    headerName: t(messages.phoneNumber()),
    width: 250,
    renderCell: (params: GridCellParams) => (
      <GridCellExpand
        value={params.value ? params.value.toString() : ''}
        width={params.colDef.width}
      />
    ),
    sortable: false,
    disableColumnMenu: true,
  },

  {
    field: 'organization',
    headerName: t(messages.organization()),
    width: 250,
    renderCell: (params: GridCellParams) => (
      <GridCellExpand
        value={params.value ? params.value.name : ''}
        width={params.colDef.width}
      />
    ),
    sortable: false,
  },
  {
    field: 'schoolClass',
    headerName: t(messages.schoolClass()),
    width: 250,
    renderCell: (params: GridCellParams) => (
      <GridCellExpand
        value={params.value ? params.value.name : ''}
        width={params.colDef.width}
      />
    ),
    sortable: false,
  },
  {
    field: 'resumeUrl',
    headerName: t(messages.resumeUrl()),
    width: 200,
    renderCell: (params: GridCellParams) => (
      <>
        {params.value !== null && params.value !== '' ? (
          <Link
            underline="hover"
            onClick={async () => {
              const url = await profileStudentApi.downloadResume(params.row.id);

              window.open(url, '_blank');
            }}
          >
            {t(messages.download())}
          </Link>
        ) : (
          t(messages.notHaveResume())
        )}
      </>
    ),
    sortable: false,
    disableColumnMenu: true,
  },
];

export { getColumns };

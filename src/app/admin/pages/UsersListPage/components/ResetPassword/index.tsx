import { Button } from '@mui/material';
import { resetPassword } from 'api/authApi';
import { t } from 'locales/i18n';
import { useSnackbar } from 'notistack';
import { messages } from '../../messages';

interface ResetPasswordProps {
  email: string;
}
export function ResetPassword({ email }: ResetPasswordProps) {
  const { enqueueSnackbar } = useSnackbar();
  const handleClick = () => {
    resetPassword(email)
      .then(res => {
        enqueueSnackbar(t(messages.resetPasswordSuccess()), {
          variant: 'success',
        });
      })
      .catch(e => {
        enqueueSnackbar(e.response?.data.message, { variant: 'error' });
      });
  };
  return (
    <Button variant="text" onClick={handleClick}>
      {t(messages.resetPassword())}
    </Button>
  );
}

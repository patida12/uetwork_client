import { GridCellParams, GridColumns } from '@mui/x-data-grid';
import GridCellExpand from 'app/components/GridCellExpand';
import { t } from 'locales/i18n';
import React from 'react';
import { formatDate } from 'utils';
import { ResetPassword } from '../components/ResetPassword';
import { messages } from '../messages';

export const getColumns = (): GridColumns => [
  {
    field: 'id',
    hide: true,
    sortable: false,
    disableColumnMenu: true,
  },
  {
    field: 'orgEmail',
    headerName: t(messages.orgEmail()),
    width: 250,
    renderCell: (params: GridCellParams) => (
      <GridCellExpand
        value={params.value ? params.value.toString() : ''}
        width={params.colDef.width}
      />
    ),
    sortable: false,
    disableColumnMenu: true,
  },
  {
    field: 'role',
    headerName: t(messages.role()),
    width: 250,
    renderCell: (params: GridCellParams) => (
      <GridCellExpand
        value={params.value ? params.value.toString() : ''}
        width={params.colDef.width}
      />
    ),
    sortable: false,
    disableColumnMenu: true,
  },

  {
    field: 'lastLoginAt',
    headerName: t(messages.lastLoginAt()),
    width: 250,
    renderCell: (params: GridCellParams) => (
      <GridCellExpand
        value={params.value ? formatDate(params.value.toString()) : ''}
        width={params.colDef.width}
      />
    ),
    sortable: false,
    disableColumnMenu: true,
  },
  {
    field: 'createdAt',
    headerName: t(messages.createdAt()),
    width: 250,
    renderCell: (params: GridCellParams) => (
      <GridCellExpand
        value={params.value ? formatDate(params.value.toString()) : ''}
        width={params.colDef.width}
      />
    ),
    sortable: false,
    disableColumnMenu: true,
  },
  {
    field: '',
    headerName: '',
    width: 150,
    sortable: false,
    disableColumnMenu: true,
    renderCell: (params: GridCellParams) => (
      <ResetPassword email={params.row.orgEmail} />
    ),
  },
];

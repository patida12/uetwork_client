import LazyLoading from 'app/components/LazyLoading/lazyLoading.component';
import { lazyLoad } from 'utils/loadable';

export const UsersListPage = lazyLoad(
  () => import('./index'),
  module => module.default,
  {
    fallback: <LazyLoading />,
  },
);

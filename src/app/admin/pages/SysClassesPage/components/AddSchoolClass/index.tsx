import { AddCircleOutlined } from '@mui/icons-material';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import Grid from '@mui/material/Grid';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import schoolClassApi from 'api/schoolClassApi';
import {
  DialogActions,
  DialogContent,
  DialogTitle,
} from 'app/components/CustomDialog';
import { useFormik } from 'formik';
import { t } from 'locales/i18n';
import { NewSchoolClass } from 'models/schoolClass';
import { useSnackbar } from 'notistack';
import React from 'react';
import * as Yup from 'yup';
import { messages } from '../../messages';

interface AddSchoolClassProps {
  refreshData: () => void;
}

function AddSchoolClassDialog({ refreshData }: AddSchoolClassProps) {
  const [open, setOpen] = React.useState(false);

  const { enqueueSnackbar } = useSnackbar();

  const AddSchoolClassSchema = Yup.object().shape({
    name: Yup.string()
      .min(2, t(messages.tooShort()))
      .max(200, t(messages.tooLong()))
      .required(t(messages.required())),
    programName: Yup.string()
      .min(2, t(messages.tooShort()))
      .max(200, t(messages.tooLong()))
      .required(t(messages.required())),
  });

  const formik = useFormik({
    initialValues: {
      name: '',
      programName: '',
    },
    validationSchema: AddSchoolClassSchema,
    onSubmit: values => {
      console.log(values);
      const newSchoolClass: NewSchoolClass = {
        name: values.name,
        programName: values.programName,
      };
      schoolClassApi
        .addSchoolClass(newSchoolClass)
        .then(res => {
          enqueueSnackbar(t(messages.addSchoolClassSuccess()), {
            variant: 'success',
          });
          handleClose();
          refreshData();
        })
        .catch(e => {
          enqueueSnackbar(e.response?.data.message, { variant: 'error' });
        });
    },
  });

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Button
        variant="contained"
        size="medium"
        onClick={handleClickOpen}
        startIcon={<AddCircleOutlined />}
      >
        {t(messages.add())}
      </Button>
      <Dialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
        scroll="paper"
        maxWidth={'md'}
        style={{ marginTop: 60 }}
      >
        <DialogTitle id="customized-dialog-title" onClose={handleClose}>
          {t(messages.addDialogTitle())}
        </DialogTitle>
        <div>
          <form onSubmit={formik.handleSubmit}>
            <DialogContent dividers>
              <Grid
                direction="column"
                container
                alignItems="center"
                justifyContent="center"
                spacing={1}
                style={{ marginRight: 60 }}
              >
                <Grid
                  item
                  direction="row"
                  container
                  alignItems="center"
                  justifyContent="center"
                >
                  <Grid item xs={3}>
                    <Typography>{t(messages.schoolClassName())}:</Typography>
                  </Grid>
                  <Grid item xs={9}>
                    <TextField
                      size="small"
                      variant="outlined"
                      style={{ width: '50ch' }}
                      id="name"
                      name="name"
                      value={formik.values.name}
                      onChange={formik.handleChange}
                      error={formik.touched.name && Boolean(formik.errors.name)}
                      helperText={formik.touched.name && formik.errors.name}
                    />
                  </Grid>
                </Grid>
                <Grid
                  item
                  direction="row"
                  container
                  alignItems="center"
                  justifyContent="center"
                >
                  <Grid item xs={3}>
                    <Typography>{t(messages.programName())}:</Typography>
                  </Grid>
                  <Grid item xs={9}>
                    <TextField
                      size="small"
                      variant="outlined"
                      style={{ width: '50ch' }}
                      id="programName"
                      name="programName"
                      value={formik.values.programName}
                      onChange={formik.handleChange}
                      error={
                        formik.touched.programName &&
                        Boolean(formik.errors.programName)
                      }
                      helperText={
                        formik.touched.programName && formik.errors.programName
                      }
                    />
                  </Grid>
                </Grid>
              </Grid>
            </DialogContent>
            <DialogActions>
              <Button autoFocus variant="contained" type="submit">
                {t(messages.save())}
              </Button>
            </DialogActions>
          </form>
        </div>
      </Dialog>
    </div>
  );
}
export default React.memo(AddSchoolClassDialog);

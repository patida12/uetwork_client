import { GridCellParams, GridColumns } from '@mui/x-data-grid';
import GridCellExpand from 'app/components/GridCellExpand';
import { t } from 'locales/i18n';
import { formatDate } from 'utils';
import EditSchoolClass from '../components/EditSchoolClass';
import { messages } from '../messages';

export const getColumns = (refreshData?: () => void): GridColumns => [
  {
    field: 'id',
    sortable: false,
    disableColumnMenu: true,
    hide: true,
  },
  {
    field: 'name',
    headerName: t(messages.schoolClassName()),
    width: 250,
    sortable: false,
  },
  {
    field: 'programName',
    headerName: t(messages.programName()),
    width: 250,
    sortable: false,
    disableColumnMenu: true,
  },
  {
    field: 'createdAt',
    headerName: t(messages.createdAt()),
    width: 250,
    renderCell: (params: GridCellParams) => (
      <GridCellExpand
        value={params.value ? formatDate(params.value.toString()) : ''}
        width={params.colDef.width}
      />
    ),
    disableColumnMenu: true,
  },
  {
    field: 'options',
    headerName: t(messages.option()),
    width: 150,
    sortable: false,
    disableColumnMenu: true,
    renderCell: (params: GridCellParams) => (
      <EditSchoolClass refreshData={refreshData!} schoolClass={params.row} />
    ),
  },
];

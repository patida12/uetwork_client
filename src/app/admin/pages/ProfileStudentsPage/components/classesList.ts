import { TFunction } from 'i18next';
import { SchoolClass } from 'models/schoolClass';
import { messages } from '../messages';

export const classesList = [
  'All',
  'CQ-J',
  'CQ-H',
  'CQ-M',
  'CQ-C',
  'CQ-C-CLC',
  'CQ-T',
  'CQ-N',
  'CQ-K',
  'CQ-E',
  'CQ-AE',
  'CQ-V',
];

const classesLists: SchoolClass[] = [
  {
    id: 1,
    name: 'CQ-N',
  },
  {
    id: 2,
    name: 'CQ-J',
  },
  {
    id: 3,
    name: 'CQ-H',
  },
  {
    id: 4,
    name: 'CQ-M',
  },
  {
    id: 5,
    name: 'CQ-C-CLC',
  },
];

export const getClasses = (t: TFunction) => [
  {
    id: 0,
    name: t(messages.all()),
  },
  ...classesLists,
];

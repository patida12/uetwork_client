import { GridColDef } from '@mui/x-data-grid';
import profileStudentApi from 'api/profileStudentApi';
import ImportData from 'app/admin/components/ImportFile';
import { URL_DOWNLOAD_LIST_STUDENTS_TEMPLATE } from 'app/constants';
import { t } from 'locales/i18n';
import { ImportStudent, Student } from 'models';
import { useSnackbar } from 'notistack';
import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { messages } from '../messages';

const columns: GridColDef[] = [
  {
    field: 'fullName',
    headerName: t(messages.fullName()),
    width: 250,
    sortable: false,
    disableColumnMenu: true,
  },
  {
    field: 'studentIdNumber',
    headerName: t(messages.studentIdNumber()),
    width: 250,
    sortable: false,
    disableColumnMenu: true,
  },
  {
    field: 'orgEmail',
    headerName: t(messages.orgEmail()),
    width: 250,
    sortable: false,
    disableColumnMenu: true,
  },
  {
    field: 'phoneNumber',
    headerName: t(messages.phoneNumber()),
    width: 250,
    sortable: false,
    disableColumnMenu: true,
  },
];

export function ImportOrgStudents() {
  const [sendMail, setSendMail] = useState(true);
  const { enqueueSnackbar } = useSnackbar();
  const history = useHistory();

  const handleUpload = value => {
    const importData: ImportStudent = {
      students: value,
      sendMail: sendMail,
    };
    profileStudentApi
      .importStudents(importData)
      .then(res => {
        if (res.item.length === 0) {
          enqueueSnackbar(t(messages.importSuccess()), {
            variant: 'success',
          });
          history.push('/admin/org/student');
        } else {
          res.item.map(err =>
            enqueueSnackbar(err.message, { variant: 'warning' }),
          );
        }
      })
      .catch(e => {
        enqueueSnackbar(e.response?.data.message, { variant: 'error' });
      });
  };
  const handleChangeCheckBox = (checked: boolean) => {
    setSendMail(checked);
  };

  return (
    <ImportData<Student>
      columns={columns}
      handleUpload={handleUpload}
      columnId="studentIdNumber"
      parentUrlPath="/admin/org/student"
      urlDownloadTemplate={URL_DOWNLOAD_LIST_STUDENTS_TEMPLATE}
      sendMailCheckBox={true}
      handleChangeCheckBox={handleChangeCheckBox}
    />
  );
}

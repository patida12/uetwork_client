import { EditOutlined } from '@mui/icons-material';
import CloseIcon from '@mui/icons-material/Close';
import Autocomplete from '@mui/material/Autocomplete';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import MuiDialogActions from '@mui/material/DialogActions';
import MuiDialogContent from '@mui/material/DialogContent';
import MuiDialogTitle from '@mui/material/DialogTitle';
import Grid from '@mui/material/Grid';
import IconButton from '@mui/material/IconButton';
import { Theme } from '@mui/material/styles';
import TextField from '@mui/material/TextField';
import Tooltip from '@mui/material/Tooltip';
import Typography from '@mui/material/Typography';
import { WithStyles } from '@mui/styles';
import createStyles from '@mui/styles/createStyles';
import withStyles from '@mui/styles/withStyles';
import { PhoneInputCustom } from 'app/components/PhoneInput';
import { useFormik } from 'formik';
import { IdProps } from 'models';
import React from 'react';
import { useTranslation } from 'react-i18next';
import 'react-phone-number-input/style.css';
import theme from 'styles/theme';
import * as Yup from 'yup';
import { messages } from '../../messages';
import { classesList } from '../classesList';

const styles = (theme: Theme) =>
  createStyles({
    root: {
      margin: 0,
      padding: theme.spacing(2),
    },
    closeButton: {
      position: 'absolute',
      right: theme.spacing(1),
      top: theme.spacing(1),
      color: theme.palette.primary.main,
    },
  });

export interface DialogTitleProps extends WithStyles<typeof styles> {
  id: string;
  children: React.ReactNode;
  onClose: () => void;
}

const DialogTitle = withStyles(styles)((props: DialogTitleProps) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle className={classes.root} {...other}>
      <div>
        <Typography variant="h6">{children}</Typography>
      </div>
      {onClose ? (
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          onClick={onClose}
          size="large"
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme: Theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

const DialogActions = withStyles((theme: Theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);

function EditStudentDialog(props: IdProps) {
  // const studentID = props.id; //TODO: use contactID to call api
  const [open, setOpen] = React.useState(false);
  const { t } = useTranslation();
  const edit = t(messages.edit());

  const row = {
    id: 1,
    fullName: 'Tran Trong Nguyen Khang xyz',
    personalEmail: 'a@gmail.com',
    major: 'Công nghệ thông tin',
    resumeUrl: 'Here',
    schoolClass: 'K63J',
    studentIdNumber: '18020201',
    phoneNumber: '+842345678',
  };

  const EditStudentSchema = Yup.object().shape({
    studentIdNumber: Yup.string().required(t(messages.required())),
    fullName: Yup.string()
      .min(2, t(messages.tooShort()))
      .max(50, t(messages.tooLong()))
      .required(t(messages.required())),
    personalEmail: Yup.string().email(t(messages.invalidEmail())),
    phoneNumber: Yup.string().max(50, t(messages.tooLong())),
    resumeUrl: Yup.string(),
    schoolClass: Yup.string().required(t(messages.required())),
    major: Yup.string().required(t(messages.required())),
  });
  const formik = useFormik({
    initialValues: row,
    validationSchema: EditStudentSchema,
    onSubmit: values => {
      alert(JSON.stringify(values, null, 2));
    },
  });

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Tooltip title={edit}>
        <IconButton
          style={{
            color: theme.palette.primary.main,
          }}
          aria-label="edit"
          onClick={handleClickOpen}
          size="large"
        >
          <EditOutlined />
        </IconButton>
      </Tooltip>
      <Dialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
        scroll="paper"
        maxWidth={'md'}
        style={{ marginTop: 60 }}
      >
        <DialogTitle id="customized-dialog-title" onClose={handleClose}>
          {t(messages.editDialogTitle())}
        </DialogTitle>
        <div>
          <form onSubmit={formik.handleSubmit}>
            <DialogContent dividers>
              <Grid
                direction="column"
                container
                alignContent="center"
                justifyContent="center"
                spacing={1}
                style={{ marginRight: 60 }}
              >
                <Grid
                  item
                  direction="row"
                  container
                  alignContent="flex-start"
                  justifyContent="center"
                >
                  <Grid item xs={3}>
                    <Typography>{t(messages.studentIdNumber())}:</Typography>
                  </Grid>
                  <Grid item xs={9}>
                    <TextField
                      size="small"
                      variant="outlined"
                      style={{ width: '50ch' }}
                      id="studentIdNumber"
                      name="studentIdNumber"
                      value={formik.values.studentIdNumber}
                      onChange={formik.handleChange}
                      error={
                        formik.touched.studentIdNumber &&
                        Boolean(formik.errors.studentIdNumber)
                      }
                      helperText={
                        formik.touched.studentIdNumber &&
                        formik.errors.studentIdNumber
                      }
                    />
                  </Grid>
                </Grid>
                <Grid
                  item
                  direction="row"
                  container
                  alignContent="flex-start"
                  justifyContent="center"
                >
                  <Grid item xs={3}>
                    <Typography>{t(messages.fullName())}:</Typography>
                  </Grid>
                  <Grid item xs={9}>
                    <TextField
                      size="small"
                      variant="outlined"
                      style={{ width: '50ch' }}
                      id="fullName"
                      name="fullName"
                      value={formik.values.fullName}
                      onChange={formik.handleChange}
                      error={
                        formik.touched.fullName &&
                        Boolean(formik.errors.fullName)
                      }
                      helperText={
                        formik.touched.fullName && formik.errors.fullName
                      }
                    />
                  </Grid>
                </Grid>
                <Grid
                  item
                  direction="row"
                  container
                  alignContent="flex-start"
                  justifyContent="center"
                >
                  <Grid item xs={3}>
                    <Typography>{t(messages.personalEmail())}:</Typography>
                  </Grid>
                  <Grid item xs={9}>
                    <TextField
                      size="small"
                      variant="outlined"
                      style={{ width: '50ch' }}
                      id="personalEmail"
                      name="personalEmail"
                      value={formik.values.personalEmail}
                      onChange={formik.handleChange}
                      error={
                        formik.touched.personalEmail &&
                        Boolean(formik.errors.personalEmail)
                      }
                      helperText={
                        formik.touched.personalEmail &&
                        formik.errors.personalEmail
                      }
                      type="email"
                    />
                  </Grid>
                </Grid>
                <Grid
                  item
                  direction="row"
                  container
                  alignContent="flex-start"
                  justifyContent="center"
                >
                  <Grid item xs={3}>
                    <Typography>{t(messages.phoneNumber())}:</Typography>
                  </Grid>
                  <Grid item xs={9}>
                    <PhoneInputCustom
                      international
                      style={{
                        width: '50ch',
                      }}
                      id="phoneNumber"
                      name="phoneNumber"
                      value={formik.values.phoneNumber}
                      onChange={(e: string) =>
                        formik.setFieldValue('phoneNumber', e)
                      }
                    />
                  </Grid>
                </Grid>
                <Grid
                  item
                  direction="row"
                  container
                  alignContent="flex-start"
                  justifyContent="center"
                >
                  <Grid item xs={3}>
                    <Typography>{t(messages.resumeUrl())}:</Typography>
                  </Grid>
                  <Grid item xs={9}>
                    <TextField
                      size="small"
                      variant="outlined"
                      style={{ width: '50ch' }}
                      id="resumeUrl"
                      name="resumeUrl"
                      value={formik.values.resumeUrl}
                      onChange={formik.handleChange}
                      error={
                        formik.touched.resumeUrl &&
                        Boolean(formik.errors.resumeUrl)
                      }
                      helperText={
                        formik.touched.resumeUrl && formik.errors.resumeUrl
                      }
                    />
                  </Grid>
                </Grid>
                <Grid
                  item
                  direction="row"
                  container
                  alignContent="flex-start"
                  justifyContent="center"
                >
                  <Grid item xs={3}>
                    <Typography>{t(messages.schoolClass())}:</Typography>
                  </Grid>
                  <Grid item xs={9}>
                    <Autocomplete
                      id="class"
                      options={classesList}
                      getOptionLabel={option => option}
                      style={{ width: '50ch' }}
                      value={formik.values.schoolClass}
                      onChange={formik.handleChange}
                      renderInput={params => (
                        <TextField
                          {...params}
                          variant="outlined"
                          name="class"
                          size="small"
                          error={
                            formik.touched.schoolClass &&
                            Boolean(formik.errors.schoolClass)
                          }
                          helperText={
                            formik.touched.schoolClass &&
                            formik.errors.schoolClass
                          }
                        />
                      )}
                    />
                  </Grid>
                </Grid>
                <Grid
                  item
                  direction="row"
                  container
                  alignContent="flex-start"
                  justifyContent="center"
                >
                  <Grid item xs={3}>
                    <Typography>{t(messages.major())}:</Typography>
                  </Grid>
                  <Grid item xs={9}>
                    <TextField
                      size="small"
                      variant="outlined"
                      style={{ width: '50ch' }}
                      id="major"
                      name="major"
                      value={formik.values.major}
                      onChange={formik.handleChange}
                      error={
                        formik.touched.major && Boolean(formik.errors.major)
                      }
                      helperText={formik.touched.major && formik.errors.major}
                    />
                  </Grid>
                </Grid>
              </Grid>
            </DialogContent>
            <DialogActions>
              <Button autoFocus variant="contained" type="submit">
                {t(messages.save())}
              </Button>
            </DialogActions>
          </form>
        </div>
      </Dialog>
    </div>
  );
}
export default React.memo(EditStudentDialog);

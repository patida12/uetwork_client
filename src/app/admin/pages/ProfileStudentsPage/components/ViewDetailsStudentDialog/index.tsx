import { InfoOutlined } from '@mui/icons-material';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import Grid from '@mui/material/Grid';
import IconButton from '@mui/material/IconButton';
import Tooltip from '@mui/material/Tooltip';
import Typography from '@mui/material/Typography';
import {
  DialogActions,
  DialogContent,
  DialogTitle,
} from 'app/components/CustomDialog';
import { t } from 'locales/i18n';
import { Student } from 'models';
import React from 'react';
import theme from 'styles/theme';
import { formatDate } from 'utils';
import { messages } from '../../messages';

export const defaultStudent: Student = {
  id: 0,
  fullName: '',
  orgEmail: '',
  studentIdNumber: '',
  personalEmail: '',
  createdAt: '',
  updatedAt: '',
  resumeUrl: '',
  phoneNumber: '',
  schoolClass: {
    id: 0,
    name: '',
  },
  userId: 0,
  organizationId: 0,
};

interface ViewStudentProps {
  student: Student;
}

function ViewDetailsStudentDialog({ student }: ViewStudentProps) {
  const detail = t(messages.detail());
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Tooltip title={detail}>
        <IconButton
          style={{
            color: theme.palette.primary.light,
          }}
          aria-label="info"
          onClick={handleClickOpen}
          size="large"
        >
          <InfoOutlined />
        </IconButton>
      </Tooltip>
      <Dialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
        scroll="paper"
        maxWidth={'lg'}
        style={{ marginTop: 60 }}
      >
        <DialogTitle id="customized-dialog-title" onClose={handleClose}>
          {t(messages.detailDialogTitle())}
        </DialogTitle>
        <DialogContent dividers>
          <Grid
            direction="column"
            container
            alignContent="center"
            justifyContent="center"
            spacing={2}
          >
            <Grid
              item
              direction="row"
              container
              alignContent="flex-start"
              justifyContent="center"
            >
              <Grid item xs={3}>
                <Typography variant="subtitle1">
                  {t(messages.studentIdNumber())}:
                </Typography>
              </Grid>
              <Grid item xs={9}>
                <Typography sx={{ width: '40ch' }}>
                  {student.studentIdNumber}
                </Typography>
              </Grid>
            </Grid>
            <Grid
              item
              direction="row"
              container
              alignContent="flex-start"
              justifyContent="center"
            >
              <Grid item xs={3}>
                <Typography variant="subtitle1">
                  {t(messages.fullName())}:
                </Typography>
              </Grid>
              <Grid item xs={9}>
                <Typography sx={{ width: '40ch' }}>
                  {student.fullName}
                </Typography>
              </Grid>
            </Grid>
            <Grid
              item
              direction="row"
              container
              alignContent="flex-start"
              justifyContent="center"
            >
              <Grid item xs={3}>
                <Typography variant="subtitle1">
                  {t(messages.personalEmail())}:
                </Typography>
              </Grid>
              <Grid item xs={9}>
                <Typography sx={{ width: '40ch' }}>
                  {student.personalEmail}
                </Typography>
              </Grid>
            </Grid>
            <Grid
              item
              direction="row"
              container
              alignContent="flex-start"
              justifyContent="center"
            >
              <Grid item xs={3}>
                <Typography variant="subtitle1">
                  {t(messages.phoneNumber())}:
                </Typography>
              </Grid>
              <Grid item xs={9}>
                <Typography sx={{ width: '40ch' }}>
                  {student.phoneNumber}
                </Typography>
              </Grid>
            </Grid>
            <Grid
              item
              direction="row"
              container
              alignContent="flex-start"
              justifyContent="center"
            >
              <Grid item xs={3}>
                <Typography variant="subtitle1">
                  {t(messages.resumeUrl())}:
                </Typography>
              </Grid>
              <Grid item xs={9}>
                <Typography sx={{ width: '40ch' }}>
                  {student.resumeUrl}
                </Typography>
              </Grid>
            </Grid>
            <Grid
              item
              direction="row"
              container
              alignContent="flex-start"
              justifyContent="center"
            >
              <Grid item xs={3}>
                <Typography variant="subtitle1">
                  {t(messages.schoolClass())}:
                </Typography>
              </Grid>
              <Grid item xs={9}>
                <Typography sx={{ width: '40ch' }}>
                  {student.schoolClass ? student.schoolClass.name : ''}
                </Typography>
              </Grid>
            </Grid>
            <Grid
              item
              direction="row"
              container
              alignContent="flex-start"
              justifyContent="center"
            >
              <Grid item xs={3}>
                <Typography variant="subtitle1">
                  {t(messages.createdAt())}:
                </Typography>
              </Grid>
              <Grid item xs={9}>
                <Typography sx={{ width: '40ch' }}>
                  {formatDate(student.createdAt)}
                </Typography>
              </Grid>
            </Grid>
            <Grid
              item
              direction="row"
              container
              alignContent="flex-start"
              justifyContent="center"
            >
              <Grid item xs={3}>
                <Typography variant="subtitle1">
                  {t(messages.updatedAt())}:
                </Typography>
              </Grid>
              <Grid item xs={9}>
                <Typography sx={{ width: '40ch' }}>
                  {formatDate(student.updatedAt)}
                </Typography>
              </Grid>
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button
            autoFocus
            onClick={handleClose}
            variant="contained"
            sx={{ m: 1 }}
          >
            {t(messages.close())}
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
export default React.memo(ViewDetailsStudentDialog);

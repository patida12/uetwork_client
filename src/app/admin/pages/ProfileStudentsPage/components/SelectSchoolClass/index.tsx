import Autocomplete from '@mui/material/Autocomplete';
import CircularProgress from '@mui/material/CircularProgress';
import TextField from '@mui/material/TextField';
import profileStudentApi from 'api/profileStudentApi';
import { Results } from 'api/types';
import { ALL_CLASSES_EN, ALL_CLASSES_VI } from 'app/constants';
import { useAsync } from 'app/hooks/useAsync';
import { t } from 'locales/i18n';
import { SchoolClass } from 'models/schoolClass';
import * as React from 'react';
import { messages } from '../../messages';

interface SelectClassProps {
  handleChangeClass: (id: string | number) => void;
}

function SelectSchoolClass({ handleChangeClass }: SelectClassProps) {
  const [schoolClassName, setSchoolClassName] = React.useState<string>('');

  const { loading, data, run } = useAsync<Results<SchoolClass[]>>({
    data: {
      codeResults: {
        code: 0,
        message: '',
      },
      item: [],
    },
  });

  const classesList = data?.item ?? [];

  React.useEffect(() => {
    run(profileStudentApi.getClassesListing());
  }, [run]);

  React.useEffect(() => {
    var schoolClass = classesList.find(
      schoolClass => schoolClass.name === schoolClassName,
    );
    var id = schoolClass ? schoolClass.id : '';
    handleChangeClass(id);
  }, [handleChangeClass, schoolClassName]);

  function handleChange(value: string) {
    setSchoolClassName(value);
  }

  return (
    <Autocomplete
      id="select-schoolClass"
      value={schoolClassName}
      onChange={(
        event: React.SyntheticEvent<Element, Event>,
        value: string | null,
      ) => {
        handleChange(value!);
      }}
      sx={{ width: 230 }}
      isOptionEqualToValue={(option, value) =>
        value === ALL_CLASSES_EN || ALL_CLASSES_VI ? true : false
      }
      options={classesList.map(schoolClass => {
        return schoolClass.name;
      })}
      getOptionLabel={option => option}
      renderInput={params => (
        <TextField
          {...params}
          label={t(messages.schoolClass())}
          InputProps={{
            ...params.InputProps,
            endAdornment: (
              <React.Fragment>
                {loading ? (
                  <CircularProgress color="inherit" size={20} />
                ) : null}
                {params.InputProps.endAdornment}
              </React.Fragment>
            ),
          }}
          size="small"
        />
      )}
    />
  );
}
export default React.memo(SelectSchoolClass);

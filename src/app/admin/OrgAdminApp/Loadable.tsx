import { LoadingPage } from 'app/pages/Common/LoadingPage/LoadingPage';
import { lazyLoad } from 'utils/loadable';

export const OrgAdminApp = lazyLoad(
  () => import('./index'),
  module => module.OrgAdminApp,
  {
    fallback: <LoadingPage />,
  },
);

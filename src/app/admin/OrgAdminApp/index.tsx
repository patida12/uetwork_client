import { Stack, styled } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import SelectTerm from 'app/components/SelectTerm';
import { AppRole } from 'app/constants';
import clsx from 'clsx';
import { TermProvider } from 'contexts/TermProvider';
import { t } from 'locales/i18n';
import * as React from 'react';
import { Helmet } from 'react-helmet-async';
import { Route, Switch, useRouteMatch } from 'react-router-dom';
import Appbar from '../components/Appbar';
import PageHeader from '../components/PageHeader';
import { InternshipLecturersPage } from '../pages/InternshipLecturersPage/Loadable';
import { InternshipPartnersPage } from '../pages/InternshipPartnersPage/Loadable';
import AddPostPage from '../pages/InternshipPostsPage/components/AddPostPage';
import EditPostPage from '../pages/InternshipPostsPage/components/EditPostPage';
import { InternshipPostsPage } from '../pages/InternshipPostsPage/Loadable';
import { InternshipRequestsPage } from '../pages/InternshipRequestsPage/Loadable';
import { InternshipStudentsPage } from '../pages/InternshipStudentsPage/Loadable';
import { InternshipTermsPage } from '../pages/InternshipTermsPage/Loadable';
import { OrgAdminDashboardPage } from '../pages/OrgAdminDashboardPage';
import { LecturersPage } from '../pages/ProfileLecturersPage/Loadable';
import { PartnersPage } from '../pages/ProfilePartnersPage/Loadable';
import { ImportOrgStudents } from '../pages/ProfileStudentsPage/components/ImportOrgStudents';
import { StudentsPage } from '../pages/ProfileStudentsPage/Loadable';
import { UsersListPage } from '../pages/UsersListPage/Loadable';
import { messages } from './messages';

const useStyles = makeStyles(theme => ({
  toolbar: theme.mixins.toolbar,
  root: {
    flexGrow: 1,
    paddingLeft: theme.spacing(3),
    paddingTop: theme.spacing(5),
    paddingRight: theme.spacing(3),
  },
  '@media (min-width: 600px)': {
    root: {
      flexGrow: 1,
      paddingLeft: theme.spacing(33),
    },
  },
}));

export interface PageHeaderProps {
  name: string;
}

export interface UserPageProps {
  user: string;
}

export function OrgAdminApp() {
  let { path } = useRouteMatch();
  const classes = useStyles();

  return (
    <>
      <Helmet>
        <title>{t(messages.title())}</title>
        <meta name="description" content="Quản lý thực tập" />
      </Helmet>
      <TermProvider>
        <Appbar roleAdmin={AppRole.ORG_ADMIN} />
        <main className={clsx(classes.root)}>
          <div className={classes.toolbar} />
          <Switch>
            <Route exact path={`${path}/home`}>
              <OrgAdminDashboardPage />
            </Route>
            <Route exact path={`${path}/term`}>
              <StyledPageHeader>
                <PageHeader name="terms" />
              </StyledPageHeader>
              <InternshipTermsPage />
            </Route>
            <Route path={`${path}/terms/:termId/posts`}>
              <StyledPageHeader>
                <Stack
                  direction="row"
                  justifyContent="space-between"
                  alignItems="flex-start"
                >
                  <PageHeader name="posts" />
                  <SelectTerm />
                </Stack>
              </StyledPageHeader>
              <InternshipPostsPage />
            </Route>
            <Route path={`${path}/terms/:termId/requests`}>
              <StyledPageHeader>
                <Stack
                  direction="row"
                  justifyContent="space-between"
                  alignItems="flex-start"
                >
                  <PageHeader name="requests" />
                  <SelectTerm />
                </Stack>
              </StyledPageHeader>
              <InternshipRequestsPage />
            </Route>
            <Route path={`${path}/terms/:termId/pending-partners`}>
              <StyledPageHeader>
                <Stack
                  direction="row"
                  justifyContent="space-between"
                  alignItems="flex-start"
                >
                  <PageHeader name="pending_partners" />
                  <SelectTerm />
                </Stack>
              </StyledPageHeader>
              InternshipPendingPartnerPage
            </Route>
            <Route path={`${path}/terms/:termId/partners`}>
              <StyledPageHeader>
                <Stack
                  direction="row"
                  justifyContent="space-between"
                  alignItems="flex-start"
                >
                  <PageHeader name="partners" />
                  <SelectTerm />
                </Stack>
              </StyledPageHeader>
              <InternshipPartnersPage />
            </Route>
            <Route path={`${path}/terms/:termId/lecturers`}>
              <StyledPageHeader>
                <Stack
                  direction="row"
                  justifyContent="space-between"
                  alignItems="flex-start"
                >
                  <PageHeader name="lecturers" />
                  <SelectTerm />
                </Stack>
              </StyledPageHeader>
              <InternshipLecturersPage />
            </Route>
            <Route path={`${path}/terms/:termId/students`}>
              <StyledPageHeader>
                <Stack
                  direction="row"
                  justifyContent="space-between"
                  alignItems="flex-start"
                >
                  <PageHeader name="students" />
                  <SelectTerm />
                </Stack>
              </StyledPageHeader>
              <InternshipStudentsPage />
            </Route>
            <Route exact path={`${path}/profiles`}>
              <StyledPageHeader>
                <PageHeader name="users" />
              </StyledPageHeader>
              <UsersListPage />
            </Route>
            <Route exact path={`${path}/student`}>
              <StyledPageHeader>
                <PageHeader name="students" />
              </StyledPageHeader>
              <StudentsPage />
            </Route>
            <Route exact path={`${path}/lecturer`}>
              <StyledPageHeader>
                <PageHeader name="lecturers" />
              </StyledPageHeader>
              <LecturersPage />
            </Route>
            <Route exact path={`${path}/partner`}>
              <StyledPageHeader>
                <PageHeader name="partners" />
              </StyledPageHeader>
              <PartnersPage />
            </Route>
            <Route exact path={`${path}/student/import`}>
              <StyledPageHeader>
                <PageHeader name="importStudents" />
              </StyledPageHeader>
              <ImportOrgStudents />
            </Route>
            <Route exact path={`${path}/terms/:termId/add-post`}>
              <StyledPageHeader>
                <Stack
                  direction="row"
                  justifyContent="space-between"
                  alignItems="flex-start"
                >
                  <PageHeader name="add-post" />
                  <SelectTerm />
                </Stack>
              </StyledPageHeader>
              <AddPostPage />
            </Route>
            <Route exact path={`${path}/terms/:termId/edit-post/:postId`}>
              <StyledPageHeader>
                <Stack
                  direction="row"
                  justifyContent="space-between"
                  alignItems="flex-start"
                >
                  <PageHeader name="edit-post" />
                  <SelectTerm />
                </Stack>
              </StyledPageHeader>
              <EditPostPage />
            </Route>
          </Switch>
        </main>
      </TermProvider>
    </>
  );
}

const StyledPageHeader = styled('div')(({ theme }) => ({
  marginBottom: -12,
}));

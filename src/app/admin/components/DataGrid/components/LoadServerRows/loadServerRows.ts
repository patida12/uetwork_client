export function loadServerRows(
  page: number,
  per_page: number,
  sort: string,
  data: any[],
): Promise<any> {
  return new Promise<any>(resolve => {
    setTimeout(() => {
      resolve(data.slice((page - 1) * per_page, page * per_page));
    }, 1000);
  });
}

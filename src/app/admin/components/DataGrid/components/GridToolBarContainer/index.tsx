import { GridToolbarDensitySelector } from '@mui/x-data-grid';
import * as React from 'react';

export default function GridToolBarContainer() {
  return (
    <>
      <GridToolbarDensitySelector />
    </>
  );
}

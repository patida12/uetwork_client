import { FileDownload, FileUpload } from '@mui/icons-material';
import {
  Button,
  Checkbox,
  FormControlLabel,
  Grid,
  Typography,
} from '@mui/material';
import { DataGrid, GridColDef } from '@mui/x-data-grid';
import { DEFAULT_PER_PAGE } from 'app/constants';
import { t } from 'locales/i18n';
import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import theme from 'styles/theme';
import * as XLSX from 'xlsx';
import { messages } from './messages';

interface ImportDataProps {
  columns: GridColDef[];
  handleUpload: (any) => void;
  columnId: string | 'id';
  parentUrlPath: string | '/admin/org';
  urlDownloadTemplate: string | '';
  sendMailCheckBox?: boolean | false;
  handleChangeCheckBox?: (checked: boolean) => void;
}

function ImportData<T>({
  columns,
  handleUpload,
  columnId,
  parentUrlPath,
  urlDownloadTemplate,
  sendMailCheckBox,
  handleChangeCheckBox,
}: ImportDataProps) {
  const [items, setItems] = useState<T[]>([]);
  const [pageSize, setPageSize] = useState<number>(DEFAULT_PER_PAGE);
  const [fileName, setFileName] = useState<string>('');
  const [checked, setChecked] = useState(true);

  React.useEffect(() => {
    if (handleChangeCheckBox && sendMailCheckBox) {
      handleChangeCheckBox(checked);
    }
  }, [checked]);

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setChecked(event.target.checked);
  };

  const readExcel = ({ target }) => {
    const promise = new Promise<T[]>((resolve, reject) => {
      setFileName(target.files[0].name);
      const fileReader = new FileReader();
      fileReader.readAsArrayBuffer(target.files[0]);

      fileReader.onload = e => {
        if (e !== null) {
          const bufferArray = e.target!.result;
          const wb = XLSX.read(bufferArray, { type: 'buffer' });
          const wsname = wb.SheetNames[0];
          const ws = wb.Sheets[wsname];
          const data: T[] = XLSX.utils.sheet_to_json(ws);
          resolve(data);
        } else resolve([]);
      };

      fileReader.onerror = error => {
        reject(error);
      };
    });

    promise.then((d: T[]) => {
      setItems(d);
    });
    target.value = '';
  };

  const handleUploadData = () => {
    handleUpload(items);
  };

  return (
    <div style={{ marginTop: theme.spacing(3) }}>
      <Grid
        container
        direction="row"
        justifyContent="space-between"
        spacing={1}
      >
        <Grid
          item
          xs={12}
          sm={9}
          md={6}
          direction="row"
          container
          alignItems="center"
          justifyContent="flex-start"
          spacing={1}
        >
          <Button
            variant="outlined"
            startIcon={<FileDownload />}
            sx={{ ml: 1 }}
            onClick={async () => {
              const url = urlDownloadTemplate;
              window.open(url, '_blank');
            }}
          >
            {t(messages.downloadTemplate())}
          </Button>
          <Button
            variant="contained"
            component="label"
            htmlFor="contained-button-file"
            sx={{ mx: 1 }}
          >
            <input
              hidden
              id="contained-button-file"
              type="file"
              onChange={readExcel}
            />
            {t(messages.chooseFile())}
          </Button>
          <label>{fileName}</label>
        </Grid>
        <Grid item>
          <Button to={parentUrlPath} component={Link} sx={{ mr: 1 }}>
            {t(messages.cancel())}
          </Button>
          {handleChangeCheckBox && sendMailCheckBox && (
            <FormControlLabel
              control={<Checkbox checked={checked} onChange={handleChange} />}
              label={
                <Typography variant="caption" sx={{ ml: -1 }}>
                  Send Mail
                </Typography>
              }
            />
          )}
          <Button
            disabled={fileName === ''}
            variant="contained"
            startIcon={<FileUpload />}
            onClick={handleUploadData}
          >
            {t(messages.upload())}
          </Button>
        </Grid>
      </Grid>

      <div style={{ height: 700, width: '100%', marginTop: theme.spacing(1) }}>
        <DataGrid
          rows={items}
          columns={columns}
          pageSize={pageSize}
          onPageSizeChange={newPageSize => setPageSize(newPageSize)}
          rowsPerPageOptions={[25, 50, 100]}
          pagination
          disableSelectionOnClick
          getRowId={row => row[columnId]}
        />
      </div>
    </div>
  );
}

export default ImportData;

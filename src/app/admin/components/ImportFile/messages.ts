import { translations } from 'locales/translations';
import { _t } from 'utils/messages';

export const messages = {
  chooseFile: () => _t(translations.common.app.chooseFile),
  upload: () => _t(translations.common.app.upload),
  cancel: () => _t(translations.common.button.cancel),
  downloadTemplate: () => _t(translations.common.button.downloadTemplate),
};

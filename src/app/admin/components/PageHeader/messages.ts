import { translations } from 'locales/translations';
import { _t } from 'utils/messages';

export const messages = {
  partnerHeader: () => _t(translations.header.partner),
  studentHeader: () => _t(translations.header.student),
  lecturerHeader: () => _t(translations.header.lecturer),
  userHeader: () => _t(translations.header.user),
  notificationHeader: () => _t(translations.header.notification),
  classHeader: () => _t(translations.header.class),
  postHeader: () => _t(translations.header.post),
  addPostHeader: () => _t(translations.header.addPost),
  editPostHeader: () => _t(translations.header.editPost),
  termHeader: () => _t(translations.header.term),
  scoreHeader: () => _t(translations.header.score),
  requests: () => _t(translations.common.app.requests),
  pending_partners: () => _t(translations.common.app.pending_partners),
  organization: () => _t(translations.common.app.organization),
  org_admins: () => _t(translations.header.org_admins),
  importStudents: () => _t(translations.header.importStudents),
};

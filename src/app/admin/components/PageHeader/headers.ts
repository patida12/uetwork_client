import { t } from 'locales/i18n';
import { messages } from './messages';

export const getHeaders = () =>
  new Map([
    ['users', t(messages.userHeader())],
    ['students', t(messages.studentHeader())],
    ['partners', t(messages.partnerHeader())],
    ['lecturers', t(messages.lecturerHeader())],
    ['notifications', t(messages.notificationHeader())],
    ['classes', t(messages.classHeader())],
    ['posts', t(messages.postHeader())],
    ['terms', t(messages.termHeader())],
    ['requests', t(messages.requests())],
    ['pending_partners', t(messages.pending_partners())],
    ['orgs', t(messages.organization())],
    ['org-admins', t(messages.org_admins())],
    ['importStudents', t(messages.importStudents())],
    ['add-post', t(messages.addPostHeader())],
    ['edit-post', t(messages.editPostHeader())],
  ]);

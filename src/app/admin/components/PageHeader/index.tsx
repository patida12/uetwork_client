import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import React from 'react';
import { getHeaders } from './headers';

interface PageHeaderProps {
  name: string;
}

function PageHeader(props: PageHeaderProps) {
  const name = props.name;
  const headers = React.useMemo(() => getHeaders(), []);
  const header = headers.get(name);
  return (
    <>
      <Typography variant="h1" component="h1">
        <Box fontWeight="fontWeightMedium">{header}</Box>
      </Typography>
    </>
  );
}

export default PageHeader;

import unitedStatesFlag from 'assets/united-states-flag.svg';
import vietnameseFlag from 'assets/vietnam-flag.svg';
import React from 'react';
import { useTranslation } from 'react-i18next';

export const LanguageSwitcher: React.FC = () => {
  const { i18n } = useTranslation();
  const changeLanguage = () => {
    i18n.changeLanguage(i18n.language === 'vi' ? 'en' : 'vi');
    // Reload windows when language is changed.
    // Why? Because getting `t` from a hook is a pain in the a** every time.
    window.location.reload();
  };

  let languageImg = '';

  let currentLanguage = i18n.language;
  if (currentLanguage === 'en' || currentLanguage.match(/^en-.*$/)) {
    languageImg = unitedStatesFlag;
  } else if (currentLanguage === 'vi') {
    languageImg = vietnameseFlag;
  }

  return (
    <img
      src={languageImg}
      onClick={changeLanguage}
      alt="country flag"
      width="20px"
    />
  );
};

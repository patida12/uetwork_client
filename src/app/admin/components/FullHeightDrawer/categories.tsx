import {
  ClassOutlined,
  CorporateFareOutlined,
  GroupOutlined,
  GroupsOutlined,
  HomeOutlined,
  HouseOutlined,
  PersonOutlined,
} from '@mui/icons-material';
import { t } from 'locales/i18n';
import { messages } from './messages';

export const getCategories = () => [
  {
    id: 1,
    key: 'home',
    label: t(messages.home()),
    icon: <HomeOutlined />,
    hasNested: false,
    items: [],
  },
  {
    id: 2,
    key: 'intern',
    label: t(messages.internship()),
    icon: <HouseOutlined />,
    hasNested: true,
    items: [
      {
        childId: 1,
        childKey: 'term',
        childLabel: t(messages.terms()),
        path: '/term',
      },
      {
        childId: 2,
        childKey: 'posts',
        childLabel: t(messages.post()),
        path: '/posts',
      },
      {
        childId: 3,
        childKey: 'requests',
        childLabel: t(messages.requests()),
        path: '/requests',
      },
      {
        childId: 4,
        childKey: 'pending-partners',
        childLabel: t(messages.pending_partners()),
        path: '/pending-partners',
      },
      {
        childId: 5,
        childKey: 'students',
        childLabel: t(messages.student()),
        path: '/students',
      },
      {
        childId: 6,
        childKey: 'partners',
        childLabel: t(messages.company()),
        path: '/partners',
      },
      {
        childId: 7,
        childKey: 'lecturers',
        childLabel: t(messages.lecturer()),
        path: '/lecturers',
      },
    ],
  },
  {
    id: 3,
    key: 'profiles',
    label: t(messages.user()),
    icon: <PersonOutlined />,
    hasNested: true,
    isSelected: true,
    items: [
      {
        childId: 2,
        childKey: 'student',
        childLabel: t(messages.student()),
        path: '/student',
      },
      {
        childId: 3,
        childKey: 'partner',
        childLabel: t(messages.company()),
        path: '/partner',
      },
      {
        childId: 4,
        childKey: 'lecturer',
        childLabel: t(messages.lecturer()),
        path: '/lecturer',
      },
    ],
  },
];

export const getSysAdminCategories = () => [
  {
    id: 1,
    key: 'home',
    label: t(messages.home()),
    icon: <HomeOutlined />,
    hasNested: false,
    items: [],
  },
  {
    id: 2,
    key: 'orgs',
    label: t(messages.organization()),
    icon: <CorporateFareOutlined />,
    hasNested: false,
    items: [],
  },
  {
    id: 3,
    key: 'classes',
    label: t(messages.classes()),
    icon: <ClassOutlined />,
    hasNested: false,
    items: [],
  },
  {
    id: 4,
    key: 'users',
    label: t(messages.user()),
    icon: <GroupsOutlined />,
    hasNested: false,
    items: [],
  },
  {
    id: 5,
    key: 'students',
    label: t(messages.student()),
    icon: <GroupOutlined />,
    hasNested: false,
    items: [],
  },
  {
    id: 6,
    key: 'partners',
    label: t(messages.company()),
    icon: <GroupOutlined />,
    hasNested: false,
    items: [],
  },
  {
    id: 7,
    key: 'lecturers',
    label: t(messages.lecturer()),
    icon: <GroupOutlined />,
    hasNested: false,
    items: [],
  },
];

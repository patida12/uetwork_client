import { translations } from 'locales/translations';
import { _t } from 'utils/messages';

export const messages = {
  notification: () => _t(translations.sidebar.notification),
  classes: () => _t(translations.header.class),
  internship: () => _t(translations.sidebar.internship),
  post: () => _t(translations.common.app.post),
  student: () => _t(translations.common.app.student),
  company: () => _t(translations.common.app.company),
  lecturer: () => _t(translations.common.app.lecturer),
  requests: () => _t(translations.common.app.requests),
  pending_partners: () => _t(translations.common.app.pending_partners),
  user: () => _t(translations.common.app.user),
  home: () => _t(translations.common.app.home),
  organization: () => _t(translations.common.app.organization),
  terms: () => _t(translations.sidebar.terms),
  major: () => _t(translations.sidebar.major),
  title: () => _t(translations.sidebar.title),
  org_admins: () => _t(translations.common.app.org_admins),
};

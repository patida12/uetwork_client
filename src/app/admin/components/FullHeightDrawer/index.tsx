import { ExpandLess, ExpandMore } from '@mui/icons-material';
import Box from '@mui/material/Box';
import Collapse from '@mui/material/Collapse';
import Divider from '@mui/material/Divider';
import Drawer from '@mui/material/Drawer';
import List from '@mui/material/List';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import { styled } from '@mui/material/styles';
import Typography from '@mui/material/Typography';
import { AppRole } from 'app/constants';
import SchoolLogo from 'assets/logo.png';
import clsx from 'clsx';
import { TermContext } from 'contexts/TermProvider';
import { t } from 'locales/i18n';
import React, { useContext, useMemo } from 'react';
import { Link, useLocation, useRouteMatch } from 'react-router-dom';
import { isMatchPath } from 'utils/isMatchPath';
import { getCategories, getSysAdminCategories } from './categories';
import { messages } from './messages';

const INTERN_CATEGORY = 'intern';
const USERS_CATEGORY = 'profiles';
const TERMS_ITEM = 'term';
const drawerWidth = 240;

const PREFIX = 'index';

const classes = {
  logoText: `${PREFIX}-logoText`,
  logo: `${PREFIX}-logo`,
  activeItem: `${PREFIX}-activeItem`,
  itemIcon: `${PREFIX}-itemIcon`,
  nestedItem: `${PREFIX}-nestedItem`,
  nestedActiveItem: `${PREFIX}-nestedActiveItem`,
  drawer: `${PREFIX}-drawer`,
};

const Root = styled('div')(({ theme }) => ({
  [`& .${classes.logoText}`]: {
    textAlign: 'center',
    paddingTop: theme.spacing(2),
  },
  [`& .${classes.logo}`]: {
    paddingBottom: theme.spacing(1),
  },
  [`& .${classes.activeItem}`]: {
    color: theme.palette.primary.main,
    '& .MuiListItemIcon-root': {
      color: theme.palette.primary.main,
    },
  },
  [`& .${classes.itemIcon}`]: {
    minWidth: 'auto',
    marginRight: theme.spacing(2),
  },
  [`& .${classes.nestedItem}`]: {
    color: theme.palette.common.black,
    textDecoration: 'none',
  },
  [`& .${classes.nestedActiveItem}`]: {
    color: theme.palette.primary.main,
    textDecoration: 'none',
  },
}));

const DrawerWrapper = styled('div')(({ theme }) => ({
  [`& .${classes.drawer}`]: {
    '& .MuiDrawer-paper': {
      boxSizing: 'border-box',
      width: drawerWidth,
      backgroundColor: '#f5f5f5',
    },
  },
}));

export interface FullHeightDrawerProps {
  mobileOpen: boolean;
  handleDrawerToggle: () => void;
  roleAdmin: AppRole;
}

export function FullHeightDrawer(props: FullHeightDrawerProps) {
  const org_categories = React.useMemo(() => getCategories(), []);
  const sys_categories = React.useMemo(() => getSysAdminCategories(), []);
  const categories =
    props.roleAdmin === AppRole.ORG_ADMIN ? org_categories : sys_categories;
  const { currentTerm } = useContext(TermContext);
  const { mobileOpen, handleDrawerToggle } = props;
  const [selectedFirstCategory, setSelectedFirstCategory] = React.useState(
    INTERN_CATEGORY,
  );
  const [selectedSecondCategory, setSelectedSecondCategory] = React.useState(
    USERS_CATEGORY,
  );
  const [selectedItem, setSelectedItem] = React.useState('');

  let { url } = useRouteMatch();
  let location = useLocation();
  var termId = useMemo(() => (currentTerm ? currentTerm.id : undefined), [
    currentTerm,
  ]);

  const handleSelectFirstCategory = (key: string) => (event: any) => {
    selectedFirstCategory !== ''
      ? setSelectedFirstCategory('')
      : setSelectedFirstCategory(key);
  };

  const handleSelectSecondCategory = (key: string) => (event: any) => {
    selectedSecondCategory !== ''
      ? setSelectedSecondCategory('')
      : setSelectedSecondCategory(key);
  };

  const handleSelectItem = (key: string) => (event: any) => {
    setSelectedItem(key);
  };

  const drawer = (
    <Root>
      <Divider />
      <Typography variant="h6" component="h6">
        <Box fontWeight="fontWeightMedium" className={classes.logoText}>
          {t(messages.title())}
        </Box>
      </Typography>
      <Box display="flex" justifyContent="center" margin="auto" m={2}>
        <img src={SchoolLogo} alt="logo" className={classes.logo} />
      </Box>
      <Divider />
      {categories.map(category => (
        <List key={category.key}>
          {category.hasNested ? (
            <ListItemButton
              onClick={
                category.key === INTERN_CATEGORY
                  ? handleSelectFirstCategory(category.key)
                  : handleSelectSecondCategory(category.key)
              }
              className={clsx(
                category.key === selectedItem ? classes.activeItem : null,
              )}
            >
              <ListItemIcon className={classes.itemIcon}>
                {category.icon}
              </ListItemIcon>
              <ListItemText>
                <Box fontWeight="fontWeightMedium">{category.label}</Box>
              </ListItemText>
              {selectedFirstCategory
                .concat(' ', selectedSecondCategory)
                .indexOf(category.key) >= 0 ? (
                <ExpandLess />
              ) : (
                <ExpandMore />
              )}
            </ListItemButton>
          ) : (
            <Link
              className={clsx(
                isMatchPath(location.pathname, `${url}/${category.key}`)
                  ? classes.nestedActiveItem
                  : classes.nestedItem,
              )}
              to={`${url}/` + category.key}
            >
              <ListItemButton
                className={clsx(
                  isMatchPath(location.pathname, `${url}/${category.key}`)
                    ? classes.activeItem
                    : null,
                )}
                onClick={handleSelectItem(category.key)}
              >
                <ListItemIcon className={classes.itemIcon}>
                  {category.icon}
                </ListItemIcon>
                <ListItemText>
                  <Box fontWeight="fontWeightMedium">{category.label}</Box>
                </ListItemText>
              </ListItemButton>
            </Link>
          )}
          <Collapse
            in={
              selectedFirstCategory
                .concat(' ', selectedSecondCategory)
                .indexOf(category.key) >= 0
            }
            timeout="auto"
            unmountOnExit
          >
            {category.items.map(item => (
              <Link
                className={clsx(
                  isMatchPath(
                    location.pathname,
                    category.key === INTERN_CATEGORY &&
                      item.childKey !== TERMS_ITEM
                      ? `${url}/terms/:id/${item.childKey}`
                      : `${url}/${item.childKey}`,
                  )
                    ? classes.nestedActiveItem
                    : classes.nestedItem,
                )}
                to={
                  category.key === INTERN_CATEGORY &&
                  item.childKey !== TERMS_ITEM
                    ? `${url}/terms/${termId}` + item.path
                    : `${url}` + item.path
                }
                key={item.childKey}
              >
                <ListItemButton dense onClick={handleSelectItem(item.childKey)}>
                  <ListItemText>
                    <Box ml={5} fontWeight="fontWeightMedium">
                      {item.childLabel}
                    </Box>
                  </ListItemText>
                </ListItemButton>
              </Link>
            ))}
          </Collapse>
        </List>
      ))}
    </Root>
  );

  return (
    <Box
      component="nav"
      sx={{
        width: { sm: drawerWidth },
        flexShrink: { sm: 0 },
      }}
      aria-label="mailbox folders"
    >
      <DrawerWrapper>
        <Drawer
          variant="temporary"
          anchor={'left'}
          open={mobileOpen}
          onClose={handleDrawerToggle}
          ModalProps={{
            keepMounted: true,
          }}
          sx={{
            display: { xs: 'block', sm: 'none' },
          }}
          className={classes.drawer}
        >
          {drawer}
        </Drawer>
        <Drawer
          variant="permanent"
          sx={{ display: { xs: 'none', sm: 'block' } }}
          className={classes.drawer}
          open
        >
          {drawer}
        </Drawer>
      </DrawerWrapper>
    </Box>
  );
}

import { lazyLoad } from 'utils/loadable';

export const FullHeightDrawer = lazyLoad(
  () => import('./index'),
  module => module.FullHeightDrawer,
);

import { translations } from 'locales/translations';
import { _t } from 'utils/messages';

export const messages = {
  changePassword: () => _t(translations.accountFeature.changePassword),
  logOut: () => _t(translations.accountFeature.logOut),
  account: () => _t(translations.common.app.account),
  notification: () => _t(translations.sidebar.notification),
  message: () => _t(translations.accountFeature.message),
};

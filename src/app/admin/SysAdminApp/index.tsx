import { styled } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import { AppRole } from 'app/constants';
import clsx from 'clsx';
import * as React from 'react';
import { Helmet } from 'react-helmet-async';
import { Route, Switch, useRouteMatch } from 'react-router-dom';
import Appbar from '../components/Appbar';
import PageHeader from '../components/PageHeader';
import { SysAdminDashboardPage } from '../pages/SysAdminDashboardPage/Loadable';
import SysClassesPage from '../pages/SysClassesPage';
import { SysLecturersPage } from '../pages/SysLecturersPage/Loadable';
import { SysOrganizationsPage } from '../pages/SysOrganizationsPage/Loadable';
import SysPartnersPage from '../pages/SysPartnersPage';
import SysStudentsPage from '../pages/SysStudentsPage';
import { UsersListPage } from '../pages/UsersListPage/Loadable';

export function SysAdminApp() {
  let { path } = useRouteMatch();
  const classes = useStyles();

  return (
    <>
      <Helmet>
        <title>SysAdmin</title>
        <meta name="description" content="Quản lý thực tập" />
      </Helmet>
      <Appbar roleAdmin={AppRole.SYSTEM_ADMIN} />
      <main className={clsx(classes.root)}>
        <div className={classes.toolbar} />
        <Switch>
          <Route exact path={`${path}/home`}>
            <SysAdminDashboardPage />
          </Route>
          <Route exact path={`${path}/orgs`}>
            <StyledPageHeader>
              <PageHeader name="orgs" />
            </StyledPageHeader>
            <SysOrganizationsPage />
          </Route>
          <Route exact path={`${path}/classes`}>
            <StyledPageHeader>
              <PageHeader name="classes" />
            </StyledPageHeader>
            <SysClassesPage />
          </Route>
          <Route exact path={`${path}/users`}>
            <StyledPageHeader>
              <PageHeader name="users" />
            </StyledPageHeader>
            <UsersListPage />
          </Route>
          <Route exact path={`${path}/students`}>
            <StyledPageHeader>
              <PageHeader name="students" />
            </StyledPageHeader>
            <SysStudentsPage />
          </Route>
          <Route exact path={`${path}/partners`}>
            <StyledPageHeader>
              <PageHeader name="partners" />
            </StyledPageHeader>
            <SysPartnersPage />
          </Route>
          <Route exact path={`${path}/lecturers`}>
            <StyledPageHeader>
              <PageHeader name="lecturers" />
            </StyledPageHeader>
            <SysLecturersPage />
          </Route>
        </Switch>
      </main>
    </>
  );
}

const StyledPageHeader = styled('div')(({ theme }) => ({
  marginBottom: -12,
}));

const useStyles = makeStyles(theme => ({
  toolbar: theme.mixins.toolbar,
  root: {
    flexGrow: 1,
    paddingLeft: theme.spacing(3),
    paddingTop: theme.spacing(5),
    paddingRight: theme.spacing(3),
  },
  '@media (min-width: 600px)': {
    root: {
      flexGrow: 1,
      paddingLeft: theme.spacing(33),
    },
  },
}));

export interface PageHeaderProps {
  name: string;
}

export interface UserPageProps {
  user: string;
}

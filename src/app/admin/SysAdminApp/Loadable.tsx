import { LoadingPage } from 'app/pages/Common/LoadingPage/LoadingPage';
import { lazyLoad } from 'utils/loadable';

export const SysAdminApp = lazyLoad(
  () => import('./index'),
  module => module.SysAdminApp,
  {
    fallback: <LoadingPage />,
  },
);

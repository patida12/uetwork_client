/**
 *
 * App
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 */

import { NotFoundPage } from 'app/pages/Common/NotFoundPage/Loadable';
import { PartnerApp } from 'app/pages/Partner/Loadable';
import { SnackbarProvider } from 'notistack';
import * as React from 'react';
import { Helmet } from 'react-helmet-async';
import { useTranslation } from 'react-i18next';
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom';
import { GlobalStyle } from 'styles/global-styles';
import { QueryParamProvider } from 'use-query-params';
import { UserContext } from '../contexts/UserContext';
import { AuthUser } from '../models';
import { OrgAdminApp } from './admin/OrgAdminApp/Loadable';
import { SysAdminApp } from './admin/SysAdminApp/Loadable';
import { PrivateRoute } from './components/PrivateRoute/PrivateRoute';
// fix for `https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Errors/Cant_access_lexical_declaration_before_init`
import './constants';
import { PATH_AUTH } from './constants/path.constants';
import { AppRole } from './constants/role.constants';
import { CompleteResetPasswordPage } from './pages/Common/CompleteResetPasswordPage/CompleteResetPasswordPage';
import { ForgotPasswordPage } from './pages/Common/ForgotPasswordPage/Loadable';
import { LoginPage as UserLoginPage } from './pages/Common/LoginPage/Loadable';
import { RegisterPage } from './pages/Common/RegisterPage/Loadable';
import { LecturerApp } from './pages/Lecturer/Loadable';
import StudentApp from './student/pages';

export function App() {
  const { i18n } = useTranslation();

  const [currentUser, setCurrentUser] = React.useState<AuthUser | undefined>(
    undefined,
  );

  return (
    <BrowserRouter
      basename={process.env.REACT_APP_BASENAME}
      getUserConfirmation={(message, callback) => {
        const allowTransition = window.confirm(message);
        callback(allowTransition);
      }}
    >
      <QueryParamProvider ReactRouterRoute={Route}>
        <Helmet
          titleTemplate={'%s - UET Work'}
          defaultTitle={'UET Work'}
          htmlAttributes={{ lang: i18n.language }}
        >
          <meta name="description" content="Quản lý thực tập" />
        </Helmet>

        <SnackbarProvider
          anchorOrigin={{ horizontal: 'right', vertical: 'top' }}
        >
          <UserContext.Provider value={{ currentUser, setCurrentUser }}>
            <Switch>
              <Redirect exact path="/" to="/home" />
              <Route exact path={PATH_AUTH.login} component={UserLoginPage} />
              <Route exact path={PATH_AUTH.register} component={RegisterPage} />
              <Route
                exact
                path={PATH_AUTH.resetPassword}
                component={ForgotPasswordPage}
              />
              <Route
                exact
                path={PATH_AUTH.completeResetPassword}
                component={CompleteResetPasswordPage}
              />
              <PrivateRoute
                requiredRoles={[
                  AppRole.LECTURER,
                  AppRole.PARTNER,
                  AppRole.STUDENT,
                  AppRole.ORG_ADMIN,
                  AppRole.SYSTEM_ADMIN,
                ]}
                path="/home"
                render={() => {
                  switch (currentUser!.role) {
                    case AppRole.LECTURER:
                      return <Redirect to="/lecturer/home" />;
                    case AppRole.PARTNER:
                      return <Redirect to="/partner/home" />;
                    case AppRole.STUDENT:
                      return <Redirect to="/student/home" />;
                    case AppRole.ORG_ADMIN:
                      return <Redirect to="/admin/org/home" />;
                    case AppRole.SYSTEM_ADMIN:
                      return <Redirect to="/admin/sys/home" />;
                    default:
                      return <Redirect to="/login" />;
                  }
                }}
              />
              <PrivateRoute
                requiredRoles={[AppRole.LECTURER]}
                path="/lecturer"
                component={LecturerApp}
              ></PrivateRoute>
              <PrivateRoute
                requiredRoles={[AppRole.PARTNER]}
                path="/partner"
                component={PartnerApp}
              />
              <PrivateRoute
                requiredRoles={[AppRole.STUDENT]}
                path="/student"
                component={StudentApp}
              />
              <PrivateRoute
                requiredRoles={[AppRole.ORG_ADMIN]}
                path="/admin/org"
                component={OrgAdminApp}
              />
              <PrivateRoute
                requiredRoles={[AppRole.SYSTEM_ADMIN]}
                path="/admin/sys"
                component={SysAdminApp}
              />
              <Route path="*" component={NotFoundPage} />
            </Switch>
          </UserContext.Provider>
        </SnackbarProvider>
        <GlobalStyle />
      </QueryParamProvider>
    </BrowserRouter>
  );
}

import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { getLatestTerm, getPartnersList } from 'api/studentApi';
import { LatestTerm, Partner } from 'types/student';

type StudentInitialState = {
  latestTerm: LatestTerm;
  partners: Partner[];
};

export const getLatestTermThunk = createAsyncThunk(
  'student/getLatestTermThunk',
  async () => {
    const response = await getLatestTerm();
    return response;
  },
);

export const getPartnersListThunk = createAsyncThunk(
  'student/getPartnersListThunk',
  async (termId: number) => {
    const response = await getPartnersList(termId);
    return response;
  },
);

const ACTIVE_TERM_INITIAL_STATE = {
  id: 0,
  year: 0,
  term: 0,
  startDate: '',
  endDate: '',
  startRegAt: '',
  endRegAt: '',
  isRegistered: false,
  isRegistrationExpired: false,
  isActive: false,
};

const initialState: StudentInitialState = {
  latestTerm: ACTIVE_TERM_INITIAL_STATE,
  partners: [],
};

// Then, handle actions in reducers
export const studentSlice = createSlice({
  name: 'student',
  initialState,
  reducers: {},
  extraReducers: builder => {
    // Add reducers for additional action types here, and handle loading state as needed
    builder.addCase(getLatestTermThunk.fulfilled, (state, action) => {
      state.latestTerm = action.payload;
    });
    builder.addCase(getPartnersListThunk.fulfilled, (state, action) => {
      state.partners = action.payload;
    });
  },
});

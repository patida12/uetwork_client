import {
  Action,
  CombinedState,
  combineReducers,
  configureStore,
} from '@reduxjs/toolkit';

import { studentSlice } from './studentSlice';

const combinedReducer = combineReducers({
  student: studentSlice.reducer,
});

const rootReducer = (state: CombinedState<any>, action: Action) => {
  return combinedReducer(state, action);
};

const store = configureStore({
  reducer: rootReducer,
  middleware: getDefaultMiddleware => getDefaultMiddleware(),
});

export type RootState = ReturnType<typeof store.getState>;

export type AppDispatch = typeof store.dispatch;

export default store;

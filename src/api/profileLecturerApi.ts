import { Lecturer, UpdateLecturer } from 'models';
import axiosClient from './axiosClient';
import {
  CURRENT_PAGE_META,
  INTERNSHIP_LECTURERS,
  ORGS,
  PER_PAGE_META,
  TOTAL_ITEMS_META,
  TOTAL_PAGES_META,
} from './constants';
import { CodeResults, PaginatedResults, QueryParameters } from './types';

const profileLecturerApi = {
  getAll(options: QueryParameters): Promise<PaginatedResults<Lecturer>> {
    const url = `${ORGS}/${INTERNSHIP_LECTURERS}`;
    var { perPage, page, sort, q, filter } = options;
    return axiosClient
      .get(url, {
        params: { perPage, page, sort, q, filter },
      })
      .then((res: any) => {
        return {
          totalItems: res.data.meta[TOTAL_ITEMS_META],
          currentPage: res.data.meta[CURRENT_PAGE_META],
          totalPages: res.data.meta[TOTAL_PAGES_META],
          perPage: res.data.meta[PER_PAGE_META],
          items: res.data.lecturers,
        };
      });
  },
  updateLecturer(id: number, lecturer: UpdateLecturer): Promise<CodeResults> {
    //TODO: integrate api
    const url = `${ORGS}/${INTERNSHIP_LECTURERS}/${id}`;
    return axiosClient.put(url, { ...lecturer }).then((res: any) => {
      return {
        code: res.data.code,
        message: res.data.message,
      };
    });
  },
  getAllSysLecturers(
    options: QueryParameters,
  ): Promise<PaginatedResults<Lecturer>> {
    const url = `${INTERNSHIP_LECTURERS}`;
    var { perPage, page, sort, q, filter } = options;
    return axiosClient
      .get(url, {
        params: { perPage, page, sort, q, filter },
      })
      .then((res: any) => {
        return {
          totalItems: res.data.meta[TOTAL_ITEMS_META],
          currentPage: res.data.meta[CURRENT_PAGE_META],
          totalPages: res.data.meta[TOTAL_PAGES_META],
          perPage: res.data.meta[PER_PAGE_META],
          items: res.data.lecturers,
        };
      });
  },
};
export default profileLecturerApi;

import axios, { AxiosError, AxiosRequestConfig, AxiosResponse } from 'axios';
import qs from 'qs';
import {
  getAccessToken,
  getRefreshToken,
  logout,
  refresh,
  updateAccessToken,
} from './authApi';

const axiosClient = axios.create({
  baseURL: process.env.REACT_APP_API_URL || 'http://localhost:8000',
  headers: {
    'Content-Type': 'application/json',
  },
  paramsSerializer: params => {
    return qs.stringify(params, {
      arrayFormat: 'brackets',
    });
  },
});

const AUTHORIZATION_HEADER = 'authorization';
const AUTH_RETRY_HEADER = 'x-auth-retry';

const authRequestInterceptor = (request: AxiosRequestConfig) => {
  const token = getAccessToken();
  if (token !== '') {
    request.headers[AUTHORIZATION_HEADER] = `Bearer ${token}`;
  }

  return request;
};

const responseInterceptor = (res: AxiosResponse) => {
  return res;
};

const authResponseInterceptor = async (err: AxiosError) => {
  const originalReq = err.config;
  if (originalReq.url !== '/auth/login' && err.response) {
    if (err.response.status === 401) {
      if (originalReq.headers[AUTH_RETRY_HEADER] != null) {
        logout();
        return Promise.reject(err);
      }

      try {
        const refreshToken = getRefreshToken();
        const resp = await refresh(refreshToken, {
          [AUTH_RETRY_HEADER]: '1',
        });

        const { accessToken } = resp;
        updateAccessToken(accessToken);

        return axiosClient(originalReq);
      } catch (e) {
        return Promise.reject(e);
      }
    }
  }

  return Promise.reject(err);
};

axiosClient.interceptors.request.use(authRequestInterceptor);
axiosClient.interceptors.response.use(
  responseInterceptor,
  authResponseInterceptor,
);

export { authResponseInterceptor };
export default axiosClient;

import { CreateAccountPartner, ProfilePartner } from 'models';
import axiosClient from './axiosClient';
import {
  CURRENT_PAGE_META,
  INTERNSHIP_PARTNERS,
  ORGS,
  PER_PAGE_META,
  TOTAL_ITEMS_META,
  TOTAL_PAGES_META,
} from './constants';
import { CodeResults, PaginatedResults, QueryParameters } from './types';

const profilePartnerApi = {
  getAll(options: QueryParameters): Promise<PaginatedResults<ProfilePartner>> {
    const url = `${ORGS}/${INTERNSHIP_PARTNERS}`;
    const include: string[] = ['contacts'];
    var { perPage, page, sort, q, filter } = options;
    return axiosClient
      .get(url, {
        params: { perPage, page, sort, q, filter, include },
      })
      .then((res: any) => {
        return {
          totalItems: res.data.meta[TOTAL_ITEMS_META],
          currentPage: res.data.meta[CURRENT_PAGE_META],
          totalPages: res.data.meta[TOTAL_PAGES_META],
          perPage: res.data.meta[PER_PAGE_META],
          items: res.data.partners,
        };
      });
  },
  getAllSysPartners(
    options: QueryParameters,
  ): Promise<PaginatedResults<ProfilePartner>> {
    const url = `${INTERNSHIP_PARTNERS}`;
    const include: string[] = ['contacts'];
    var { perPage, page, sort, q, filter } = options;
    return axiosClient
      .get(url, {
        params: { perPage, page, sort, q, filter, include },
      })
      .then((res: any) => {
        return {
          totalItems: res.data.meta[TOTAL_ITEMS_META],
          currentPage: res.data.meta[CURRENT_PAGE_META],
          totalPages: res.data.meta[TOTAL_PAGES_META],
          perPage: res.data.meta[PER_PAGE_META],
          items: res.data.partners,
        };
      });
  },
  addAccountPartner(accPartner: CreateAccountPartner): Promise<CodeResults> {
    const url = `users/partners`;
    return axiosClient
      .post(url, {
        ...accPartner,
      })
      .then((res: any) => {
        return {
          code: res.code,
          message: res.message,
        };
      });
  },
};

export default profilePartnerApi;

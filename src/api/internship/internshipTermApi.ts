import axiosClient from 'api/axiosClient';
import {
  COMMON_INTERSHIP_URL,
  CURRENT_PAGE_META,
  LISTING,
  PER_PAGE_META,
  TOTAL_ITEMS_META,
  TOTAL_PAGES_META,
} from 'api/constants';
import {
  CodeResults,
  PaginatedResults,
  QueryParameters,
  Results,
} from 'api/types';
import {
  InternshipTerm,
  InternshipTermListing,
  NewTerm,
} from 'models/internshipTerm';

const internshipTermApi = {
  getAllTerms(
    options: QueryParameters,
  ): Promise<PaginatedResults<InternshipTerm>> {
    const url = `${COMMON_INTERSHIP_URL}`;
    var { perPage, page, sort, q, filter } = options;
    return axiosClient
      .get(url, {
        params: { perPage, page, sort, q, filter },
      })
      .then((res: any) => {
        return {
          totalItems: res.data.meta[TOTAL_ITEMS_META],
          currentPage: res.data.meta[CURRENT_PAGE_META],
          totalPages: res.data.meta[TOTAL_PAGES_META],
          perPage: res.data.meta[PER_PAGE_META],
          items: res.data.terms,
        };
      });
  },
  upsertTerm(newTerm: NewTerm, termId?: number): Promise<CodeResults> {
    const url = `${COMMON_INTERSHIP_URL}`;
    return axiosClient.post(url, { ...newTerm, termId }).then((res: any) => {
      return {
        code: res.code,
        message: res.message,
      };
    });
  },
  getTermListing(): Promise<Results<InternshipTermListing[]>> {
    const url = `${COMMON_INTERSHIP_URL}/${LISTING}`;
    return axiosClient.get(url).then((res: any) => {
      return {
        codeResults: { code: res.code, message: res.message },
        item: res.data,
      };
    });
  },
};

export default internshipTermApi;

import axiosClient from 'api/axiosClient';
import {
  COMMON_INTERSHIP_URL,
  CONTACTS,
  CURRENT_PAGE_META,
  INFO,
  INTERNSHIP_PARTNERS,
  INTERNSHIP_STATUS,
  LISTING,
  PER_PAGE_META,
  TERM,
  TOTAL_ITEMS_META,
  TOTAL_PAGES_META,
} from 'api/constants';
import {
  CodeResults,
  PaginatedResults,
  QueryParameters,
  Results,
} from 'api/types';
import { ContactInfo, Partner } from 'models';
import { InternshipPartner, PartnerListing } from 'models/internshipPartner';

const internshipPartnerApi = {
  getAllInternshipPartners(
    options: QueryParameters,
    termId: number,
  ): Promise<PaginatedResults<InternshipPartner>> {
    const url = `${COMMON_INTERSHIP_URL}/${termId}/${INTERNSHIP_PARTNERS}`;
    var { perPage, page, sort, q, filter } = options;
    return axiosClient
      .get(url, {
        params: { perPage, page, sort, q, filter },
      })
      .then((res: any) => {
        return {
          totalItems: res.data.meta[TOTAL_ITEMS_META],
          currentPage: res.data.meta[CURRENT_PAGE_META],
          totalPages: res.data.meta[TOTAL_PAGES_META],
          perPage: res.data.meta[PER_PAGE_META],
          items: res.data.partners,
        };
      });
  },
  acceptOrReject(
    partnerIds: number[],
    status: string,
    termId: number,
  ): Promise<CodeResults> {
    const url = `${COMMON_INTERSHIP_URL}/${termId}/${INTERNSHIP_PARTNERS}/${INTERNSHIP_STATUS}`;
    return axiosClient
      .put(url, {
        partnerIds: partnerIds,
        status: status,
      })
      .then((res: any) => {
        return {
          code: res.code,
          message: res.message,
        };
      });
  },
  getListPartners(id: number): Promise<Results<PartnerListing[]>> {
    const url = `${TERM}/${id}/${INTERNSHIP_PARTNERS}/${LISTING}`;
    return axiosClient.get(url).then((res: any) => {
      return {
        codeResults: { code: res.code, message: res.message },
        item: res.data,
      };
    });
  },
  getInfoPartner(partnerId: number): Promise<Results<Partner>> {
    const url = `${INTERNSHIP_PARTNERS}/${partnerId}/${INFO}`;
    return axiosClient.get(url).then((res: any) => {
      return {
        codeResults: { code: res.code, message: res.message },
        item: res.data,
      };
    });
  },
  getContactsListing(partnerId: number): Promise<Results<ContactInfo[]>> {
    const url = `${INTERNSHIP_PARTNERS}/${partnerId}/${CONTACTS}`;
    return axiosClient.get(url).then((res: any) => {
      return {
        codeResults: { code: res.code, message: res.message },
        item: res.data,
      };
    });
  },
  addPartnersToTerm(
    termId: number,
    partnerIds: number[],
  ): Promise<Results<number>> {
    const url = `/orgs/terms/${termId}/partners`;
    return axiosClient
      .post(url, {
        partnerIds: partnerIds,
      })
      .then((res: any) => {
        return {
          codeResults: { code: res.code, message: res.message },
          item: res.data.successIds,
        };
      });
  },
};

export default internshipPartnerApi;

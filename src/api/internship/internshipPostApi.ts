import axiosClient from 'api/axiosClient';
import {
  COMMON_INTERSHIP_URL,
  CURRENT_PAGE_META,
  INTERNSHIP_POSTS,
  PER_PAGE_META,
  TOTAL_ITEMS_META,
  TOTAL_PAGES_META,
} from 'api/constants';
import {
  CodeResults,
  PaginatedResults,
  QueryParameters,
  Results,
} from 'api/types';
import { InternshipPost, UpdatePost } from 'models/internshipPost';

const internshipPostApi = {
  getAllPosts(
    options: QueryParameters,
    termId: number,
  ): Promise<PaginatedResults<InternshipPost>> {
    const url = `${COMMON_INTERSHIP_URL}/${termId}/${INTERNSHIP_POSTS}`;
    var { perPage, page, sort, q, filter } = options;
    return axiosClient
      .get(url, {
        params: { perPage, page, sort, q, filter },
      })
      .then((res: any) => {
        return {
          totalItems: res.data.meta[TOTAL_ITEMS_META],
          currentPage: res.data.meta[CURRENT_PAGE_META],
          totalPages: res.data.meta[TOTAL_PAGES_META],
          perPage: res.data.meta[PER_PAGE_META],
          items: res.data.posts,
        };
      });
  },
  getDetailPost(
    postId: number,
    termId: number,
  ): Promise<Results<InternshipPost>> {
    const url = `${COMMON_INTERSHIP_URL}/${termId}/${INTERNSHIP_POSTS}/${postId}`;
    return axiosClient.get(url).then((res: any) => {
      return {
        codeResults: {
          code: res.code,
          message: res.message,
        },
        item: res.data.post,
      };
    });
  },
  updatePost(
    postId: number,
    termId: number,
    updatePost: UpdatePost,
  ): Promise<CodeResults> {
    const url = `${COMMON_INTERSHIP_URL}/${termId}/${INTERNSHIP_POSTS}/${postId}`;
    return axiosClient
      .put(url, {
        ...updatePost,
      })
      .then((res: any) => {
        return {
          code: res.code,
          message: res.message,
        };
      });
  },
  addPost(termId: number, addPost: UpdatePost): Promise<CodeResults> {
    const url = `${COMMON_INTERSHIP_URL}/${termId}/${INTERNSHIP_POSTS}`;
    return axiosClient
      .post(url, {
        ...addPost,
      })
      .then((res: any) => {
        return {
          code: res.code,
          message: res.message,
        };
      });
  },
};

export default internshipPostApi;

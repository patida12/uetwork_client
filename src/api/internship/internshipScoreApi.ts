import axiosClient from 'api/axiosClient';
import {
  COMMON_INTERSHIP_URL,
  CURRENT_PAGE_META,
  INTERNSHIP_ASSIGN,
  INTERNSHIP_LECTURERS,
  INTERNSHIP_STUDENTS,
  PER_PAGE_META,
  TOTAL_ITEMS_META,
  TOTAL_PAGES_META,
} from 'api/constants';
import { CodeResults, PaginatedResults, QueryParameters } from 'api/types';
import { InternshipStudent } from 'models/internshipStudent';

const internshipStudentApi = {
  getAllResultInternships(
    options: QueryParameters,
    termId: number,
  ): Promise<PaginatedResults<InternshipStudent>> {
    const url = `${COMMON_INTERSHIP_URL}/${termId}/${INTERNSHIP_STUDENTS}`;
    var { perPage, page, sort, q, filter } = options;
    return axiosClient
      .get(url, {
        params: { perPage, page, sort, q, filter },
      })
      .then((res: any) => {
        return {
          totalItems: res.data.meta[TOTAL_ITEMS_META],
          currentPage: res.data.meta[CURRENT_PAGE_META],
          totalPages: res.data.meta[TOTAL_PAGES_META],
          perPage: res.data.meta[PER_PAGE_META],
          items: res.data.students,
        };
      });
  },
  assignSupervisor(
    supervisorId: number,
    studentIds: number[],
    termId: number,
  ): Promise<CodeResults> {
    const url = `${COMMON_INTERSHIP_URL}/${termId}/${INTERNSHIP_LECTURERS}/${INTERNSHIP_ASSIGN}`;
    return axiosClient
      .post(url, {
        assignments: [{ studentIds: studentIds, supervisorId: supervisorId }],
      })
      .then((res: any) => {
        return {
          code: res.code,
          message: res.message,
        };
      });
  },
};

export default internshipStudentApi;

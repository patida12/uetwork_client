import axiosClient from 'api/axiosClient';
import {
  COMMON_INTERSHIP_URL,
  CURRENT_PAGE_META,
  INTERNSHIP_LECTURERS,
  PER_PAGE_META,
  TOTAL_ITEMS_META,
  TOTAL_PAGES_META,
} from 'api/constants';
import { PaginatedResults, QueryParameters } from 'api/types';
import { InternshipLecturer } from 'models/internshipLecturer';

const internshipLecturerApi = {
  getAllInternshipLecturers(
    options: QueryParameters,
    termId: number,
  ): Promise<PaginatedResults<InternshipLecturer>> {
    const url = `${COMMON_INTERSHIP_URL}/${termId}/${INTERNSHIP_LECTURERS}`;
    var { perPage, page, sort, q, filter } = options;
    return axiosClient
      .get(url, {
        params: { perPage, page, sort, q, filter },
      })
      .then((res: any) => {
        return {
          totalItems: res.data.meta[TOTAL_ITEMS_META],
          currentPage: res.data.meta[CURRENT_PAGE_META],
          totalPages: res.data.meta[TOTAL_PAGES_META],
          perPage: res.data.meta[PER_PAGE_META],
          items: res.data.lecturers,
        };
      });
  },
};

export default internshipLecturerApi;

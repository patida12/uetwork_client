import axiosClient from 'api/axiosClient';
import {
  COMMON_INTERSHIP_URL,
  CURRENT_PAGE_META,
  INTERNSHIP_APPLICATIONS_URL,
  INTERNSHIP_NOTIFY_STATUS,
  INTERNSHIP_STATUS_URL,
  PER_PAGE_META,
  TOTAL_ITEMS_META,
  TOTAL_PAGES_META,
} from 'api/constants';
import { CodeResults, PaginatedResults, QueryParameters } from 'api/types';
import { InternshipRequest } from 'models/internshipRequest';

const internshipRequestApi = {
  getAllApplicants(
    options: QueryParameters,
    termId: number,
  ): Promise<PaginatedResults<InternshipRequest>> {
    const url = `${COMMON_INTERSHIP_URL}/${termId}/${INTERNSHIP_APPLICATIONS_URL}`;
    var { perPage, page, sort, q, filter } = options;
    return axiosClient
      .get(url, {
        params: { perPage, page, sort, q, filter },
      })
      .then((res: any) => {
        return {
          totalItems: res.data.meta[TOTAL_ITEMS_META],
          currentPage: res.data.meta[CURRENT_PAGE_META],
          totalPages: res.data.meta[TOTAL_PAGES_META],
          perPage: res.data.meta[PER_PAGE_META],
          items: res.data.students,
        };
      });
  },
  markAsPassedOrFailed(
    registrationIds: number[],
    status: string,
    termId: number,
  ): Promise<CodeResults> {
    const url = `${COMMON_INTERSHIP_URL}/${termId}/${INTERNSHIP_STATUS_URL}`;
    return axiosClient
      .post(url, {
        registrationIds: registrationIds,
        status: status,
      })
      .then((res: any) => {
        return {
          code: res.code,
          message: res.message,
        };
      });
  },
  sendNotice(registrationIds: number[], termId: number): Promise<CodeResults> {
    const url = `${COMMON_INTERSHIP_URL}/${termId}/${INTERNSHIP_APPLICATIONS_URL}/${INTERNSHIP_NOTIFY_STATUS}`;
    return axiosClient
      .post(url, {
        registrationIds: registrationIds,
      })
      .then((res: any) => {
        return {
          code: res.code,
          message: res.message,
        };
      });
  },
};

export default internshipRequestApi;

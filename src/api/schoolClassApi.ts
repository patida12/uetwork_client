import { NewSchoolClass, SchoolClassInfo } from 'models/schoolClass';
import axiosClient from './axiosClient';
import {
  CURRENT_PAGE_META,
  PER_PAGE_META,
  TOTAL_ITEMS_META,
  TOTAL_PAGES_META,
} from './constants';
import { CodeResults, PaginatedResults, QueryParameters } from './types';

const schoolClassApi = {
  getAll(options: QueryParameters): Promise<PaginatedResults<SchoolClassInfo>> {
    const url = `classes`;
    var { perPage, page, sort, q, filter } = options;
    return axiosClient
      .get(url, {
        params: { perPage, page, sort, q, filter },
      })
      .then((res: any) => {
        return {
          totalItems: res.data.meta[TOTAL_ITEMS_META],
          currentPage: res.data.meta[CURRENT_PAGE_META],
          totalPages: res.data.meta[TOTAL_PAGES_META],
          perPage: res.data.meta[PER_PAGE_META],
          items: res.data.classes,
        };
      });
  },
  addSchoolClass(newOrg: NewSchoolClass): Promise<CodeResults> {
    //TODO
    const url = `classes`;
    return axiosClient
      .post(url, {
        ...newOrg,
      })
      .then((res: any) => {
        return {
          code: res.code,
          message: res.message,
        };
      });
  },
  editSchoolClass(
    schoolClassId: number,
    updateOrg: NewSchoolClass,
  ): Promise<CodeResults> {
    //TODO
    const url = `classes/${schoolClassId}`;
    return axiosClient
      .put(url, {
        ...updateOrg,
      })
      .then((res: any) => {
        return {
          code: res.code,
          message: res.message,
        };
      });
  },
};
export default schoolClassApi;

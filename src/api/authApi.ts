import { AuthUser, ServerResponse } from '../models';
import axiosClient from './client';

const getUserInfo = async (): Promise<AuthUser> => {
  return axiosClient.post<ServerResponse<AuthUser>>('/auth/info').then(res => {
    return res.data.data;
  });
};

const login = async (dto: LoginDto): Promise<LoginDtoResponse> => {
  return axiosClient
    .post<ServerResponse<LoginDtoResponse>>('/auth/login', dto)
    .then(res => {
      const dataObj = res.data.data;
      updateAccessToken(dataObj.accessToken);
      updateRefreshToken(dataObj.refreshToken);

      return dataObj;
    });
};

const loginWithGoogle = async (idToken: string): Promise<LoginDtoResponse> => {
  return axiosClient
    .post<ServerResponse<LoginDtoResponse>>('/auth/login/google', { idToken })
    .then(res => {
      const dataObj = res.data.data;
      updateAccessToken(dataObj.accessToken);
      updateRefreshToken(dataObj.refreshToken);

      return dataObj;
    });
};

const refresh = async (
  refreshToken: string,
  headers?: Record<string, string>,
): Promise<{ accessToken: string }> => {
  return axiosClient
    .post<ServerResponse<{ accessToken: string }>>(
      '/auth/refresh',
      {
        refreshToken,
      },
      {
        headers,
      },
    )
    .then(res => res.data.data);
};

const logout = () => {
  updateAccessToken('');
  updateRefreshToken('');
  window.location.assign('/login');
};

const ACCESS_TOKEN_KEY = 'at';
const REFRESH_TOKEN_KEY = 'rt';

const getAccessToken = (): string => {
  return localStorage.getItem(ACCESS_TOKEN_KEY) ?? '';
};

const updateAccessToken = (accessToken: string): void => {
  localStorage.setItem(ACCESS_TOKEN_KEY, accessToken);
};

const getRefreshToken = (): string => {
  return localStorage.getItem(REFRESH_TOKEN_KEY) ?? '';
};

const updateRefreshToken = (token: string): void => {
  localStorage.setItem(REFRESH_TOKEN_KEY, token);
};

const register = (dto: RegisterDto): Promise<void> => {
  return axiosClient.post('/auth/register', dto).then(() => {});
};

const resetPassword = (email: string): Promise<void> => {
  return axiosClient.post('/auth/reset-password', { email }).then(() => {});
};

const completeResetPassword = async (
  resetToken: string,
  requestId: number,
  newPassword: string,
): Promise<void> => {
  await axiosClient.post('/auth/reset-password/complete', {
    resetToken,
    requestId,
    newPassword,
  });
};

const updatePassword = async (
  currentPassword: string,
  newPassword: string,
): Promise<void> => {
  const dto = { currentPassword, newPassword };
  await axiosClient.put('/auth/password', dto);
};

interface RegisterDto {
  email: string;
  fullName: string;
  organizationId: number;
}

interface LoginDto {
  email: string;
  password: string;
}

interface LoginDtoResponse {
  refreshToken: string;
  accessToken: string;
  user: AuthUser;
}

export {
  login,
  refresh,
  getAccessToken,
  updateAccessToken,
  getRefreshToken,
  updateRefreshToken,
  getUserInfo,
  logout,
  register,
  resetPassword,
  updatePassword,
  completeResetPassword,
  loginWithGoogle,
};
export type { LoginDto, LoginDtoResponse, RegisterDto };

import { OrganizationInfo } from '../../models/organization';
import { RegisterDto } from '../authApi';
import { awaitTimeout } from './utils';

const register = async (dto: RegisterDto) => {
  console.log(dto);
  await awaitTimeout();
  return;
};

const getOrgs = async (): Promise<OrganizationInfo[]> => {
  await awaitTimeout();
  return [
    {
      id: 1,
      name: 'Khoa CNTT',
    },
    {
      id: 2,
      name: 'Khoa DTVT',
    },
  ];
};

const resetPassword = async (email: string): Promise<void> => {
  console.log(email);
  await awaitTimeout();
};

export { register, getOrgs, resetPassword };

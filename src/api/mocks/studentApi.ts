import { CreatePartnerRequestDto } from '../studentApi';
import { awaitTimeout } from './utils';

export const createPartnerRequest = async (
  dto: CreatePartnerRequestDto,
): Promise<void> => {
  console.log(dto);
  await awaitTimeout();
};

import {
  LecturerLittleInfo,
  PaginatedResultsMeta,
  PartnerInfo,
  TermLittleInfo,
} from '../../models';
import {
  InternshipType,
  RegistrationStatus,
  StudentApplication,
} from '../../models/appliedStudent';
import { RecruitmentPost } from '../../models/internshipPost';
import { SchoolClassLittleInfo } from '../../models/schoolClass';
import {
  AddContactDto,
  GetPendingApplicationsQuery,
  UpdateContactDto,
  UpdatePartnerInfoDto,
  CreatePostDto,
} from '../partnerApi';
import { PaginableQuery, SearchableQuery, SortableQuery } from '../types';
import { awaitTimeout } from './utils';

interface GetStudentsFilterQuery {
  isSelfRegistered?: boolean;
  termId?: number;
}

interface GetStudentsQuery
  extends PaginableQuery,
    SortableQuery,
    SearchableQuery {
  filter?: GetStudentsFilterQuery;
}

interface GetStudentsResponseData {
  id: number;
  fullName: string;
  orgEmail: string;
  personalEmail: string;
  phoneNumber: string;
  schoolClass?: SchoolClassLittleInfo;
  supervisor?: LecturerLittleInfo;
  registeredAt: string;
  isSelfRegistered: boolean;
}

const getStudents = async (
  query: GetStudentsQuery,
): Promise<{
  meta: PaginatedResultsMeta;
  students: GetStudentsResponseData[];
}> => {
  console.log(query);
  await awaitTimeout();

  return {
    meta: {
      currentPage: 1,
      totalPages: 1,
      totalItems: 1,
      perPage: 10,
    },
    students: [
      {
        id: 1,
        fullName: 'string',
        orgEmail: 'user@example.com',
        personalEmail: 'user@example.com',
        phoneNumber: 'string',
        schoolClass: {
          id: 1,
          name: 'string',
        },
        supervisor: {
          id: 1,
          name: 'string',
        },
        registeredAt: '2019-08-24T14:15:22Z',
        isSelfRegistered: false,
      },
    ],
  };
};

const getTerms = async (): Promise<TermLittleInfo[]> => {
  await awaitTimeout();

  return [
    {
      id: 1,
      name: 'Thuc tap chuyen nganh',
      year: 2022,
      term: 1,
    },
  ];
};

const getStudentApplications = async (
  query: GetPendingApplicationsQuery,
): Promise<{
  meta: PaginatedResultsMeta;
  students: StudentApplication[];
}> => {
  console.log(query);
  await awaitTimeout();

  return {
    meta: { currentPage: 1, totalPages: 1, totalItems: 3, perPage: 10 },
    students: [
      {
        id: 0,
        studentId: 0,
        fullName: 'string',
        studentIdNumber: 'string',
        orgEmail: 'user@example.com',
        phoneNumber: 'string',
        partnerId: 0,
        partnerName: 'string',
        registeredAt: '2019-08-24T14:15:22Z',
        status: RegistrationStatus.WAITING,
        internshipType: InternshipType.ASSOCIATE,
        isMailSent: true,
      },
      {
        id: 1,
        studentId: 1,
        fullName: 'string',
        studentIdNumber: 'string',
        orgEmail: 'user@example.com',
        phoneNumber: 'string',
        partnerId: 0,
        partnerName: 'string',
        registeredAt: '2019-08-24T14:15:22Z',
        status: RegistrationStatus.PASSED,
        internshipType: InternshipType.OTHER,
        isMailSent: true,
      },
      {
        id: 2,
        studentId: 2,
        fullName: 'string',
        studentIdNumber: 'string',
        orgEmail: 'user@example.com',
        phoneNumber: 'string',
        partnerId: 0,
        partnerName: 'string',
        registeredAt: '2019-08-24T14:15:22Z',
        status: RegistrationStatus.FAILED,
        internshipType: InternshipType.ASSOCIATE,
        isMailSent: true,
      },
    ],
  };
};

const getPostById = async (postId: number): Promise<RecruitmentPost> => {
  console.log(postId);
  await awaitTimeout();
  return {
    id: 1,
    termId: 2,
    partnerId: 3,
    partnerName: 'string',
    partnerContactId: 1,
    partnerContactName: 'string',
    partnerContactEmail: 'user@example.com',
    partnerContactPhoneNumber: 'string',
    startRegAt: '2019-08-24T14:15:22Z',
    endRegAt: '2019-08-24T14:15:22Z',
    createdAt: '2019-08-24T14:15:22Z',
    title: 'string',
    content:
      '{"ops":[{"insert":"Tôi là ai giữa cuộc đời này. "},{"attributes":{"link":"http://example.com"},"insert":"Em từ đâu lạc đến nơi"},{"insert":" đây.\\n\\nHeading 1"},{"attributes":{"header":1},"insert":"\\n"},{"insert":"Header 2"},{"attributes":{"header":2},"insert":"\\n"},{"attributes":{"bold":true},"insert":"Nghe"},{"insert":" "},{"attributes":{"italic":true},"insert":"này"},{"insert":"\\n\\n"},{"insert":{"image":"https://firebasestorage.googleapis.com/v0/b/uetwork-dbbf5.appspot.com/o/12320221649747858190image.png?alt=media&token=e66ffb20-56e2-42c4-ac90-2946e3f49ab5"}},{"insert":"\\n"}]}',
    jobCount: 10,
  };
};

const upsertPostById = async (dto: CreatePostDto): Promise<void> => {
  console.log(dto);
  await awaitTimeout();
};

const getPartnerInfo = async (): Promise<PartnerInfo> => {
  await awaitTimeout();
  return {
    id: 1,
    name: 'string',
    homepageUrl: 'http://example.com',
    logoUrl: 'http://example.com',
    description: 'string',
    userId: 1,
    contacts: [
      {
        id: 1,
        fullName: 'Contact Name',
        phoneNumber: '+84988888321',
        partnerId: 0,
        email: 'contact@partner.co',
      },
    ],
    address: 'string',
    phoneNumber: '+84988888321',
    email: 'support@partner.co',
  };
};

const updatePartnerInfo = async (dto: UpdatePartnerInfoDto): Promise<void> => {
  console.log(dto);
  await awaitTimeout();
  return;
};

const updateContact = async (dto: UpdateContactDto): Promise<void> => {
  console.log(dto);
  await awaitTimeout();
  return;
};

const addContact = async (dto: AddContactDto): Promise<void> => {
  console.log(dto);
  await awaitTimeout();
  return;
};

export {
  getStudents,
  getTerms,
  getStudentApplications,
  getPostById,
  upsertPostById,
  getPartnerInfo,
  updatePartnerInfo,
  addContact,
  updateContact,
};
export type {
  GetStudentsQuery,
  GetStudentsFilterQuery,
  GetStudentsResponseData,
};

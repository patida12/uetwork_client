import {
  TermLittleInfo,
  PaginatedResultsMeta,
  TermStudent,
  LecturerInfo,
} from 'models';
import { SchoolClass } from 'models/schoolClass';
import { GetStudentsQuery } from '../lecturerApi';
import { awaitTimeout } from './utils';

const getStudents = async (
  query: GetStudentsQuery = {},
): Promise<{
  students: TermStudent.TermStudent[];
  meta: PaginatedResultsMeta;
}> => {
  console.log('getStudents queries: ', query);

  await awaitTimeout();

  const obj = {
    id: 0,
    fullName: 'string',
    phoneNumber: 'string',
    orgEmail: 'user@example.com',
    personalEmail: 'user@example.com',
    organizationId: 0,
    userId: 0,
    selectedAt: '2019-08-24T14:15:22Z',
    internshipType: 'ASSOCIATE',
    studentIdNumber: '18020112',
    reportSubmitted: true,
    termId: 1,
    schoolClass: {
      id: 0,
      name: 'string',
    },
    selectedPartner: {
      id: 0,
      name: 'string',
    },
    supervisor: {
      id: 0,
      name: 'string',
    },
  };

  const students: TermStudent.TermStudent[] = [];

  for (let i = 0; i < 100; i++) {
    const newObj = { ...obj, id: i };
    students.push(newObj);
  }

  return {
    students: students,
    meta: {
      currentPage: 1,
      perPage: 100,
      totalItems: 100,
      totalPages: 1,
    },
  };
};

const getTerms = async (): Promise<TermLittleInfo[]> => {
  await awaitTimeout();

  return [
    {
      id: 1,
      name: 'Thuc tap chuyen nganh',
      year: 2022,
      term: 1,
    },
  ];
};

const getClasses = async (): Promise<SchoolClass[]> => {
  await awaitTimeout(3000);
  return [
    {
      id: 1,
      name: 'QH-2018-CQ-J',
    },
  ];
};

const getReportDownloadURL = async (studentId: number, termId: number) => {
  await awaitTimeout(1500);

  return {
    downloadUrl: 'https://example.com/',
  };
};

const getLecturerInfo = async (): Promise<LecturerInfo> => {
  await awaitTimeout();

  return {
    id: 1,
    userId: 1,
    fullName: 'Le Thanh Thao',
    personalEmail: 'thanhthao@gmail.com',
    orgEmail: 'thanhthao@vnu.edu.vn',
    phoneNumber: '+84923810323',
  };
};

const updateLecturerInfo = async (dto: {
  fullName: string;
  personalEmail: string;
  phoneNumber: string;
}): Promise<void> => {
  await awaitTimeout();
  return;
};

const updatePassword = async (dto: {
  currentPassword: string;
  newPassword: string;
}): Promise<void> => {
  await awaitTimeout();
  return;
};

export {
  getStudents,
  getTerms,
  getClasses,
  getReportDownloadURL,
  getLecturerInfo,
  updateLecturerInfo,
  updatePassword,
};

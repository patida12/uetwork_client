export const CURRENT_PAGE_META = 'currentPage';
export const TOTAL_PAGES_META = 'totalPages';
export const TOTAL_ITEMS_META = 'totalItems';
export const PER_PAGE_META = 'perPage';
export const PER_PAGE_QUERY = 'perPage';
export const PAGE_QUERY = 'page';
export const SORT_QUERY = 'sort';
export const FILTER_QUERY = 'filter';
export const SEARCH_QUERY = 'q';

//Internship
export const COMMON_INTERSHIP_URL = 'orgs/terms';
export const INTERNSHIP_APPLICATIONS_URL = 'students/applications';
export const INTERNSHIP_STATUS_URL = 'students/status/batch';
export const INTERNSHIP_NOTIFY_STATUS = 'notify-status';
export const INTERNSHIP_STUDENTS = 'students';
export const INTERNSHIP_PARTNERS = 'partners';
export const INTERNSHIP_LECTURERS = 'lecturers';
export const INTERNSHIP_STATUS = 'status';
export const INTERNSHIP_ASSIGN = 'assign';
export const INTERNSHIP_POSTS = 'posts';
export const INFO = 'info';
export const TERM = 'terms';
export const LISTING = 'listing';
export const ORGS = 'orgs';
export const CLASSES = 'classes';
export const CONTACTS = 'contacts';

export const USERS = 'users';
export const ORGANIZATION = 'orgs';

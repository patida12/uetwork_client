import { NewOrg, Organization, UpdateOrg } from 'models/organization';
import axiosClient from './axiosClient';
import {
  CURRENT_PAGE_META,
  ORGS,
  PER_PAGE_META,
  TOTAL_ITEMS_META,
  TOTAL_PAGES_META,
} from './constants';
import { CodeResults, PaginatedResults, QueryParameters } from './types';

const orgApi = {
  getAll(options: QueryParameters): Promise<PaginatedResults<Organization>> {
    const url = `${ORGS}`;
    var { perPage, page, sort, q, filter } = options;
    return axiosClient
      .get(url, {
        params: { perPage, page, sort, q, filter },
      })
      .then((res: any) => {
        return {
          totalItems: res.data.meta[TOTAL_ITEMS_META],
          currentPage: res.data.meta[CURRENT_PAGE_META],
          totalPages: res.data.meta[TOTAL_PAGES_META],
          perPage: res.data.meta[PER_PAGE_META],
          items: res.data.orgs,
        };
      });
  },
  addOrganization(newOrg: NewOrg): Promise<CodeResults> {
    const url = `${ORGS}`;
    return axiosClient
      .post(url, {
        ...newOrg,
      })
      .then((res: any) => {
        return {
          code: res.code,
          message: res.message,
        };
      });
  },
  editOrganization(orgId: number, updateOrg: UpdateOrg): Promise<CodeResults> {
    const url = `${ORGS}/${orgId}`;
    return axiosClient
      .put(url, {
        ...updateOrg,
      })
      .then((res: any) => {
        return {
          code: res.code,
          message: res.message,
        };
      });
  },
};
export default orgApi;

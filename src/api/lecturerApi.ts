import {
  LecturerInfo,
  PaginatedResultsMeta,
  ServerResponse,
  TermStudent,
} from '../models';
import axiosClient from './client';

interface GetStudentsFilterQuery {
  schoolClassId?: number;
  termId?: number;
  reportSubmitted?: boolean;
  scoreGiven?: boolean;
}

interface GetStudentsQuery {
  page?: number;
  perPage?: number;
  q?: string;
  sort?: string[];
  filter?: GetStudentsFilterQuery;
}

const getStudents = async (
  query: GetStudentsQuery = {},
): Promise<{
  students: TermStudent.TermStudent[];
  meta: PaginatedResultsMeta;
}> => {
  return axiosClient
    .get<
      ServerResponse<{
        meta: PaginatedResultsMeta;
        students: TermStudent.TermStudent[];
      }>
    >('terms/my-students', {
      params: query,
    })
    .then(res => res.data.data);
};

const getLecturerInfo = async (): Promise<LecturerInfo> => {
  return axiosClient
    .get<ServerResponse<LecturerInfo>>('me/lecturer/info')
    .then(res => res.data.data);
};

const updateLecturerInfo = async (dto: {
  fullName: string;
  personalEmail: string;
  phoneNumber: string;
}): Promise<void> => {
  await axiosClient.put('me/lecturer/info', dto);
};

const assignScore = async ({
  termId,
  studentId,
  score,
}: {
  score: number;
  studentId: number;
  termId: number;
}): Promise<void> => {
  await axiosClient.post(`terms/${termId}/my-students/${studentId}/score`, {
    score,
  });
};

const downloadStudentReport = (
  termId: number,
  studentId: number,
): Promise<string> => {
  return axiosClient
    .get<ServerResponse<{ downloadUrl: string }>>(
      `terms/${termId}/my-students/${studentId}/report`,
    )
    .then(res => res.data.data.downloadUrl);
};

export {
  getStudents,
  getLecturerInfo,
  updateLecturerInfo,
  assignScore,
  downloadStudentReport,
};

export type { GetStudentsFilterQuery, GetStudentsQuery };

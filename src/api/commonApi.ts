import { ServerResponse, TermLittleInfo } from '../models';
import axiosClient from './client';

const getTerms = async (): Promise<TermLittleInfo[]> => {
  return axiosClient
    .get<ServerResponse<TermLittleInfo[]>>('terms/listing')
    .then(res => res.data.data);
};

const getLecturerListing = async (
  termId: number,
): Promise<{ id: number; fullName: string }[]> => {
  return axiosClient
    .get(`/terms/${termId}/lecturers/listing`)
    .then(res => res.data.data);
};

export { getTerms, getLecturerListing };

import {
  ContactInfo,
  LecturerLittleInfo,
  PaginatedResultsMeta,
  PartnerInfo,
  ServerResponse,
} from '../models';
import {
  RegistrationStatus,
  StudentApplication,
} from '../models/appliedStudent';
import { RecruitmentPost } from '../models/internshipPost';
import { SchoolClassLittleInfo } from '../models/schoolClass';
import axiosClient from './client';
import { PaginableQuery, SearchableQuery, SortableQuery } from './types';

const getPartnerInfo = (): Promise<PartnerInfo> => {
  return axiosClient
    .get<ServerResponse<PartnerInfo>>('me/partner/info')
    .then(res => res.data.data);
};

const updatePartnerInfo = async (dto: UpdatePartnerInfoDto): Promise<void> => {
  console.log(dto);
  await axiosClient.put('me/partner/info', dto);
};

const addContact = async (dto: AddContactDto): Promise<void> => {
  await axiosClient.post('me/partner/contacts', dto);
};

const updateContact = async (dto: UpdateContactDto): Promise<void> => {
  await axiosClient.put('me/partner/contacts', dto);
};

const deleteContact = async (contactId: number): Promise<void> => {
  await axiosClient.delete('me/partner/contacts', {
    data: {
      id: contactId,
    },
  });
};

const getStudentApplications = async (
  query: GetPendingApplicationsQuery,
): Promise<{
  meta: PaginatedResultsMeta;
  students: StudentApplication[];
}> => {
  return await axiosClient
    .get<
      ServerResponse<{
        meta: PaginatedResultsMeta;
        students: StudentApplication[];
      }>
    >('terms/applications', { params: query })
    .then(res => res.data.data);
};

const batchUpdateRegistrationStatus = async (
  dto: BatchUpdateRegStatusDto,
): Promise<void> => {
  await axiosClient.post('terms/applications/status', dto);
};

const getStudents = async (
  query: GetStudentsQuery,
): Promise<{
  meta: PaginatedResultsMeta;
  students: GetStudentsResponseData[];
}> => {
  const res = await axiosClient.get<
    ServerResponse<{
      meta: PaginatedResultsMeta;
      students: GetStudentsResponseData[];
    }>
  >('terms/passed-students', { params: query });

  return res.data.data;
};

const createPost = async (dto: CreatePostDto): Promise<void> => {
  await axiosClient.post('terms/posts', dto);
};

const getPosts = async (
  query: GetPostsQuery,
): Promise<{ meta: PaginatedResultsMeta; posts: RecruitmentPost[] }> => {
  const res = await axiosClient.get('terms/posts', {
    params: query,
  });

  return res.data.data;
};

const updatePost = async (id: number, dto: UpdatePostDto): Promise<void> => {
  await axiosClient.put(`terms/posts/${id}`, dto);
};

const getPostById = async (postId: number): Promise<RecruitmentPost> => {
  const res = await axiosClient.get<ServerResponse<{ post: RecruitmentPost }>>(
    `terms/posts/${postId}`,
  );

  return res.data.data.post;
};

const getContacts = async (): Promise<ContactInfo[]> => {
  const res = await axiosClient.get<ServerResponse<ContactInfo[]>>(
    'me/partner/contacts',
  );

  return res.data.data;
};

interface GetStudentsResponseData {
  id: number;
  fullName: string;
  orgEmail: string;
  personalEmail: string;
  phoneNumber: string;
  schoolClass?: SchoolClassLittleInfo;
  supervisor?: LecturerLittleInfo;
  registeredAt: string;
  isSelfRegistered: boolean;
}

interface GetStudentsFilterQuery {
  termId?: number;
}

interface GetStudentsQuery
  extends PaginableQuery,
    SortableQuery,
    SearchableQuery {
  filter?: GetStudentsFilterQuery;
}

interface GetStudentsResponseData {
  id: number;
  fullName: string;
  orgEmail: string;
  personalEmail: string;
  phoneNumber: string;
  schoolClass?: SchoolClassLittleInfo;
  supervisor?: LecturerLittleInfo;
  registeredAt: string;
  isSelfRegistered: boolean;
}

interface GetPostsFilterQuery {
  termId?: number;
}

interface GetPendingApplicationsFilterQuery {
  termId?: number;
}

interface GetPendingApplicationsQuery
  extends PaginableQuery,
    SortableQuery,
    SearchableQuery {
  filter?: GetPendingApplicationsFilterQuery;
}

interface GetPostsQuery extends PaginableQuery, SortableQuery, SearchableQuery {
  filter?: GetPostsFilterQuery;
}

interface UpdatePostDto {
  partnerContactId: number;
  title: string;
  content: string;
  jobCount: number;
  startRegAt: Date;
  endRegAt: Date;
}

interface CreatePostDto extends UpdatePostDto {
  termId: number;
}

interface UpdatePartnerInfoDto {
  name: string;
  homepageUrl: string;
  logoUrl: string;
  address: string;
  phoneNumber: string;
  description?: string;
}

interface AddContactDto {
  fullName: string;
  phoneNumber: string;
  email: string;
}

interface UpdateContactDto extends AddContactDto {
  id: number;
}

interface BatchUpdateRegStatusDto {
  termId?: number;
  registrationIds: number[];
  status: RegistrationStatus;
}

export {
  getPartnerInfo,
  updatePartnerInfo,
  addContact,
  updateContact,
  deleteContact,
  getStudentApplications,
  batchUpdateRegistrationStatus,
  getStudents,
  createPost,
  updatePost,
  getPostById,
  getContacts,
  getPosts,
};

export type {
  GetStudentsFilterQuery,
  GetStudentsQuery,
  GetStudentsResponseData,
  GetPendingApplicationsQuery,
  GetPendingApplicationsFilterQuery,
  CreatePostDto,
  UpdatePostDto,
  UpdatePartnerInfoDto,
  UpdateContactDto,
  AddContactDto,
  GetPostsQuery,
  GetPostsFilterQuery,
};

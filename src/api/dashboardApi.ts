import { LecturerDashboard } from 'models/lecturerDashboard';
import { OrgAdminDashboard } from 'models/orgAdminDashboard';
import { PartnerDashboard } from 'models/partnerDashboard';
import { SysAdminDashboard } from 'models/sysAdminDashboard';
import axiosClient from './axiosClient';
import { Results } from './types';

const dashboardApi = {
  getOrgAdminDashboard(): Promise<Results<OrgAdminDashboard>> {
    const url = `dashboard/org-admin`;
    return axiosClient.get(url).then((res: any) => {
      return {
        codeResults: { code: res.code, message: res.message },
        item: res.data,
      };
    });
  },
  getSysAdminDashboard(): Promise<Results<SysAdminDashboard>> {
    const url = `dashboard/sys-admin`;
    return axiosClient.get(url).then((res: any) => {
      return {
        codeResults: { code: res.code, message: res.message },
        item: res.data,
      };
    });
  },
  getPartnerDashboard(): Promise<Results<PartnerDashboard>> {
    const url = `dashboard/partner`;
    return axiosClient.get(url).then((res: any) => {
      return {
        codeResults: { code: res.code, message: res.message },
        item: res.data,
      };
    });
  },
  getLecturerDashboard(): Promise<Results<LecturerDashboard>> {
    const url = `dashboard/lecturer`;
    return axiosClient.get(url).then((res: any) => {
      return {
        codeResults: { code: res.code, message: res.message },
        item: res.data,
      };
    });
  },
};
export default dashboardApi;

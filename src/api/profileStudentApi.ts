import { ImportStudent, Student } from 'models';
import { OrganizationInfo } from 'models/organization';
import { SchoolClass } from 'models/schoolClass';
import axiosClient from './axiosClient';
import {
  CLASSES,
  CURRENT_PAGE_META,
  INTERNSHIP_STUDENTS,
  LISTING,
  ORGANIZATION,
  ORGS,
  PER_PAGE_META,
  TOTAL_ITEMS_META,
  TOTAL_PAGES_META,
} from './constants';
import {
  FailedRecord,
  PaginatedResults,
  QueryParameters,
  Results,
} from './types';

const profileStudentApi = {
  getAll(options: QueryParameters): Promise<PaginatedResults<Student>> {
    const url = `${ORGS}/${INTERNSHIP_STUDENTS}`;
    var { perPage, page, sort, q, filter } = options;
    return axiosClient
      .get(url, {
        params: { perPage, page, sort, q, filter },
      })
      .then((res: any) => {
        return {
          totalItems: res.data.meta[TOTAL_ITEMS_META],
          currentPage: res.data.meta[CURRENT_PAGE_META],
          totalPages: res.data.meta[TOTAL_PAGES_META],
          perPage: res.data.meta[PER_PAGE_META],
          items: res.data.students,
        };
      });
  },
  getClassesListing(): Promise<Results<SchoolClass[]>> {
    const url = `${CLASSES}/${LISTING}`;
    return axiosClient.get(url).then((res: any) => {
      return {
        codeResults: {
          code: res.data.code,
          message: res.data.message,
        },
        item: res.data,
      };
    });
  },
  getAllSysStudents(
    options: QueryParameters,
  ): Promise<PaginatedResults<Student>> {
    const url = `${INTERNSHIP_STUDENTS}`;
    var { perPage, page, sort, q, filter } = options;
    return axiosClient
      .get(url, {
        params: { perPage, page, sort, q, filter },
      })
      .then((res: any) => {
        return {
          totalItems: res.data.meta[TOTAL_ITEMS_META],
          currentPage: res.data.meta[CURRENT_PAGE_META],
          totalPages: res.data.meta[TOTAL_PAGES_META],
          perPage: res.data.meta[PER_PAGE_META],
          items: res.data.students,
        };
      });
  },
  getOrganizationListing(): Promise<Results<OrganizationInfo[]>> {
    const url = `${ORGANIZATION}/${LISTING}`;
    return axiosClient.get(url).then((res: any) => {
      return {
        codeResults: {
          code: res.data.code,
          message: res.data.message,
        },
        item: res.data,
      };
    });
  },
  downloadResume(studentId: number): Promise<string> {
    const url = `${INTERNSHIP_STUDENTS}/${studentId}/cv`;
    return axiosClient.get(url).then(res => res.data.downloadUrl);
  },
  importStudents(
    importStudent: ImportStudent,
  ): Promise<Results<FailedRecord[]>> {
    const url = `/users/students/import`;
    return axiosClient
      .post(url, {
        ...importStudent,
      })
      .then((res: any) => {
        return {
          codeResults: {
            code: res.data.code,
            message: res.data.message,
          },
          item: res.data.failedRecords,
        };
      });
  },
};

export default profileStudentApi;

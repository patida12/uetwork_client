import axiosClient from 'api/client';
import {
  AppliedPartner,
  PaginatedResultsMeta,
  ServerResponse,
  Student,
  Supervisor,
} from 'models';
import { LatestTerm, Partner } from 'types/student';
import { StudentRecruitmentPost } from '../models/internshipPost';
import { PaginableQuery } from './types';

interface HiringPostsDto {
  cursor?: string;
  limit?: number;
}

interface CreatePartnerRequestDto {
  name: string;
  homepageUrl: string;
  address: string;
  phoneNumber: string;
  description?: string;
}

interface UpdateInfoDto {
  fullName?: string;
  schoolClassId?: number;
  personalEmail?: string;
  phoneNumber?: string;
}

interface InternshipDetailsResponse {
  supervisor?: Omit<Supervisor, 'userId'>;
  selectedPartnerIndex?: number;
  appliedPartners: AppliedPartner[];
  reportFileName: string;
}

const getLatestTerm = (): Promise<LatestTerm> => {
  return axiosClient
    .get<ServerResponse<LatestTerm>>('/terms/latest')
    .then(res => {
      const dataObj = res.data.data;
      return dataObj;
    });
};

const getStudentPosts = (
  termId: number,
  query: PaginableQuery,
): Promise<{ posts: StudentRecruitmentPost[]; meta: PaginatedResultsMeta }> => {
  return axiosClient
    .get(`/terms/${termId}/student-posts`, { params: query })
    .then(res => {
      return res.data.data;
    });
};

const getStudentPostsById = (
  termId: number,
  postId: number,
): Promise<StudentRecruitmentPost> => {
  return axiosClient
    .get(`/terms/${termId}/student-posts/${postId}`)
    .then(res => res.data.data);
};

const getPartnersList = (termId: number): Promise<Partner[]> => {
  return axiosClient
    .get<ServerResponse<Partner[]>>(`/terms/${termId}/partners/listing`)
    .then(res => {
      const dataObj = res.data.data;
      return dataObj;
    });
};

const registerTerm = (termId: number) => {
  return axiosClient
    .post<ServerResponse>(`/terms/${termId}/register`)
    .then(res => {
      const dataObj = res.data;
      return dataObj;
    });
};

const unregisterTerm = (termId: number) => {
  return axiosClient
    .delete<ServerResponse>(`/terms/${termId}/register`)
    .then(res => {
      const dataObj = res.data;
      return dataObj;
    });
};

const registerPartner = (termId: number, partnerId: number) => {
  return axiosClient
    .post<ServerResponse>(`terms/${termId}/partners/${partnerId}/register`)
    .then(res => {
      const dataObj = res.data;
      return dataObj;
    });
};

const registerPartnerPost = (termId: number, postId: number) => {
  return axiosClient
    .post<ServerResponse>(`terms/${termId}/posts/${postId}/apply`)
    .then(res => {
      const dataObj = res.data;
      return dataObj;
    });
};

const unregisterPartner = async (
  termId: number,
  partnerId: number,
): Promise<void> => {
  await axiosClient.delete<ServerResponse>(
    `terms/${termId}/partners/${partnerId}/unregister`,
  );
};

const getInternshipDetails = (termId: number) => {
  return axiosClient
    .get<ServerResponse<InternshipDetailsResponse>>(
      `terms/${termId}/internship-details`,
    )
    .then(res => res.data.data);
};

const selectPartner = async (
  termId: number,
  partnerId: number,
): Promise<void> => {
  await axiosClient.post(`terms/${termId}/partners/${partnerId}/select`, {});
};

const uploadReport = async (termId: number, file: File): Promise<void> => {
  const formData = new FormData();

  formData.append('file', file, file.name);

  await axiosClient.post(`terms/${termId}/report`, formData, {
    headers: {
      'content-type': 'multipart/form-data',
    },
  });
};

const downloadReport = (termId: number): Promise<string> => {
  return axiosClient
    .get<ServerResponse<{ downloadUrl: string }>>(`terms/${termId}/report`)
    .then(res => res.data.data.downloadUrl);
};

const getInfo = (): Promise<Student> => {
  return axiosClient
    .get<ServerResponse<Student>>('me/student/info')
    .then(res => res.data.data);
};

const updateInfo = async (dto: UpdateInfoDto): Promise<void> => {
  await axiosClient.put('me/student/info', dto);
};

export {
  getLatestTerm,
  getPartnersList,
  registerTerm,
  unregisterTerm,
  registerPartner,
  registerPartnerPost,
  unregisterPartner,
  getInternshipDetails,
  selectPartner,
  uploadReport,
  downloadReport,
  getInfo,
  updateInfo,
  getStudentPosts,
  getStudentPostsById,
};

export type {
  HiringPostsDto,
  InternshipDetailsResponse,
  CreatePartnerRequestDto,
  UpdateInfoDto,
};

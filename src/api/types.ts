export interface PagingOptions {
  page?: number;
  perPage?: number;
  sort?: string;
  q?: string;
  filter?: Record<string, any>;
  include?: string;
}

export interface PaginatedResults<T> {
  totalItems: number;
  currentPage: number;
  totalPages: number;
  perPage: number;
  items: T[];
}

export interface CodeResults {
  code: number;
  message: string;
}

export interface Results<T> {
  codeResults: CodeResults;
  item: T;
}

export interface PaginableQuery {
  page?: number;
  perPage?: number;
}

export interface SearchableQuery {
  q?: string;
}

export interface SortableQuery {
  sort?: string[];
}

export interface QueryParameters {
  page?: number;
  perPage?: number;
  sort?: string[];
  q?: string;
  filter?: Record<string, any>;
  include?: string[];
}

export interface FailedRecord {
  index: number;
  message: string;
}

import { User } from 'models';
import axiosClient from './axiosClient';
import {
  CURRENT_PAGE_META,
  PER_PAGE_META,
  TOTAL_ITEMS_META,
  TOTAL_PAGES_META,
  USERS,
} from './constants';
import { PaginatedResults, QueryParameters } from './types';

const usersApi = {
  getAll(options: QueryParameters): Promise<PaginatedResults<User>> {
    const url = `${USERS}`;
    var { perPage, page, sort, q, filter } = options;
    return axiosClient
      .get(url, {
        params: { perPage, page, sort, q, filter },
      })
      .then((res: any) => {
        return {
          totalItems: res.data.meta[TOTAL_ITEMS_META],
          currentPage: res.data.meta[CURRENT_PAGE_META],
          totalPages: res.data.meta[TOTAL_PAGES_META],
          perPage: res.data.meta[PER_PAGE_META],
          items: res.data.users,
        };
      });
  },
};

export default usersApi;

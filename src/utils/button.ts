import { CustomCardButtonText } from 'app/constants/student.constants';

export const getButtonText = data => {
  if (data?.isRegistered) {
    return CustomCardButtonText.CANCEL;
  }
  if (Date.now() - Date.parse(data?.endRegAt)) {
    return CustomCardButtonText.EXPIRED;
  }
  return CustomCardButtonText.REGISTER;
};

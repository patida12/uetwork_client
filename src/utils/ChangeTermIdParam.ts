export const ChangeTermIdParam = (pathname: string, termId: string) => {
  const newPath = pathname.replace(':termId', termId);
  return newPath;
};

import dayjs from 'dayjs';
import { QuillDeltaToHtmlConverter } from 'quill-delta-to-html';

/**
 * Check if a value is zero value (null, undefined, 0, '')
 * @param value
 * @returns
 */
const isZeroValue = (value: any) => {
  return value == null || value === '' || value === 0;
};

/**
 * Parse a boolean string. If the string is invalid, return undefined.
 * @param value
 * @returns
 */
const safeParseBool = (value: string): boolean | undefined => {
  switch (value) {
    case 'true':
      return true;
    case 'false':
      return false;
    default:
      return undefined;
  }
};

/**
 * Parse a numeric string. If the string is invalid, return undefined.
 * @param value
 * @returns
 */
const safeParseInt = (value: string): number | undefined => {
  const intValue = parseInt(value);
  if (isNaN(intValue)) return undefined;

  return intValue;
};

/** Transform a sort model into sort queries for API.
 * Accept an optional second arguments to map model key to API key.
 */
const sortModelToQuery = (
  model: Record<string, 'asc' | 'desc'>,
  mapping: Record<string, string> = {},
): string[] => {
  const sortQuery: string[] = [];

  let mappedKey = '';
  for (const key in model) {
    mappedKey = key;
    if (mapping[key] != null) {
      mappedKey = mapping[key];
    }

    if (model[key] === 'asc') {
      sortQuery.push('+' + mappedKey);
    } else if (model[key] === 'desc') {
      sortQuery.push('-' + mappedKey);
    } else {
      console.error(
        'sort direction for key ' + key + 'is invalid: ',
        model[key],
      );
      continue;
    }
  }

  return sortQuery;
};

const DATE_FORMAT = 'DD/MM/YYYY';
const formatDate = (dateString: string): string => {
  return dayjs(dateString).format(DATE_FORMAT);
};

const TIMESTAMP_FORMAT = 'HH:mm, DD/MM/YYYY';
const formatTimestamp = (dateString: string): string => {
  return dayjs(dateString).format(TIMESTAMP_FORMAT);
};

const formatScore = (score: number): string => {
  return score.toFixed(2);
};

export const deltaToHTML = (content: string): string => {
  var html = '';
  if (content !== '') {
    let deltaOps: any[] = [];
    try {
      deltaOps = JSON.parse(content)?.ops;
      if (deltaOps == null) {
        throw new Error('error ops field is null');
      }
    } catch (e) {
      deltaOps = [];
      console.error(e);
    }
    var converter = new QuillDeltaToHtmlConverter(deltaOps, {});
    html = converter.convert();
  }
  return html;
};

export {
  isZeroValue,
  safeParseBool,
  safeParseInt,
  sortModelToQuery,
  formatDate,
  formatScore,
  formatTimestamp,
};

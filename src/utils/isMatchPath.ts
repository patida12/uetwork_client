import { matchPath } from 'react-router-dom';

export const isMatchPath = (pathname: string, path: string): boolean => {
  const isMatchPath = matchPath(pathname, {
    path: path,
    exact: false,
  })
    ? true
    : false;

  return isMatchPath;
};

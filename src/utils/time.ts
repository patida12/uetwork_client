import { differenceInDays, format } from 'date-fns';

export function getFormattedDate(stringDate?: string) {
  if (stringDate) {
    return format(new Date(stringDate), 'dd/MM/yyyy');
  }
}

export function getRelativeTime(expiredStringDate: string) {
  const baseDate = new Date();
  const expiredDate = new Date(expiredStringDate);
  const days = differenceInDays(baseDate, expiredDate);
  return days;
}

import { AppRole } from '../app/constants/role.constants';

export interface User {
  id: number;
  userName: string;
  avatarUrl: string;
  roleId: number;
  role: string;
  orgEmail: string;
  isEmailVerified: boolean;
  lastLoginAt: string;
  createdAt: string;
  updatedAt: string;
}

export interface AuthUser {
  id: number;
  avatarUrl: string;
  orgEmail: string;
  roleId: number;
  role: AppRole;
  isEmailVerified: boolean;
}

export * from './common';
export * from './contact';
export * from './lecture';
export * from './partner';
export * from './student';
export * from './user';

export interface RouteDef {
  exact?: boolean;
  path: string;
  name: string;
  component: React.ComponentType;
  icon: React.ComponentType<{ className?: string }>;
  // if `children` property exists, `path` will be ignored.
  children?: ChildRouteDef[];
  hide?: boolean;
}

export interface ChildRouteDef {
  exact?: boolean;
  path: string;
  name: string;
  component: React.ComponentType;
}

export interface ServerResponse<T = any> {
  message: string;
  code: number;
  data: T;
}

export interface ServerErrorResponse {
  code: number;
  message: string;
  error: string;
  details: string[];
}

export interface PaginatedResultsMeta {
  perPage: number;
  totalItems: number;
  totalPages: number;
  currentPage: number;
}

export namespace TermStudent {
  export interface SchoolClass {
    id: number;
    name: string;
  }

  export interface SelectedPartner {
    id: number;
    name: string;
  }

  export interface Supervisor {
    id: number;
    name: string;
  }

  export interface TermStudent {
    id: number;
    fullName: string;
    phoneNumber: string;
    orgEmail: string;
    personalEmail: string;
    organizationId: number;
    userId: number;
    selectedAt: string;
    internshipType: string;
    studentIdNumber: string;
    reportSubmitted: boolean;
    termId: number;
    schoolClass?: SchoolClass;
    selectedPartner?: SelectedPartner;
    supervisor?: Supervisor;
  }
}

import { SchoolClass } from './schoolClass';
import { SelectedPartner } from './selectedPartner';
import { Supervisor } from './supervisor';

export interface InternshipStudent {
  id: number;
  fullName: string;
  orgEmail: string;
  phoneNumber: string;
  personalEmail: string;
  organizationId: number;
  userId: number;
  selectedAt: string;
  internshipType: string;
  studentIdNumber: number;
  reportSubmitted: boolean;
  schoolClass: SchoolClass;
  selectedPartner: SelectedPartner;
  supervisor: Supervisor;
  score: number;
}

export interface InternshipRequest {
  id: number;
  studentId: string;
  fullName: string;
  orgEmail: string;
  phoneNumber: string;
  internshipType: string;
  partnerId: number;
  partnerName: string;
  registeredAt: string;
  status: string;
  isMailSent: boolean;
}

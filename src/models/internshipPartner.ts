import { AppliedStudent } from './appliedStudent';

export interface InternshipPartner {
  id: number;
  name: string;
  email: string;
  homepageUrl: string;
  userId: number;
  numOfAppliedStudents: number;
  termId: number;
  joinedAt: string;
  isAssociate: boolean;
  appliedStudents?: AppliedStudent[];
  status: string;
}

export interface PartnerListing {
  id: number;
  name: string;
  termStatus: string;
  isAssociate: boolean;
  logoUrl: string;
}

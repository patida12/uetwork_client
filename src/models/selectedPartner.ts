export interface SelectedPartner {
  id: number;
  name: string;
}

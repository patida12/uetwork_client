import { OrganizationInfo } from './organization';
import { SchoolClass } from './schoolClass';

export interface Student {
  id: number;
  fullName: string;
  orgEmail: string;
  studentIdNumber: string;
  personalEmail: string;
  createdAt: string;
  updatedAt: string;
  resumeUrl: string;
  phoneNumber: string;
  schoolClass?: SchoolClass;
  userId: number;
  organizationId: number;
  organization?: OrganizationInfo;
}

export interface ImportStudent {
  students: [
    {
      fullName: string;
      orgEmail: string;
      phoneNumber: string;
      studentIdNumber: string;
    },
  ];
  sendMail: boolean;
}

export interface AdminInfo {
  id: number;
  email: string;
}

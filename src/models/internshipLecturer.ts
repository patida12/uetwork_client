import { SupervisedStudent } from './supervisedStudent';

export interface InternshipLecturer {
  id: number;
  fullName: string;
  orgEmail: string;
  personalEmail: string;
  organizationId: number;
  userId: number;
  numOfSupervisedStudents: number;
  supervisedStudents?: SupervisedStudent[];
}

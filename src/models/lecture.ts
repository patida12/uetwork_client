export interface Lecture {
  id: number;
  fullName: string;
  personalEmail?: string;
  phoneNumber?: string;
}

export interface LecturerLittleInfo {
  id: number;
  name: string;
}

export interface TermLittleInfo {
  id: number;
  name: string;
  year: number;
  term: number;
}

export interface LecturerInfo {
  id: number;
  fullName: string;
  phoneNumber: string;
  personalEmail: string;
  orgEmail: string;
  userId: number;
}

export interface Lecturer {
  id: number;
  fullName: string;
  phoneNumber: string;
  personalEmail: string;
  orgEmail: string;
  userId: number;
  organizationId: number;
}

export interface UpdateLecturer {
  fullName: string;
  phoneNumber: string;
  personalEmail: string;
}

export interface Supervisor {
  id: number;
  name: string;
  phoneNumber: string;
  personalEmail: string;
  orgEmail: string;
}

import { RegistrationStatus } from './appliedStudent';
import { Contact, ContactInfo } from './contact';

export interface Partner {
  id: number;
  name: string;
  homepageUrl: string;
  isAssociate: boolean;
  description: string;
  contacts: Contact[];
}

export interface PartnerInfo {
  id: number;
  name: string;
  homepageUrl: string;
  logoUrl: string;
  description: string;
  contacts: ContactInfo[];
  address: string;
  phoneNumber: string;
  email: string;
  userId: number;
}

export interface ProfilePartner {
  id: number;
  name: string;
  homepageUrl: string;
  logoUrl: string;
  description: string;
  contacts: ContactInfo[];
  address?: string;
  userId: number;
  isAssociate: boolean;
  associateExpirationDate?: string;
}

export enum PartnerStatus {
  ACCEPTED = 'ACCEPTED',
  PENDING = 'PENDING',
  REJECTED = 'REJECTED',
}

export interface AppliedPartner {
  id: number;
  name: string;
  applyStatus: RegistrationStatus;
  createdAt: string;
  isSelfRegistered: boolean;
  partnerStatus: PartnerStatus;
  internshipType: string;
}

export interface CreateAccountPartner {
  name: string;
  homepageUrl: string;
  logoUrl: string;
  description: string;
  orgEmail: string;
}

export interface InternshipPost {
  id: number;
  termId: number;
  partnerId: number;
  partnerName: string;
  partnerContactId: number;
  partnerContactName: string;
  partnerContactEmail: string;
  partnerContactPhoneNumber: string;
  title: string;
  createdAt: string;
  startRegAt: string;
  endRegAt: string;
  jobCount: number;
  content: string;
}

export interface UpdatePost {
  partnerId?: number;
  partnerContactId: number;
  title: string;
  startRegAt: string;
  endRegAt: string;
  jobCount: number;
  content: string;
}

export interface RecruitmentPost {
  id: number;
  termId: number;
  partnerId: number;
  partnerName: string;
  partnerContactId: number;
  partnerContactName: string;
  partnerContactEmail: string;
  partnerContactPhoneNumber: string;
  startRegAt: string;
  endRegAt: string;
  createdAt: string;
  title: string;
  content?: string;
  jobCount: number;
}

export interface StudentRecruitmentPost {
  post: RecruitmentPost;
  isRegistered: boolean;
  isSelfRegistered: boolean;
  isRegistrationExpired: boolean;
}

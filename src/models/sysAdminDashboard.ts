export interface SysAdminDashboard {
  orgsStat: {
    total: number;
  };
  classesStat: {
    total: number;
  };
  studentsStat: {
    total: number;
  };
  partnersStat: {
    total: number;
  };
  lecturersStat: {
    total: number;
  };
}

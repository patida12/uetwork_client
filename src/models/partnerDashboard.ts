import { StudentApplication } from './appliedStudent';

export interface PartnerDashboard {
  workingStudentsStat: {
    total: number;
  };
  recentApplications: StudentApplication[];
}

export interface SupervisedStudent {
  id: number;
  studentId: string;
  fullName: string;
  orgEmail: string;
}

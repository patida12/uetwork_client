export interface LecturerDashboard {
  scoring: {
    scored: number;
    notScored: number;
  };
  reporting: {
    submitted: number;
    notSubmitted: number;
  };
}

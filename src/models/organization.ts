import { AdminInfo } from './admin';

export interface OrganizationInfo {
  id: number;
  name: string;
}
export interface Organization {
  id: number;
  name: string;
  admin: AdminInfo;
  createdAt: string;
}

export interface NewOrg {
  name: string;
  admin: {
    email: string;
  };
}

export interface UpdateOrg {
  name: string;
}

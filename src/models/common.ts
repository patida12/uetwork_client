import { SelectChangeEvent } from '@mui/material';
import {
  GridColDef,
  GridRowData,
  GridRowId,
  GridSortModel,
} from '@mui/x-data-grid';

export interface Params {
  per_page?: number;
  page?: number;
  sort?: string;

  [key: string]: any;
}

export interface ListResponse<T> {
  data: T[];
}

export interface DataTableProps {
  rows: Readonly<GridRowData[]>;
  columns: GridColDef[];
  pageSize: number;
  rowCount: number;
  loading?: boolean;
  page: number;
  selectionModel?: GridRowId[];
  customToolbar: React.JSXElementConstructor<any>;
  onSortModelChange?: (model: GridSortModel) => void;
  onPageChange?: (page: number) => void;
  onPageSizeChange?: (pageSize: number) => void;
  onFilterChange?: (newFilter: FilterState) => void;
  onSelectionModelChange?: (newSelectionModel: GridRowId[]) => void;
  initialFilters?: FilterState;
  refreshData?: () => void;
}

export interface IdProps {
  id: number;
  name?: string;
}

export interface CustomPaginationProps {
  pageSize: number;
  handleChange: (event: SelectChangeEvent<string>) => void;
  page: number;
  pageCount: number;
  onPageChange: (page: number) => void;
  onPageSizeChange: (pageSize: number) => void;
}

export interface CustomToolbarProps {
  onFilterChange: (value: any) => void;
  initialFilters?: FilterState;
  selectionModel?: GridRowId[];
  refreshData?: () => void;
}

export interface FilterState {
  query: string;
  filter: Record<string, any>;
}

export interface TermIdParam {
  termId: string;
}

export interface PostIdParam {
  postId: string;
}

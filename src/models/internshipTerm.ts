export interface InternshipTerm {
  id: number;
  term: number;
  year: number;
  startRegAt: string;
  endRegAt: string;
  startDate: string;
  endDate: string;
  createdAt: string;
  organizationId: number;
}

export interface NewTerm {
  term: number;
  year: number;
  startRegAt: string;
  endRegAt: string;
  startDate: string;
  endDate: string;
}

export interface InternshipTermListing {
  id: number;
  term: number;
  year: number;
}

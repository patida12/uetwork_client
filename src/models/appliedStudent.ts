export interface AppliedStudent {
  id: number;
  fullName: string;
  studentIdNumber: string;
  orgEmail: string;
  isSelfRegistered: boolean;
  status: string;
}

export enum RegistrationStatus {
  WAITING = 'WAITING',
  PASSED = 'PASSED',
  FAILED = 'FAILED',
  SELECTED = 'SELECTED',
}

export enum InternshipType {
  ASSOCIATE = 'ASSOCIATE',
  OTHER = 'OTHER',
}

export interface StudentApplication {
  id: number;
  fullName: string;
  orgEmail: string;
  phoneNumber: string;
  partnerId: number;
  partnerName: string;
  status: RegistrationStatus;
  registeredAt: string;
  internshipType: InternshipType;
  studentIdNumber: string;
  isMailSent: boolean;
  studentId: number;
  termId?: number;
}

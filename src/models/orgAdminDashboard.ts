interface TotalStat {
  lastTotal: number;
  currentTotal: number;
}

export interface PartnersStat {
  id: number;
  name: string;
  studentsCount: number;
  logoUrl: string;
}

export interface PartnersStatusStat {
  totalPending: number;
  totalAccepted: number;
  totalRejected: number;
}

export interface OrgAdminDashboard {
  termStudentsStat: TotalStat;
  termPartnersStat: TotalStat;
  termLecturersStat: TotalStat;
  termPartnersStatusStat: PartnersStatusStat;
  mostPopularPartnersStat: [
    {
      partners: PartnersStat[];
    },
  ];
}

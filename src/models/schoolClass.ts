export interface SchoolClass {
  id: number;
  name: string;
}

export interface SchoolClassLittleInfo {
  id: number;
  name: string;
}

export interface SchoolClassInfo {
  id: number;
  name: string;
  majorId: number;
  majorName: string;
  programName: string;
  createdAt: string;
}

export interface NewSchoolClass {
  name: string;
  programName: string;
}

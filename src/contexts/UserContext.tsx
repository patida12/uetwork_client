import { AuthUser } from '../models';
import { createContext } from 'react';

interface UserContextType {
  currentUser?: AuthUser;
  setCurrentUser: (user?: AuthUser) => void;
}

const UserContext = createContext<UserContextType>({
  setCurrentUser: (user?: AuthUser) => {},
});

export type { UserContextType };
export { UserContext };

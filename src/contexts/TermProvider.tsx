import internshipTermApi from 'api/internship/internshipTermApi';
import { LoadingPage } from 'app/pages/Common/LoadingPage/LoadingPage';
import { InternshipTermListing } from 'models/internshipTerm';
import React, { createContext, useEffect, useReducer } from 'react';

export interface TermContextType {
  listTerms: InternshipTermListing[];
  currentTerm?: InternshipTermListing | undefined;
  isLoading?: boolean;
}

export enum ActionTypes {
  INITIALIZE = 'INITIALIZE',
  SET_LIST_TERMS = 'SET_LIST_TERMS',
  SET_CURRENT_TERMS = 'SET_CURRENT_TERMS',
  SET_LOADING = 'SET_LOADING',
}

const initialState: TermContextType = {
  listTerms: [],
  currentTerm: undefined,
  isLoading: true,
};

const handlers = {
  INITIALIZE: (state, action) => {
    const { listTerms, currentTerm } = action.payload;
    return { ...state, listTerms, currentTerm };
  },
  SET_LIST_TERMS: (state, action) => {
    const { listTerms } = action.payload;
    return { ...state, listTerms };
  },
  SET_CURRENT_TERMS: (state, action) => {
    const { currentTerm } = action.payload;
    return { ...state, currentTerm };
  },
  SET_LOADING: (state, action) => {
    const { isLoading } = action.payload;
    return { ...state, isLoading };
  },
};

const reducer = (state, action) =>
  handlers[action.type] ? handlers[action.type](state, action) : state;

const TermContext = createContext({
  ...initialState,
  set_list_terms: (terms: InternshipTermListing[]) => Promise.resolve(),
  set_current_term: (term: InternshipTermListing | undefined) =>
    Promise.resolve(),
});

const TermProvider: React.FC = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  useEffect(() => {
    const initialize = async () => {
      set_loading(true);
      const listTerms = await internshipTermApi
        .getTermListing()
        .then(res => res.item)
        .catch(e => []);
      const currentTerm = listTerms.length > 0 ? listTerms[0] : undefined;

      dispatch({
        type: ActionTypes.INITIALIZE,
        payload: { listTerms, currentTerm },
      });
      set_loading(false);
    };
    initialize();
  }, []);

  const set_list_terms = (terms: InternshipTermListing[]) => {
    set_loading(true);
    const listTerms = terms;
    dispatch({ type: ActionTypes.SET_LIST_TERMS, payload: { listTerms } });
    const term = listTerms.find(term => term.id === state.currentTerm.id);
    if (term === undefined) {
      const firstTerm = listTerms.length > 0 ? listTerms[0] : undefined;
      set_current_term(firstTerm);
    }
    set_loading(false);
  };

  const set_current_term = (term: InternshipTermListing | undefined) => {
    set_loading(true);
    const currentTerm = term;
    dispatch({ type: ActionTypes.SET_CURRENT_TERMS, payload: { currentTerm } });
    set_loading(false);
  };

  const set_loading = (isLoading: boolean) => {
    dispatch({ type: ActionTypes.SET_LOADING, payload: { isLoading } });
  };

  if (state.isLoading) {
    return <LoadingPage />;
  }

  return (
    <TermContext.Provider
      value={{ ...state, set_list_terms, set_current_term }}
    >
      {children}
    </TermContext.Provider>
  );
};

export { TermContext, TermProvider };
